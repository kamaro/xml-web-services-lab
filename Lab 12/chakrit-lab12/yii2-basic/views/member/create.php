<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Member2 */

$this->title = 'Create Member2';
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member2-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
