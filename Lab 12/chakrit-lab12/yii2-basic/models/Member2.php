<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member2".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $std_id
 */
class Member2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'std_id'], 'required'],
            [['firstname'], 'string', 'max' => 150],
            [['lastname'], 'string', 'max' => 255],
            [['std_id'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'std_id' => 'Std ID',
        ];
    }
}
