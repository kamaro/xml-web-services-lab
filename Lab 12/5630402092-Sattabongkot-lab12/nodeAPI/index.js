var app = require('express')();
var users = require("./users");
var port = process.env.PORT || 7777;

app.get('/', function (req, res) {
	res.send('<h1>Hello Node.js</h1>');	
});
app.get('/index', function(req, res) {
	res.send('<h1>This is index page</h1>');
});
app.get('/user', function(req, res){
	res.send(users.findAll());
});

app.get('/user/:id', function(req, res){
	id = req.params.id;
	res.send(users.findById(id));
});
app.listen(port, function() {
	console.log('Starting node.js on port ' + port);
});