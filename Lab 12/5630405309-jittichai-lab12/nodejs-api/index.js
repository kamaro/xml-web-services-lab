/*��Ŵ Express ����ҹ*/
/* global process */

var app = require('express')();

/*�� port 7777 ���ͨ�������ҵ͹�ѹ app ����*/
var port = process.env.PORT || 7777;

var callusers = require('./users');

/* Routing */
app.get('/', function (req, res) {
    res.send('<h1>Hell Node.js</h1>');
});
app.get('/index', function (req, res) {
    res.send('<h1>This is index page</h1>');
});
app.get('/user', function (req, res) {
    res.json(callusers.findAll());
});
app.get('/user/:id', function (req, res) {
    var id
	= req.params.id;
    res.json(callusers.findByID(id));
});

/* ������ Server �ӡ���ѹ Web Server ���� port �����ô */
app.listen(port, function () {
    console.log('Starting node.js on port ' + port);
});