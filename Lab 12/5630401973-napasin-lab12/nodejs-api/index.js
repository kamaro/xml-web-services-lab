/*Express ����ҹ*/
var app = require('express')();

/*�� port 7777 ���ͨ�������ҵ͹�ѹ app ����*/
var port = process.env.PORT || 7777;

var users = require("./users.js");
var index = 3;

/* Routing */
app.get('/', function(req, res){
	res.send('<h1>Hello Node.js</h1>');
});

app.get('/user', function(req, res){
	res.send(users.findAll());
});

app.get('/user/*:index(\\d+)', function(req, res){
	index = req.params.index;
	res.send(users.findByID(index));
});

/*������ server �ӡ���ѹ web server ���� port �����ҡ�˹��ͧ */
app.listen(port, function(){
	console.log('Starting node.js on port ' + port);
});