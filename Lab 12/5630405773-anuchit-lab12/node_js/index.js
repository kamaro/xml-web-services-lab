var app = require('express')();
var port = process.env.PORT || 7777;
var callusers = require('./users');

app.get('/', function (req, res) {
	res.send('<h1>Hello Node.js</h1>');
});

app.get('/index', function (req, res) {
	res.send('<h1>This is index page</h1>');
});

app.get('/users', function (req, res) {
    res.json(callusers.findAll());
});

app.get('/users/:id', function (req, res) {
    var id
	= req.params.id;
    res.json(callusers.findById(id));
});

app.listen(port, function() {
	console.log('Starting node.js on port ' + port);
});
