var users = require('./users');
var app = require('express')();


var port = process.env.PORT || 7777;


app.get('/', function (req, res) {
    res.send('<h1>Hello Node.js</h1>');
});
 
app.get('/users', function (req, res) {
    res.json(users.findAll());
});
 
app.get('/users/:id', function (req, res) {
    var id = req.params.id;
    res.json(users.findById(id));
});

 
app.listen(port, function() {
    console.log('Starting node.js on port ' + port);
});