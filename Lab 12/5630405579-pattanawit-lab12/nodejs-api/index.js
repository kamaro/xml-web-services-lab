var app = require('express')();
var port = process.env.PORT || 7777;
var users = require("./users.js");

app.get('/', function (req, res) {
	res.send('<h1>Hello Node.js</h1>');
});

app.get('/index', function (req, res) {
	res.send('<h1>This is index page</h1>');
});

app.get('/user', function (req, res) {
    res.json(users.findAll());
});
 
app.get('/user/:id', function (req, res) {
    res.json(users.findById(req.params.id));
});

app.listen(port, function() {
	console.log('Starting node.js on port ' + port);
});