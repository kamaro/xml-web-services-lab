var app = require('express')();

var port = process.env.PORT || 7777;

var users = require("./users.js");
var index = 3;

app.get('/', function(req, res){
	res.send('<h1>Hello Node.js</h1>');
});
app.get('/index', function(req, res){
	res.send('<h1>This is index page</h1>');
});
app.get('/user', function(req, res){
	res.send(users.findAll());
});
app.get('/test', function(req, res){
	res.send('<h1>T - E - S - T</h1>');
});
app.get('/user/*:index(\\d+)', function(req, res){
	index = req.params.index;
	res.send(users.findByID(index));
});

app.listen(port, function(){
	console.log('Starting node.js on port ' + port);
});