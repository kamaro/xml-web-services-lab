<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-index">
	<meta http-equiv=Content-Type content="text/html; charset=tis-620">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Member', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'columns' => [
		['class' => 'yii\grid\SerialColumn'],
		//'id',
		'firstname',
		'lastname',
		'std_id',
		
		['class' => 'yii\grid\ActionColumn'],
        ],
        'filterModel' => $searchModel
       
    ]); ?>

</div>
