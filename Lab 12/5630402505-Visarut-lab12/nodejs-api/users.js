//ลองใส่ข้อมูลเอาไว้ทดสอบ
var users = [
{
	"id" : 1,
	"username" : "goldroger",
	"name" : "Gol D. Roger",
	"position" : "Pirate King"
},
{
	"id" : 2,
	"username" : "mrzero",
	"name" : "Sir Crocodile",
	"position" : "Former-Shichibukai"
},
{
	"id" : 3,
	"username" : "luffy",
	"name" : "Monkey D. Luffy",
	"position" : "Captain"
},
{
	"id" : 4,
	"username" : "kuzan",
	"name" : "Aokiji",
	"position" : "Former Marine Admiral"
},
{
	"id" : 5,
	"username" : "shanks",
	"name" : "'Red-Haired' Shanks",
	"position" : "The 4 Emperors"
}
];

/* ฟังก์ชันสำหนับคืนค่า user ทั้งหมดกลับไปให้ผู้ใช้ */
exports.findAll = function(){
	return users;
};

/* ฟังก์ชันสำหรับคืนค่า users ที่มี id ตามที่ผู้ใช้ส่ง request เข้ามา */
exports.findByID = function(id){
	for(var i = 0; i < users.length; i++){
		if(users[i].id == id)	return users[i];
	}
};