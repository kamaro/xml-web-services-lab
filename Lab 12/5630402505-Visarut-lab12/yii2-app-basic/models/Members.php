<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "members".
 *
 * @property integer $id
 * @property string $name
 * @property string $lastnames
 * @property string $std-id
 */
class Members extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'lastnames', 'std-id'], 'required'],
            [['id'], 'integer'],
            [['name', 'lastnames'], 'string', 'max' => 255],
            [['std-id'], 'string', 'max' => 112]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lastnames' => 'Lastnames',
            'std-id' => 'Std ID',
        ];
    }
}
