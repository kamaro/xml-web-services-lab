/*load  Express มาใช้งาน*/
var app = require('express')();

/*ใช้ port 7777 หรือจะส่งเข้ามาตอนรัน app ก็ได้*/
var port = process.env.PORT || 7777;

var users = require("./users.js");
var index = 3;

/* Routing */
app.get('/', function(req, res){
	res.send('<h1>Hello Node.js</h1>');
});
app.get('/index', function(req, res){
	res.send('<h1>This is index page</h1>');
});
app.get('/user', function(req, res){
	res.send(users.findAll());
});
app.get('/test', function(req, res){
	res.send('<h1>T - E - S - T</h1>');
});
app.get('/user/*:index(\\d+)', function(req, res){
	index = req.params.index;
	res.send(users.findByID(index));
	//if(/(\d)+/.test(req.params.int))
	
		//res.send('<h1>' + index + '</h1><br/><h1>' + /(\d)+/.test(req.params.int) + '</h1>');
    	//res.send(users.findByID(/(\d)+/));
    //else
    //	res.send('<h1>========== ERROR ==========</h1>');
    //check input
    //res.send(/(\d)+/.test(req.params.int));
});

/*สั่งให้ server ทำการรัน web server ด้วย port ที่เรากำหนดเอง */
app.listen(port, function(){
	console.log('Starting node.js on port ' + port);
});