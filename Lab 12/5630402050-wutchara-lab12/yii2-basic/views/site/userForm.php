<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php
	if(yii::$app->session->hasFlash('success')){
		echo "<div class='alert alert-success'>".yii::$app->session->getFlash('success')."</div>";
	}
?>

<?php $form = ActiveForm::begin(); ?>
<?= $form -> field($model, 'name'); ?>
<?= $form -> field($model, 'email'); ?>

<?= Html::submitButton('Submit', ['class'=>'btn btn-success']); ?>