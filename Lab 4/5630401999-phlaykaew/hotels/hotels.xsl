<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>   
                  <h1>
                    <xsl:for-each select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href">
                        <img>
                            <xsl:attribute name="src">
                                <xsl:value-of select="."/>
                            </xsl:attribute>
                        </img>
                    </xsl:for-each>
                    <xsl:apply-templates select="kml:kml/kml:Document/kml:name"/>
                    
                </h1>  
                   List of hotels  
                   <ul>  
                <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                    <li><xsl:value-of select="kml:name"/></li>
                    <ul>
                        <li> <xsl:element name="a">
                                            <xsl:attribute name="href">
                                                <xsl:value-of select="substring-before(substring-after(string(),'href=&quot;'),'&quot;>')"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="substring-before(substring-after(string(),'href=&quot;'),'&quot;>')"/>
                                        </xsl:element></li> 
                        <li>Coordinates :<xsl:value-of select="kml:Point/kml:coordinates"/></li>
                    </ul>                   
                </xsl:for-each>   
                </ul>                                   
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>