<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
         <html>
            <head>
                <title>XSLT Lab : Problem 1.1</title>
            </head>
            <body>
                <ul>
                    <li><xsl:value-of select="//companies/company/name"/></li>
                    <li><xsl:value-of select="//companies/company[2]/name"/></li>
                </ul>
            </body>
</html>
    </xsl:template>
</xsl:stylesheet>
