<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotels.xsl
    Created on : September 19, 2015, 3:13 PM
    Author     : 2PYii
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="kml:kml/kml:Document">
        <html>
            <body>
                <h1>
                    <img src="{kml:Style/kml:IconStyle/kml:Icon/kml:href}" />
                    <xsl:value-of select="kml:name"/>
                </h1>
                List of hotels
                <xsl:for-each select="kml:Placemark">
                    <xsl:sort select="kml:name"/>
                    <ul>
                        <li>
                            <xsl:value-of select="kml:name"/>
                            <xsl:variable name="text1" 
                                          select="substring-before(substring-after(kml:description, '&lt;'), '&gt;')"/>
                            <xsl:variable name="text2" 
                                          select="substring($text1,9,string-length($text1)-9)"/>
                            <ul>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="$text2"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="$text2"/>
                                    </a>
                                </li>
                                <li>Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/></li>
                            </ul>
                        </li>
                    </ul>
                </xsl:for-each>
                
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
