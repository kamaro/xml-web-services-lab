<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>companies2.xsl</title>
            </head>
            <body>
                <h2> These are the leading companies:</h2>
             
                <xsl:for-each	select="companies/company">	
                    
                        <ul>
                            <li>
                                <xsl:value-of select="name"/>
                            </li>
                            <ul>
                                
                                <xsl:for-each select="research/labs/lab">
                                    <li>
                                        <xsl:value-of select="."/> 
                                    </li>
                                </xsl:for-each>
                                   
                               
                            </ul>
                        </ul>
                    
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
