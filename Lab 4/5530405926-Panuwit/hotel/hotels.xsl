<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
    <xsl:output method="html"/>
    <xsl:template match="/">
        
        <html>
            <head>
               
            </head>
            <body>
                <h1>
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                        </xsl:attribute>
                    </xsl:element>
                    <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                </h1>
                <P>
                    List of hotels
                    <xsl:for-each	select="kml:kml/kml:Document/kml:Placemark">
                        <ul>
                            <li>
                                <xsl:value-of select="kml:name"/>
                            </li>
                            <ul>
                                <li>
                                    <xsl:element name="a">
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="substring-after(substring-before(kml:description,'&quot;&gt;'),'&lt;a href=&quot;')"/> 
                                        </xsl:attribute>                                    
                                        <xsl:value-of select="substring-after(substring-before(kml:description,'&quot;&gt;'),'&lt;a href=&quot;')"/>
                                    </xsl:element>
                                </li>
                              
                                <xsl:for-each select="kml:Point/kml:coordinates">
                                    <li>
                                  Coordinates:<xsl:value-of select="."/> 
                                    </li>
                                </xsl:for-each>
                                   
                               
                            </ul>
                        </ul>
                    </xsl:for-each>
                </P>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
