<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies5.xsl
    Created on : September 22, 2015, 3:43 PM
    Author     : Poppy
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <xsl:element name="companies">
            <xsl:for-each select="companies/company">
                <xsl:element name="company">
                    
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol" />
                    </xsl:attribute>
                    
                    <xsl:element name="name">
                        <xsl:value-of select="name"/>
                    </xsl:element>
                    
                    <xsl:for-each select="research/labs/lab">
                        <xsl:element name="lab">                        
                            <xsl:choose>
                                <xsl:when test="@location='new york city'">
                                    <xsl:attribute name="state">New York</xsl:attribute>
                                </xsl:when>
                                <xsl:when test="@location='san jose'">
                                    <xsl:attribute name="state">California</xsl:attribute>
                                </xsl:when>
                                <xsl:when test="@location='pittsburgh'">
                                    <xsl:attribute name="state">Pennsylvania</xsl:attribute>
                                </xsl:when>
                            </xsl:choose>	
                            <xsl:value-of select="."/>
                        </xsl:element>
                    </xsl:for-each>
                    
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
