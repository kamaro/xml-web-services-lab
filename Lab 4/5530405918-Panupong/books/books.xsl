<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:lib="http://www.zvon.org/library">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>books.xsl</title>
            </head>
            <body>
                <table border="1">
                <tr>
                    <td>
                        <center>
                            <b>Title</b>
                        </center>
                    </td>
                    
                    <td>
                        <center><b>Pages</b></center>
                    </td>   
                </tr>
                
                <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort order="ascending" select="lib:pages"/>
                        <xsl:if test="not(lib:pages)=0">
                        <tr>
                            <td>
                                <xsl:value-of select="@about"/>
                            </td>
                            
                            <td>
                                <xsl:value-of select="lib:pages"/>
                            </td>
                        </tr>
                        </xsl:if>
                </xsl:for-each>
              </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
