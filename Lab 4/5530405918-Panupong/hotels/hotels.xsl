<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotels.xsl
    Created on : September 22, 2015, 9:22 PM
    Author     : Poppy
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>
                <h1>
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="/kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                        </xsl:attribute>
                    </xsl:element>
                    
                    <xsl:value-of select="/kml:kml/kml:Document/kml:name"/>
                </h1>
                <font size="4">List of hotels</font>
                
                <ul>
                <xsl:for-each select="/kml:kml/kml:Document/kml:Placemark">
                    <li>
                        <xsl:value-of select="kml:name"/>
                        
                        <ul>
                            <li>
                                <xsl:element name="a">
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="substring((substring-before(kml:description, '&quot;>')),11)"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="substring((substring-before(kml:description, '&quot;>')),11)"/>
                                </xsl:element>
                            </li>
                            
                            <li>Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/></li>
                        </ul>
                        
                    </li>
                </xsl:for-each>
                </ul>
                
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
