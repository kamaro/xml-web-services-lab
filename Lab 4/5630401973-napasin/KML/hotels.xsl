<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotels.xsl
    Created on : September 20, 2015, 4:37 PM
    Author     : ACER
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            
            <body>
                <h1>
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href">
                            </xsl:value-of>
                        </xsl:attribute>
                    </img>
                    
                    <xsl:for-each select="kml:kml/kml:Document">
                        <xsl:value-of select="kml:name"> 
                        </xsl:value-of>
                    </xsl:for-each>  
                
                </h1>
              
                list of hotel
                    
                <ul>
                    <xsl:for-each select ="kml:kml/kml:Document/kml:Placemark">
                        <xsl:sort select="kml:name">
                        </xsl:sort>
                        <li> 
                            <xsl:value-of select="kml:name">
                            </xsl:value-of>
                            <ul>
                                <li>
                                    <a> 
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="substring-after(substring-before(kml:description,'&quot;&gt;'),'=&quot;')" >
                                            </xsl:value-of>
                                        </xsl:attribute>
                                        
                                        <xsl:value-of select="substring-after(substring-before(kml:description,'&quot;&gt;'),'=&quot;')" >
                                        </xsl:value-of>
                                    </a>
                               
                                </li>
                                <li>
                                    Coordinates:
                                    <xsl:value-of select="kml:Point/kml:coordinates">
                                    </xsl:value-of>
                                </li>
                            </ul>
                        </li>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
