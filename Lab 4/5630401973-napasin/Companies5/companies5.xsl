<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies5.xsl
    Created on : September 19, 2015, 7:14 PM
    Author     : ACER
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <xsl:element name="companies">
            <xsl:for-each select= "companies/company">
           
                <xsl:element name="company">
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol">
                        </xsl:value-of>
                    </xsl:attribute>
                    
                    <xsl:element name="name">
                        <xsl:value-of select="name">
                        </xsl:value-of>
                    </xsl:element>

                    <xsl:for-each select= "research/labs/lab">
                        <xsl:element name="lab">
                            <xsl:attribute name="state">
                                <xsl:choose>
                                    <xsl:when test="@location='new york city'">New York</xsl:when>
                                    <xsl:when test="@location='san jose'">California</xsl:when>
                                    <xsl:otherwise>Pennsyvernia</xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                   
                            <xsl:value-of select=".">
                            </xsl:value-of>
                        </xsl:element>
                    </xsl:for-each>
                
                </xsl:element>
            </xsl:for-each>
           
        </xsl:element>
               
    </xsl:template>
</xsl:stylesheet>
