<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotels.xsl
    Created on : September 22, 2015, 4:17 PM
    Author     : ดิวดิว
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:KM="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom"> <!--ตั้งชื่อเนมสเปซว่าKM-->
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>
                   
                    <xsl:value-of select="KM:kml/KM:Document/KM:name"></xsl:value-of> <!--ชื่อหัวข้อ-->
                </title>
                
            </head>
            <body>
                  <h1> 
                <xsl:element name ="img">
                    <xsl:attribute name = "src">
                    <xsl:value-of select="KM:kml/KM:Document/KM:Style/KM:IconStyle/KM:Icon/KM:href"></xsl:value-of>
                    </xsl:attribute>
                </xsl:element>   <!--จะเรียกรูปมาต้องสร้าง ele attri ให้มัน-->
                
              
                    <xsl:value-of select="KM:kml/KM:Document/KM:name"></xsl:value-of>
                </h1>
                <ul>
                <list>List of Hotel</list>
                </ul>
                
                <ul>
                <xsl:for-each select="KM:kml/KM:Document/KM:Placemark">
                    <li><xsl:value-of select="KM:name"></xsl:value-of>
                    <ul>
                        <li>
                    <xsl:element name="a">
                        
                            
                                <xsl:attribute name="href">
                            <xsl:value-of select="substring-after(substring-before(KM:description,'&quot;&gt;'),'&lt;a href=&quot;')">
                                <!--เป็นคำสั่งเอาตัวข้างหน้า กับข้างหลังออก เพื่อให้เหลือแต่ตัวเว็บ เพราะถ้าเอามาหมดมันจะเป็นtext &quotคือ::&gt grather than &lt less than-->
                            </xsl:value-of>
                        </xsl:attribute>
                        <xsl:value-of select="substring-after(substring-before(KM:description,'&quot;&gt;'),'&lt;a href=&quot;')">
                            
                        </xsl:value-of>
                        
                    </xsl:element>
                    
                    </li>
                    <li>Coordinates:<xsl:value-of select="KM:Point/KM:coordinates"></xsl:value-of></li>
                    </ul>
                </li>
                </xsl:for-each>
                </ul>
                
                
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
