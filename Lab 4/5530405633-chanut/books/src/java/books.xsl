<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:lib="http://www.zvon.org/library">
    <xsl:output method="html"/>

      <xsl:template match="/">
        
          <html>
            <head>
                <title>books.xsl</title>
            </head>
            <body>
                
        <table border="1">
                    <tr style="text-align:center;">
                        <th>Title</th>
                        <th>Pages</th>
                    
                    </tr>
                    
              <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort order="ascending" select="lib:pages"/>
                        <xsl:if test="lib:pages">
                        <tr>
                         
                         <td>
                                <xsl:value-of select="@about"/> <!--เอาชื่อหนังสือมาใส่-->
                         </td>
                         
                         <td>
                             
                                <xsl:value-of select="lib:pages"/><!--จำนวนหน้า-->
                             
                         </td>
                        
                        </tr>
                        </xsl:if>
              </xsl:for-each>
         </table>
                
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
