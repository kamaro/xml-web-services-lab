<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>companies.xsl</title>  <!-- ออกข้างบนหน้าต่าง -->
            </head>
            <body> <!-- แสดงส่วนต่างๆในหน้าจอ -->
                <h2>There are the leading companies</h2><!-- หัวข้อเฮดเดอ -->
                
                <ul>
                    <xsl:for-each select="companies/company"><!-- อยากเลือกคอมปานีทั้งหมดทำโดยการวนลูปเลือกตัวแรก ต่อด้วยตัวสอง -->
                        <li>
                            <xsl:value-of select="name"/><!-- เลือกเนมแต่ละคอมปานี -->
                                <ul>
                                    <xsl:for-each select="research/labs/lab">  <!-- เลือกแลปมาโดยบอกว่าเอาตัวนี้ดดยการจุด -->
                                        <li><xsl:value-of select="."/></li>
                                        
                                    </xsl:for-each>
                                </ul>
                        </li>
                            
                 </xsl:for-each>
                </ul>
         </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
