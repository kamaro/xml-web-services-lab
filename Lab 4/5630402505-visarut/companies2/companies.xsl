<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies.xsl
    Created on : September 17, 2015, 5:21 PM
    Author     : VJunsone
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <title>XSLT Lab : Problem 1.2</title>
            </head>
            <body>
                <h2> These are the leading companies:</h2>
                <ul>
                    <xsl:apply-templates/>
                </ul>
            </body>
        </html>

    </xsl:template>
    <xsl:template match="company">
        <p>
            <xsl:apply-templates select="name"/>
            <xsl:apply-templates select="research"/>
        </p>   
    </xsl:template>
    <xsl:template match="name">
        <li>         
            <xsl:value-of select="."/>
        </li>
    </xsl:template>
    <xsl:template match="research">
        <xsl:apply-templates select="labs"/>
    </xsl:template>
    <xsl:template match="labs">
        <xsl:apply-templates select="lab"/>
        
    </xsl:template>
    <xsl:template match="lab"> 
        <ul>
            <li>
                <xsl:value-of select="."/>
            
            </li>
        </ul>
        
    </xsl:template>
    

</xsl:stylesheet>
