<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies.xsl
    Created on : September 18, 2015, 6:48 PM
    Author     : VJunsone
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method=""/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <xsl:element name="companies">
            <xsl:for-each select="companies/company">
                <xsl:element name="company">
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol"/>                       
                    </xsl:attribute>
                    <xsl:element name="name"> 
                        <xsl:value-of select="name"/>
                    </xsl:element>
                    <xsl:if test="name='International Business Machine Cooperation'">                                            
                        <xsl:element name="lab">
                            <xsl:attribute name="state">New York</xsl:attribute>
                            <xsl:value-of select="research/labs/lab[1]"/>
                        </xsl:element> 
                        <xsl:element name="lab">
                            <xsl:attribute name="state">California</xsl:attribute>
                            <xsl:value-of select="research/labs/lab[2]"/>
                        </xsl:element>                 
                    </xsl:if>
                    <xsl:if test="name='Intel Cooperation'"> 
                        <xsl:element name="lab">
                            <xsl:attribute name="state">California</xsl:attribute>
                            <xsl:value-of select="research/labs/lab[1]"/>
                        </xsl:element> 
                        <xsl:element name="lab">
                            <xsl:attribute name="state">Pennsylvania</xsl:attribute>
                            <xsl:value-of select="research/labs/lab[2]"/>
                        </xsl:element>                         
                    </xsl:if>                    
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
        
    </xsl:template>

</xsl:stylesheet>
