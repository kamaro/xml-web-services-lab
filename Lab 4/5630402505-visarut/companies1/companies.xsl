<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies.xsl
    Created on : September 17, 2015, 4:59 PM
    Author     : VJunsone
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>XSLT Lab : Problem 1.1</title>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
   <xsl:template match="company">
    <p><ul>
    <xsl:apply-templates select="name"/>
    </ul></p>
   </xsl:template>
   <xsl:template match="name">
    <li>
    <xsl:value-of select="."/>
    <br />
    </li>
   </xsl:template>

</xsl:stylesheet>
