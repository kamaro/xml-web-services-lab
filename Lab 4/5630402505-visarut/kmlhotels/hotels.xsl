<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotels.xsl
    Created on : September 19, 2015, 2:46 PM
    Author     : VJunsone
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>
                <xsl:apply-templates select="kml:kml/kml:Document"/>                
                List of hotels
                <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                    <xsl:sort select="kml:name"/>
                    <ul>
                        <li>
                            <xsl:value-of select="kml:name"/>                           
                            <ul>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                                    </a>
                                </li>
                                <li>
                                    Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </xsl:for-each>                                              
            </body>
        </html>
    </xsl:template>
    <xsl:template match="kml:Document">
        <h2>
            <xsl:apply-templates select="kml:Style"/>
            <xsl:apply-templates select="kml:name"/>
        </h2>
    </xsl:template>
    <xsl:template match="kml:name">
        <xsl:value-of select="."/>
    </xsl:template>
    <xsl:template match="kml:Style/kml:IconStyle/kml:Icon">
        <xsl:apply-templates select="kml:href"/>
    </xsl:template>
    <xsl:template match="kml:href">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="."/>
            </xsl:attribute>
        </img>
    </xsl:template>

</xsl:stylesheet>
