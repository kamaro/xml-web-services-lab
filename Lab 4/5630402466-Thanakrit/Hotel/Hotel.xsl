<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:kml="http://www.opengis.net/kml/2.2" 
                version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Hotel</title>
            </head>
            <body>
                <h1>
                    <xsl:apply-templates select= "kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                    <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                </h1>
                <xsl:text>
                    List of hotels
                </xsl:text>
                <ul>
                    <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                        <li>
                            <xsl:value-of select="kml:name"/>
                        </li>
                        <ul>
                            <li>
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                                </a>
                            </li>
                            <li>Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/></li>
                        </ul>
                        <xsl:sort select="."/>
                    </xsl:for-each>
                </ul>
            </body>
        </html> 
    </xsl:template>
    <xsl:template match="kml:href">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="."/>
            </xsl:attribute>
        </img>
    </xsl:template>
   
</xsl:stylesheet>
