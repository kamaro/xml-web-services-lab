<?xml version="1.0" encoding="UTF-8"?>
<!--
    Document   : books.xsl
    Created on : September 19, 2015, 1:56 AM
    Author     : thinkearth
    Description:
        Purpose of transformation follows.
-->
<xsl:stylesheet version="1.0" xmlns:lib="http://www.zvon.org/library"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>books.xsl</title>
            </head>
            <body>
                <table border="2">
                    <tr>
                        <th align="center">Title</th>
                        <th align="center">Pages</th>
                    </tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort select="lib:pages"/>
                        <tr>
                            <xsl:if test="lib:pages">
                                <td>
                                    <xsl:value-of select="./@about"/>
                                </td>
                                <td>
                                    <xsl:value-of select="lib:pages"/>
                                </td>
                            </xsl:if>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>