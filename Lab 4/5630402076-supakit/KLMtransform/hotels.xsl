<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotels.xsl
    Created on : September 19, 2015, 10:15 AM
    Author     : thinkearth
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>
                <xsl:for-each select="kml:kml/kml:Document">
                    <h1><xsl:value-of select="kml:name"/></h1>
                    List of Hotels
                    <ul>
                        <xsl:for-each select="kml:Placemark">
                            <li>
                                <xsl:value-of select="kml:name"/>
                                <ul>
                                    <li>
                                        <xsl:variable name="des" select="kml:description"></xsl:variable>
                                        <xsl:variable name="link" select="substring-after(substring-before($des,'&quot;&gt;&lt;img src'),'href=&quot;')" />
                                        <a href = "{$link}">
                                            <xsl:value-of select="$link" />
                                        </a>                         
                                    </li>
                                    <li>
                                        Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/>
                                    </li>
                                </ul>
                            </li>
                        </xsl:for-each>
                    </ul>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
