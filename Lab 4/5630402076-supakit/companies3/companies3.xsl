<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <xsl:for-each select="companies/company">
            <xsl:element name="companies">
                <xsl:element name="company">
                    <xsl:attribute name="symbol" >
                        <xsl:value-of select="symbol"/>
                    </xsl:attribute>
                    <xsl:copy-of select="name"></xsl:copy-of>
                    <xsl:for-each select="research/labs/lab" >
                        <xsl:element name="lab">
                            <xsl:attribute name="city">                    
                                <xsl:value-of select="@location"/>
                            </xsl:attribute>
                            <xsl:value-of select="."/>
                        </xsl:element>       
                    </xsl:for-each><xsl:output method="xml"/>    
                </xsl:element>        
            </xsl:element>    
        </xsl:for-each> 
               
    </xsl:template>

</xsl:stylesheet>
