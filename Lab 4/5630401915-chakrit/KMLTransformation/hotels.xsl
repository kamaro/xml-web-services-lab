<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet 
    version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:kml="http://www.opengis.net/kml/2.2"
>
    <xsl:output method="html" version="4.0"
                encoding="UTF-8" indent="yes"/>
    
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <body>
                <xsl:for-each select="kml:kml/kml:Document">
                    <h1>
                        <xsl:variable name="image" select="kml:Style/kml:IconStyle/kml:Icon/kml:href" />
                        <img src="{$image}">
                            <xsl:value-of select="kml:name"/>
                        </img>
                    </h1>
                    <p>List of hotels</p>
                    <xsl:for-each select="kml:Placemark">
                        <xsl:sort select="kml:name"/>
                        <ul>
                            <li>
                                <xsl:value-of select="kml:name"/>
                                <ul>
                                    <li>
                                        <xsl:value-of select="kml:description"/>
                                        <xsl:for-each select="kml:description">
                                            
                                        </xsl:for-each>
                                    </li>
                                    <li>
                                        <xsl:value-of select="kml:Point/kml:coordinates"/> 
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </xsl:for-each>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
