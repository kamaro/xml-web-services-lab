<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="companies">
        <companies>
            <xsl:apply-templates select="//company"/>
        </companies>
    </xsl:template>
    <xsl:template match="company">
        <company> 
            <xsl:attribute name="symbol">
                <xsl:value-of select="symbol"/>
            </xsl:attribute>
            <name>
                <xsl:value-of select="name"/>
            </name>
            <xsl:apply-templates select="research/labs/lab"/>
        </company>
    </xsl:template>
    <xsl:template match="lab">
        <lab>
            <xsl:attribute name="city">
                <xsl:value-of select="@location"/>
            </xsl:attribute> 
            <xsl:value-of select="."/>
        </lab>
    </xsl:template>
</xsl:stylesheet>
