<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="companies">
        <companies>
            <xsl:apply-templates select="//company"/>
        </companies>
    </xsl:template>
    
    <xsl:template match="company">
        <company> 
            <xsl:attribute name="symbol">
                <xsl:value-of select="symbol"/>
            </xsl:attribute>
            <name>
                <xsl:value-of select="name"/>
            </name>
            <xsl:if test="position()=1">
                <xsl:apply-templates select="research/labs/lab">
                    <xsl:with-param name="state1">New York</xsl:with-param>
                    <xsl:with-param name="state2">California</xsl:with-param>
                </xsl:apply-templates>
            </xsl:if>
            <xsl:if test="position()=2">
                <xsl:apply-templates select="research/labs/lab">
                    <xsl:with-param name="state1">California</xsl:with-param>
                    <xsl:with-param name="state2">Pennsylvania</xsl:with-param>
                </xsl:apply-templates>
            </xsl:if>
        </company>
    </xsl:template>
    
    <xsl:template match="lab">
        <xsl:param name="state1"/>
        <xsl:param name="state2"/>
        <lab>
            <xsl:attribute name="state">
                <xsl:if test="position()=1">
                    <xsl:value-of select="$state1"/>
                </xsl:if>
                <xsl:if test="position()=2">
                    <xsl:value-of select="$state2"/>
                </xsl:if>
            </xsl:attribute>
            <xsl:value-of select="."/>
        </lab>
    </xsl:template>
</xsl:stylesheet>
