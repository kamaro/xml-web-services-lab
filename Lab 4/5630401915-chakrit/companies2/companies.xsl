<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" version="4.0"
                encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>XSLT Lab : Problem 1.2</title>
            </head>
            <body>
                <h2> These are the leading companies:</h2>
                    <ul>
                        <xsl:for-each select="companies/company">
                        <li><xsl:value-of select="name"/>
                        <xsl:for-each select="research/labs">
                            <ul>
                                    <xsl:apply-templates select="lab"/>
                            </ul>
                        </xsl:for-each>
                        </li>
                     </xsl:for-each>
                 </ul>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="lab">
        <li>
            <xsl:value-of select="."/>
        </li>
    </xsl:template>
</xsl:stylesheet>
