<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>
    <xsl:template match="/">
        <xsl:element name="companies">
            <xsl:for-each select="companies/company">
                
                <xsl:element name="company">
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol"/>
                    </xsl:attribute>     
                    <xsl:element name="name">
                        <xsl:value-of select="name"/>
                    </xsl:element>
                    <xsl:for-each select="research/labs/lab">
                        <xsl:element name="lab">                            
                            <xsl:attribute name="state">
                                <xsl:if test="@location='new york city'">new york</xsl:if>
                                <xsl:if test="@location='san jose'">California</xsl:if>
                                <xsl:if test="@location='pittsburgh'">Pennsylvania</xsl:if>                                
                            </xsl:attribute>
                            <xsl:value-of select="."/>
                        </xsl:element> 
                    </xsl:for-each>                   
                </xsl:element>
                
            </xsl:for-each>
        </xsl:element>            
    </xsl:template>
</xsl:stylesheet>
