<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:kml ="http://www.opengis.net/kml/2.2" >
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <title>XSLT Lab : Problem 2 Hotels</title>
            </head>
            <body>
                <h2>
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                        </xsl:attribute>
                    </xsl:element>
                    <value-of select="kml:kml/kml:Document">
                        <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                    </value-of>
                </h2>               
                    
                <ui>List of hotels
                    <dd>
                        <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                            <li>
                                <xsl:value-of select="kml:name"/>
                                <dd>                 
                                    <li>
                                        <a>                                     
                                            <xsl:attribute  name="href">
                                                <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>   
                                            </xsl:attribute>      
                                            <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>                                   
                                        </a>   
                                    </li>
                                    <li>Coordinates: <xsl:value-of select="kml:Point/kml:coordinates"/></li>
                                </dd>
                            </li>
                        </xsl:for-each>
                    </dd>
                </ui>
            </body>
            
        </html>
    </xsl:template>
</xsl:stylesheet>
