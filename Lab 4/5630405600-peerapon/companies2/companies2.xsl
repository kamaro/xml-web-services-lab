<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies2.xsl
    Created on : September 17, 2015, 6:45 PM
    Author     : SLURP3
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="companies">
        <html>
            <head>
                <title>XSLT Lab : Problem 1.2</title>
            </head>
            <body>
                <h2> These are the leading companies:</h2>
                <ul>
                    <xsl:for-each select="company"> 
                        <li>
                            <xsl:apply-templates select="name"/>
                        

                        <ul>
                            <xsl:for-each select="research/labs/lab"> 
                                <li>
                                    <xsl:apply-templates select="."/>
                                </li>
                            </xsl:for-each> 
                        </ul>
                        </li> 
                    </xsl:for-each>     
                </ul>
                    
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
