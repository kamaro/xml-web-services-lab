<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies.xsl
    Created on : September 17, 2015, 5:55 PM
    Author     : SLURP3
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="company"> 
        <html>
            <head>
                <title>XSLT Lab : Problem 1.1</title>
            </head>
            <body>
                <ul>
                    <li> 
                        <xsl:value-of select="name"/> 
                    </li>
                </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
