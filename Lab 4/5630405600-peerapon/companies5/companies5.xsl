<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies3.xsl
    Created on : September 22, 2015, 11:39 AM
    Author     : SLURP3
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="companies">
        <xsl:element name="companies">
            <xsl:for-each select="company">
                <xsl:element name="company">  
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol"/>
                    </xsl:attribute>
                    <xsl:element name="name">
                        <xsl:value-of select="name"/>        
                    </xsl:element>
                    <xsl:if test="symbol='IBM'">
                        <xsl:for-each select="research/labs/lab">
                            <xsl:element name="lab">
                                <xsl:attribute name="state">
                                    <xsl:if test="@location='new york city'">new york</xsl:if>
                                    <xsl:if test="@location='san jose'">California</xsl:if>
                                </xsl:attribute>
                                <xsl:value-of select="."/>
                            </xsl:element>
                        </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="symbol='INTEL'">
                        <xsl:for-each select="research/labs/lab">
                            <xsl:element name="lab">
                                <xsl:attribute name="state">
                                    <xsl:if test="@location='san jose'">California</xsl:if>
                                    <xsl:if test="@location='pittsburgh'">Pennsylvania</xsl:if>
                                </xsl:attribute>
                                <xsl:value-of select="."/>
                            </xsl:element>
                        </xsl:for-each>
                    </xsl:if>    
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>

