<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies.xsl
    Created on : September 17, 2015, 6:46 PM
    Author     : Pichaichan
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->

<xsl:template match="companies">     
        <html>  
                 
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
                <title>XSLT Lab : Problem 1.2</title>
            </head>
            <body>
                <h2> These are the leading companies:</h2>
                <ul>
                    <xsl:for-each select="company">
                    <li>
                        <xsl:apply-templates select="name"/> 
                        <ul>
                            <xsl:apply-templates select="research/labs/lab"/>
                        </ul>
                    </li>
                    </xsl:for-each>
                </ul>
            </body>         
        </html>
</xsl:template>  

<xsl:template match="research/labs/lab">
    <li><xsl:value-of select="."/></li>
</xsl:template>
</xsl:stylesheet>
