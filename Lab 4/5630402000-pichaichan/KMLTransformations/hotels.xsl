<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotels.xsl
    Created on : September 21, 2015, 1:25 PM
    Author     : Pichaichan
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <xsl:value-of select="kml:name"/>
            </head>
            <body>
                <h1><xsl:for-each select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href">
                     <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </img>   
                </xsl:for-each>
                    <xsl:apply-templates select="kml:kml/kml:Document/kml:name"/>
                </h1>
                <xsl:text>
                    List of hotels
                </xsl:text>
                <ul>
                    <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                    <li>
                        <xsl:value-of select="kml:name"/>
                        <ul>
                            <li><a>
                            <xsl:attribute name="href">
                            <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                            </xsl:attribute>
                            <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                            </a></li>
                            <li><xsl:text>Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/></xsl:text></li>       
                        </ul>
                    </li>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
