<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>
                    hotels
                </title>
            </head>
            <body>
                <h1>
                    <xsl:apply-templates select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                    <xsl:apply-templates select="kml:kml/kml:Document/kml:name"/>
                </h1>
                <xsl:apply-templates select="kml:kml/kml:Document"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="kml:name">
        <xsl:value-of select="."/>
    </xsl:template>
    <xsl:template match="kml:href">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="."/>
            </xsl:attribute>
        </img>
    </xsl:template>
    <xsl:template match="kml:Document">
        <xsl:text>
            List of hotels
        </xsl:text>
        <ul>
            <xsl:for-each select="kml:Placemark">
                <li>
                    <b>
                        <xsl:value-of select="kml:name"/>
                    </b>
                    <ul>
                        <li>
                            <a>
                                <xsl:attribute name="href">
                                    <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                                </xsl:attribute>
                                <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                            </a>
                        </li>
                        <li>Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/>
                        </li>
                    </ul>
                </li>
                <xsl:sort select="."/>
            </xsl:for-each>
        </ul>
    </xsl:template>
</xsl:stylesheet>
