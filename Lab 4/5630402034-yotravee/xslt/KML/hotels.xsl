<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
        <xsl:template match="/">
                <html>
                    <head>
                        <title>hotels</title>
                    </head>
                    <body>
                        <h1>
                            <img>
                                <xsl:attribute name="src">
                                    <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"></xsl:value-of>
                                </xsl:attribute>
                            </img>                       
                            <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                        </h1>
                        List of hotels
                        <ul>
                            <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                                <xsl:sort select="kml:name"/>
                                <li>
                                    <xsl:value-of select="kml:name"/>
                                    <ul>
                                        <li>
                                            <a>
                                                <xsl:attribute name="href">
                                                    <xsl:if test="@id='placemark 59024'">
                                                        <xsl:value-of select="substring(kml:description, 11, 60)"/>
                                                    </xsl:if>
                                                    <xsl:if test="@id='placemark 24241'">
                                                        <xsl:value-of select="substring(kml:description, 11, 63)"/>
                                                    </xsl:if>
                                                    <xsl:if test="@id='placemark 9962'">
                                                        <xsl:value-of select="substring(kml:description, 11, 57)"/>
                                                    </xsl:if>
                                                    <xsl:if test="@id='placemark 23433'">
                                                        <xsl:value-of select="substring(kml:description, 11, 70)"/>
                                                    </xsl:if>
                                                </xsl:attribute>
                                                <xsl:if test="@id='placemark 59024'">
                                                    <xsl:value-of select="substring(kml:description, 11, 60)"/>
                                                </xsl:if>
                                                <xsl:if test="@id='placemark 24241'">
                                                    <xsl:value-of select="substring(kml:description, 11, 63)"/>
                                                </xsl:if>
                                                <xsl:if test="@id='placemark 9962'">
                                                    <xsl:value-of select="substring(kml:description, 11, 57)"/>
                                                </xsl:if>
                                                <xsl:if test="@id='placemark 23433'">
                                                    <xsl:value-of select="substring(kml:description, 11, 70)"/>
                                                </xsl:if>
                                            </a>
                                        </li>
                                        <li>
                                            Coordinate: <xsl:value-of select="kml:Point/kml:coordinates"/>
                                        </li>
                                    </ul>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </body>
                </html>
        </xsl:template>
</xsl:stylesheet>
