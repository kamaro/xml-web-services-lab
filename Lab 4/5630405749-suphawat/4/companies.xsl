<?xml version='1.0' encoding='UTF-8' ?> 
<!-- was: <?xml version="1.0"?> -->

<!--
    Document   : companies.xsl
    Created on : 22 เธ�เธฑเธ�เธขเธฒเธขเธ� 2558, 15:27 เธ�.
    Author     : MM
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="companies">
        <xsl:element name="companies">
            <xsl:for-each select="company">
                <xsl:element name="company">
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol"/>
                    </xsl:attribute>
                    <xsl:element name="name">
                        <xsl:value-of select="name"/>
                    </xsl:element>
                    <xsl:apply-templates select="research"/>
                    <xsl:if test="symbol ='IBM'">
                        <xsl:element name="lab">
                            <xsl:attribute name="city">
                                austin
                            </xsl:attribute>
                            IBM Austin Research Lab
                        </xsl:element>
                    </xsl:if>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
    <xsl:template match="research">
        <xsl:for-each select="labs/lab">
            <xsl:element name="lab">
                <xsl:attribute name="city">
                    <xsl:value-of select="@location"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
