<?xml version='1.0' encoding='UTF-8' ?> 
<!-- was: <?xml version="1.0"?> -->

<!--
    Document   : book.xsl
    Created on : 22 เธ�เธฑเธ�เธขเธฒเธขเธ� 2558, 23:56 เธ�.
    Author     : MM
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>book</title>
            </head>
            <body>
                <table border="1">
                    <tr>
                        <th>Title</th>
                        <th>Pages</th>
                    </tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort select="lib:pages"/>	
                        <xsl:if test="lib:pages">
                            <tr>
                                <td><xsl:value-of select="@about"/></td>
                                <td><xsl:value-of select="lib:pages"/></td>
                            </tr>                            
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>