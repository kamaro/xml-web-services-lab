<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotel_kml.xsl
    Created on : 21 กันยายน 2558, 10:07 น.
    Author     : golfyzzz
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>hotel_kml.xsl</title>
            </head>
            <body>
                <img style = "float:left;">
                    <xsl:attribute name = "src">
                        <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                    </xsl:attribute>
                </img>
                <h1>
                    <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                </h1>
                List of hotels
                <ul>
                    <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                        <xsl:sort lang="kml:name"/> 
                        <li>
                            <xsl:value-of select="kml:name"/>
                        </li>
                        <ul>
                            <li>
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="substring-before(substring-after(kml:description/.,'href=&quot;'),'&quot;>')"/>
                                    </xsl:attribute>                       
                                        <xsl:value-of select="substring-before(substring-after(kml:description/.,'href=&quot;'),'&quot;>')"/>
                                </a>
                            </li>                      
                            <li>Coordinates;<xsl:value-of select="kml:Point/kml:coordinates"/></li>
                        </ul>
                    </xsl:for-each>
                </ul>
               
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
