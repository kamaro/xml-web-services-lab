<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : ิbook_rdf.xsl
    Created on : 21 กันยายน 2558, 22:51 น.
    Author     : golfyzzz
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:lib="http://www.zvon.org/library" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>book_rdf.xsl</title>
            </head>
            <body>
                <table border="1">
                    <tr><th>title</th><th>page</th></tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort ascending="lib:pages"/>
                        <xsl:if test="lib:pages">
                            <tr><td><xsl:value-of select="@about"/></td><td><xsl:value-of select="lib:pages"/></td></tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
