<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotel.xsl
    Created on : 20 September 2015, 12:47
    Author     : Amilaz
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:kml="http://www.opengis.net/kml/2.2" 
                xmlns:atom="http://www.w3.org/2005/Atom" 
                version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <body>
                <h1>
                    <img src="{kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href}"/>
                    <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                </h1>
                <xsl:apply-templates select="kml:kml/kml:Document"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="kml:kml/kml:Document">
        <p>
            List of hotels
            <ul>
                <xsl:for-each select="kml:Placemark">
                    <xsl:sort select="kml:name"/>
                    <li>
                        <xsl:value-of select="kml:name"/>
                        <ul>
                            <li>
                                <xsl:variable name="string" select='kml:description'/>
                                <xsl:variable name="tempf" select="substring-after($string,'&quot;')"/>
                                <xsl:variable name="tempb" select="substring-before($tempf,'&quot;')"/>
                                <a href="{$tempb}">
                                    <xsl:value-of select="$tempb"/>
                                </a>
                                
                            </li>
                            <li>
                                Coordinates:
                                <xsl:value-of select="kml:Point/kml:coordinates"/>
                            </li>
                        </ul>
                    </li>
                </xsl:for-each>
            </ul>
        </p>
    </xsl:template>

</xsl:stylesheet>
