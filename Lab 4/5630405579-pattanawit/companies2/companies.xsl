<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies.xsl
    Created on : 17 September 2015, 19:47
    Author     : Amilaz
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
   
    <xsl:template match="/">
        <html>
        <head>
            <title>XSLT Lab : Problem 1.2</title>
        </head>
        <body>
            <h2> These are the leading companies:</h2>
            <ul>
                <xsl:for-each select="companies/company">
                    <li><xsl:value-of select="name"/> 
                        <ul>
                            <xsl:apply-templates select="research/labs/lab"/>
                        </ul>
                    </li>
                </xsl:for-each>
            </ul>
        </body>
        </html>
    </xsl:template>

    <xsl:template match="research/labs/lab">
        <li>
            <xsl:value-of select="."/>
        </li>   
    </xsl:template>
    
</xsl:stylesheet>
