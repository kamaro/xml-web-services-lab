<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : books.xsl
    Created on : 20 September 2015, 15:01
    Author     : Amilaz
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library"
                version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <body>
                <table border="1">
                    <tr align="center">
                        <td>
                            <b>Title</b>
                        </td>
                        <td>
                            <b>Pages</b>
                        </td> 
                    </tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort select="lib:pages"/>
                        <tr>
                            <xsl:if test="lib:pages!=''">
                                <td>
                                    <xsl:value-of select="@about"/>
                                </td>
                                <td>
                                    <xsl:value-of select="lib:pages"/>
                                </td> 
                            </xsl:if>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
