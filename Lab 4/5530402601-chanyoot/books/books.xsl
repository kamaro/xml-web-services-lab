<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:lib="http://www.zvon.org/library">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html lang="en">
          <body>
           <table border="1">
              <tr align="center">
                <td><b>Title</b></td>
                 <td><b>Pages</b></td>
                </tr>
              <xsl:for-each select="rdf:RDF/rdf:Description">
                <xsl:sort order="ascending" select="lib:pages"/>
                <xsl:if test="lib:pages">
                 <tr>
                  <td>
                      <xsl:value-of select="@about"/>
                  </td>
                  <td>
                     <xsl:value-of select="lib:pages"/>
                  </td>
                </tr>
                 </xsl:if>
               </xsl:for-each>
          </table>
          </body>
         </html>
    </xsl:template>
</xsl:stylesheet>