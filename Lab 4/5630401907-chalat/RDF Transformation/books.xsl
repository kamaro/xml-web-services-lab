<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : books.xsl
    Created on : 19 September 2015, 23:02
    Author     : crazynova
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library"
                version="1.0">
    
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>books.xsl</title>
                <style>
                    table , th , td {
                    border: 1px solid black;
                    }
                </style>
            </head>
            <body>
                <table >
                    <tr>
                        <th>Title</th>
                        <th>Pages</th>
                    </tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:choose>
                            <xsl:when test="lib:pages !=''">
                                <tr>
                                    <td><xsl:value-of select="@about"/></td>
                                    <td><xsl:value-of select="lib:pages"/></td>
                                </tr>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:sort select="lib:pages"/>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
    

</xsl:stylesheet>
