<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies4.xsl
    Created on : September 21, 2015, 12:18 PM
    Author     : winashi
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="companies">
        <xsl:element name="companies">
            <xsl:for-each select="company">
                <xsl:element name="company">  
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol"/>
                    </xsl:attribute>
                    <xsl:element name="name">
                        <xsl:value-of select="name"/>        
                    </xsl:element>
                    <xsl:if test="symbol='IBM'">
                        <xsl:for-each select="research/labs/lab">
                            <xsl:element name="lab">
                                <xsl:attribute name="city">
                                    <xsl:value-of select="@location"/>
                                </xsl:attribute>
                                <xsl:value-of select="."/>
                            </xsl:element>
                        </xsl:for-each>
                        <xsl:element name="lab">
                            <xsl:attribute name="city">austin</xsl:attribute>IBM Austin Research Lab
                        </xsl:element>
                    </xsl:if>
                    <xsl:if test="symbol='INTEL'">
                        <xsl:for-each select="research/labs/lab">
                            <xsl:element name="lab">
                                <xsl:attribute name="city">
                                    <xsl:value-of select="@location"/>
                                </xsl:attribute>
                                <xsl:value-of select="."/>
                            </xsl:element>
                        </xsl:for-each>
                    </xsl:if>    
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
