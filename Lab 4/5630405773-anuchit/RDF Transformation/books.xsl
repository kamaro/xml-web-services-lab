<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : books.xsl
    Created on : September 22, 2015, 1:26 PM
    Author     : winashi
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="rdf:RDF">
        <html>
            <head>
                <title>books</title>
            </head>
            <body>
                <table border="1">
                    <tr>
                        <th>Title</th>
                        <th>Page</th>
                    </tr>
                    <xsl:for-each select="rdf:Description">
                        <xsl:sort select="lib:pages"/>
                        <xsl:if test="lib:pages">
                            <tr>
                                <td>
                                    <xsl:value-of select="@about"/>
                                </td>
                                <td>
                                    <xsl:value-of select="lib:pages"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
