<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies2.xsl
    Created on : September 19, 2015, 11:06 PM
    Author     : winashi
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    
    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="companies">
        <html>
            <head>
                <title>XSLT Lab : Problem 1.2</title>
            </head>
            <body>
                <xsl:element name="h2">
                    <xsl:text>These are the leading companies:</xsl:text>
                </xsl:element>
                <ul>
                    <xsl:for-each select="company">                   
                        <li>
                            <xsl:value-of select="name"/>
                            <xsl:apply-templates select="research"/>
                        </li>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="research">
        <ul>
            <xsl:for-each select="labs/lab">
                <li>
                    <xsl:value-of select="."/>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
</xsl:stylesheet>
