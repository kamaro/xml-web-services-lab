<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <companies>
            <xsl:for-each select="companies/company">  
                <company>       
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol"/>  
                    </xsl:attribute>
                    <name>
                        <xsl:value-of select="name"/>
                    </name>
                    <xsl:for-each select="research/labs/lab">
                        <lab>
                            <xsl:attribute name="state">
                                <xsl:choose>
                                    <xsl:when test = "@location = 'new york city'">New York</xsl:when>
                                    <xsl:when test = "@location = 'san jose'">California</xsl:when>
                                    <xsl:otherwise>Pennsylvania</xsl:otherwise>
                                </xsl:choose>     
                            </xsl:attribute>
                            <xsl:value-of select="."/>
                        </lab>
                    </xsl:for-each>
                </company>
            </xsl:for-each>
        </companies>
    </xsl:template>
</xsl:stylesheet>
