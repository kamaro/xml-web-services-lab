<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>
		<table>
			<tr>
				<td>
					<img>
						<xsl:attribute name="src"><xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href" /></xsl:attribute>
					</img>
				</td>
				<td>
					<h1><xsl:value-of select="kml:kml/kml:Document/kml:name"/></h1>
				</td>
			</tr>
		</table>
		<br/>
                List of hotels
                <xsl:for-each select = "kml:kml/kml:Document/kml:Placemark">
                    <xsl:sort select ="kml:name"/>
                    <ul>
                        <li>
                            
                            <xsl:value-of select = "kml:name"/>
                            
                            <xsl:for-each select="kml:description">
                                <ul>
                                    <u style="color:blue">
                                        <li style="color:blue">      
                                            <xsl:value-of select = "substring-before(substring-after(.,'f=&quot;'),'&quot;&gt;')"/>
                                        </li>
                                    </u>
                                </ul>
                            </xsl:for-each> 
                            <xsl:for-each select="kml:Point">
                                <ul>
                                    <li>    
                                        Coordinates:<xsl:value-of select ="."/>
                                    </li>
                                </ul>
                            </xsl:for-each>  
                        </li>
                    </ul>           
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
