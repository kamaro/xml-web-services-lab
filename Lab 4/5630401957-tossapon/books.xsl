<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>books.xsl</title>
            </head>
            <body>
                <table border="1">
                    <H2><th align = "center">Title</th>
                    <th align = "center">Pages</th></H2>
                    <xsl:for-each select = "rdf:RDF/rdf:Description">
                        <xsl:sort select="number(lib:pages)" data-type="number"/>
                        <xsl:choose> 
                            <xsl:when test="string-length(@about)&gt;3">
                                <tr>
                                    <td>
                                        <xsl:value-of select="@about"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="lib:pages"/>       
                                    </td>   
                                </tr>
                            </xsl:when>                            
                        </xsl:choose>                                  
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
