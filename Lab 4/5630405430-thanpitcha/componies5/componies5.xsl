<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>
    <xsl:template match="/">
    <xsl:element name="companies">
        <xsl:for-each select="companies/company">    
            <xsl:element name="company">        
                <xsl:attribute name="symbol">
                    <xsl:value-of select="symbol"/>
                </xsl:attribute>
                <xsl:element name="name">                                   
                    <xsl:value-of select="name"> 
                    </xsl:value-of>
                </xsl:element>             
                    <xsl:if test="symbol='IBM'">
                        <xsl:element name="lab">
                            <xsl:attribute name="state">New York</xsl:attribute>
                            <xsl:value-of select="descendant::lab[position()=1]"> 
                            </xsl:value-of>                         
                        </xsl:element>
                        <xsl:element name="lab">
                            <xsl:attribute name="state">California</xsl:attribute>
                            <xsl:value-of select="descendant::lab[position()=2]"> 
                            </xsl:value-of>                         
                        </xsl:element>          
                    </xsl:if>
                    <xsl:if test="symbol='INTEL'">
                        <xsl:element name="lab">
                            <xsl:attribute name="state">California</xsl:attribute>
                            <xsl:value-of select="descendant::lab[position()=1]"> 
                            </xsl:value-of>                         
                        </xsl:element>
                        <xsl:element name="lab">
                            <xsl:attribute name="state">Pennsylvania</xsl:attribute>
                            <xsl:value-of select="descendant::lab[position()=2]"> 
                            </xsl:value-of>                         
                        </xsl:element>
                    </xsl:if>                                                                                                                                                                                                                                               
            </xsl:element>
        </xsl:for-each>     
    </xsl:element>   
    </xsl:template>
</xsl:stylesheet>