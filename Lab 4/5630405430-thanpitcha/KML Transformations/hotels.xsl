<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>
                <h2>
                    <xsl:for-each select="kml:kml/kml:Document">
                        <xsl:for-each select="kml:Style/kml:IconStyle/kml:Icon">
                            <xsl:element name="img">
                                <xsl:attribute name="src">
                                    <xsl:value-of select="kml:href"/>
                                </xsl:attribute>                                
                            </xsl:element>
                        </xsl:for-each>    
                    <xsl:value-of select="kml:name"/>
                    </xsl:for-each>
                </h2>
                <ui>List of hotels</ui>
                <ul>
                    <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                    <xsl:sort select="kml:name"/>
                    <li><xsl:value-of select="kml:name"/>
                        <ul>
                            <li><a>
                            <xsl:attribute name="href">
                            <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                            </xsl:attribute>
                            <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>                          
                            </a></li>
                            <xsl:for-each select="kml:Point">
                            <li>Coordinates:<xsl:value-of select="kml:coordinates"/></li>
                            </xsl:for-each>
                        </ul> 
                    </li>                
                    </xsl:for-each>
                </ul>                           
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
