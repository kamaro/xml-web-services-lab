<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <xsl:element name= "companies">        
            <xsl:for-each select="//company">
                <xsl:element name= "company">
                    <xsl:attribute name="symbol">
                        <xsl:value-of select="symbol"></xsl:value-of>
                    </xsl:attribute>
                    <xsl:element name="name">
                        <xsl:value-of select="name"></xsl:value-of>
                    </xsl:element>
                    <xsl:for-each select="research/labs/lab">
                        <xsl:element name="lab">
                            <xsl:attribute name="city">
                                <xsl:value-of select="./@location"></xsl:value-of>
                            </xsl:attribute>
                            <xsl:value-of select="."></xsl:value-of>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>   
                    
    </xsl:template>
</xsl:stylesheet>
