<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Hostels in Bangkok, Thailand</title>
            </head>
            <body>
                <h1>
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                        </xsl:attribute>
                    </img>
                    <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                </h1>
                List Of Hotels
                <xsl:for-each select = "kml:kml/kml:Document/kml:Placemark">
                    <xsl:sort select ="kml:name"/>
                    <ul>
                        <li>
                            <xsl:value-of select = "kml:name"/>
                            <xsl:for-each select="kml:description">
                                <ul>
                                    <u>
                                        <li>
                                            <url style="color:blue">
                                                <xsl:value-of select = "substring-before(substring-after(.,'href=&quot;'),'&quot;&gt;')"/>
                                            </url>
                                        </li>
                                    </u>
                                </ul>
                            </xsl:for-each> 
                            <ul>
                                <li>    
                                    Coordinates:<xsl:value-of select ="kml:Point/."/>
                                </li>
                            </ul>      
                        </li>
                    </ul>           
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
