<?xml version='1.0' encoding='UTF-8' ?> 
<!-- was: <?xml version="1.0"?> -->

<!--
    Document   : companies.xsl
    Created on : 17 เธ�เธฑเธ�เธขเธฒเธขเธ� 2558, 16:40 เธ�.
    Author     : user
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
         <html>
            <head>
                <title>XSLT Lab : Problem 1.1</title>
            </head>
            <body>
                
                <ul>
                    <xsl:for-each select="companies/company">
                     <li><xsl:value-of select="name"/></li>
                    </xsl:for-each>
                </ul>
            </body>
        </html>

    </xsl:template>

</xsl:stylesheet>
