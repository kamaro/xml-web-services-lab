<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : books.xsl
    Created on : September 20, 2015, 2:27 PM
    Author     : user
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <style>
                    table, th, td {
                    border: 1px solid black;
                    } 
                    p {
                        font-size: 20px; 
                    }
                    p.header {
                        text-align: center
                    } 
                </style>
                <title>books.xsl</title>
            </head>
            <body>
                <table>
                    <tr>
                        <td>
                            <p class="header"><b>Title</b></p>
                        </td>
                        <td>
                            <p class="header"><b>Page</b></p>
                        </td> 
                    </tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort select="lib:pages/." data-type="number"/>
                        <xsl:if test="lib:pages > 0">    
                            <tr>
                                <td>
                                    <p>
                                        <xsl:value-of select="@about"/>
                                    </p>
                                </td>
                                <td>
                                    <p>
                                        <xsl:value-of select="lib:pages"/>
                                    </p>
                                </td> 
                            </tr>
                        </xsl:if>
                    </xsl:for-each>    
                </table> 
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
