<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : adasd.xsl
    Created on : September 17, 2015, 6:24 PM
    Author     : Jittichai
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <companies>
            <xsl:for-each select="//company">    
                <company> 
                    
                    <xsl:attribute name="symbol">
                        <xsl:value-of select ="symbol"> 
                        </xsl:value-of>
                    </xsl:attribute>

                    <name> 
                                                                
                        <xsl:value-of select ="name"> 
                        </xsl:value-of>
                                       
                    </name>

                    <xsl:for-each select="research/labs/lab">
    
                        <lab> 
                            <xsl:attribute name="state"> 
                                <xsl:for-each select="@location">
                                    <xsl:if test = ".= 'new york city'">NEW YORK</xsl:if>
                                </xsl:for-each>
                                <xsl:for-each select="@location">
                                    <xsl:if test = ".= 'san jose'">California</xsl:if>
                                </xsl:for-each>
                                <xsl:for-each select="@location">
                                    <xsl:if test = ".= 'pittsburgh'">Pennsylvania</xsl:if>
                                </xsl:for-each>    
                               
                                
                            </xsl:attribute>
                            
                            
                            

                            <xsl:value-of select ="."/> 
                        </lab>
                        
                        
                            
                     
                                    
                        
                    </xsl:for-each>
                 
                                       

                                                        

                </company>
            </xsl:for-each> 
                
        </companies> 
            
    </xsl:template>
    

</xsl:stylesheet>
