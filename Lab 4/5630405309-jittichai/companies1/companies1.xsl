<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : adasd.xsl
    Created on : September 17, 2015, 6:24 PM
    Author     : Jittichai
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head> 
                <title>XSLT Lab : Problem 1.1</title>
            </head>
            <body>
            <ul>
                <xsl:for-each select="//company"> 
                    <li> 
                        <xsl:value-of select ="name"> 
                        </xsl:value-of>
                    </li>    
                </xsl:for-each>
    
            </ul>      
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
