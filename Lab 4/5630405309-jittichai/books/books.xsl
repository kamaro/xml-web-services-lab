<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:lib="http://www.zvon.org/library"
                version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>books.xsl</title>
                
            </head>
            <body>
                <TABLE BORDER="1">
                    <TR>
                        <TH>Title</TH>
                        <TH>Pages</TH>
                    </TR>
                    <TR>
                        <xsl:for-each select="rdf:RDF/rdf:Description">
                            <xsl:sort select="lib:pages"/>
                            <xsl:choose>
                                <xsl:when test="lib:pages != ''">
                                    <TR>
                                        <TD>
                                            <xsl:value-of select="@about"/>
                                        </TD>
                                        <TD>
                                            <xsl:value-of select="lib:pages"/>
                                        </TD>
                                    </TR>
                                </xsl:when>
                            </xsl:choose>

                        </xsl:for-each>
   
                    </TR>
                </TABLE>
            </body>
        </html>
    </xsl:template>
    

</xsl:stylesheet>
