<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2"  version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
 
        <html>
            <head>
                <title>hotel.xsl</title>
            </head>
            <body>
                <h2>
                        
                    <img>
                        
                        <xsl:attribute name="src"> 
                                
                            <xsl:value-of select="/kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>  
                        </xsl:attribute>
                    </img>
                    
                    <xsl:value-of select="/kml:kml/kml:Document/kml:name"/>

                </h2>
                
                <h3>List of hotels</h3>
                
                
                
                    
                <ul> 
                    <xsl:for-each select = "kml:kml/kml:Document/kml:Placemark/.">
                        <xsl:sort select="kml:name/."/> 
                        <li> 
                            <xsl:value-of select="kml:name/."/>
                        </li>
                        <ul> 
                            
                            <a href="link">
                                
                                <li>
                                    <cstr>
                                        <xsl:value-of select="substring-before(substring-after(.,'&lt;a href=&quot;'),'&quot;&gt;')"/>
                                    </cstr>
                                </li>
                               
                            </a> 
                                
                        </ul>
                        <ul> 
                                
                            <coor>Coordinates:<xsl:value-of select="kml:Point/kml:coordinates/."/></coor>
                                
                        </ul>
                            
                            
                        
                    </xsl:for-each>    
                </ul> 
                    
                 
            </body>
        </html>

    </xsl:template>

</xsl:stylesheet>
