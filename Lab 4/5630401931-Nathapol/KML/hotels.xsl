<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
    
    <xsl:template match="kml:kml/kml:Document">
        <html>
            <head>
                <title>hotels</title>
            </head>
            <body>
                <h1>
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                        </xsl:attribute>
                    </img>
                    <xsl:value-of select="kml:name"/>
                </h1>  
                <list>Lists of hotels</list>
                <information>
                    <xsl:for-each select="kml:Placemark">
                    <xsl:sort select="kml:name"/>
                        <ul>
                            <li>
                                <xsl:value-of select="kml:name"/>
                                <xsl:variable name="webaddress" select='kml:description'/> 
                                <xsl:variable name="varafter" select="substring-after($webaddress,'Book online : &lt;a href=')"/>
                                <xsl:variable name="varbefore" select="substring-before($varafter,'&gt;')"/>
                                <xsl:variable name="varlast" select="substring($varbefore,2,string-length($varbefore)-2)"/>                     
                                <ul>
                                    <li> 
                                        <a>
                                            <a href="{$varlast}">
                                                <span>
                                                    <xsl:value-of select="$varlast"/>
                                                </span>
                                            </a> 
                                        </a>
                                    </li>
                                    <li>
                                        Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </xsl:for-each>
                </information>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
