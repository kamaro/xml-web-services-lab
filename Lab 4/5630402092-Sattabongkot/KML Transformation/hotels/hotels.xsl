<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" 
                xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <xsl:for-each select="kml:kml/kml:Document">
                <body>  
                    <h1>
                        <img>
                            <xsl:attribute name="src">
                                <xsl:value-of select="kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                            </xsl:attribute>
                        </img> 
                        <xsl:value-of select="kml:name"/>
                    </h1>
                    <p>
                        List of hotels
                    </p>
                    <xsl:for-each select="kml:Placemark">
                        <ul>
                            <li>
                                <xsl:value-of select="kml:name"/>
                                <ul>
                                    <li>
                                        <xsl:element name="a">
                                            <xsl:attribute name="href">
                                                <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="substring-before(substring-after(kml:description,'&quot;'),'&quot;')"/>
                                        </xsl:element> 
                                    </li>
                                    <li>
                                        Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </xsl:for-each>
                </body>
            </xsl:for-each>
        </html>
    </xsl:template>

</xsl:stylesheet>
