<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
    <xsl:template match="kml:kml/kml:Document">
        <html>
            <body>
                <h1>
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                        </xsl:attribute>
                    </img>
                    <xsl:value-of select="kml:name"/>
                </h1>
                List of hotels
                <xsl:for-each select="kml:Placemark">
                    <xsl:sort select="kml:name"/>
                    <ul>
                        <li>
                            <xsl:value-of select="kml:name"/>
                            
                            <xsl:variable name="str" 
                                select='kml:description'/>
                            <xsl:variable name="cutafter"
                                select="substring-after($str,'Book online : &lt;a href=')"/>
                            <xsl:variable name="cutbefore"
                                select="substring-before($cutafter,'&gt;')"/>
                            <xsl:variable name="cutfin"
                                select="substring($cutbefore,2,string-length($cutbefore)-2)"/>
                            <ul>
                                <li>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="$cutfin"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="$cutfin"/>
                                    </a>
                                </li>
                                <li>Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/></li>
                            </ul>
                        </li>
                    </ul>
                </xsl:for-each>
            </body>
        </html>
   </xsl:template>
</xsl:stylesheet>