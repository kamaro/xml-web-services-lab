<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
    <xsl:template match="/">
        <companies>
            <xsl:for-each select="//company">
                <companies>
                    <company>
                        <xsl:attribute name="symbol">
                            <xsl:value-of select="symbol"/>
                        </xsl:attribute>
                       <name><xsl:value-of select="name"/></name>
                       <xsl:if test="position()=last()-1">
                            <xsl:for-each select="research/labs/lab">
                                 <lab>
                                     <xsl:attribute name="state">
                                         <xsl:if test="position()=last()-1">
                                             <xsl:text>New York</xsl:text>
                                         </xsl:if>
                                         <xsl:if test="position()=last()">
                                             <xsl:text>California</xsl:text>
                                         </xsl:if>
                                     </xsl:attribute>
                                     <xsl:value-of select="."/>
                                 </lab>
                             </xsl:for-each>
                       </xsl:if>
                       <xsl:if test="position()=last()">
                            <xsl:for-each select="research/labs/lab">
                                 <lab>
                                     <xsl:attribute name="state">
                                         <xsl:if test="position()=last()-1">
                                             <xsl:text>California</xsl:text>
                                         </xsl:if>
                                         <xsl:if test="position()=last()">
                                             <xsl:text>Pennsylvania</xsl:text>
                                         </xsl:if>
                                     </xsl:attribute>
                                     <xsl:value-of select="."/>
                                 </lab>
                             </xsl:for-each>
                       </xsl:if>
                    </company>
                </companies>
            </xsl:for-each>
        </companies>
    </xsl:template>
</xsl:stylesheet>