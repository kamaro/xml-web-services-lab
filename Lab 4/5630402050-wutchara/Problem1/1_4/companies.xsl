<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
    <xsl:template match="/">
        <companies>
            <xsl:for-each select="//company">
                <companies>
                    <company>
                        <xsl:attribute name="symbol">
                            <xsl:value-of select="symbol"/>
                        </xsl:attribute>
                       <name><xsl:value-of select="name"/></name>
                       <xsl:for-each select="research/labs/lab">
                            <lab>
                                <xsl:attribute name="city">
                                    <xsl:value-of select="@location"/>
                                </xsl:attribute>
                                <xsl:value-of select="."/>
                            </lab>
                        </xsl:for-each>
                        <xsl:if test="position()=last()-1">
                            <lab city="austin">IBM Austin Research Lab</lab>
                        </xsl:if>
                    </company>
                </companies>
            </xsl:for-each>
        </companies>
    </xsl:template>
</xsl:stylesheet>