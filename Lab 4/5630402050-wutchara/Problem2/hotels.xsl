<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:kml="http://www.opengis.net/kml/2.2"
                xmlns:atom="http://www.w3.org/2005/Atom">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
            </head>
            <body>
                <img>
                    <xsl:attribute name="src">
                        <xsl:value-of select="kml:kml//kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                    </xsl:attribute>
                    <xsl:attribute name="alt">
                        <xsl:value-of select="kml:kml//kml:Document/kml:Style/@id"/>
                    </xsl:attribute>
                </img>
                <np>
                    <b>
                        <font size = "6">
                            <xsl:value-of select="kml:kml//kml:Document/kml:name"/>
                        </font>
                    </b>
                </np>
                <p>
                    <xsl:text>List of hotels</xsl:text>
                </p>
                    <ul>
                        <xsl:for-each select="kml:kml//kml:Document/kml:Placemark">
                        <xsl:variable name="web" select="substring-before(substring-after(kml:description,'http'),'.html')"/>
                           <li><xsl:value-of select="kml:name"/>
                               <ul>
                                    <li>
                                        <a><xsl:attribute name="href">
                                                <xsl:text>
                                                    <xsl:text>http</xsl:text>
                                                        <xsl:value-of select="$web" />
                                                    <xsl:text>.html</xsl:text>
                                                </xsl:text> 
                                            </xsl:attribute>
                                                <!--<xsl:value-of select="kml:kml/kml:Document/kml:Placemark/kml:description" />-->
                                            <xsl:text>
                                                <xsl:text>http</xsl:text>
                                                    <xsl:value-of select="$web" />
                                                <xsl:text>.html</xsl:text>
                                            </xsl:text>    
                                        </a>
                                    </li>
                                    <li>
                                        <xsl:text>Coordinates:
                                        </xsl:text>
                                        <xsl:value-of select="kml:Point/kml:coordinates"/>
                                    </li>
                               </ul>
                           </li>
                        </xsl:for-each>
                    </ul>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>