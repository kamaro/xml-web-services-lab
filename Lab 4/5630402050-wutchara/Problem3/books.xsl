<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <style>
                    table, tr, td , th
                        {border: 1px solid black;}
                    th 
                        {text-align: center;}

                </style>
            </head>
            <body>
                <table>
                    <tr>
                        <th>
                            <xsl:text>Title</xsl:text>
                        </th>
                        <th>
                            <xsl:text>Page</xsl:text>
                        </th>
                    </tr>
                    <tr>
                        <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort select="lib:pages"/>
                            <xsl:if test="lib:pages > 0">
                                <tr>
                                    <td>
                                        <xsl:value-of select="@about"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="lib:pages"/>
                                    </td>       
                                </tr>
                            </xsl:if>
                        </xsl:for-each>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>