<?xml version="1.0"?>

<!--
    Document   : companies.xsl
    Created on : September 17, 2015, 5:08 PM
    Author     : Nicha
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>XSLT Lab : Problem 1.1</title>
            </head>
            <body>
                <h2>These are the leading companies: </h2>
                <ul>
                    <xsl:for-each select="companies/company">
                       
                        <li>
                            <xsl:value-of select="name"/>
                        </li>
                        
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
