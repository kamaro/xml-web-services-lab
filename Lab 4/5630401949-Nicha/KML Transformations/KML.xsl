<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
    
    <xsl:template match="kml:kml/kml:Document">
        <html>
            <head>
                <title>Hostels</title>
            </head>
            <body>
                <h1>
                    <img>
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                        </xsl:attribute>
                    </img>
                    <xsl:value-of select="kml:name"/>
                </h1>  
                <list>Lists of hotels</list>
                <information>
                    <xsl:for-each select="kml:Placemark">
                    <xsl:sort select="kml:name"/>
                        <ul>
                            <li>
                                <xsl:value-of select="kml:name"/>
                                <xsl:variable name="hotelinformation" select='kml:description'/> 
                                <xsl:variable name="subafter" select="substring-after($hotelinformation,'Book online : &lt;a href=')"/>
                                <xsl:variable name="subbefore" select="substring-before($subafter,'&gt;')"/>
                                <xsl:variable name="sublast" select="substring($subbefore,2,string-length($subbefore)-2)"/>                     
                                <ul>
                                    <li> 
                                        <a>
                                            <a href="{$sublast}">
                                                <span>
                                                    <xsl:value-of select="$sublast"/>
                                                </span>
                                            </a> 
                                        </a>
                                    </li>
                                    <li>
                                        
                                        Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </xsl:for-each>
                </information>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
