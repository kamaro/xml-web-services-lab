<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:lib="http://www.zvon.org/library"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>XSLT Lab : Problem 3</title>
            </head>
            <body>
                <table border="1" solid = "black">
                    <tr>
                        <th>
                            <text>
                                Title
                            </text>
                        </th>
                        <th>
                            <text>
                                Pages
                            </text>
                        </th>
                    </tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort data-type="number" select="lib:pages"/>
                        <xsl:if test="@about!='RD' and @about!='JC'">
                            <tr>
                                <td>
                                    <xsl:value-of select="@about"/>
                                </td>
                                <td>
                                    <xsl:value-of select="lib:pages"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
