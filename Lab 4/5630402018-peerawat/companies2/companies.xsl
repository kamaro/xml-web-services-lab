<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>XSLT Lab : Problem 1.2</title>
            </head>
            
            <body>
                <h2>These are the leading companies:</h2>
                <ul>
                    <xsl:for-each select="//company">
                        <li>
                            <xsl:value-of select="name"/>
                            <ul>
                                <xsl:for-each select="research/labs/lab">
                                    <li>
                                    <xsl:value-of select="."/>
                                    </li>
                            </xsl:for-each>
                            </ul>
                        </li>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
