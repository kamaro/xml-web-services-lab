<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>
                <h1>
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>                                                   
                        </xsl:attribute>
                    </xsl:element>
                    Hotels in Bankok,Thailand
                </h1>
                <h3>List of hotels</h3>                
                <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                    <xsl:sort name="kml:name"/>
                    <ul>
                        <li>                                    
                            <xsl:value-of select="kml:name"/>                                                                                           
                            <ul>
                                <li>
                                    <xsl:for-each select="kml:description">
                                        <xsl:element name="a">
                                            <xsl:attribute name="href">
                                                <xsl:value-of select="substring-before(substring-after(string(),'href=&quot;'),'&quot;>')"/>                                                            
                                            </xsl:attribute>
                                            <xsl:value-of select="substring-before(substring-after(string(),'href=&quot;'),'&quot;>')"/>                                                            
                                        </xsl:element>
                                    </xsl:for-each>
                                </li>    
                                <li>
                                    Coordinate:    
                                    <xsl:value-of select="kml:Point/kml:coordinates"/>                                             
                                </li>
                            </ul>     
                        </li>
                    </ul>                                  
                </xsl:for-each>
                   
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
