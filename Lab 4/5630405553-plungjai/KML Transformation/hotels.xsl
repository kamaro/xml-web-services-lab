<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>XSLT Lab : Problem 2</title>
            </head>
            <body>
                <h1>
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                        </xsl:attribute>
                    </xsl:element>
                    <xsl:value-of select="kml:kml/kml:Document/kml:name"/>
                </h1>
                <xsl:text>
                    List of hotels
                </xsl:text>
                <br/>
                <ul>
                    <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                        <xsl:sort select="kml:name"/>
                        <li>
                            <xsl:value-of select="kml:name"/>
                            <ul>
                                <xsl:for-each select="kml:description">
                                    <li>
                                        <xsl:element name="a">
                                            <xsl:attribute name="href">
                                                <xsl:value-of select="substring-before(substring-after(string(),'href=&quot;'),'&quot;>')"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="substring-before(substring-after(string(),'href=&quot;'),'&quot;>')"/>
                                        </xsl:element>
                                    </li>
                                </xsl:for-each>
                                <li>
                                    <xsl:text>
                                        Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/>
                                    </xsl:text>
                                </li>
                            </ul>
                        </li>
                    </xsl:for-each>
                </ul>
            </body>
        </html> 
    </xsl:template>

</xsl:stylesheet>
