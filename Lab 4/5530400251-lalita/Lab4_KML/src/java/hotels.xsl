<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="kml:kml/kml:Document">
        <html>
            <body>
                <h1>
                    <img src="{kml:Style/kml:IconStyle/kml:Icon/kml:href}" />
                    <xsl:value-of select="kml:name"/>
                </h1>
                List of hotels
                <xsl:for-each select="kml:Placemark">
                    <xsl:sort select="kml:name"/> <!-- sorting Statement -->
                    <ul>
                        <li>
                            <xsl:value-of select="kml:name"/> 
                        </li>
                    <!-- select string from text -->
                        <xsl:variable name="url">
                            <xsl:value-of select="substring-before(substring-after(kml:description, '&lt;'), '&gt;')"/>
                        </xsl:variable>
                        <xsl:variable name="coordinates">
                            <xsl:value-of select="substring($url,9,string-length($url)-9)"/>
                        </xsl:variable>
                               
                                <ul>
                                    <li>
                                        <a>
                                            <xsl:attribute name="href">
                                                <xsl:value-of select="$url"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="$coordinates"/>
                                        </a>
                                    </li>
                                    <li>Coordinates:<xsl:value-of select="kml:Point/kml:coordinates"/></li>
                                </ul>
                                                  
                    </ul>
                </xsl:for-each>
                
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
