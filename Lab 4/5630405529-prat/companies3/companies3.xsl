<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <companies>
            <xsl:element name="company">
                <xsl:attribute name="symbol">IBM</xsl:attribute>
                <name>
                <xsl:value-of select="//name"/>
                </name>
                <xsl:element name="lab">
                    <xsl:attribute name="city">new york city</xsl:attribute>
                    <xsl:value-of select="//lab"/>
                </xsl:element>
                <xsl:element name="lab">
                    <xsl:attribute name="city">san jose</xsl:attribute>
                    <xsl:value-of select="//lab[2]"/>
                </xsl:element>
            </xsl:element>
            <xsl:element name="company">
                <xsl:attribute name="sumbol">INTEL</xsl:attribute>
                <name>
                <xsl:value-of select="//company[2]/name"/>
                </name>
                <xsl:element name="lab">
                    <xsl:attribute name="city">san jose</xsl:attribute>
                    <xsl:value-of select="//company[2]/research/labs/lab"/>
                </xsl:element>
                <xsl:element name="lab">
                    <xsl:attribute name="city">pittsburgh</xsl:attribute>
                    <xsl:value-of select="//company[2]/research/labs/lab[2]"/>
                </xsl:element>
            </xsl:element>
            
        </companies>
    </xsl:template>

</xsl:stylesheet>
