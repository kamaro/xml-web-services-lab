<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <body>
                <xsl:element namr="img">
                      <xsl:attribute name="src">     
                            <xsl:value-of select="/kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>  
                        </xsl:attribute>
                     </xsl:element>
                <xsl:value-of select="/Document/name"/>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
