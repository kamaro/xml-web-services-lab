<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : hotels.xsl
    Created on : September 22, 2015, 5:38 PM
    Author     : Miki
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:kml="http://www.opengis.net/kml/2.2">
    
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>XSL Lab : Problem 2 Hotels</title>
            </head>
            <body>
                
                <h1>
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"></xsl:value-of>
                        </xsl:attribute>
                    </xsl:element>
 
                    <xsl:value-of select="kml:kml/kml:Document/kml:name"></xsl:value-of>
                </h1>
                
            <text>list of hotels</text>
            <ul>
                <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                    <xsl:sort select="kml:name"></xsl:sort>
                    <li>
                        <xsl:value-of select="kml:name"></xsl:value-of>
                        <ul><li>  
                            <xsl:element name="a">
                                <xsl:attribute name="href">
                                    <xsl:value-of select="substring-after(substring-before(kml:description,'&quot;&gt;'),'&quot;')">
                                    </xsl:value-of> 
                                </xsl:attribute>
                                    <xsl:value-of select="substring-after(substring-before(kml:description,'&quot;&gt;'),'&quot;')">
                                    </xsl:value-of> 
                            </xsl:element>                        
                            </li>
                        
                            <li> 
                                <text>Coordinates:</text>
                                <xsl:value-of select="kml:Point/kml:coordinates"></xsl:value-of>
                            </li> 
                        </ul>
                    </li>
                </xsl:for-each>
            </ul>

            </body>
        </html>
    </xsl:template>
                    
</xsl:stylesheet>
