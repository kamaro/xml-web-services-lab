<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>XSLT Lab : Problem 1.2</title>
            </head>
            <h2>These are the leading companies:</h2>
            <body>
                <ul>
                    <xsl:for-each select="companies/company"> 
                     <li><xsl:value-of	select="name"/> 
                        <ul>
                              <xsl:apply-templates select="research/labs/lab">
                                 </xsl:apply-templates>
                        </ul>
                        </li>
                   </xsl:for-each>
 
                </ul>                                                   
            </body>
        </html>
    </xsl:template>
    
                <xsl:template match="research/labs/lab">
                 <li><xsl:value-of select="."/></li>
                    
                </xsl:template>

</xsl:stylesheet>
