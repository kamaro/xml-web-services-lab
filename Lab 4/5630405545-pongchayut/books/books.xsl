<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : books.xsl
    Created on : September 22, 2015, 7:00 PM
    Author     : Miki
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:lib="http://www.zvon.org/library">
    <xsl:output method="html"/>

  
    <xsl:template match="/">
        <html>
            <head>
                <title>XSL Lab : Problem 3 Books</title>
            </head>
            <body>
                <table border="1">
                    <tr>
                        <th>Title</th>
                        <th>Pages</th>
                    </tr>
                    <xsl:for-each select="rdf:RDF/rdf:Description">
                        <xsl:sort select="lib:pages"></xsl:sort>
                        <xsl:if test="@about!='JC' and @about!='RD'">
                            <tr>
                        
                                <td>
                                    <xsl:value-of select="@about"></xsl:value-of>
                                </td>
                                <td> 
                                    <xsl:value-of select="lib:pages"></xsl:value-of>
                                </td>
  
                            </tr>
                        </xsl:if>
                        
                    </xsl:for-each>
                    
                </table>
    
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
