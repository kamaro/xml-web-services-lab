<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>XSLT Lab : Problem 1.2</title>
            </head>
            <body>
                <H2> These are the leading companies: </H2>
                     <ul>  
                          <xsl:for-each select = "companies/company" >
                                <li><xsl:value-of select="name" /> </li>
                                  <ul> 
                                       <xsl:for-each select = "research/labs/lab" >
                                              <li><xsl:value-of select="." /></li> 
                                        </xsl:for-each>
                                 </ul> 
                           </xsl:for-each>
                     </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
