<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:kml="http://www.opengis.net/kml/2.2">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>
                 <H2>   <xsl:element name="img">
                            <xsl:attribute name="src">
                                <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/>
                            </xsl:attribute>
                        </xsl:element>
                        <xsl:value-of select="kml:kml/kml:Document/kml:name"/> 
                 </H2>
                 <xsl:text>List of hotels</xsl:text>
                 <ul>  
                     <xsl:for-each select = "kml:kml/kml:Document/kml:Placemark" >
                         <li><xsl:value-of select="kml:name"/> </li>
                         <ul>
                             <xsl:for-each select="kml:description">
                                 <li>
                                     
                                     <xsl:element name="description">
                                        <a><xsl:attribute name="href">
                                                <xsl:value-of select="substring-before(substring-after(string(),'href=&quot;'),'&quot;>')"/>
                                        </xsl:attribute>
                                                <xsl:value-of select="substring-before(substring-after(string(),'href=&quot;'),'&quot;>')"/>
                                        </a>
                                     </xsl:element>                                      
                                 </li>
                             </xsl:for-each>
                                 <li> 
                                    <xsl:text>coordinates</xsl:text>
                                    <xsl:value-of select="kml:Point/kml:coordinates"/> 
                                 </li>
                         </ul>
                     </xsl:for-each>
                 </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>