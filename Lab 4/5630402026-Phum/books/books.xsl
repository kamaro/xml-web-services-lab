<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:lib="http://www.zvon.org/library"> version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>books.xsl</title>
                <style>
                    table, th, td{
                    border:1px solid black;}
                </style>
            </head>
            <body>
                <table>
                    <tr>
                        <th>Title</th>
                        <th>Pages</th>
                    </tr> 
                    
                        
                        <xsl:for-each select="rdf:RDF/rdf:Description">
                            <xsl:sort name="lib:pages"></xsl:sort>
                            <tr>                            
                                <xsl:if test="lib:pages != 0">
                                    <td>
                                    <xsl:value-of select="@about">
                        
                                    </xsl:value-of>   
                                    </td>
                                    <td>
                                        <xsl:value-of select="lib:pages"></xsl:value-of>
                                    </td>
                                </xsl:if>                
                            </tr>                    
                        </xsl:for-each>                
                            
                    
                </table>
                
                
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
