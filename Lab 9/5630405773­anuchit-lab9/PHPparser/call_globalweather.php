<?php

header('Content-Type: text/xml; charset=utf-8');
$wsdl = 'http://webservicex.com/globalweather.asmx?WSDL';
$client = new SoapClient($wsdl);
$methodName = 'GetWeather';
$params = array('CityName' => 'Khon Kaen', 'CountryName' => 'Thailand');
$soapAction = 'http://www.webserviceX.NET/GetWeather';
$objectResult = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));
$response = $objectResult->GetWeatherResult;
echo $response;
?>