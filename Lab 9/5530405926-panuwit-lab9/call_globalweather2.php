<?php 
$wsdl = 'http://webservicex.com/globalweather.asmx?WSDL';
$client = new SoapClient($wsdl, array("SOAPAction"=>"http://www.webserviceX.NET/GetWeather"));
$param = array("CityName" => 'Khon Kaen', "CountryName" => 'Thailand');
$result = $client->__soapCall('GetWeather',array($param));
$get = simplexml_load_string(preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $result->GetWeatherResult));

?>
<html>
    <body>
        <p>At <font color='blue'><?php echo $get->Location; ?></font></p>
        <p>on <font color='blue'><?php echo $get->Time; ?></font></p>
        <p>the temperature is <font color='blue'><?php echo $get->Temperature; ?></font></p>
    </body>
</html>