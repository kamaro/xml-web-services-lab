/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CallRDWS;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author John
 */
public class CallRDWS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    try {
            // สร้าง MessageFactory เพื่อเตรียมสร้าง SOAPMessage
            MessageFactory mFac = MessageFactory.newInstance();
            // สร้าง SOAPMessage จาก MessageFactory
            SOAPMessage message = mFac.createMessage();
            // ดึงส่วนของ SOAPBody จาก SOAPMessage
            SOAPBody body = message.getSOAPBody();
            // สร้าง SOAPFactory เพื่อเตรียมสร้างรายละเอียดของ SOAP
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            // สร้าง object Name (javax.xml.soap.Name)
            // ที่มี local name คือ GetGeoIP มี prefix คือ ns และ
            // มี namespace คือ http://www.webservicex.net/
            String prefix = "ns";
            String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
            Name opName = soapFactory.createName("ServiceTIN",
                    prefix, namespace);
            // ใส่ opName เข้าไปใน SOAPBody
            SOAPBodyElement opElem = body.addBodyElement(opName);
           
            SOAPElement user = opElem.addChildElement(
                    soapFactory.createName("username", prefix, namespace));
            user.addTextNode("anonymous"); 
            
            SOAPElement pass = opElem.addChildElement(
                    soapFactory.createName("password", prefix, namespace));
            pass.addTextNode("anonymous"); 
            
            SOAPElement tin = opElem.addChildElement(
                    soapFactory.createName("TIN", prefix, namespace));
            tin.addTextNode("3570501221191"); 
            
            // เพิ่มส่วนการกำหนด SOAPAction
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");
            // แสดงข้อความ SOAP Request
        	//displayMessage(message, out);
            // สร้าง SOAPConnection เพื่อเรียกใช้เว็บเซอร์วิส และรับค่าผลลัพธ์ด้วย SOAPMessage
      displayMessage(message);
        System.out.println("\n\n");
//add code below for trust x.509 ceritficate
        XTrustProvider.install();
        SOAPConnection conn =
                SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = conn.call(message,
                "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx");
        System.out.println("RESPONSE:");
//Display Response Message
        displayMessage(response);
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    
        //Display Request Message
       
    }

    
      public static void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
transformer.setOutputProperty(OutputKeys.INDENT, "yes");    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }

    
    }
    

