/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9_3;

/**
 *
 * @author zpyii
 */
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Source;
import javax.xml.soap.MessageFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.MimeHeaders;
import com.sun.net.ssl.internal.ssl.*;
import java.security.Security;

public class CheckTin {

    public void msgEnvelope(String[] args) throws Exception {

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection connection = soapConnectionFactory.createConnection();
        MessageFactory messageFactory = MessageFactory.newInstance();

        // Create a message
        SOAPMessage message = messageFactory.createMessage();

        // Get the SOAP header and body from the message
        // and remove the header
        SOAPHeader header = message.getSOAPHeader();
        SOAPBody body = message.getSOAPBody();
        header.detachNode();

        // Create a SOAP factory
        // Create a UDDI v2 checkPin body element
        String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
        SOAPFactory soapFactory = SOAPFactory.newInstance();
        SOAPBodyElement checkPin = body.addBodyElement(soapFactory.createName("ServiceTIN", "ns", namespace));

        SOAPElement username = checkPin.addChildElement(soapFactory.createName("username", "ns", namespace));
        username.addTextNode("anonymous");

        SOAPElement password = checkPin.addChildElement(soapFactory.createName("password", "ns", namespace));
        password.addTextNode("anonymous");

        SOAPElement pin = checkPin.addChildElement(soapFactory.createName("TIN", "ns", namespace));
        pin.addTextNode("3300900417333");

        MimeHeaders hd = message.getMimeHeaders();
        String Action ="https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN";
        hd.addHeader("SOAPAction",Action );

        message.saveChanges();
        System.out.println("REQUEST:");
        //Display Request Message
        displayMessage(message);
        System.out.println("\n\n");
        //add code below for trust x.509 ceritficate
        XTrustProvider.install();
        SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = conn.call(message, "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx");

        System.out.println("RESPONSE:");
        //Display Response Message
        displayMessage(response);
    }

    public void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);

    }

    public static void main(String[] args) throws Exception {
        CheckTin clientApp = new CheckTin();
        clientApp.msgEnvelope(args);
    }

}
