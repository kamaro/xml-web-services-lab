<?php

//header("Content-type: text/xml");
$URL = 'http://webservicex.com/globalweather.asmx?WSDL';
$SOAPAction = 'http://www.webserviceX.NET/GetWeather';
$client = new SoapClient($URL,array("SOAPAction"=>$SOAPAction));
$methodName = 'GetWeather';
$params = array(
    'CityName' => 'Khon Kaen',
    'CountryName' => 'Thailand'
);

$objectResult = $client->__soapCall($methodName, array($params));
$response = $objectResult->GetWeatherResult;
$corrected_xml_file= preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $response);

$xml=simplexml_load_string($corrected_xml_file) or die("Error: Cannot create object");
echo "<font size=\"5\">At <font color=\"blue\">".$xml->Location."</font></font></br>";
echo "<font size=\"5\">On <font color=\"blue\">".$xml->Time . "</font></font></br>";
echo "<font size=\"5\">The temperature is <font color=\"blue\">".$xml->Temperature . "</font></font></br>";

?>
