<?php

header("Content-type: text/xml");
$URL = 'http://webservicex.com/globalweather.asmx?WSDL';
$SOAPAction = 'http://www.webserviceX.NET/GetWeather';
$client = new SoapClient($URL,array("SOAPAction"=>$SOAPAction));
$methodName = 'GetWeather';
$params = array(
    'CityName' => 'Khon Kaen',
    'CountryName' => 'Thailand'
);

$objectResult = $client->__soapCall($methodName, array($params));
$response = $objectResult->GetWeatherResult;
echo $response;
?>
