<%-- 
    Document   : newjsp
    Created on : Nov 5, 2015, 6:53:40 PM
    Author     : Bourne
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Geo IP Web Application</title>
    </head>
    <body>
        <h1>Find the Location of the IP Address</h1>
    	<form action="GetGeoIpWs">
        	IP Address : <input type="text" size="80" name="ip"/>
            <br/><br/>
            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>
