import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;

public class call_rdws {

    public static void main(String... args) throws Exception {

        final String s_username = "anonymous";
        final String s_password = "anonymous";
        final String s_tin = "3570501221191";

        MessageFactory mFac = MessageFactory.newInstance();
        SOAPMessage message = mFac.createMessage();
        SOAPBody body = message.getSOAPBody();
        SOAPFactory soapFactory = SOAPFactory.newInstance();

        String prefix = "ns";
        String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
        Name opName = soapFactory.createName("ServiceTIN", prefix, namespace);

        SOAPBodyElement opElem = body.addBodyElement(opName);

        SOAPElement username = opElem.addChildElement(soapFactory.createName("username", prefix, namespace));
        username.addTextNode(s_username);
        SOAPElement password = opElem.addChildElement(soapFactory.createName("password", prefix, namespace));
        password.addTextNode(s_password);
        SOAPElement tin = opElem.addChildElement(soapFactory.createName("TIN", prefix, namespace));
        tin.addTextNode(s_tin);
        
        MimeHeaders header = message.getMimeHeaders();
        header.addHeader("SOAPAction", "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");

        //Display Request Message
        new call_rdws().displayMessage(message);
        System.out.println("\n\n");
        XTrustProvider.install();
        SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
        String endpoint = "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx";
        SOAPMessage response = conn.call(message,endpoint);
        System.out.println("RESPONSE:");
        new call_rdws().displayMessage(response);
    }

    public void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }
}
