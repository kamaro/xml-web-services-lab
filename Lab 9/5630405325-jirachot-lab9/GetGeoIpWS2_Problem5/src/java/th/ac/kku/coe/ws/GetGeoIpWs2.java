package th.ac.kku.coe.ws;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author Palmitic
 */
@WebServlet(name = "GetGeoIpWs", urlPatterns = {"/GetGeoIpWs"})
public class GetGeoIpWs2 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String ipAddr = request.getParameter("ip");

        try (PrintWriter out = response.getWriter()) {

            MessageFactory mFac = MessageFactory.newInstance();
            SOAPMessage message = mFac.createMessage();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory soapFactory = SOAPFactory.newInstance();

            String prefix = "ns";
            String namespace = "http://www.webservicex.net/";
            Name opName = soapFactory.createName("GetGeoIP", prefix, namespace);

            SOAPBodyElement opElem = body.addBodyElement(opName);

            SOAPElement ip = opElem.addChildElement(soapFactory.createName("IPAddress", prefix, namespace));
            ip.addTextNode(ipAddr);

            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "GetGeoIP");

            SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);

            String ip_tag = resp.getSOAPBody()
                    .getElementsByTagName("IP").item(0).getFirstChild().getNodeValue();
            String country_tag = resp.getSOAPBody()
                    .getElementsByTagName("CountryName").item(0).getFirstChild().getNodeValue();

            out.println("<html>");
            out.println("<head>");
            out.println("<title>GetGeoIpWs2</title>");
            out.println("</head>");
            out.println("<body>");
            out.print("<font size=\"5\" color=\"blue\">" + ip_tag + "</font>");
            out.println(" <font size=\"5\"> is in </font> " +
                    "<font size=\"5\" color=\"blue\">" +country_tag +"</font>");
            out.println("</body>");
            out.println("</html>");
          
        } catch (SOAPException | UnsupportedOperationException ex) {

        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


}
