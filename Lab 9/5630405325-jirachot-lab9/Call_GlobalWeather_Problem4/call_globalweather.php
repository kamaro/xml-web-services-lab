<?php

header("content-type:text/xml ;charset=UTF-8");
$wsdl = 'http://webservicex.com/globalweather.asmx?WSDL';

$client = new SoapClient($wsdl);

$methodName = 'GetWeather';

$params = array('CountryName' => 'Thailand',
                'CityName' => 'Khon Kaen');

$soapAction = 'http://www.webserviceX.NET/GetWeather';

$objectResult = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));

$response = $objectResult->GetWeatherResult;

print_r ($response);

?>
