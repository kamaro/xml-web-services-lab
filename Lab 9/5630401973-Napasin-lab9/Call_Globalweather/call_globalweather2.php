<?php

$client = new SoapClient("http://webservicex.com/globalweather.asmx?WSDL");
$methodName = "GetWeather";
$params = array("CityName" => "Khon Kaen", "CountryName" => "Thailand");
$soapaction = 'http://www.webserviceX.NET/GetWeather';
$response = $client->__soapCall($methodName, 
        array('parameters' => $params), 
        array('soapaction' => $soapaction));

    
    $result = $response->GetWeatherResult;
    $result = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $result);
    
    $xml_load = simplexml_load_string($result);

    echo "At ".'<font color="blue">'.$xml_load->Location."</font>"."</br>";
    echo "on ".'<font color="blue">'.$xml_load->Time."</font>"."</br>";
    echo "the temperature is " .'<font color="blue">'.$xml_load->Temperature."</font>";

?>