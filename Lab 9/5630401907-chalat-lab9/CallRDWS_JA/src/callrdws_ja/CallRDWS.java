﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package callrdws_ja;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author crazynova
 */
public class CallRDWS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        MessageFactory mFac = MessageFactory.newInstance();
        SOAPMessage message = mFac.createMessage();
        SOAPBody body = message.getSOAPBody();
        SOAPFactory soapFactory = SOAPFactory.newInstance();
        String prefix = "ns";
        String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
        Name opName = soapFactory.createName("ServiceTIN",prefix, namespace);
        
        // ใส่ opName เข้าไปใน SOAPBody
        SOAPBodyElement opElem = body.addBodyElement(opName);
        SOAPElement userN = opElem.addChildElement(soapFactory.createName("username"));
        userN.addTextNode("anonymous");
        SOAPElement pass = opElem.addChildElement(soapFactory.createName("password"));
        pass.addTextNode("anonymous");
        SOAPElement tin = opElem.addChildElement(soapFactory.createName("TIN"));
            tin.addTextNode("3570501221191");
        MimeHeaders header = message.getMimeHeaders();
        header.addHeader("SOAPAction", namespace + "/ServiceTIN");
        
        //SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
        //String endpoint = "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx";
        //SOAPMessage resp = soapConn.call(message, endpoint);
        // แสดงข้อความ SOAP Response
        //Display Request Message
        System.out.println("REQUEST:");
        displayMessage(message);
        System.out.println("\n\n");
//add code below for trust x.509 ceritficate
        XTrustProvider.install();
        SOAPConnection conn
                = SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = conn.call(message,
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService.asmx");
        System.out.println("RESPONSE:");
//Display Response Message
        displayMessage(response);
    }

    public static void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }

}
