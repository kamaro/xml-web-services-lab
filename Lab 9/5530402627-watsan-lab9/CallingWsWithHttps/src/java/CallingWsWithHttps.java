
import java.io.PrintWriter;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Watsan
 */
public class CallingWsWithHttps {
    public static void main (String[] args) throws SOAPException, Exception {
        
            MessageFactory mFac = MessageFactory.newInstance();
            SOAPMessage message = mFac.createMessage();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            
            String prefix = "ns";
            String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
            Name opName = soapFactory.createName("ServiceTIN", prefix, namespace);
            
            SOAPBodyElement opElem = body.addBodyElement(opName);
            SOAPElement usernameElem = opElem.addChildElement(soapFactory.createName("username", prefix, namespace));
            usernameElem.addTextNode("anomymous");
            SOAPElement passwordeElem = opElem.addChildElement(soapFactory.createName("password", prefix, namespace));
            passwordeElem.addTextNode("anomymous");
            SOAPElement tinElem = opElem.addChildElement(soapFactory.createName("TIN", prefix, namespace));
            tinElem.addTextNode("1349700123591");
            
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "/ServiceTIN");
            
            displayMessage(message);
            System.out.println("\n\n");
            
            XTrustProvider.install();
            SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
            
            System.out.println("RESPOND");
            displayMessage(resp);
            
    }
    
    private static void displayMessage(SOAPMessage message) throws Exception {
            TransformerFactory tFac = TransformerFactory.newInstance();
            Transformer transformer = tFac.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent­amount", "2");
            Source src = message.getSOAPPart().getContent();
            StreamResult result = new StreamResult(System.out);
            transformer.transform(src, result);
    }
}
