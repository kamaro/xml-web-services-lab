/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kku.coe.ws;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;


/**
 *
 * @author thinkearth
 */
@WebServlet(name = "GetGeoIpWs", urlPatterns = {"/GetGeoIpWs"})
public class GetGeoIpWs extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SOAPException {
        response.setContentType("text/xml;charset=UTF-8");
        String ipAddr = request.getParameter("ip");       
        try (PrintWriter out = response.getWriter()){   
                MessageFactory mFac = MessageFactory.newInstance();
                SOAPMessage message = mFac.createMessage();
                SOAPBody body = message.getSOAPBody();
                SOAPFactory soapFactory = SOAPFactory.newInstance();
                String prefix = "ns";
                String namespace = "http://www.webservicex.net/";
                Name opName = soapFactory.createName("GetGeoIP",
                        prefix, namespace);
                SOAPBodyElement opElem = body.addBodyElement(opName);
                SOAPElement ip = opElem.addChildElement(
                        soapFactory.createName("IPAddress", prefix, namespace));
                ip.addTextNode (ipAddr);
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "GetGeoIP");
            
            SOAPConnection soapConn
                    = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
            displayMessage(resp, out);
   
    
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    /* TODO output your page here. You may use following sample code. */
   

    private void displayMessage(SOAPMessage message, PrintWriter out) {
        
        try {
        	TransformerFactory tFac = TransformerFactory.newInstance();
        	Transformer transformer = tFac.newTransformer();
        	Source src = message.getSOAPPart().getContent();
        	StreamResult result = new StreamResult(out);
 	       transformer.transform(src, result);
    	} catch (Exception ex) {
        	ex.printStackTrace(); //To change body of generated methods, choose Tools | Templates.
    }
    }
    
	

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(GetGeoIpWs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(GetGeoIpWs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
        public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
