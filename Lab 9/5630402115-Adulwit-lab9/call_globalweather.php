<?php

header("Content-type: text/xml");
$wsdl = 'http://webservicex.com/globalweather.asmx?WSDL';
$client = new SoapClient($wsdl);
$methodName = 'GetWeather';
$params = array('CityName' => 'Khon Kaen', 'CountryName' => 'Thailand');
$soapAction = 'http://www.webserviceX.NET/GetWeather';
$objectResult = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));
echo $objectResult->GetWeatherResult;
?>