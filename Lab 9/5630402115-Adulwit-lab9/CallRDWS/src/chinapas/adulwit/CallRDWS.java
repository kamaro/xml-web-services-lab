package chinapas.adulwit;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

public class CallRDWS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        try {
            MessageFactory mFac;
            mFac = MessageFactory.newInstance();
            SOAPMessage message = mFac.createMessage();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            String prefix = "ns";
            String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
            Name opName = soapFactory.createName("ServiceTIN", prefix, namespace);
            SOAPBodyElement opElem = body.addBodyElement(opName);
            SOAPElement username = opElem.addChildElement(soapFactory.createName("username", prefix, namespace));
            username.addTextNode("anonymous");
            SOAPElement password = opElem.addChildElement(soapFactory.createName("password", prefix, namespace));
            password.addTextNode("anonymous");
            SOAPElement tin = opElem.addChildElement(soapFactory.createName("TIN", prefix, namespace));
            tin.addTextNode("3570501221191");
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");

            //Display Request Message
            System.out.println("REQUEST:");
            displayMessage(message);
            System.out.println("\n\n");
//add code below for trust x.509 ceritficate
            XTrustProvider.install();
            SOAPConnection conn
                    = SOAPConnectionFactory.newInstance().createConnection();
            SOAPMessage response = conn.call(message,
                    "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx");
            System.out.println("RESPONSE:");
//Display Response Message
            displayMessage(response);

        } catch (SOAPException ex) {
            Logger.getLogger(CallRDWS.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }
}
