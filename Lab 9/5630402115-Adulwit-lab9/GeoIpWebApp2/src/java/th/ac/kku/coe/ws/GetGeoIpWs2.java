/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kku.coe.ws;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author Adul7
 */
@WebServlet(name = "GetGeoIpWs2", urlPatterns = {"/GetGeoIpWs2"})
public class GetGeoIpWs2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF­​8​");
        String ipAddr = request.getParameter("ip");
        PrintWriter out = response.getWriter();

        try {
// สร้าง MessageFactory เพ่ือเตรียมสร้าง SOAPMessage
            MessageFactory mFac = MessageFactory.newInstance();
// สร้าง SOAPMessage จาก MessageFactory
            SOAPMessage message = mFac.createMessage();
// ดึงสว่นของ SOAPBody จาก SOAPMessage
            SOAPBody body = message.getSOAPBody();
// สร้าง SOAPFactory เพ่ือเตรียมสร้างรายละเอียดของ SOAP
            SOAPFactory soapFactory = SOAPFactory.newInstance();
// สร้าง object Name (javax.xml.soap.Name)
// ท่ีมีlocal name คือ GetGeoIP มีprefix คือ ns และ
// มีnamespace คือ http://www.webservicex.net/
            String prefix = "ns";
            String namespace = "http://www.webservicex.net/";
            Name opName = soapFactory.createName("GetGeoIP", prefix, namespace);
// ใส่opName เขา้ไปใน SOAPBody
            SOAPBodyElement opElem = body.addBodyElement(opName);
// สร้าง SOAPElement ของ IpAddress
            SOAPElement ip = opElem.addChildElement(
                    soapFactory.createName("IPAddress", prefix, namespace));
            ip.addTextNode(ipAddr); // เพิม่ip ท่ีตอ้งการคน้หา
// เพิม่สว่นการกาํหนด SOAPAction
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "GetGeoIP");
// แสดงขอ้ความ SOAP Request
// displayMessage(message, out);
// สร้าง SOAPConnection เพ่ือเรียกใชเ้วบ็เซอร์วสิและรับคา่ผลลัพธด์ว้ย SOAPMessage
            SOAPConnection soapConn
                    = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
// แสดงขอ้ความ SOAP Response
            String ipValue = resp.getSOAPBody().getOwnerDocument()
                    .getElementsByTagName("IP").item(0).getFirstChild().getNodeValue();
            String countryValue = resp.getSOAPBody().getOwnerDocument()
                    .getElementsByTagName("CountryName").item(0).getFirstChild().getNodeValue();
            out.print("<h2>" + changeBlue(ipValue) + " is in " + changeBlue(countryValue) + "</h2>");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    String changeBlue(String s) {
        return "<font color='blue'>" + s + "</font>";
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
