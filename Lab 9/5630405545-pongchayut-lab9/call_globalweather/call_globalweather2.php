<?php

    //header("content-type:text/xml");
    $client = new SoapClient("http://webservicex.com/globalweather.asmx?WSDL");
    $parameters = array('CityName' => 'Khon Kaen', 'CountryName' => 'Thailand');
    $response = $client->__soapCall("GetWeather", array($parameters));
    
    $xml = $response->GetWeatherResult;
    $xml = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $xml);
    
    $xmlElement = simplexml_load_string($xml);

    echo "At ".'<font color="blue">'.$xmlElement->Location."</font>"."</br>";
    echo "on ".'<font color="blue">'.$xmlElement->Time."</font>"."</br>";
    echo "the temperature is " .'<font color="blue">'.$xmlElement->Temperature."</font>";
?>
