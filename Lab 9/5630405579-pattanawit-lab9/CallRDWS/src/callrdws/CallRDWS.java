package callrdws;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author Amilaz
 */
public class CallRDWS {

    public static void main(String[] args) throws SOAPException, Exception {
        SOAPFactory factory = SOAPFactory.newInstance();
        String url = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
        String prefix = "ns";
        
        Name nameService = factory.createName("ServiceTIN", prefix, url);
        Name nameUsername = factory.createName("username", prefix, url);
        Name namePassword = factory.createName("password", prefix, url);
        Name nameTIN = factory.createName("TIN", prefix, url);
        
        MessageFactory messageFac = MessageFactory.newInstance();
        SOAPMessage message = messageFac.createMessage();

        SOAPBody body = message.getSOAPBody();
        
        SOAPElement requestINPIN = body.addBodyElement(nameService);
        
        SOAPElement username = requestINPIN.addChildElement(nameUsername);
        username.addTextNode("anonymous");
        
        SOAPElement password = requestINPIN.addChildElement(namePassword);
        password.addTextNode("anonymous");
        
        SOAPElement tin = requestINPIN.addChildElement(nameTIN);
        tin.addTextNode("3570501221191");
        
        MimeHeaders mimeHeader = message.getMimeHeaders();
        mimeHeader.addHeader("SOAPAction", "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");
        message.saveChanges();
        
        //Display Request Message
        System.out.println("REQUEST:");
        displayMessage(message);
        System.out.println("\n\n");
        
        //add code below for trust x.509 ceritficate
        SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
        XTrustProvider.install();
        SOAPMessage response = conn.call(message,
                "https://rdws.rd.go.th/serviceRD3/checktinpinservice.asmx");
        
        System.out.println("RESPONSE:");
        //Display Response Message
        displayMessage(response);
    }

    public static void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }
}
