<?php
header("Content-type:text/xml");

$wsdl = "http://www.webservicex.net/globalweather.asmx?WSDL";
$methodName = 'GetWeather';
$param = array('CityName' => 'Khon Kaen','CountryName' => 'Thailand');
$soapAction = 'http://www.webserviceX.NET/GetWeather';

$client = new SoapClient($wsdl);
$result = $client->__soapCall($methodName, array('parameters'=>$param), array($soapAction));
$resultGetWeatherResult = $result->GetWeatherResult;

echo $resultGetWeatherResult;
