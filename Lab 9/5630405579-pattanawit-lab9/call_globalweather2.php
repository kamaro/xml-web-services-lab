<?php

header("Content-type:text/html");
$wsdl = "http://www.webservicex.net/globalweather.asmx?WSDL";
$methodName = 'GetWeather';
$param = array('CityName' => 'Khon Kaen', 'CountryName' => 'Thailand');
$soapAction = 'http://www.webserviceX.NET/GetWeather';

$client = new SoapClient($wsdl);
$result = $client->__soapCall($methodName, array('parameters' => $param), array($soapAction));
$resultGetWeatherResult = $result->GetWeatherResult;

$resultGetWeatherResult = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $resultGetWeatherResult);

$xml = new XMLReader();
$xml->XML($resultGetWeatherResult);

while ($xml->read()) {
    if ($xml->name == "Location") {
        $xml->read();
        echo 'At <font color="blue">' . $xml->value . '</font>' . "</br>";
        $xml->read();
    }
    if ($xml->name == "Time") {
        $xml->read();
        echo 'On <font color="blue">' . $xml->value . '</font>' . "</br>";
        $xml->read();
    }
    if ($xml->name == "Temperature") {
        $xml->read();
        echo 'the temperature is <font color="blue">' . $xml->value . '</font>' . "</br>";
        $xml->read();
    }
}
$xml->close();


