<?php
    $client = new SoapClient('http://webservicex.com/globalweather.asmx?WSDL');
    $methodCall = 'GetWeather';

    $params = array('CountryName' => 'Thailand',
                    'CityName' => 'Khon Kaen');
    $soapAction = 'http://www.webserviceX.NET/GetWeather';

    $Result = $client->__soapCall($methodCall, array('parameters' => $params));
    $xml = $Result->GetWeatherResult;
    $xml = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $xml);  
    $read = new XMLReader();
    $read -> XML($xml);
    while($read -> read()){
        $read->read();
        $read->read();
        if($read->name == "Location"){
            $read->read();
            echo "At "."<font color=\"blue\">$read->value</font>"."</br>";
        }
        if($read->name == "Time"){
            $read->read();
            echo "On "."<font color=\"blue\">$read->value</font>"."</br>";
        }
        if($read->name == "Temperature"){
            $read->read();
            echo "the temperature is "."<font color=\"blue\">$read->value</font>"."</br>";
        }
    }
    $read ->close();
?>
