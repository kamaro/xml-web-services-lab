/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kku.coe.ws;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
//import javax.xml.transform.Source;
//import javax.xml.transform.Transformer;
//import javax.xml.transform.TransformerException;
//import javax.xml.transform.TransformerFactory;
//import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author MM
 */
public class GetGeoIpWs2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String ipAddr = request.getParameter("ip");
        try (PrintWriter out = response.getWriter()) {
            // สร้าง MessageFactory เพื่อเตรียมสร้าง SOAPMessage
            MessageFactory mFac = MessageFactory.newInstance();
            // สร้าง SOAPMessage จาก MessageFactory
            SOAPMessage message = mFac.createMessage();
            // ดึงส่วนของ SOAPBody จาก SOAPMessage
            SOAPBody body = message.getSOAPBody();
            // สร้าง SOAPFactory เพื่อเตรียมสร้างรายละเอียดของ SOAP
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            // สร้าง object Name (javax.xml.soap.Name)
            // ที่มี local name คือ GetGeoIP มี prefix คือ ns และ
            // มี namespace คือ http://www.webservicex.net/
            String prefix = "ns";
            String namespace = "http://www.webservicex.net/";
            Name opName = soapFactory.createName("GetGeoIP", prefix, namespace);
            // ใส่ opName เข้าไปใน SOAPBody
            SOAPBodyElement opElem = body.addBodyElement(opName);
            // สร้าง SOAPElement ของ IpAddress
            SOAPElement ip = opElem.addChildElement(
                    soapFactory.createName("IPAddress", prefix, namespace));
            ip.addTextNode(ipAddr); // เพิ่ม ip ที่ต้องการค้นหา
            // เพิ่มส่วนการกำหนด SOAPAction
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "GetGeoIP");
            // แสดงข้อความ SOAP Request
//        	displayMessage(message, out);
            // สร้าง SOAPConnection เพื่อเรียกใช้เว็บเซอร์วิส และรับค่าผลลัพธ์ด้วย SOAPMessage
            SOAPConnection soapConn
                    = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
            // แสดงข้อความ SOAP Response
            displayMessage(resp, out);
        } catch (Exception ex) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void displayMessage(SOAPMessage resp, PrintWriter out) {
        try {
            //TransformerFactory tFac = TransformerFactory.newInstance();
            //Transformer transformer = tFac.newTransformer();
            NodeList node = resp.getSOAPBody().getOwnerDocument().getElementsByTagName("GetGeoIPResult");
            for (int i = 0; i < node.getLength(); i++) {
                Element element = (Element) node.item(i);
                out.println("<font color='blue'>" + element.getElementsByTagName("IP").item(0).getFirstChild().getTextContent() + "</font>" + " is in " + 
                          "<font color='blue'>" + element.getElementsByTagName("CountryName").item(0).getFirstChild().getTextContent() + "</font>");
            }
            /*Source src = resp.getSOAPPart().getContent();
            StreamResult result = new StreamResult(out);
            transformer.transform(src, result);*/
        } catch (SOAPException ex) {
        }
    }
}
