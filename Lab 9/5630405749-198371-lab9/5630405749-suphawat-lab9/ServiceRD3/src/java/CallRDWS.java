
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPHeader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MM
 */
public class CallRDWS {
    public static void main(String[] args) {
        CallRDWS mm = new CallRDWS();
        mm.call(args);
    }
    
    public void call(String[] args) {
        try {
            String url = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
            MessageFactory mFac = MessageFactory.newInstance();
            SOAPMessage message = mFac.createMessage();
            SOAPBody body = message.getSOAPBody();
            SOAPHeader header = message.getSOAPHeader();
            SOAPFactory factory = SOAPFactory.newInstance();
            header.detachNode();
            
            SOAPBodyElement checkTINPIN = body.addBodyElement(factory.createName("ServiceTIN", "ns", url));
            SOAPElement username = checkTINPIN.addChildElement(factory.createName("username", "ns", url));
            SOAPElement password = checkTINPIN.addChildElement(factory.createName("password", "ns", url));
            SOAPElement tin = checkTINPIN.addChildElement(factory.createName("TIN", "ns", url));
            
            username.addTextNode("anonymous");
            password.addTextNode("anonymous");
            tin.addTextNode("1409901180761");
            
            MimeHeaders mimeHeader = message.getMimeHeaders();
            mimeHeader.addHeader("SOAPAction", "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");
            message.saveChanges();
            
            System.out.println("REQUSET:");
            //Display Request Message
            displayMessage(message);
            
            System.out.println("\n\n");
            //add code below for trust x.509 certificate
            XTrustProvider.install();
            SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
            SOAPMessage response = conn.call(message, "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx");
            
            System.out.println("RESPONSE:");
            //Display Response Message
            displayMessage(response);
        } catch(Exception ex) {
            
        }
    }
    
    public void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }
}
