package th.ac.kku.coe.ws;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;

/**
 *
 * @author Yotravee
 */
@WebServlet(name = "GetGeoIpWs", urlPatterns = {"/GetGeoIpWs"})
public class GetGeoIpWs2 extends HttpServlet {

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String ipAddr = request.getParameter("ip");
        PrintWriter out = response.getWriter();
        try {
            
            MessageFactory mFac = MessageFactory.newInstance();
            
            SOAPMessage message = mFac.createMessage();
            
            SOAPBody body = message.getSOAPBody();
            
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            
            String prefix = "ns";
            String namespace = "http://www.webservicex.net/";
            Name opName = soapFactory.createName("GetGeoIP",
                    prefix, namespace);
            
            SOAPBodyElement opElem = body.addBodyElement(opName);
            
            SOAPElement ip = opElem.addChildElement(
                    soapFactory.createName("IPAddress", prefix, namespace));
            ip.addTextNode(ipAddr); 
            
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "GetGeoIP");
            
            SOAPConnection soapConn
                    = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
            
            String ipAddress = resp.getSOAPBody().getElementsByTagName("IP").item(0).getFirstChild().getNodeValue();
            String country = resp.getSOAPBody().getElementsByTagName("CountryName").item(0).getFirstChild().getNodeValue();
            
            out.print("<h2>");
            out.print("<font color='blue'>");
            out.print(ipAddress);
            out.print("</font>");
            out.print(" is in ");
            out.print("<font color='blue'>");
            out.print(country);
            out.print("</font>");
            out.print("</h2>");
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
