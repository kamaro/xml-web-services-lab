<?php
//choose format type
header("Content-type:text/xml");
//Choose WSDL
$client = new SoapClient("http://webservicex.com/globalweather.asmx?wsdl");
//call Function by using soapCall ("Function name",array of functiom(array of input(a,b)))
$data = $client->__soapCall("GetWeather", array(array('CityName' => 'Khon Kaen', 'CountryName' => 'Thailand')));
//get output from function
$out = $data->GetWeatherResult;
//print output
print_r($out);
/*use function without soapCall
$data = $client->GetWeather(array('CityName' => 'Khon Kaen', 'CountryName' => 'Thailand'));
print $data->GetWeatherResult ;*/
?>

