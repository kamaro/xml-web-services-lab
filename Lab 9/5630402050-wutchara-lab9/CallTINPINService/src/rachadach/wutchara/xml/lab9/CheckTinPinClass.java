package rachadach.wutchara.xml.lab9;


import java.io.PrintWriter;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author HAM
 */
public class CheckTinPinClass {
    
    private String prefix = "ns";
    private String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
    private String userName = "anonymous";
    private String password = "anonymous";
    private String tIN = "1559900260396";
    
    public static void main(String[] args) throws Exception {
        CheckTinPinClass c = new CheckTinPinClass();
        c.run();
    }

    public void run() throws Exception {
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            // สร้าง MessageFactory เพื่อเตรียมสร้าง SOAPMessage
            MessageFactory mFac = MessageFactory.newInstance();
            // สร้าง SOAPMessage จาก MessageFactory
            SOAPMessage message = mFac.createMessage();
            // ดึงส่วนของ SOAPBody จาก SOAPMessage
            SOAPBody body = message.getSOAPBody();
            // สร้าง SOAPFactory เพื่อเตรียมสร้างรายละเอียดของ SOAP
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            // สร้าง object Name (javax.xml.soap.Name)
            // ที่มี local name คือ GetGeoIP มี prefix คือ ns และ
            // มี namespace คือ http://www.webservicex.net/
            Name opName = soapFactory.createName("ServiceTIN",
                    prefix, namespace);
            // ใส่ opName เข้าไปใน SOAPBody
            SOAPBodyElement opElem = body.addBodyElement(opName);
            // สร้าง SOAPElement ของ IpAddress
            SOAPElement user = opElem.addChildElement(
                    soapFactory.createName("username", prefix, namespace));
            user.addTextNode(userName); // เพิ่ม ip ที่ต้องการค้นหา
            SOAPElement pass = opElem.addChildElement(
                    soapFactory.createName("password", prefix, namespace));
            pass.addTextNode(password); // เพิ่ม ip ที่ต้องการค้นหา
            SOAPElement tin = opElem.addChildElement(
                    soapFactory.createName("TIN", prefix, namespace));
            tin.addTextNode(tIN); // เพิ่ม ip ที่ต้องการค้นหา
            // เพิ่มส่วนการกำหนด SOAPAction
        /*
             Name nameServiceTIN = soapFactory.createName("ServiceTIN", prefix, namespace);
             Name nameUsername = soapFactory.createName("username", prefix, namespace);
             Name namePassword = soapFactory.createName("password", prefix, namespace);
             Name nameTIN = soapFactory.createName("TIN", prefix, namespace);
             SOAPElement checkTINPIN = body.addBodyElement(nameServiceTIN);
             SOAPElement username = checkTINPIN.addChildElement(nameUsername);
             SOAPElement password = checkTINPIN.addChildElement(namePassword);
             SOAPElement pin = checkTINPIN.addChildElement(nameTIN);
             username.addTextNode("anonymous");
             password.addTextNode("anonymous");
             pin.addTextNode("1559900260396");
             */

            MimeHeaders mimeHeader = message.getMimeHeaders();
            mimeHeader.addHeader("SOAPAction", namespace + "/ServiceTIN");
            // แสดงข้อความ SOAP Request
            message.saveChanges();
            /* Print the request message */
            System.out.print("REQUEST:\n");
            displayMessage(message);

            //displayMessage(message);
            System.out.println("\n\n");
            //add code below for trust x.509 ceritficate
            XTrustProvider.install();
            SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "https://rdws.rd.go.th/serviceRD3/checktinpinservice.asmx";
            SOAPMessage responses = soapConn.call(message, endpoint);
            System.out.println("RESPONSE:");
            //Display Response Message
            displayMessage(responses);

            soapConnection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }

}
