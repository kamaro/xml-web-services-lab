<?php

// The URL to POST to
$url = "http://webservicex.com/globalweather.asmx?op=GetWeather";

// The value for the SOAPAction: header
$action = "http://www.webserviceX.NET/GetWeather";

// Get the SOAP data into a string, I am using HEREDOC syntax
// but how you do this is irrelevant, the point is just get the
// body of the request into a string
$mySOAP = <<<EOD
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetWeather xmlns="http://www.webserviceX.NET">
      <CityName>string</CityName>
      <CountryName>string</CountryName>
    </GetWeather>
  </soap:Body>
</soap:Envelope>
EOD;

// The HTTP headers for the request (based on image above)
$headers = array(
    'Content-Type: application/xml; charset=utf-8',
    'Content-Length: ' . strlen($mySOAP),
    'SOAPAction: ' . $action
);
//--------------------------------------------------------\\
$client = new SoapClient("http://webservicex.com/globalweather.asmx?wsdl");

$params = array(
    "CityName" => "Khon Kaen",
    "CountryName" => "Thailand"
);
//input to SOAP
$response = $client->__soapCall('GetWeather', array($params));
/* @var $result type */
$result = $response->GetWeatherResult;
//echo $result;
$result = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $result);
$reader = new XMLReader();
$reader->XML($result);
while ($reader->read()) {
    $reader->read();
    $reader->read();
    if ($reader->name == "Location") {
        $reader->read();
        echo "At <font color=&quotblue&quot> " . $reader->value . "</font></br>";
    } else if ($reader->name == "Time") {
        $reader->read();
        echo "on <font color=&quotblue&quot>" . $reader->value . "</font></br>";
        $reader->read();
    } else if ($reader->name == "Temperature") {
        $reader->read();
        $reader->read();
        $reader->read();
        echo "the tamperature is <font color=&quotblue&quot>" . $reader->value . "</font></br>";
    }
}
$reader->close();
?>