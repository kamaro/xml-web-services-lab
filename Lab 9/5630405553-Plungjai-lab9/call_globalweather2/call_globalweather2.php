<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $wsdl = 'http://webservicex.com/globalweather.asmx?WSDL';

        $requestParams = array(
            'CityName' => 'Khon Kaen',
            'CountryName' => 'Thailand'
        );

        $client = new SoapClient('http://www.webservicex.net/globalweather.asmx?WSDL');
        $response = $client->GetWeather($requestParams);

        $xml = $response->GetWeatherResult;

        $xml = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $xml);
        $xmlDoc = simplexml_load_string($xml);

        echo "At <font color=\"blue\">" . $xmlDoc->Location . "</font></br>";
        echo "on <font color=\"blue\">" . $xmlDoc->Time . "</font>,</br>";
        echo "the temperature <font color=\"blue\">" . $xmlDoc->Temperature . "</font>";
        ?>
    </body>
</html>
