<?php 
	header("Content-type: text/html");
	$client = new SoapClient('http://www.webservicex.net/globalweather.asmx?WSDL');
	$params = array('CityName' => 'Khon Kaen','CountryName' => 'Thailand');
	$response = $client->__soapCall('GetWeather',array($params));
	$getW = $response->GetWeatherResult;
	$preg = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $getW);
	$str = simplexml_load_string($preg);
?>
<html>
    <body>
		<h2>
			<?php 	echo "At <font color='blue'>".$str->Location ."</font>"."<br />".
						"on <font color='blue'>".$str->Time ."</font>"."<br />".
						"the temperature is <font color='blue'>".$str->Temperature ."</font>";
			?>
		</h2>
    </body>
</html>