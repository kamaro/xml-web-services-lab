/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checktin;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author Narawit
 */
public class CheckTIN {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            MessageFactory mFac = MessageFactory.newInstance();

            SOAPMessage message = mFac.createMessage();
            
            SOAPHeader header = message.getSOAPHeader();
            SOAPBody body = message.getSOAPBody();
            header.detachNode();
            
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            String prefix = "ns";
            String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";


            SOAPBodyElement opElem = body.addBodyElement(soapFactory.createName("ServiceTIN", prefix, namespace));
            // สร้าง SOAPElement ของ IpAddress
            SOAPElement user = opElem.addChildElement(soapFactory.createName("username", prefix, namespace));
            user.addTextNode("anonymous");
            SOAPElement pass = opElem.addChildElement(soapFactory.createName("password", prefix, namespace));
            pass.addTextNode("anonymous");
            SOAPElement tin = opElem.addChildElement(soapFactory.createName("TIN", prefix, namespace));
            tin.addTextNode(args[0]);

            MimeHeaders hd = message.getMimeHeaders();        
            hd.addHeader("SOAPAction", "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");
            message.saveChanges();
            
            System.out.println("REQUEST:");
            displayMessage(message);
            System.out.println("\n\n");

            XTrustProvider.install();
            SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
          
            SOAPMessage response = soapConn.call(message,"https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx");
            System.out.println("RESPONSE:");

            displayMessage(response);

        } catch (Exception e) {

        }
    }

    public static void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }

}
