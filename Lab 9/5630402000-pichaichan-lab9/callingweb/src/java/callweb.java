
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author Pichaichan
 */
public class callweb {
     public static void main(String[] args) throws Exception {

        MessageFactory mFac = MessageFactory.newInstance();
        SOAPMessage message = mFac.createMessage();
        SOAPBody body = message.getSOAPBody();
        SOAPFactory soapFactory = SOAPFactory.newInstance();

        String prefix = "ns";
        String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
        Name opName = soapFactory.createName("ServiceTIN", prefix, namespace);

        SOAPBodyElement opElem = body.addBodyElement(opName);
        String username_s = "anonymous";
        SOAPElement username = opElem.addChildElement(soapFactory.createName("username", prefix, namespace));
        username.addTextNode(username_s);
        String password_s = "anonymous";
        SOAPElement password = opElem.addChildElement(soapFactory.createName("password", prefix, namespace));
        password.addTextNode(password_s);
         String tin_s = "1460300186481";
        SOAPElement tin = opElem.addChildElement(soapFactory.createName("TIN", prefix, namespace));
        tin.addTextNode(tin_s);
        
        MimeHeaders header = message.getMimeHeaders();
        header.addHeader("SOAPAction", "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");

        new callweb().displayMessage(message);
        System.out.println("\n\n");

        XTrustProvider.install();
        SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
        String endpoint = "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx";
        SOAPMessage response = conn.call(message,endpoint);
        System.out.println("RESPONSE:");

        new callweb().displayMessage(response);
    }
     public void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }
     
}
