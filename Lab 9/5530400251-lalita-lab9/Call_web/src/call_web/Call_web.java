/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package call_web;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Source;
import javax.xml.soap.MessageFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.MimeHeaders;
import com.sun.net.ssl.internal.ssl.*;
import java.security.Security;

/**
 *
 * @author Lalita
 */
public class Call_web {

    public void msgEnvelope(String[] args) throws Exception {

        SOAPConnectionFactory soapConnectionFactory
                = SOAPConnectionFactory.newInstance();
        SOAPConnection connection
                = soapConnectionFactory.createConnection();
        MessageFactory messageFactory
                = MessageFactory.newInstance();

        // Create a message
        SOAPMessage message
                = messageFactory.createMessage();

        // Get the SOAP header and body from the message
        // and remove the header
        SOAPHeader header = message.getSOAPHeader();
        SOAPBody body = message.getSOAPBody();
        header.detachNode();

        // Create a SOAP factory
        // Create a UDDI v2 checkPin body element
        SOAPFactory soapFactory
                = SOAPFactory.newInstance();
        SOAPBodyElement checkPin
                = body.addBodyElement(soapFactory.createName("ServiceTIN",
                                "ns", "https://rdws.rd.go.th/serviceRD3/checktinpinservice"));

        SOAPElement username
                = checkPin.addChildElement(
                        soapFactory.createName("username", "ns",
                                "https://rdws.rd.go.th/serviceRD3/checktinpinservice"));
        username.addTextNode("anonymous");

        SOAPElement password
                = checkPin.addChildElement(
                        soapFactory.createName("password", "ns",
                                "https://rdws.rd.go.th/serviceRD3/checktinpinservice"));
        password.addTextNode("anonymous");

        SOAPElement pin
                = checkPin.addChildElement(
                        soapFactory.createName("TIN", "ns",
                                "https://rdws.rd.go.th/serviceRD3/checktinpinservice"));
        pin.addTextNode("3670400595647");

        MimeHeaders hd = message.getMimeHeaders();
        hd.addHeader("SOAPAction",
                "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");

        message.saveChanges();
        System.out.println("REQUEST:");
        //Display Request Message
        displayMessage(message);

        System.out.println("\n\n");
        //add code below for trust x.509 ceritficate
        XTrustProvider.install();
        SOAPConnection conn
                = SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = conn.call(message,
                "https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx");

        System.out.println("RESPONSE:");
        //Display Response Message
        displayMessage(response);
    }

    public void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
transformer.setOutputProperty(OutputKeys.INDENT, "yes");
transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }

    public static void main(String[] args) throws Exception {
        Call_web clientApp = new Call_web();
        clientApp.msgEnvelope(args);
    }
}
