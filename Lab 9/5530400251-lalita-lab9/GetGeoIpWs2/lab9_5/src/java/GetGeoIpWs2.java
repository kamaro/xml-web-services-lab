/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
/**
 *
 * @author Lalita
 */
@WebServlet(urlPatterns = {"/GetGeoIpWs2"})
public class GetGeoIpWs2 {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String ipAddr = request.getParameter("ip");
        PrintWriter out = response.getWriter();

        try {
            MessageFactory mFac = MessageFactory.newInstance();
            SOAPMessage message = mFac.createMessage();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            String prefix = "ns";
            String namespace = "http://www.webservicex.net/";
            Name opName = soapFactory.createName("GetGeoIP", prefix, namespace);
            SOAPBodyElement opElem = body.addBodyElement(opName);
            SOAPElement ip = opElem.addChildElement(soapFactory.createName("IPAddress", prefix, namespace));
            ip.addTextNode(ipAddr); 
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "GetGeoIP");
            SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
            displayMessage(resp, out);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    private void displayMessage(SOAPMessage message, PrintWriter out) {
        try {
            TransformerFactory tFac = TransformerFactory.newInstance();
            Transformer transformer = tFac.newTransformer();
            
            Document document = message.getSOAPBody().getOwnerDocument();
            NodeList node = document.getElementsByTagName("GetGeoIPResult");
            for (int i = 0; i < node.getLength(); i++) {
                Element element = (Element) node.item(i);
                NodeList IP = element.getElementsByTagName("IP");
                NodeList CountyName = element.getElementsByTagName("CountryName");
                out.print("<font color='blue'>"+ IP.item(0).getFirstChild().getTextContent()+"</font>");
                out.print(" is in ");
                out.print("<font color='blue'>"+ CountyName.item(0).getFirstChild().getTextContent()+"</font>");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
