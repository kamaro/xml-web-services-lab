/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kku.coe.ws;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;


/**
 *
 * @author Bank
 */
@WebServlet(name = "GetGeoIpWs", urlPatterns = {"/GetGeoIpWs"})
public class GetGeoIpWs extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
          protected void processRequest(HttpServletRequest request,
    HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        String ipAddr = request.getParameter("ip");
        PrintWriter out = response.getWriter();
        try {
          
            MessageFactory mFac = MessageFactory.newInstance();
        
            SOAPMessage message = mFac.createMessage();
           
            SOAPBody body = message.getSOAPBody();
            // สร้าง SOAPFactory เพื่อเตรียมสร้างรายละเอียดของ SOAP
            SOAPFactory soapFactory = SOAPFactory.newInstance();
     
            String namespace = "http://www.webservicex.net/";
            String prefix = null;
            Name opName = soapFactory.createName("GetGeoIP",prefix, namespace);
            // ใส่ opName เข้าไปใน SOAPBody
            SOAPBodyElement opElem = body.addBodyElement(opName);
            // สร้าง SOAPElement ของ IpAddress
            SOAPElement ip = opElem.addChildElement(
                    soapFactory.createName("IPAddress", prefix, namespace));
      ip.addTextNode(ipAddr); // เพิ่ม ip ที่ต้องการค้นหา
            // เพิ่มส่วนการกำหนด SOAPAction
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "GetGeoIP");
 
            SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
           
            displayMessage(resp, out);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
}
          private void displayMessage(SOAPMessage message, PrintWriter out) {
        try {
            TransformerFactory tFac = TransformerFactory.newInstance();
            Transformer transformer = tFac.newTransformer();
            Source src = message.getSOAPPart().getContent();
            StreamResult result = new StreamResult(out);
            transformer.transform(src, result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
