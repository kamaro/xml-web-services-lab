/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author Bank
 */
public class Lab9 {

   
    public static void main(String[] args) throws SOAPException, Exception {
         try {
          
            MessageFactory mFac = MessageFactory.newInstance();
        
            SOAPMessage message = mFac.createMessage();
           
            SOAPBody body = message.getSOAPBody();
     
            SOAPFactory soapFactory = SOAPFactory.newInstance();
     
            String namespace = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
            String prefix = null;
            Name opName = soapFactory.createName("ServiceTIN",prefix, namespace);
        
            SOAPBodyElement opElem = body.addBodyElement(opName);
                SOAPElement user = opElem.addChildElement(soapFactory.createName("username",prefix, namespace));
                SOAPElement pass = opElem.addChildElement(soapFactory.createName("password",prefix, namespace));
                SOAPElement tin =  opElem.addChildElement(soapFactory.createName("TIN",prefix, namespace));
                user.addTextNode("anonymous");
                pass.addTextNode("anonymous");
                tin.addTextNode("1489900174008");
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "ServiceTIN");
            message.saveChanges();
        displayMessage(message);
        System.out.println("\n\n");

        XTrustProvider.install();
        SOAPConnection conn =SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = conn.call(message,"https://rdws.rd.go.th/ServiceRD3/CheckTINPINService.asmx");
        System.out.println("RESPONSE:");

        displayMessage(response);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
     
    }

     public static void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
transformer.setOutputProperty(OutputKeys.INDENT, "yes");    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }
    }
    

