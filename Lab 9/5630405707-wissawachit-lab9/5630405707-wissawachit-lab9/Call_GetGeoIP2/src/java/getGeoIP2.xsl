<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : October 21, 2015, 8:28 PM
    Author     : HAM
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                >
    <!-- prefix "k" is prefix of namespace in XML file -->
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <body>

                <xsl:value-of select="//GetGeoIPResult/IP"/>
                <xsl:text> is in </xsl:text>
                <xsl:value-of select="//GetGeoIPResult/CountryName"/>

            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>