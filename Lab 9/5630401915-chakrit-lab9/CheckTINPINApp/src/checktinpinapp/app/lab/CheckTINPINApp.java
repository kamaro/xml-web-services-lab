package checktinpinapp.app.lab;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Source;
import java.security.Security; 

/**
 *
 * @author Chakrit Tokuhara
 */
public class CheckTINPINApp {

    public static void main(String[] args) throws Exception {
        CheckTINPINApp app = new CheckTINPINApp();
        app.checkTINPIN();
    }

    public void checkTINPIN() throws Exception {
        try {
            //String url = "https://rdws.rd.go.th/ServiceRD/CheckTINPINService.asmx";
            String url = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection conn = soapConnectionFactory.createConnection();
            MessageFactory mFac = MessageFactory.newInstance();

            SOAPMessage message = mFac.createMessage();
            SOAPHeader header = message.getSOAPHeader();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory factory = SOAPFactory.newInstance();
            Name nameServiceTIN = factory.createName("ServiceTIN", "ns", url);
            Name nameUsername = factory.createName("username", "ns", url);
            Name namePassword = factory.createName("password", "ns", url);
            Name nameTIN = factory.createName("TIN", "ns", url);
            SOAPElement checkTINPIN = body.addBodyElement(nameServiceTIN);
            SOAPElement username = checkTINPIN.addChildElement(nameUsername);
            SOAPElement password = checkTINPIN.addChildElement(namePassword);
            SOAPElement pin = checkTINPIN.addChildElement(nameTIN);
            username.addTextNode("anonymous");
            password.addTextNode("anonymous");
            pin.addTextNode("1469900311072");
            MimeHeaders mimeHeader = message.getMimeHeaders();
            mimeHeader.addHeader("SOAPAction", "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");
            message.saveChanges();
            //Display Request Message
            System.out.println("REQUEST:");
            displayMessage(message);
            
            
            System.out.println("\n\n");
            //add code below for trust x.509 ceritficate
            XTrustProvider.install();
            SOAPConnection conn2 = SOAPConnectionFactory.newInstance().createConnection();
            SOAPMessage response = conn2.call(message, "https://rdws.rd.go.th/serviceRD3/checktinpinservice.asmx");
            System.out.println("RESPONSE:");
            //Display Response Message
            displayMessage(response);
        } catch (Exception ex) {

        }
    }

    public void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }
}
