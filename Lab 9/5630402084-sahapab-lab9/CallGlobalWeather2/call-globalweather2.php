<?php
    $params = array(
    'CityName' => 'Khon Kaen',
    'CountryName' => 'Thailand'
);

    $client = new SoapClient('http://webservicex.com/globalweather.asmx?WSDL');
    $response = $client->GetWeather($params);
    
    $xml = $response->GetWeatherResult;
    $xml = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $xml);
    $xmlDoc = simplexml_load_string($xml);

    echo "At ".'<font color="blue">'.$xmlDoc -> Location."</font>"."</br>";
    echo "on ".'<font color="blue">'.$xmlDoc->Time."</font>".","."</br>";
    echo "the temperature is ".'<font color="blue">'.$xmlDoc->Temperature."</font>";
?>
