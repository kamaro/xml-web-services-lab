package lab5;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DOMParser {

    public static void main(String argv[]) {
        try {
            File fXmlFile = new File("src/lab5/nation.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
            System.out.println("The root element name is " + doc.getDocumentElement().getNodeName());
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
