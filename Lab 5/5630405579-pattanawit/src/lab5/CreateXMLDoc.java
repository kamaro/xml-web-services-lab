package lab5;

import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class CreateXMLDoc {

    public static void main(String argv[]) {
        try {
            String xmlProfile = "src/lab5/myProfile.xml";
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            org.w3c.dom.Document doc = dBuilder.newDocument();

            Element eRoot = doc.createElement("profile");
            Element eName = doc.createElement("name");
            Text tName = doc.createTextNode("Pattanawit Wannoot");
            eName.appendChild(tName);
            eRoot.appendChild(eName);
            doc.appendChild(eRoot);

            OutputStream outfile = new FileOutputStream(xmlProfile);

            TransformerFactory file = TransformerFactory.newInstance();
            Transformer trans = file.newTransformer();
            trans.transform(new DOMSource(doc), new StreamResult(outfile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
