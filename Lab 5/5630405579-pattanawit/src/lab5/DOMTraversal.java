package lab5;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DOMTraversal {

    public static void main(String argv[]) {
        try {
            File fXmlFile = new File("src/lab5/nation.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
            Element root = doc.getDocumentElement();
            DOMTraversal dom = new DOMTraversal();
            dom.followNode(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void followNode(Node node) throws IOException {
        if(node != null){
            System.out.println("Node:type " + node.getNodeType() + " name = " + node.getNodeName()
                + " value = " + node.getNodeValue());
        }
        
        if (node.hasChildNodes()) {
                String name = node.getNodeName();
                int numChild = node.getChildNodes().getLength();
                System.out.println("node " + name + " has " + numChild
                        + " children");
                Node firstChild = node.getFirstChild();
                followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        
        if (nextNode != null) {
            followNode(nextNode);
        }
    }
}
