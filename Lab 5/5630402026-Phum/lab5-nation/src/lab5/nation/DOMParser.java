package lab5.nation;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class DOMParser {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        String filepath = "nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document xmlDoc = parser.parse(filepath);
        Element root = xmlDoc.getDocumentElement();
        System.out.println("The root element name is "+root.getNodeName());
    }
    
}
