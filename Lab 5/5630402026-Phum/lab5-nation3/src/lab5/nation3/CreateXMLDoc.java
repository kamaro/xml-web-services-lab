package lab5.nation3;

import java.io.*;
import java.io.IOException;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class CreateXMLDoc {

    public static void main(String[] args) throws SAXException, ParserConfigurationException, IOException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document doc = parser.newDocument();

        Element rootE = doc.createElement("profile");
        doc.appendChild(rootE);

        Element subE = doc.createElement("name");
        subE.appendChild(doc.createTextNode("Phum"));
        rootE.appendChild(subE);
        String xmlProfile = "MyProfile.xml";
        OutputStream os = new FileOutputStream("src/lab5/nation3/"+xmlProfile);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc), new StreamResult(os));

    }

}
