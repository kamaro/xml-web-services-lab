/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author YeeMine
 */
public class CreateXMLDoc {

    public static void main(String[] args) throws ParserConfigurationException, TransformerConfigurationException, TransformerException, FileNotFoundException {
        DocumentBuilderFactory factory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser
                = factory.newDocumentBuilder();
        Document doc = parser.newDocument();
        Element rootElement = doc.createElement("Profile");
        doc.appendChild(rootElement);
        Element child1 = doc.createElement("name");
        child1.appendChild(doc.createTextNode("Peerawat"));
        rootElement.appendChild(child1);
                    String xmlProfile = "src/lab5/MyProfile.xml";
        OutputStream os = new FileOutputStream(xmlProfile);
        TransformerFactory tf
                = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc),
                new StreamResult(os));

    }

}
