package domparser;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class CreateXMLDoc {

    private static String documentName;

    public static void main(String[] args) throws TransformerConfigurationException, TransformerException {
        try {
            String file = "myProfile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();

            //สร้างอิลิเมนต์
            Element root = document.createElement("profile");
            Element nameElement = document.createElement("name");
            Text nameText = document.createTextNode("Yotravee Sanit-in");
            nameElement.appendChild(nameText);
            root.appendChild(nameElement);
            document.appendChild(root);

            //แปลงไฟล์ DOM ไปเป็น ไฟล์ Xml
            OutputStream os = new FileOutputStream(file);
            TransformerFactory tfac = TransformerFactory.newInstance();
            Transformer t = tfac.newTransformer();
            t.transform(new DOMSource(document), new StreamResult(os));
        } catch (ParserConfigurationException | DOMException | FileNotFoundException | TransformerException e) {
            System.out.println(e);
        }
    }
}
