package domparser;

import java.io.IOException;
import javax.xml.parsers.*;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DOMParser {

    private static String documentName;

    public static void main(String[] args) {
        try {
            String file = "nation.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(file);
            System.out.println("The root element name is " + document.getDocumentElement().getNodeName());
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println(e);
        }
    }
}
