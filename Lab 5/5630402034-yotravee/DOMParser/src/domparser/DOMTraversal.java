package domparser;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class DOMTraversal {

    public static void main(String[] args) {
        try {
            String file = "nation.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(file);
            followNode(document.getDocumentElement());
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println(e);
        }
    }

    public static void followNode(Node node) throws IOException {
        wrietNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has "
                    + numChildren + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }

    private static void wrietNode(Node node) {
        System.out.println("Node:type = " + node.getNodeType() + " name = " + node.getNodeName() + " value = " + node.getNodeValue());
    }

}
