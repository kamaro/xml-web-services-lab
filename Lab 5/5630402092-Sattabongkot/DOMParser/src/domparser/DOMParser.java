package domparser;

import javax.xml.parsers.*;
import org.w3c.dom.*;


public class DOMParser {
    public static void main(String[] args) throws Exception  {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document xmlDoc = parser.parse("src/domparser/nation.xml");
        Element rootElement = xmlDoc.getDocumentElement();
        System.out.println("The root element name is " + rootElement.getNodeName());
    };
    
}
