package domparser;
import java.io.IOException;
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class DOMTraversal {

    public static void main(String[] args) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document xmlDoc = parser.parse("src/domparser/nation.xml");
        followNode(xmlDoc.getDocumentElement());
    }
    public static void followNode(Node node) throws IOException{
    if(node.hasChildNodes()) {
        short type = node.getNodeType(); 
        String name = node.getNodeName();
        int numChildren = node.getChildNodes().getLength();
        String value = node.getNodeValue();
        System.out.println("Node:type = " + type + " name = " + name + " value = " + value);
        System.out.println("node " + name + " has " + numChildren + " children");
        followNode(node.getFirstChild());
    }
    else {
        short type = node.getNodeType(); 
        String name = node.getNodeName();
        String value = node.getNodeValue();
        System.out.println("Node:type = " + type + " name = " + name + " value = " + value);
          
    }
     if(node.getNextSibling() != null) followNode(node.getNextSibling()); 
   } 
}

    
    
