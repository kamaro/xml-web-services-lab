package domparser;
import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

public class CreateXMLDoc {

    public static void main(String[] args) throws Exception {
        String xmlProfile = "myProfile.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element root = doc.createElement("profile");
        Element nameE = doc.createElement("name");
        Text nameT = doc.createTextNode("Sattabongkot Inunchote");

        nameE.appendChild(nameT);
        root.appendChild(nameE);
        doc.appendChild(root);

        OutputStream os = new FileOutputStream("src/domparser/" + xmlProfile);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc), new StreamResult(os));

    }

}
