/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 *
 * @author 2PYii
 */
public class CreateXMLDoc {

    public static void main(String[] args) {
        try {
            String xmlProfile = "myProfile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document Doc = builder.newDocument();

            Element root = Doc.createElement("profile");
            Element nameE = Doc.createElement("name");
            Text nameT = Doc.createTextNode("Thanaphat Yardsuntea");
            // Link relationships between created nodes
            nameE.appendChild(nameT);
            root.appendChild(nameE);
            Doc.appendChild(root);

            OutputStream os = new FileOutputStream("src/javaapplication1/"+xmlProfile);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(Doc),new StreamResult(os));
        } catch (Exception e) {
            e.printStackTrace(System.err);
        };

    }
}
