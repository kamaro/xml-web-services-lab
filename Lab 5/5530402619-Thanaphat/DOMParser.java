/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author 2PYii
 */
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class DOMParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String filePath = "src/javaapplication1/thai-nation.xml";
        // Create an instance of a DOM parser and
        // get the Document Object
        try{
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        // xmlDoc is the root node of DOM tree
        Document xmlDoc = parser.parse(filePath);
        // obtain the root element of the document
        Element rootElement = xmlDoc.getDocumentElement();
        
        String rootElementName = rootElement.getNodeName();
        System.out.println("The root element name is " + rootElementName);
        }
        catch(Exception e){
            e.printStackTrace(System.err); 
        };
        
    }

}
