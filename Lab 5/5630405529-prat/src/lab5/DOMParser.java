package lab5;
import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class DOMParser {

  public static void main(String args[]) {
      String filePath = "nation.xml";
      
    try {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dc = factory.newDocumentBuilder();
        Document document = dc.parse(new File(filePath));
        Node root = document.getDocumentElement();
        System.out.println("The root element is "+root.getNodeName());   
    }catch (Exception e) {
      System.err.println("Error");
    }

  }
}
