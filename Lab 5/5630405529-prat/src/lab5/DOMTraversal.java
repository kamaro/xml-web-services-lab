package lab5;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMTraversal {

    public static void main(String[] args) {
        String filePath = "nation.xml";
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dc = factory.newDocumentBuilder();
            Document document = dc.parse(new File(filePath));
            Node root = document.getDocumentElement();
            followNode(root);
        } catch (Exception e) {
            System.err.println("Error");
        }
    }

    public static void writeNode(Node node) {
        {
            System.out.println("Node:type = " + node.getNodeType()
                    + " name " + node.getNodeName() + " value = " + node.getNodeValue());
        }
    }

    public static void followNode(Node node) throws IOException {
        writeNode(node);
        if (node.hasChildNodes()) {
            System.out.println("Node " + node.getNodeName() + " has "
                    + node.getChildNodes().getLength() + " children");

            followNode(node.getFirstChild());
        }

        if (node.getNextSibling() != null) {
            followNode(node.getNextSibling());
        }
    }
}
