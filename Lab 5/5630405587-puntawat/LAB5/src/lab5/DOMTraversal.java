/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;
import java.io.*;
/**
 *
 * @author Admin
 */
public class DOMTraversal {
 public static void main(String[] args)  throws Exception {
        DocumentBuilderFactory BuilderF;
        BuilderF = DocumentBuilderFactory.newInstance();
        DocumentBuilder Builder = BuilderF.newDocumentBuilder();
        Document doge = Builder.parse("nation.xml");
        Element rootE;
        rootE = doge.getDocumentElement();
        followNode(rootE);
    
    }
    public static void followNode(Node node) throws IOException {
       if(node != null) {
            
            short nodeType = node.getNodeType();
            String name = node.getNodeName();
            String value = node.getNodeValue();
            System.out.println("Node:type = "+ nodeType + " name = " + name 
                                + " value = " + value);
            if(node.hasChildNodes()){
                int numChildren = node.getChildNodes().getLength();
                System.out.println("node " + name + " has " + numChildren +
                        " children");
                Node firstChild = node.getFirstChild();
                followNode(firstChild);
            }
            Node nextNode = node.getNextSibling();
            if(nextNode != null) followNode(nextNode);
        }
       
    }
    
}