/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

/**
 *
 * @author Admin
 */
public class CreateXMLDoc {

    public static void main(String[] args) throws ParserConfigurationException, FileNotFoundException, TransformerConfigurationException, TransformerException {
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
        Element root = doc.createElement("profile");
        Element nameE = doc.createElement("name");
        Element hobby = doc.createElement("hobby");
        Element game = doc.createElement("game");
        Text hobbyT = doc.createTextNode("Play Game");
        Text gameT = doc.createTextNode("CS:GO");
        Text nameT = doc.createTextNode("Puntawat Klongnawang");
        nameE.appendChild(nameT);
        hobby.appendChild(hobbyT);
        hobby.appendChild(game);
        game.appendChild(gameT);
        root.appendChild(nameE);
        root.appendChild(hobby);
        doc.appendChild(root);

        OutputStream os = new FileOutputStream("src/lab5/myProfile.xml");

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc), new StreamResult(os));
    }

}
