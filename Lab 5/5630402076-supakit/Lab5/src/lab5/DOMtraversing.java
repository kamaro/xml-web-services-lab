/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import org.w3c.dom.*;

/**
 *
 * @author thinkearth
 */
public class DOMtraversing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String Docname = "src/lab5/nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File(Docname));
            followNode(doc.getDocumentElement());
           
        } catch (Exception e) {
            System.err.println("ERR");
        }
    }

    public static void followNode(Node node) throws IOException {
        writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren
                    + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }

    public static void writeNode(Node node) {
        System.out.println("Node:Type = " + node.getNodeType() + " name = " + node.getNodeName() + " value = " + node.getNodeValue());
        //To change body of generated methods, choose Tools | Templates.
    }
}
