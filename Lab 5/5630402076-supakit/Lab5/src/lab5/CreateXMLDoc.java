/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import javax.xml.parsers.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

/**
 *
 * @author thinkearth
 */
public class CreateXMLDoc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            String xmlProfile = "myProfile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            Element root = doc.createElement("profile");
            Element NameE = doc.createElement("name");
            Text nameT = doc.createTextNode("Supakit  Siri-amnat");
            NameE.appendChild(nameT);
            root.appendChild(NameE);
            doc.appendChild(root);
            OutputStream os = new FileOutputStream("src/lab5/"+xmlProfile);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(doc),
                    new StreamResult(os));
        } catch (Exception e) {
            System.err.println("ERR");
        }
// TODO code application logic here
    }

}
