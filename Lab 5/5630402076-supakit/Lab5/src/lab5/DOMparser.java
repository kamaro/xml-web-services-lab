/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;
import javax.xml.parsers.*;
import java.io.File;
import org.w3c.dom.*;
/**
 *
 * @author thinkearth
 */
public class DOMparser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String Docname = "src/lab5/nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse( new File(Docname));
            Element rootElement = doc.getDocumentElement();
            String rootNodeName = rootElement.getNodeName();
            System.out.println("The root element name is " + rootNodeName);     
        }
        catch(Exception e){
            System.err.println("ERR");
        }
    }
    
}
