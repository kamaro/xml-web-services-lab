/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5_javaapplication;

import java.io.FileOutputStream;
import java.io.OutputStream;
import static java.util.stream.DoubleStream.builder;
import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 *
 * @author Lalita
 */
public class CreateXMLDoc {
       public static void main(String[] args) {
        try {
            String xmlProfile = "myProfile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document Doc = builder.newDocument();

            Element root = Doc.createElement("profile");
            Element nameE = Doc.createElement("name");
            Text nameT = Doc.createTextNode("Lalita Lokniyom");
            // Link relationships between created nodes
            nameE.appendChild(nameT);
            root.appendChild(nameE);
            Doc.appendChild(root);

            OutputStream os = new FileOutputStream("src/lab5_javaapplication/" + xmlProfile);
           
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(Doc),new StreamResult(os));
        } catch (Exception e) {
            e.printStackTrace(System.err);
        };

    }

}
