/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5_javaapplication;

import java.io.IOException;
import org.w3c.dom.Node;
import javax.xml.parsers.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Lalita
 */
public class DOMTraversal {

    public static void main(String[] args) {
        // note use of recursion
        String filePath = "src/lab5_javaapplication/thai-nation.xml";
        // Create an instance of a DOM parser and
        // get the Document Object
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            // xmlDoc is the root node of DOM tree
            Document xmlDoc = parser.parse(filePath);
            // obtain the root element of the document
            Element rootElement = xmlDoc.getDocumentElement();
            followNode(rootElement);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        };

    }

    public static void followNode(Node node) throws IOException {
        System.out.println("Node:type = " + node.getNodeType() + " name = " + node.getNodeName() + " value = " + node.getNodeValue());
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }
      // Read the entire document into memory
    //Node document = parser.parse(args[0]);
    // Start processing from the root node of DOM tree followNode(document);
}
