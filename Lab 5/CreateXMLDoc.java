package lab5;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class CreateXMLDoc {

    String filename;
    Document doc;

    public CreateXMLDoc(String filename) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.newDocument();

        } catch (Exception e) {
        }
        this.filename = filename;
    }

    public Element creElement(String str, Node node) {
        Element element = doc.createElement(str);
        node.appendChild(element);
        return element;
    }

    public Text creTextNode(String str, Node node) {
        Text text = doc.createTextNode(str);
        node.appendChild(text);
        return text;
    }

    public Attr creAttr(String str, Node node) {
        Attr attr = doc.createAttribute(str);
        node.appendChild(attr);
        return attr;
    }

    public void saved() {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filename));
            transformer.transform(source, result);
            System.out.println("Save complete");
        } catch (TransformerException tr) {
            System.out.println("Save incomplete");
        }
    }

    public static void main(String[] args) {
        CreateXMLDoc dt = new CreateXMLDoc("profile.xml");
        Element root = dt.creElement("profile", dt.doc);
        Element name = dt.creElement("name", root);
        dt.creTextNode("Narawit", name);
        dt.saved();
    }

}
