
import javax.xml.parsers.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

public class CreateXMLDoc {

    static String rootElement = "profile";
    static String nameElement = "name";
    static String nameText = "Panupong Puttarathamrongkul";
    static String pathNameOutput = "src/java/myProfile.xml";

    public static void main(String[] args) {
        DocumentBuilderFactory factory;
        factory = DocumentBuilderFactory.newInstance();
        createDocAndElement(factory);
    }

    public static void createDocAndElement(DocumentBuilderFactory factory) {
        DocumentBuilder parser;
        try {
            parser = factory.newDocumentBuilder();
            // Create a new Document node
            Document doc = parser.newDocument();
            // Create various nodes
            Element root = doc.createElement(rootElement);
            Element nameE = doc.createElement(nameElement);
            Text nameT = doc.createTextNode(nameText);
            // Link relationships between created nodes
            nameE.appendChild(nameT);
            root.appendChild(nameE);
            doc.appendChild(root);
            //writ File
            writeFile(doc);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    public static void writeFile(Document doc) throws FileNotFoundException,
            TransformerConfigurationException, TransformerException {
        OutputStream os = new FileOutputStream(pathNameOutput);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        StreamResult result = new StreamResult(os);
        trans.transform(new DOMSource(doc), result);
        if (result.getOutputStream() != null) {
            System.out.println("Success");
        }
    }
}
