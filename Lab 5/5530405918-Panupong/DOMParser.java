import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import org.w3c.dom.*;
public class DOMParser {
    public static void main(String[] args){
        try {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document xmlDoc = parser.parse("src/java/thai-nation.xml");
        Element rootElement = xmlDoc.getDocumentElement();
        
        String root = rootElement.getNodeName();
            System.out.println("The root element node is " + root);
        }catch(Exception e){
            e.printStackTrace(System.err);
        }
    }
    
}