import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import org.w3c.dom.*;
//3
public class DOMTraversal {
    static String path = "src/java/thai-nation.xml";
    static String rootStr;
    static Node rootNode;
    static Document xmlDoc;
    
    public static void main(String[] args) throws IOException{
        builderFactory();
        //printer("The root element node is " + rootStr);
        
        rootNode = (Node)xmlDoc.getDocumentElement();
        followNode(rootNode);
    }
    
    public static void builderFactory(){
        DocumentBuilderFactory factory;
        DocumentBuilder parser;
        Element rootElement;
        
        try {
            factory = DocumentBuilderFactory.newInstance();
            parser = factory.newDocumentBuilder();
            xmlDoc = parser.parse(path);
            rootElement = xmlDoc.getDocumentElement();
            rootStr = rootElement.getNodeName();
        }catch(Exception e){
            e.printStackTrace(System.err);
        }
    }
    
    public static void followNode(Node node) throws IOException {
	printer(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            printer("node " + name + " has " + numChildren + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }
    
    public static void printer(String str){
        System.out.println(str);
    }
    
    public static void printer(Node node){
        System.out.println("Node: type = "+node.getNodeType()+" name = "+ node.getNodeName()+" value = "+ node.getNodeValue());
    }
    
}
