package lab5;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DOMParser {
Document xmlDoc;
    public DOMParser(String filePath) {
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            xmlDoc = parser.parse(filePath);
        }catch(ParserConfigurationException | SAXException | IOException e){
        }
    }
    
    public String getrootname(){
        return xmlDoc.getDocumentElement().getNodeName(); 
    }
    
    public static void main(String[] args) {
        DOMParser DP =new DOMParser("nation.xml");
        System.out.println("The root element name is " + DP.getrootname());
    }
    
}
