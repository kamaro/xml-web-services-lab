package dom_parser;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


        
public class DOM_Parser {

    
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
        File filePath= new File("src/dom_parser/Nation.xml");
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
             DocumentBuilder parser = factory.newDocumentBuilder();
	Document xmlDoc;
        xmlDoc = parser.parse(filePath);
        xmlDoc.getDocumentElement().normalize();

	System.out.println("The root element name is " + xmlDoc.getDocumentElement().getNodeName());
        
    }
    
}
