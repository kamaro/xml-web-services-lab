/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dom_parser;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;


        
public class DOMTraversal {

    
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder parser = factory.newDocumentBuilder();
        Document xmlDoc = parser.parse("src/dom_parser/Nation.xml");
        Element rootNode = xmlDoc.getDocumentElement();
          followNode(rootNode);
        
    }
        public static void followNode(Node node)throws IOException{
            
            writeNode(node);
            if (node.hasChildNodes()){
                String name = node.getNodeName();
                int numChildren = node.getChildNodes().getLength();
                System.out.println("node "+ name + " has "+ numChildren +" children ");
                Node firstChild = node.getFirstChild();
                followNode(firstChild);
                
            }
        Node nextNode = node.getNextSibling();
        if(nextNode != null)
        followNode(nextNode);
        }

    private static void writeNode(Node node) {
        System.out.println(" Node:type= "+node.getNodeType()+" name= "+node.getNodeName()+" value= "+node.getNodeValue());
    }

    
}
