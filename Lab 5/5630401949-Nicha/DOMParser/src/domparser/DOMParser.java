/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domparser;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author Nicha
 */
public class DOMParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            String filepath = "nations.xml";
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(filepath);

            Element rootelement = doc.getDocumentElement();

            String rootname = rootelement.getNodeName();
            System.out.println("The root element name is " + rootname);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace(System.err);
        }

    }

}
