/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domparser;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author Nicha
 */
public class CreatXMLDoc {

    public static void main(String[] args) throws TransformerException {
        try {
            String xmlProfile = "myProfile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            // Create various nodes
            Element root = doc.createElement("profile");
            Element nameE = doc.createElement("name");
            Text nameT = doc.createTextNode("Nicha Wannapakdee");
            doc.appendChild(root);
            root.appendChild(nameE);
            nameE.appendChild(nameT);

            OutputStream os = new FileOutputStream("src/domparser/" + xmlProfile);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();

            trans.transform(new DOMSource(doc), new StreamResult(os));

        } catch (ParserConfigurationException | IOException e) {
            e.printStackTrace(System.err);
        }

    }

}
