/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tset3;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 *
 * @author MM
 */
public class Tset3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            DocumentBuilderFactory Factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder Parser = Factory.newDocumentBuilder();
            
            Document doc = Parser.newDocument();
            Element root = doc.createElement("profile");
            doc.appendChild(root);
            
            Element nameE = doc.createElement("name");
            root.appendChild(nameE);
            Text nameT = doc.createTextNode("Suphawat Jutrasong");
            nameE.appendChild(nameT);
            
            TransformerFactory transformer = TransformerFactory.newInstance();
            Transformer transform = transformer.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("C:\\Users\\MM\\Desktop\\test.xml"));
            
            transform.transform(source, result);         
        } catch (ParserConfigurationException | TransformerException e) {
            
        }
    }
    
}
