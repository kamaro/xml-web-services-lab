
package pkg5630402000.pichaichan.lab5;
import java.io.IOException;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
/**
 *
 * @author Pichaichan
 */
public class DOMParser {

     public static void main(String[] args) {
        String filePath = "nation.xml";   
        try{
            DocumentBuilderFactory nation =DocumentBuilderFactory.newInstance();
                 DocumentBuilder db = nation.newDocumentBuilder();
                 Document doc = db.parse(filePath);
                 
                  Element rootElement = doc.getDocumentElement();

                 String rootname = rootElement.getNodeName();
                 System.out.println("The root element name is " + rootname);
                 } catch(Exception e){
            e.printStackTrace();
        }
    }
    
}
