
package pkg5630402000.pichaichan.lab5;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DOMTraversal {
    public static void main(String[] args) {
        String filePath = "nation.xml";   
        try{
            DocumentBuilderFactory nation =DocumentBuilderFactory.newInstance();
                 DocumentBuilder db = nation.newDocumentBuilder();
                 Document doc = db.parse(filePath);
                 
                  Element rootElement = doc.getDocumentElement();

                 followNode(rootElement);
                 } catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public static void followNode(Node node) throws IOException {
        writeNode(node);      
            if(node.hasChildNodes()){ //ลูกโหนดทั้งหมด
              String name = node.getNodeName();   // ชื่อโหนด
                int numChildren = node.getChildNodes().getLength();
                System.out.println("node " + name + " has " + numChildren +
                        " children");
                Node firstChild = node.getFirstChild();
                followNode(firstChild);
            }
            Node nextNode = node.getNextSibling();//โหนดถัดไป
            if(nextNode != null) 
               followNode(nextNode);
    }

    private static void writeNode(Node node) {
        System.out.println("Node:type = " + node.getNodeType() //ชนิดข้อมูลโหนด
                +" name = " +node.getNodeName() //ชื่อโหนด
                + " value = " + node.getNodeValue()); //ค่าโหนด
    }
    
}
