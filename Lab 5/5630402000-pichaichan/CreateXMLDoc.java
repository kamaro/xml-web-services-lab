
package pkg5630402000.pichaichan.lab5;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;


public class CreateXMLDoc {
    public static void main(String[] args) {  
        try{
        String xmlProfile = "myProfile.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
        
        Element root = doc.createElement("profile");
        Element nameE = doc.createElement("name");
        Text nameT = doc.createTextNode("Pichaichan kanjanasorn");
        
        nameE.appendChild(nameT);
        root.appendChild(nameE);
        doc.appendChild(root);
        
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("src/pkg5630402000/pichaichan/lab5/myProfile.xml"));
        transformer.transform(source, result);
                 } catch(Exception e){
            e.printStackTrace();
        }
    }
}
