package domtraversal;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author Siribhorn
 */
public class DOMTraversal {

    private static Object parser;

    public static void main(String argv[]) {
        try {

            String filepath = "thai-nation.xml";
            DocumentBuilderFactory docFactory;
            docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // xmlDoc is the root node of DOM tree
            Document xmlDoc = docBuilder.parse(filepath);
            xmlDoc.getDocumentElement().normalize();
            
            // obtain the root element of the document
            Element rootElement = xmlDoc.getDocumentElement();
            System.out.println("The root element name is " + xmlDoc.getDocumentElement().getNodeName());
            
            followNode(rootElement);
        } catch (Exception pce) {
            pce.printStackTrace();
        }
    }

    public static void followNode(Node node) throws IOException {

        writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " children ");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }
    private static void writeNode(Node node) {
        System.out.println("Node: Type = " + node.getNodeType() +" Name = "+node.getNodeName() + " value = " + node.getNodeValue());
    }
}
