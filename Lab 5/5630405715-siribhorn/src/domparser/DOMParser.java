package domparser;

import static java.time.Clock.system;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 *
 * @author Siribhorn
 */
public class DOMParser {

    public static void main(String argv[]) {
        try {

            String filepath = "thai-nation.xml";
            DocumentBuilderFactory docFactory;
            docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            
            Document xmlDoc = docBuilder.parse(filepath);
            xmlDoc.getDocumentElement().normalize();
            
            Element rootElement = xmlDoc.getDocumentElement();
            System.out.println("The root element name is "+ xmlDoc.getDocumentElement().getNodeName());
        } 
        catch (Exception pce) {
            pce.printStackTrace();
        } 
        
    }
}
