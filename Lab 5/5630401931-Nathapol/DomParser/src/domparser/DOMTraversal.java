package domparser;

import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class DOMTraversal {

    public static void main(String[] args) {
        
        try {
            String filePath = "nations.xml";
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document document = docBuilder.parse(filePath);
            Traversal(document.getDocumentElement());
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println(System.err);
        }
    }

    public static void Traversal(Node node) {
        writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " children");
            Node firstChild = node.getFirstChild();
            Traversal(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if(nextNode != null){
            Traversal(nextNode);
        }
    }

    public static void writeNode(Node node) {
        System.out.println("Node:type = " + node.getNodeType() + " name = "
                + node.getNodeName() + " value = " + node.getNodeValue());
    }

}
