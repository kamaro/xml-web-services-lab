package domparser;

import java.io.File;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

public class CreateXMLDoc {

    public static void main(String[] args) {
        try {
            String xmlProfile = "src/domparser/profile.xml";
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document document = docBuilder.newDocument();

            Element rootElement = document.createElement("profile");
            document.appendChild(rootElement);
            
            Element nameE = document.createElement("name");
            rootElement.appendChild(nameE);
            
            Text nameT = document.createTextNode("Nathapol  Tanomsup");
            nameE.appendChild(nameT);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(xmlProfile));
            
            transformer.transform(source, result);
            
            System.out.println("File saved !");
            System.out.println("Congratulations Mac ^_^");
            System.out.println("You can do it bro.");
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace(System.err);
        } catch (TransformerException tfe){
            tfe.printStackTrace(System.err);
        }
    }
    
    
}
