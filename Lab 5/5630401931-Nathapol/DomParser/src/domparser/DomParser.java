
package domparser;

import java.io.IOException;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class DomParser {

    public static void main(String[] args) {
        
        try {
            String filePath = "nations.xml";
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(filePath);

            Element rootElement = doc.getDocumentElement();

            String rootname = rootElement.getNodeName();
            System.out.println("The root element name is " + rootname);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace(System.err);
        }
    }

}
