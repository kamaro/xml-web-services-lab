package dom;

import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class CreateXMLDoc {
    public static void main(String[] args){
        
        try {
            String xmlProfile = "src/dom/myProfile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();
            
            Element root = doc.createElement("profile");
            Element nameE = doc.createElement("name");
            Text nameT = doc.createTextNode("Adulwit Chinapas");
            
            nameE.appendChild(nameT);
            root.appendChild(nameE);
            doc.appendChild(root);
            
            OutputStream os = new FileOutputStream(xmlProfile);
            TransformerFactory tf= TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(doc), new StreamResult(os));
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
}
