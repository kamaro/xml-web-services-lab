package dom;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class DOMTraversal {
    public static void main(String[] argv){
        try {
            String fileNation = new String("nation.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(fileNation);
            followNode(doc.getDocumentElement());
            //System.out.println("The root element name is " + doc.getDocumentElement().getNodeName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void followNode(Node node )throws IOException {
        writeNode(node);
        if(node.hasChildNodes()){
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node "+name+" has "+numChildren + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if(nextNode != null)
            followNode(nextNode);
    }

    private static void writeNode(Node node) {
        int nodeType = node.getNodeType();
        String nodeName = node.getNodeName();
        String nodeValue = node.getNodeValue();
        System.out.println("Node:type = "+nodeType+" name " +nodeName+" value = "+nodeValue);
    }
}
