package lab5;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;
import java.io.*;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author crazynova
 */
public class DOMTraversal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  throws Exception {
        DocumentBuilderFactory factory;
        factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document xmlDoc = parser.parse("nation.xml");
        Element rootElement;
        rootElement = xmlDoc.getDocumentElement();
        followNode(rootElement);
    
    }
    public static void followNode(Node node) throws IOException {
        if(node != null) {
            short nodeType = node.getNodeType();
            String name = node.getNodeName();
            String value = node.getNodeValue();
            System.out.println("Node:type = "+ nodeType + " name = " + name 
                                + " value = " + value);
            if(node.hasChildNodes()){
                int numChildren = node.getChildNodes().getLength();
                System.out.println("node " + name + " has " + numChildren +
                        " children");
                Node firstChild = node.getFirstChild();
                followNode(firstChild);
            }
            Node nextNode = node.getNextSibling();
            if(nextNode != null) followNode(nextNode);
        }
        
    }
    
}
