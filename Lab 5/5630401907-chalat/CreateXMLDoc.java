package lab5;

import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author crazynova
 */
public class CreateXMLDoc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, FileNotFoundException, TransformerConfigurationException, TransformerException {
        String xmlProfile = "myProfile.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
        
        
        Element root = doc.createElement("profile");
        Element nameE = doc.createElement("name");
        Text nameT = doc.createTextNode("Chalat Chansima");
        nameE.appendChild(nameT);
        root.appendChild(nameE);
        doc.appendChild(root);
        
        OutputStream os = new FileOutputStream(xmlProfile);
        
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans =tf.newTransformer();
        trans.transform(new DOMSource(doc),new StreamResult(os));
    }
    
}
