package CreateXMLDoc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;


public class CreateXMLDoc {

    public static void main(String[] args) {
        try {
            String profile = "myprofile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();

            Element rootEle = document.createElement("myprofile");
            Element nameE = document.createElement("name");
            Text nameT = document.createTextNode("wissawachit namwongsa");

            nameE.appendChild(nameT);
            rootEle.appendChild(nameE);
            document.appendChild(rootEle);
            OutputStream os = new FileOutputStream(profile);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(document),
                    new StreamResult(os));

        } catch (ParserConfigurationException | DOMException | FileNotFoundException | TransformerException e) {

        }
    }
}