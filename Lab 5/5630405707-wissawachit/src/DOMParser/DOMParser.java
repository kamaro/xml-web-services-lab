package DOMParser;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class DOMParser {

    public static void main(String[] args) {
        try {
            String xml1 = "nation.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder built = factory.newDocumentBuilder();
            Document document = built.parse(xml1);

            Element rootElement = document.getDocumentElement();
            String name = rootElement.getNodeName();
            System.out.println("The root element name is " + name);
        } catch (ParserConfigurationException | SAXException | IOException e) {

        }

    }
}
