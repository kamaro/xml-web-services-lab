package lab5;
import org.w3c.dom.Node;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class DOMTraversal {
    public static void main(String[] args){
         try {
            
            String filepath = "nation.xml";
            DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = factory.newDocumentBuilder();
            Document doc = dBuilder.parse(filepath);
           // doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();
            followNode(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void followNode(Node node) throws IOException{
        writeNode(node);
        if(node.hasChildNodes()){
            String name = node.getNodeName();
        int numChildren = node.getChildNodes().getLength();
        System.out.println("node "+name+" has "+ numChildren+ " Children");
        Node firstChild = node.getFirstChild();
        followNode(firstChild);
        }
        
        Node nextNode = node.getNextSibling();
        if(nextNode != null)
        followNode(nextNode);
               
    }

    private static void writeNode(Node node) {
       System.out.println("Node:type = " + node.getNodeType()+" Name = " 
                          + node.getNodeName()+" value = " + node.getNodeValue());
    }
}
