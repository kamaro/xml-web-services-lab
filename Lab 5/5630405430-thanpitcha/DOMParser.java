package lab5;
import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
public class DOMParser {
    public static void main(String[] args) {      
        
        try {
            File filepath = new File("nation.xml");
            DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = factory.newDocumentBuilder();
            Document doc = dBuilder.parse(filepath);
            doc.getDocumentElement().normalize();
            System.out.print("The root element name is "+ doc.getDocumentElement().getNodeName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
}
