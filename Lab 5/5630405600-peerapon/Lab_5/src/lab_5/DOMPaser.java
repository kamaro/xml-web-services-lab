package lab_5;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.attribute.standard.DocumentName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DOMPaser {

    public static void main(String[] args) {
        String name = "src/lab_5/nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File(name));
            System.out.println("The root element name is " + doc.getDocumentElement().getTagName());
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(DOMPaser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
