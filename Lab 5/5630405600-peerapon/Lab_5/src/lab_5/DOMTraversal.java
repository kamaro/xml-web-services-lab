package lab_5;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.html.parser.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;

public class DOMTraversal {

    public static void main(String[] args) {
        String name = "src/lab_5/nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(name);
            followNode(doc.getDocumentElement());
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(DOMPaser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void followNode(Node node) throws IOException {
        writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " childen");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }

    private static void writeNode(Node node) {
       System.out.println("Node:type =  " + node.getNodeType() + " name = " + node.getNodeName() + " value = " + node.getNodeValue()); 
    }
}
