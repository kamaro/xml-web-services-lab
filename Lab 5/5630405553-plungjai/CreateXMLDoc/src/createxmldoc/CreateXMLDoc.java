package createxmldoc;

import java.io.File;
import java.io.FileOutputStream;
import javax.xml.parsers.*;
import java.io.IOException;
import java.io.OutputStream;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class CreateXMLDoc {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();

        Document doc = parser.newDocument();
        Element rootE = doc.createElement("profile");

        doc.appendChild(rootE);

        Element nameE = doc.createElement("name");

        nameE.appendChild(doc.createTextNode("Plungjai Kaewsiribundit"));

        rootE.appendChild(nameE);

        String myProfile = "myProfile.xml";

        OutputStream os = new FileOutputStream("src/createxmldoc/"+ myProfile);

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transf = tf.newTransformer();

        transf.transform(new DOMSource(doc), new StreamResult(os));

    }

}
