package domparser;

import javax.xml.parsers.*;
import java.io.IOException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class DOMParser {

    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
        String filepath = "src/domparser/nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();

        Document nationDoc = parser.parse(filepath);

        Element rootElement = nationDoc.getDocumentElement();
        String name = rootElement.getNodeName();

        System.out.println("The root element name is " + name);
    }

}
