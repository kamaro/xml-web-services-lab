package domtraversal;

import javax.xml.parsers.*;
import java.io.IOException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class DOMTraversal {

    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
        String filepath = "/src/domtraversal/nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();

        Document doc = parser.parse(filepath);

        Element node = doc.getDocumentElement();

        followNode(node);

    }

    public static void followNode(Node node) {
        writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }

    }

    public static void writeNode(Node node) {

        int type = node.getNodeType();
        String name = node.getNodeName();
        String value = node.getNodeValue();

        System.out.println("Node:type = " + type + " name = " + name + " value = " + value);

    }

}
