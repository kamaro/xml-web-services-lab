package rachadach.wutchara.lab5.dom;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * File : DOMTraversal.java
 * Lab 5
 * write by : wutchara rachadach
 * ID : 563040205-0
 * 
 */

public class DOMTraversal {
    public static void main(String args[]){

		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder parser = dbf.newDocumentBuilder();
			Document doc = parser.parse("src\\rachadach\\wutchara\\lab5\\dom\\" + new File("nation.xml"));

			Element elements = doc.getDocumentElement();
			followNode(elements);

		} catch (ParserConfigurationException | IOException | SAXException pce) {
					System.err.println("---- ERROR ----");
			}
        
    }
    
    public static void followNode(Node node){
        writeNode(node);
        if(node.hasChildNodes()){
            String name = node.getNodeName();
            int numChildern = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildern + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if(nextNode != null)
            followNode(nextNode);
    }
    
    private static void writeNode(Node node) {
		if (node != null) {
			System.out.println("Node:type = " + node.getNodeType() + " name = " 
				+  node.getNodeName() + " value = " + node.getNodeValue());
		} 
    }
}
