package rachadach.wutchara.lab5.dom;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 * File : createXMLDoc.java
 * Lab 5
 * write by : wutchara rachadach
 * ID : 563040205-0
 * 
 */

public class CreateXMLDoc {
    public static void main(String args[]) throws TransformerException{
        try{
            String xmlProfile = "myProfile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();
            
            //create various nodes
            Element root = doc.createElement("profile");
            Element nameE = doc.createElement("name");
            Text nameT = doc.createTextNode("Wutchara  Rachadach");
            
            //add Nodde
            nameE.appendChild(nameT); 
            root.appendChild(nameE); 
            doc.appendChild(root); 
            
            //write file
            Source source = new DOMSource(doc);
              File file = new File("src\\rachadach\\wutchara\\lab5\\dom\\" + new File(xmlProfile));
                
                Result result = new StreamResult(file);
                
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer xformer = transformerFactory.newTransformer();

		xformer.transform(source, result);
                
        }catch (ParserConfigurationException pce) {
		System.err.println("---- ERROR ----");
        }
    }
    
}
