package rachadach.wutchara.lab5.dom;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * File : DOMParser.java
 * Lab 5
 * write by : wutchara rachadach
 * ID : 563040205-0
 * 
 */

public class DOMParser {

    public static void main(String args[]) {
        
        Document doc = null;
        
	  try {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder parser = dbf.newDocumentBuilder();
        doc = parser.parse("src\\rachadach\\wutchara\\lab5\\dom\\" + new File("nation.xml"));   
				
        Element rootNode = doc.getDocumentElement();
		System.out.println("The root element name is " + rootNode.getNodeName());

	  } catch (ParserConfigurationException | SAXException | IOException pce) {
		System.err.println("---- ERROR ----");
	  }
	}
    
}
