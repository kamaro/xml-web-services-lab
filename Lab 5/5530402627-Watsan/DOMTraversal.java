import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMTraversal {

    public static void main(String[] args) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document xmlDoc = parser.parse("src/java/nation.xml");
            //Node root = (Node)xmlDoc.getDocumentElement();
            followNode2((Node)xmlDoc.getDocumentElement());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void followNode2(Node node) {
        if(node.getNodeType() == Node.ELEMENT_NODE){
            short type = node.getNodeType();
            String name = node.getNodeName();
            String value = node.getNodeValue();
            int numChildren = node.getChildNodes().getLength();
            
            System.out.println("Node:type = " + type + " name = " + name + " value = " + value);
            System.out.println("node " + name + " has " + numChildren + " children");
            
            NodeList nodelist = node.getChildNodes();
            for(int i = 0; i < nodelist.getLength(); i++){
                Node childNode = nodelist.item(i);
                if(i == 0){
                    System.out.println("Node:type = " + childNode.getNodeType() + " name = " + childNode.getNodeName() + " value = " + childNode.getNodeValue() + "\n" );
                }
                if (childNode.hasChildNodes()) {
                    followNode2(childNode);
                }
            }
        }
    }
}