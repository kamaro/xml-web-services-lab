
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Watsan
 */
public class CreateXMLDoc {
    public static void main(String[] args){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();
            
            Element root = doc.createElement("profile");
            Element nameE = doc.createElement("name");
            Text nameT = doc.createTextNode("Watsan Homsin");
            
            nameE.appendChild(nameT); 
            root.appendChild(nameE); 
            doc.appendChild(root); 
            
            OutputStream os = new FileOutputStream("src/java/myProfile.xml"); 
            TransformerFactory tf = TransformerFactory.newInstance(); 
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(doc), new StreamResult(os)); 
            
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
