
import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import org.w3c.dom.*;

public class DOMParser {

    public static void main(String[] args) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder parser = factory.newDocumentBuilder();

            // xmlDoc is the root node of DOM tree 
            Document xmlDoc = parser.parse("src/java/nation.xml");
            
            // obtain the root element of the document  
            Element rootElement = xmlDoc.getDocumentElement();

            String root = rootElement.getNodeName();
            System.out.println("The root element name is " + root);
        } catch (Exception e) {

        }
    }
}
