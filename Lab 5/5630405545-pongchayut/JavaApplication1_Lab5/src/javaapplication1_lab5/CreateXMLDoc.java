package javaapplication1_lab5;

/**
 *
 * @author Miki
 */
import java.io.FileOutputStream;
import org.w3c.dom.*;
import java.io.IOException;
import java.io.OutputStream;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.xml.sax.SAXException;

public class CreateXMLDoc {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException {
        String xmlProfile = "src/javaapplication1_lab5/myProfile.xml";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        // Create various nodes
        Element root = doc.createElement("profile");
        Element nameE = doc.createElement("name");
        Text nameT = doc.createTextNode("Pongchayut Kongraksakiat");

        //link relation of element
        nameE.appendChild(nameT);
        root.appendChild(nameE);
        doc.appendChild(root);

        // Create XML file
        OutputStream os = new FileOutputStream(xmlProfile);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc),
                new StreamResult(os));
    }

}
