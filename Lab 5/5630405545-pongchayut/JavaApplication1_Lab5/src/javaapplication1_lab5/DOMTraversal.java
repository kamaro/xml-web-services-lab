package javaapplication1_lab5;

/**
 *
 * @author Miki
 */
import org.w3c.dom.*;
import java.io.IOException;
import javax.xml.parsers.*;
import org.xml.sax.SAXException;

public class DOMTraversal {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        String filePath = "src/javaapplication1_lab5/nation.xml";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();

        Document xmlDoc = parser.parse(filePath);

        Element node = xmlDoc.getDocumentElement();
        followNode(node);

    }

    public static void followNode(Node node) throws IOException {
        writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " children");

            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }

        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }

    }

    private static void writeNode(Node node) {
        // Create value for getNode
        short nodeType = node.getNodeType();
        String nodeName = node.getNodeName();
        String nodeValue = node.getNodeValue();

        System.out.println("Node:Type " + nodeType + " name = " + nodeName + " value = " + nodeValue);

    }
}
