package javaapplication1_lab5;

/**
 *
 * @author Miki
 */
import java.io.IOException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;

public class DOMParser {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        String filePath = "src/javaapplication1_lab5/nation.xml";
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder(); 
        
        Document xmlDoc = parser.parse(filePath); 

        Element rootElement = xmlDoc.getDocumentElement(); 
        
        String name = rootElement.getNodeName();
        System.out.println("the root Element name is " + name);
    }
    
}
