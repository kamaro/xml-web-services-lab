package Lab5;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class DOMTraversal {
	public static void main(String args[]) {
		
		try {
			File fXmlFile = new File("nation.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		
			followNode(doc);
			
		} catch (Exception e) {
			e.printStackTrace();
	    }
		
	}

	@SuppressWarnings("static-access")
	public static void followNode(Node node) throws IOException {
		if (node.getNodeType() != node.DOCUMENT_NODE) {
			writeNode(node);
		}
		
		if (node.hasChildNodes()) {
			String name = node.getNodeName();
			int numChildren = node.getChildNodes().getLength();
			if (node.getNodeType() != node.DOCUMENT_NODE) {
				System.out.println("node " + name + " has " + numChildren + " children");
			}
			
			Node firstChild = node.getFirstChild();
			followNode(firstChild);
		}
		Node nextNode = node.getNextSibling();
		if (nextNode != null) {
			followNode(nextNode);
		}
	}

	private static void writeNode(Node node) {
            int num = node.getNodeType();
		System.out.print("Node:type = " + num);	
		System.out.println(" name = " + node.getNodeName() + " value = " + node.getNodeValue());
	}
}
