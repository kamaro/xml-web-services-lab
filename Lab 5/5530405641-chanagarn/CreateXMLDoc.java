
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import static java.util.stream.DoubleStream.builder;
import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

public class CreateXMLDoc {

    public static void main(String[] arg) throws TransformerException, FileNotFoundException, ParserConfigurationException {
        // Create a new Document node
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();

        //parser = factory.newDocumentBuilder();
        Document doc = parser.newDocument();

        // Create various nodes
        Element root = doc.createElement("profile");
        Element nameE = doc.createElement("name");
        Text nameT = doc.createTextNode("Chanagarn Duangpim");

        // Link relationships between created nodes
        nameE.appendChild(nameT);
        root.appendChild(nameE);
        doc.appendChild(root);
        OutputStream os = new FileOutputStream("src/java/chanagarn.xml");
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc), new StreamResult(os));

    }
}
