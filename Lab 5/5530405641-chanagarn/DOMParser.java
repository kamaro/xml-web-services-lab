
import javax.xml.parsers.*;
import java.io.File;
import java.io.InputStream;
import org.w3c.dom.*;

public class DOMParser {

    public static void main(String[] arg) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = null;
            parser = factory.newDocumentBuilder();

            Document xmlDoc = parser.parse("src/java/thai-nation.xml");

            // obtain the root element of the document
            Element rootElement = xmlDoc.getDocumentElement();

            String rot = rootElement.getNodeName();
            System.out.println("The root element name is " + rot);
        } catch (Exception e) {
            e.printStackTrace(System.err);

        }
    }
}
