
import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.w3c.dom.*;

public class DOMTraversal {

    public static void main(String[] args) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = null;
            parser = factory.newDocumentBuilder();

            Document xmlDoc = parser.parse("src/java/thai-nation.xml");

            // obtain the root element of the document
            Element rootElement = xmlDoc.getDocumentElement();

            String rot = rootElement.getNodeName();
            System.out.println("The root element name is " + rot);

            Node nnode = (Node) xmlDoc.getDocumentElement();
            followNode(nnode);

        } catch (Exception e) {
            e.printStackTrace(System.err);

        }
    }

    // note use of recursion
    public static void followNode(Node node) throws IOException {
        printer(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }

        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }

    // Read the entire document into memory
    //Node document = parser.parse(args[0]);
    // Start processing from the root node of DOM tree
    //followNode(document);
    public static void printer(Node nnde) {
        System.out.println("Node:Type = " + nnde.getNodeType() + " name = " + nnde.getNodeName() + " value = " + nnde.getNodeValue());
    }
}
