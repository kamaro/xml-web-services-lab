/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab05;

/**
 *
 * @author user
 */
import javax.xml.parsers.*;
import java.io.File;
import java.io.FileOutputStream;
import org.w3c.dom.*;
import java.io.IOException;
import java.io.OutputStream;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class CreateXMLDoc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            String xmlLocation = "myProfile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder
                    = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            Element root = doc.createElement("profile");
            Element nameE = doc.createElement("name");
            Text nameT = doc.createTextNode("Nevin Ruttanaboonta");
            nameE.appendChild(nameT);
            root.appendChild(nameE);
            doc.appendChild(root);

            OutputStream os = new FileOutputStream("src/Lab05/" + xmlLocation);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(doc),
                    new StreamResult(os));
        } catch (Exception e) {
            System.err.println("error");
        }
    }

}
