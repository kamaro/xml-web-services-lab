/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Lab05;
import javax.xml.parsers.*;
import java.io.File;
import org.w3c.dom.*;

/**
 *
 * @author user
 */
public class DOMParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String xmlLocation = "src/Lab05/thai-nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder
                    = factory.newDocumentBuilder();
            Document doc = builder.parse(new File(xmlLocation));
            Element rootElement = doc.getDocumentElement();
            String rootNodeName = rootElement.getNodeName();
            System.out.println("the root element name is : " + rootNodeName);
        } catch (Exception e) {
            System.err.println("error");
        }
        // TODO code application logic here
    }

}
