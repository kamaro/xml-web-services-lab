/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab05;



/**
 *
 * @author user
 */
import javax.xml.parsers.*;
import java.io.File;
import org.w3c.dom.*;
import java.io.IOException;

public class DOMTraversal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String xmlLocation = "src/Lab05/thai-nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder
                    = factory.newDocumentBuilder();
            Document doc = builder.parse(new File(xmlLocation));
            Element rootElement = doc.getDocumentElement();
            followNode(rootElement);

        } catch (Exception e) {
            System.err.println("error");
        }
        // TODO code application logic here
    }

    public static void followNode(Node node) throws IOException {
        writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " childen.");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }

    public static void writeNode(Node node) {
        System.out.println("Node:type = " + node.getNodeType() + " name = " + node.getNodeName() + " value = " + node.getNodeValue());
    }

}
