/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;

/**
 *
 * @author VJunsone
 */
public class DOMTraversal {
    public static void main(String[] args) throws Exception {
        DocumentBuilderFactory DBF = DocumentBuilderFactory.newInstance();
        DocumentBuilder DB = DBF.newDocumentBuilder();
        Document D = DB.parse("nations.xml");
        Element rootElement = D.getDocumentElement();
        CalculateNode(rootElement);       
    }
    private static void CalculateNode(Node N) {
        if(N != null) {
            short type = N.getNodeType();
            String name = N.getNodeName();
            String Value = N.getNodeValue();
            OutputText(type,name,Value);
            if (N.hasChildNodes()){
                int ChildNum = N.getChildNodes().getLength();
                OutputChild(ChildNum,name);
                Node Childnode = N.getFirstChild();
                CalculateNode(Childnode);
            }
            Node next = N.getNextSibling();
            if(next != null) {
                CalculateNode(next);
            }
                    
        }
    }

    private static void OutputText(short type,String name,String Value) {
        System.out.println("Node:type = " + type + " name = " + name + " value = " + Value);
        
    }

    private static void OutputChild(int Cn,String name) {
        System.out.println("node " + name + " has " + Cn + " children");
        
    }

    
    
}
