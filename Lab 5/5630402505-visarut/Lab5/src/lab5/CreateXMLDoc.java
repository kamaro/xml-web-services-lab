/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

/**
 *
 * @author VJunsone
 */
public class CreateXMLDoc {

    public static void main(String[] args) throws Exception {
        String xml = "Myprofile.xml";
        DocumentBuilderFactory DBF = DocumentBuilderFactory.newInstance();
        DocumentBuilder DB = DBF.newDocumentBuilder();
        Document D = DB.newDocument();

        Element rootnode = D.createElement("profile");
        Element name = D.createElement("name");
        rootnode.appendChild(name);
        Text Tname = D.createTextNode("Visarut Junsone");
        name.appendChild(Tname);
        D.appendChild(rootnode);
        JAVAtoXMLConverter(xml,D);
        
        

    }

    private static void JAVAtoXMLConverter(String filename,Document D) throws Exception {
        OutputStream Output = new FileOutputStream("src/lab5/" + filename);
        TransformerFactory TranformFac = TransformerFactory.newInstance();
        Transformer Tranform = TranformFac.newTransformer();
        Tranform.transform(new DOMSource(D), new StreamResult(Output));
    }
}
