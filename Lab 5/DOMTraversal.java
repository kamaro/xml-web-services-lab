package lab5;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class DOMTraversal {

   Node root;
    public DOMTraversal(String filePath) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document xmlDoc = parser.parse(filePath);
            root = (Node)xmlDoc.getDocumentElement(); 
        } catch (ParserConfigurationException | SAXException | IOException e) {
        }
    }

    public void traversing(Node node) {
        if (node.hasChildNodes()) {
            short type = node.getNodeType();
            String name = node.getNodeName();
            String value = node.getNodeValue();
            int numChildren = node.getChildNodes().getLength();
            
            System.out.println("Node:Type = " + type 
                    + " Node:Name = " + name 
                    + " Node:Value = " + value);
            
            System.out.println("node " + name + " has " + numChildren + " children");
       
            Node firstChild = node.getFirstChild();
            System.out.println("Node:Type = " + firstChild.getNodeType() 
                    + " Node:Name = " + firstChild.getNodeName() 
                    + " Node:Value = " + firstChild.getNodeValue() );
            traversing(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            traversing(nextNode);
        }
    }
    
    public static void main(String[] args) {
        DOMTraversal dt = new DOMTraversal("nation.xml");
        dt.traversing(dt.root);
    }

}
