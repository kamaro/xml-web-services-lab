
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author golfyzzz
 */
public class Traversal {

 public static void main(String[] args){
    try{
        String xml1= "src/lab5/nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder built = factory.newDocumentBuilder();
        Document document = built.parse(xml1);
            
        Element rootEle = document.getDocumentElement();
        followNode(rootEle);
     
        }
        catch(Exception e){
    
    }
}
   
public static void followNode(Node node)throws IOException {
        writeNode(node);
        if(node.hasChildNodes()){
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node" + name + " has " + numChildren + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null)
            followNode(nextNode);
    }

    private static void writeNode(Node node) {
         //To change body of generated methods, choose Tools | Templates.
        System.out.println("Node:type = " + node.getNodeType() + " name = " + node.getNodeName() + " value = " + node.getNodeValue());
    }
}