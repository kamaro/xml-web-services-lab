package lab5;


import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

public class XMLdocument {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         try{
        String profile = "src/lab5/profile.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
            
        Element rootEle = document.createElement("Profile");
        Element nameE = document.createElement("Name");
        Text nameT = document.createTextNode("raveerote");
        
        nameE.appendChild(nameT);
        rootEle.appendChild(nameE);
        document.appendChild(rootEle);
        OutputStream os = new FileOutputStream (profile);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(document),
        new StreamResult(os));
     
        }
        catch(Exception e){
    
        }
    }
}