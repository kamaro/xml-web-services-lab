import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.net.*;
import java.io.*;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
/**
 *
 * @author Jittichai
 */
@WebServlet(urlPatterns = {"/CallingIKKU"})
public class CallingIKKU extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String link = "https://www.kku.ac.th/ikku/api/activities/services/topActivity.php";
        URL url = new URL(link);
        InputStream inpS = url.openStream();
        JsonReader rdr = Json.createReader(inpS);
        JsonObject jObj = rdr.readObject();
        JsonArray results = jObj.getJsonArray("activities");
        
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CallingIKKU</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Calling iKKU Web Service at /CallWebServices" + "</h1>");

            for (JsonObject result : results.getValuesAs(JsonObject.class)) {
                String newsTitle = result.getString("title","");
                String newsDate = result.getString("dateSt","");
                String newsURL = result.getString("url","");
                out.println("<p>" + newsDate + "  " + "<a href=" + newsURL + ">" + newsTitle + "</a></p>");
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

     /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}