/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServletFeed;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Amilaz
 */
@WebServlet(name = "CallingIKKU", urlPatterns = {"/CallingIKKU"})
public class CallingIKKU extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String ikkuURL = "https://www.kku.ac.th/ikku/api/activities/services/topActivity.php";
            URL url = new URL(ikkuURL);
            InputStream input = url.openStream();
            JsonReader jsReader = Json.createReader(input);
            JsonObject jsObj = jsReader.readObject();
            JsonArray activityArray = jsObj.getJsonArray("activities");
            /* TODO output your page here. You may use following sample code. */
            display(out, request,activityArray);
        }
    }

    private void display(PrintWriter out, HttpServletRequest request,JsonArray resultsArray) {
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet CallingIKKU</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Servlet CallingIKKU Web Service at " + request.getContextPath() + "</h1>");
        
         for (JsonObject result : resultsArray.getValuesAs(JsonObject.class)) {
                out.println("<p>" + result.getString("dateSt","") + "  " + "<a href=" + result.getString("url","") + ">" + 
                        result.getString("title","") + "</a></p>");
            }
        
        out.println("</body>");
        out.println("</html>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
