/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServletFeed;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;

/**
 *
 * @author Amilaz
 */
@WebServlet(name = "CallPTTWS", urlPatterns = {"/CallPTTWS"})
public class CallPTTWS extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            try {
                SOAPFactory factory = SOAPFactory.newInstance();

                String url = "http://www.pttplc.com/ptt_webservice/";
                String prefix = "ns";
                Name serviceName = factory.createName("CurrentOilPrice", prefix, url);
                Name languageName = factory.createName("Language", prefix, url);

                MessageFactory messageFac = MessageFactory.newInstance();
                SOAPMessage message = messageFac.createMessage();

                SOAPBody body = message.getSOAPBody();

                SOAPElement requestCOP = body.addBodyElement(serviceName);

                SOAPElement username = requestCOP.addChildElement(languageName);
                username.addTextNode("ENG");

                MimeHeaders mimeHeader = message.getMimeHeaders();
                mimeHeader.addHeader("SOAPAction", "http://www.pttplc.com/ptt_webservice/CurrentOilPrice");
                message.saveChanges();

                SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
                SOAPMessage soapResponse = conn.call(message,
                        "http://www.pttplc.com/webservice/pttinfo.asmx");

                dispaly(out, request, soapResponse);
            } catch (Exception ex) {

            }
            /* TODO output your page here. You may use following sample code. */

        }
    }

    public void dispaly(PrintWriter out, HttpServletRequest request, SOAPMessage soapResponse) throws SOAPException {

        String result = soapResponse.getSOAPBody().getElementsByTagName("CurrentOilPriceResult").item(0).getFirstChild().getNodeValue();

        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet CallPTTWS</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Servlet CallPTTWS Web Service at " + request.getContextPath() + "</h1>");
        out.println("<p>" + result + "</p>");
        out.println("</body>");
        out.println("</html>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
