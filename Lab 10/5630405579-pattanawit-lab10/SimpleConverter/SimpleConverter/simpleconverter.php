<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    </head>
    <body>
        <h1>Simple Convertor Web Application</h1>
        1 <input type="text" value="THB" id="fromC" name="fromC"  size="3"/> 
        = <input type="text" name="money" id="output"  size="8" disabled ="disabled "/>
        <input type="text" value="USD" id="toC" name="toC" size="3"/>
        <input type="submit" name="btnconv" value="Convert" id="conv" />
    </body>
    <script>
        $("#conv").click(function () {
            var fromC = document.getElementById('fromC').value;
            var toC = document.getElementById('toC').value;
            var FormData = "fromC="+fromC+"&toC="+toC;
            $.ajax(
                    {
                        dataType: "json",
                        url: "call_currencyconvertor2.php" ,
                        type: "GET",
                        data: FormData,
                        success: function (data)
                        {
                            $("input#output").val(data['rate']);
                        }
                    });
        });
    </script>
</html>              
