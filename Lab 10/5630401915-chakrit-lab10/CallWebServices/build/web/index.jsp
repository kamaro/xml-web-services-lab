<%-- 
    Document   : index
    Created on : Nov 11, 2015, 1:31:40 PM
    Author     : Chakrit Tokuhara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Call Web Service Using Java</title>
    </head>
    <body>
        <h2>Lab 10:Calling and Building Web Services Using Java</h2>
        <ul type="disc">
            <li><a href="CallingFeed">Calling Feed</a></li>
            <li><a href="CallingPTTWS">Calling PTT Web Service</a></li>
            <li><a href="CallingIKKU">Calling iKKU</a></li>
            <li><a href="CallingGoogleMap.html">Calling Google Map</a></li>
            <li><a href="BuildingJSON">Building JSON Data</a></li>
        </ul>
    </body>
</html>
