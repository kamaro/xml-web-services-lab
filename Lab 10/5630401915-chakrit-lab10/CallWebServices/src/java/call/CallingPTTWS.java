/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package call;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author Maewdamn
 */
public class CallingPTTWS extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<h1>Servlet Calling PTT Web Service at " + request.getContextPath() + "</h1>");
            
            String serverURL = "http://www.pttplc.com/ptt_webservice/";
            //SOAPConnectionFactory factory = SOAPConnectionFactory.newInstance();
            //OAPConnection soapConn = factory.createConnection();
            
            MessageFactory mesFac = MessageFactory.newInstance();
            SOAPMessage message = mesFac.createMessage();
            SOAPHeader header = message.getSOAPHeader();
            //SOAPPart part = message.getSOAPPart();
            //SOAPEnvelope envelope = part.getEnvelope();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory soapFac = SOAPFactory.newInstance();
            Name bodyName = soapFac.createName("CurrentOilPrice", "ns", serverURL);
            Name name = soapFac.createName("Language", "ns", serverURL);
            SOAPBodyElement bodyElement = body.addBodyElement(bodyName);
            SOAPElement result = bodyElement.addChildElement(name);
            result.addTextNode("EN");
            MimeHeaders mimeHeader = message.getMimeHeaders();
            mimeHeader.addHeader("SOAPAction", "http://www.pttplc.com/ptt_webservice/CurrentOilPrice");
            message.saveChanges();
            
            //Name bodyName = soapFac.createName("CurrentOilPrice");
            //bodyElement.addNamespaceDeclaration("",serverURL);
            //Name name = soapFac.createName("Language");
            //SOAPMessage answer = soapConn.call(message, serverURL);
            //soapConn.close();
            //out.println(answer);
            
            //ByteArrayOutputStream bt = new ByteArrayOutputStream();
            //message.writeTo(bt);
            //String requestMessage = new String(bt.toByteArray());
            //out.println(requestMessage);
            
            
            //XTrustProvider.install();
            
            String serverURL2 = "http://www.pttplc.com/webservice/pttinfo.asmx";
            SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
            SOAPMessage res = conn.call(message, serverURL2);
            //ByteArrayOutputStream bt2 = new ByteArrayOutputStream();
            //res.writeTo(bt2);
            //String responseMessage = new String(bt2.toByteArray());
            //out.println(responseMessage);
            //String finalString = displayMessage(res);
            String finalString = res.getSOAPBody()
                .getElementsByTagName("CurrentOilPriceResult").item(0).getFirstChild().getNodeValue();
            //out.println("<p>" + finalString + "</p>");
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CallingPTTWS</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CallingPTTWS at " + request.getContextPath() + "</h1>");
            out.println("<p>" + finalString + "</p>");
            out.println("</body>");
            out.println("</html>");
            
        } catch (SOAPException ex) {
            
        } catch (Exception ex) {
            
        }
    }
    
    /*protected String displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Source src = message.getSOAPPart().getContent();
        StringWriter outWriter = new StringWriter();
        StreamResult result = new StreamResult(outWriter);
        transformer.transform(src, result);
        StringBuffer sb = outWriter.getBuffer();
        String finalString = sb.toString();
        
        return finalString;
    }*/
    
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
