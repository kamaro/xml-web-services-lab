/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package call;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maewdamn
 */
public class CallingGoogleMap extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String locationURL = request.getParameter("location");
        URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?address=" 
                + locationURL + "&sensor=false&language=en");
        
        try (PrintWriter out = response.getWriter()) {
            //URLConnection connection = url.openConnection();
            InputStream is = url.openStream();
            JsonReader rdr = Json.createReader(is);
            JsonObject object = (JsonObject) rdr.readObject();
            
            JsonArray jObject1 = (JsonArray) object.get("results");
            JsonObject jObject2 = (JsonObject) jObject1.get(0);
            JsonObject jObject3 = (JsonObject) jObject2.get("geometry");
            JsonObject location = (JsonObject) jObject3.get("location");

            String maps = "http://maps.google.com/?q=" + location.get("lat") + "," + location.get("lng");
            
            out.println("Redirecting to " + maps);
            out.println("<meta http-equiv=\"refresh\" content=\"0; URL=" + maps + "\" />");

            /*while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }

            in.close();
            
            String jsonText = sb.toString();
            JSONObject json = new JSONObject(jsonText);
            JSONArray arr = json.getJSONArray("activities");
            for (int i=0; i<arr.length(); i++) {
                 JSONArray arr2 = arr.getJSONArray(0);
                 out.println("URL = "+ url);
            }
            
            /*JSONArray arr3 = arr2.getJSONArray("geometry");
            JSONObject arr4 = arr3.getJSONObject(0);
            JSONArray arr5 = arr4.getJSONArray("location");
            JSONObject arr6 = arr5.getJSONObject(0);
            String lat = arr6.getString("lat");
            String lng = arr6.getString("lng");
            
            out.println("URL = "+ url);
            out.println("Location URL ="+ locationURL);
            out.println("Lat = "+ lat);
            out.println("Lng = "+ lng);*/
            
            
            /*InputStream in = connection.getInputStream();
            ObjectMapper mapper = new ObjectMapper();
            
            GoogleMap gm = (GoogleMap)mapper.readValue(in, GoogleMap.class);
            in.close();
            if (gm.getStatus() == "OK") {
                for (Results rs : gm.getResults()) {
                    String lat = rs.getGeometry().getLocation().getFirst();
                    String lon = rs.getGeometry().getLocation().getSecond();
                    out.println("Test Google Map</br>");
                    out.println("Lat = "+ lat);
                    out.println("Lng = "+ lon);
                }
            }
            
            
            
            //BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            //StringBuilder sb = new StringBuilder();
            //String inputLine;
            
            //while ((inputLine = bf.readLine()) != null) {
            //    sb.append(inputLine);
            //}
            
            
            //out.println("Test Google Map</br>");
            //out.println("Lat = "+ lat);
            //out.println("Lng = "+ lon);
            /*bf.close();
            
            
            String jsonText = sb.toString();
            JSONObject ob = new JSONObject(jsonText);
            JSONArray arr = new JSONArray(ob.get("results"));
            
            JSONObject arr_or = (JSONObject) arr.get(0);
            JSONObject arr_geo = (JSONObject) arr_or.get("geometry");
            JSONObject arr_lo = (JSONObject) arr_geo.get("location");
            String lat = arr_lo.getString("lat");
            String lon = arr_lo.getString("lon");*/
            
            
            //out.println(lat + "</br>");
            //out.println(lon);
            
            //out.println("Lat = "+ arr_lo.get("lat"));
            //out.println("Lng = "+ arr_lo.get("lng"));
            
            
            /*for (int i=0; i<arr.length(); i++) {
                JSONObject temp = arr.getJSONObject(i);
                Geometry geo = new Geometry();
                JSONObject locationObject = temp.getJSONObject("location");
                Location location = new Location();
                location.setFirst(locationObject.getString("first"));
                location.setSecond(locationObject.getString("second"));
                geo.setLocation(location);// setting the cuisine
                list.add(geo);
            }
            
            for(int i=0;i<list.size();i++){
                Geometry geo = list.get(0);
                String first = Geometry.getLocation().getFirst();
                String second = Geometry.getLocation().getSecond();
                out.println(First "+first+"Second "+second);
            }/*
            
            
            
            //out.println(arr.getJSONArray(0).toString());
            
            //JSONArray arr_address = arr.getJSONArray(0);
            //JSONArray arr_geo = arr_address.getJSONArray(2);
            //JSONArray arr_lo = arr_geo.getJSONArray(0);
            //String lati = arr_lo.getJSONObject(0).getString("lat");
            //String longti = arr_lo.getJSONObject(0).getString("lng");
            
            //out.println(lati + "</br>");
            //out.println(longti);*/
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /*private static class GoogleMap {
        private Results[] results;
        private String status;
        public Results[] getResults() {
            return results;
        }
        
        public void setResults(Results[] results) {
            this.results = results;
            
        }
        
        public String getStatus() {
            return status;
        }
    }
    
    private static class Results {
        private Geometry geometry;
        public Geometry getGeometry() {
            return geometry;
        }
        
        public void setGeometry(Geometry geometry) {
            this.geometry = geometry;
        }
    }
    
    private static class Geometry {
        private Location location;
        public Location getCuisines() {
            return location;
        }
        
        public void setLocation(Location location) {
            this.location = location;
        }
        
        public Location getLocation() {
            return location;
        }
    }

    private static class Location {
        private String first;
        private String second;
        public String getFirst() {
            return first;
        }
        public void setFirst(String first) {
            this.first = first;
        }
        public String getSecond() {
            return second;
        }
        public void setSecond(String second) {
            this.second = second;
        }
    }*/
}
