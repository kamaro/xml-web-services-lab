/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plungjai.kaewsiribundit.lab10;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author PJ
 */
@WebServlet(name = "CallingFeed", urlPatterns = {"/CallingFeed"})
public class CallingFeed extends HttpServlet {

    String title = "", link = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<h2>Servlet CallingFeed at " + request.getContextPath() + "</h2>");
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document doc = parser.parse("http://www.dailynews.co.th/rss/rss.xml");
            displayMessage(doc, out);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
        } finally {
            out.close();
        }
    }

    private void displayMessage(Document xmlDoc, PrintWriter out) {
        try {
            Element rootNode = xmlDoc.getDocumentElement();
            followNode(rootNode, out);

        } catch (Exception ex) {
        }
    }

    public void followNode(Node node, PrintWriter out) {
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            
            switch (name) {
                case "title":
                    if (true) {
                        title = node.getFirstChild().getNodeValue();
                    }
                    break;
                case "link":
                    if (true) {
                        link = node.getFirstChild().getNodeValue();
                    }
                    break;
            }
            
            if (!title.equals("") && !link.equals("")) {
                out.println("<li><a href = " + link + " target=\"_blank\">" + title + "</a></li>");
                title = "";
                link = "";
            }
            Node firstChild = node.getFirstChild();
            followNode(firstChild, out);
        }
        org.w3c.dom.Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode, out);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
