<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Call Web Service</title>
    </head>
    <body>
        <h2>Lab 10:Calling and Building Web Service Using Java</h2>

        <ul>
            <li>
                <a onclick="jsfunction()" href="CallingFeed">Calling Feed</a>
            </li>
            <li>
                <a onclick="jsfunction()" href="CallingPTTWS">Calling PTT Web Service</a>
            </li>
            <li>
                <a onclick="jsfunction()" href="CallingIKKU">Caling iKKU</a>
            </li>
            <li>
                <a onclick="jsfunction()" href="CallingGoogleMap.html">Calling Google Map</a>
            </li>
            <li>
                <a onclick="jsfunction()" href="BuildingJSON">Building JSON Data</a>
            </li>
        </ul>
    </body>
</html>
