<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

header('Content-Type: application/json; charset=utf-8');
$soapclient = new SoapClient('http://webservicex.net/CurrencyConvertor.asmx?WSDL');
$method = 'ConversionRate';
$params = array('FromCurrency' => $_GET["fromC"], 'ToCurrency' => $_GET["toC"]);
$soapAction = 'http://www.webserviceX.NET/ConversionRate';
$result = $soapclient->__soapCall($method, array('parameters' => $params), array('soapaction' => $soapAction));
echo json_encode(array(
    "FromCurrency" => 'THB',
    "ToCurrency" => 'USD',
    "rate" => $result->ConversionRateResult
        )
);

