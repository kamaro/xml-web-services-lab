/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;

/**
 *
 * @author MM
 */
public class BuildingJSON extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws org.json.JSONException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        String petition[] = {"พร ริรุณ", "สุนทราภรณ์"};
        String meloday[] = {"เอื้อ สุนทรสนาน", "สุนทราภรณ์"};
        String title[] = {"มาร์ช ม.ข.", "เสียงสนครวญ"};
        String content[] = {"มหาวิทยาลัยขอนแก่นเกริกไกรวิทยา", "เสียงยอดสนโอนโอนผิ้วโยนอ่อนไหว"};
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            /*JSONArray list = new JSONArray();
            
            JSONObject obj_author = new JSONObject();
            JSONObject obj_title = new JSONObject();
            JSONObject obj_content = new JSONObject();
            
            int i = 0;
            while(petition[i] != null || meloday[i] != null || title[i] != null || content[i] != null) {
                obj_author.put("petition", petition[i]);
                obj_author.put("meloday", meloday[i]);
                obj_title.put("title", title[i]);
                obj_content.put("content", content[i]);
                
                list.put(obj_author);
                list.put(obj_title);
                list.put(obj_content);
                i++;
            } */
            
            JsonArrayBuilder builder = Json.createArrayBuilder();
            int i = 0;
            while(petition[i] != null || meloday[i] != null || title[i] != null || content[i] != null) {
                builder.add(Json.createObjectBuilder()
                            .add("- author", Json.createObjectBuilder()
                                            .add("petition", petition[i])
                                            .add("meloday", meloday[i])
                            )
                            .add("title", title[i])
                            .add("content", content[i]));
                JsonArray arr = builder.build();
                out.println(arr);
                i++;
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(BuildingJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(BuildingJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
