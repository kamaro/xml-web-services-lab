<html>
	<head>
		<title>Simple Converter Web Application</title>
		<meta name="Description" content="Simple converter Web Application Call Google Finance"
		<link rel="stylesheet" type="text/css" href="/finance/s/OVd9g2P4lGg/styles/finance_us.css">
	</head>
	<body style="margin-left">
		<?php
			echo "<h1>Simple Converter Web Application</h1>";
		?>
		<div class="g-doc">
			<form action="/finance/converter" method="get" name="f">
				<div class="left">
					<input name="a" maxlength="12" size="5" autocomplete="off" value="1">
					<select name="from" value>
						<option value="THB">Thai Baht (THB)</option>
						<option value="USD">US Dollar ($)</option>
					</select>
					=
					<select name="to" value>
						<option value="USD">US Dollar ($)</option>
						<option value="THB">Thai Baht (THB)</option>
					</select>
					<button id="submit" type="submit">Convert</button>
				</div>
			</form>
		</div>				
	</body>
</html>