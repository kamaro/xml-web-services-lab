<?php
	header('Content­Type: text/xml; charset=utf­8');
	
	// specify WSDL Web service that we want to call
	$wsdl = 'http://webservicex.net/CurrencyConvertor.asmx?WSDL';
	
	// create SoapClient object
	$client = new SoapClient($wsdl);
	
	// initialize method name
	$methodName = 'ConversionRate';

	$fromC = filter_input(INPUT_GET, 'fromC');
	$toC = filter_input(INPUT_GET, 'toC');
	
	// initialize method parameters
	$params = array('FromCurrency' => $fromC,
   					 'ToCurrency' => $toC);
					 
	// initialize soapAction
	$soapAction = 'http://www.webserviceX.NET/ConversionRate';

	// calling WS operation using soapCall
	$objectResult = $client->__soapCall($methodName, 
	array('parameters' => $params), array('soapaction' => $soapAction));
	
	// save the response
	$response = $objectResult->ConversionRateResult;
    
	// display response
	echo $response;
?>
   




