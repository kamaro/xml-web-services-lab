/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singkhon.anuchit;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;

/**
 *
 * @author winashi
 */
public class CallingPTTWS extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SOAPException, Exception {
        response.setContentType("text/html;charset=UTF-8");

        MessageFactory mFac = MessageFactory.newInstance();
        SOAPMessage message = mFac.createMessage();
        SOAPBody body = message.getSOAPBody();
        SOAPFactory soapFactory = SOAPFactory.newInstance();

        String prefix = "ns";
        String namespace = "http://www.pttplc.com/ptt_webservice/";
        Name opName = soapFactory.createName("CurrentOilPrice", prefix, namespace);

        SOAPBodyElement opElem = body.addBodyElement(opName);

        SOAPElement language = opElem.addChildElement(
                soapFactory.createName("Language", prefix, namespace));
        language.addTextNode("TH");

        MimeHeaders header = message.getMimeHeaders();
        header.addHeader("SOAPAction", "http://www.pttplc.com/ptt_webservice/CurrentOilPrice");

        SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
        String endpoint = "http://www.pttplc.com/webservice/pttinfo.asmx";
        SOAPMessage response2 = conn.call(message, endpoint);
        String currentOilPrice = response2.getSOAPBody()
                .getElementsByTagName("CurrentOilPriceResult").item(0).getFirstChild().getNodeValue();

        try (PrintWriter out = response.getWriter()) {

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CallingPTTWS</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CallingPTTWS at " + request.getContextPath() + "</h1>");
            out.println("<p>" + currentOilPrice + "</p>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
