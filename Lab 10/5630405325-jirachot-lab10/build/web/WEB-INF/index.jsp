
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calling Feed</title>
    </head>
    <body>
        <h1>Lab 10:Calling and Building Web Services Using Java </h1>
        <ul>
            <li> <a href="http://localhost:8080/5630405325-jirachot-lab10/CallingFeed"><font size='4'>Calling a Feed Using Java Servlet </font><br/></a></li>
            <li> <a href="http://localhost:8080/5630405325-jirachot-lab10/CallingPTTWS"><font size='4'>Calling  a SOAP Web Service Using Java Servlet</font><br/></a></li>
            <li> <a href="http://localhost:8080/5630405325-jirachot-lab10/CalingIKKU"><font size='4'>Calling a JSON Web Service Using Java Servlet</font><br/></a></li>
            <li> <a href="CallingGoogleMap.html"><font size='4'>Calling Google Map Web Service Using HTML and Java Servlet</font><br/></a></li>
            <li> <a href="http://localhost:8080/5630405325-jirachot-lab10/BuildingJSON"><font size='4'>Building JSON Data Using Java Servlet</font><br/></a></li>
        </ul>
    </body>
</html>
