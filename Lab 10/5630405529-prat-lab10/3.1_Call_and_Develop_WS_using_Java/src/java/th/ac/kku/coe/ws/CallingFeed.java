package th.ac.kku.coe.ws;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



public class CallingFeed extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {
         URL u = new URL("http://www.dailynews.co.th/rss/rss.xml");
         DocumentBuilderFactory factory =
         DocumentBuilderFactory.newInstance();
          DocumentBuilder parser = factory.newDocumentBuilder();
          Document xmlDoc = parser.parse(u.openStream());
          NodeList nodeItem =  xmlDoc.getElementsByTagName("item");
          int num = nodeItem.getLength();
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>CallingFeed</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CallingFeed at CallWebServices" + "</h1>");
            out.println("</body>");
            out.println("<ul>");
            for (int i = 0; i<num; i++){
            out.println("<li><a href=' "+ nodeItem.item(i).getFirstChild().getNextSibling().getTextContent()+ 
                    "' >" +nodeItem.item(i).getFirstChild().getTextContent()+ "</a></li>");
            }
            out.println("</ul>");
            out.println("</html>");
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(CallingFeed.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(CallingFeed.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
