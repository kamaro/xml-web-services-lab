<%-- 
    Document   : index
    Created on : Nov 12, 2015, 7:55:44 PM
    Author     : prat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CallWebServices</title>
    </head>
    <body>
        <h1>Lab10:Calling and Building Web Services using Java</h1>
        <ul>
            <li><a href='CallingFeed'>Calling Feed</a></li>
            <li><a href='CallingPTTWS'>Calling PTT Web Services</a></li>
            <li><a href='CallingGoogleMaps'>Calling Google Maps</a></li>
            <li><a href='BulidingJSON'>Buliding JSON</a></li>
             <li><a href='CallingIKKU'>Calling IKKU</a></li>
        </ul>
    </body>
</html>
