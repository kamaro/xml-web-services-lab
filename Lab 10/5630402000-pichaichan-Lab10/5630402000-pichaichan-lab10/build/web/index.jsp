
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calling Feed</title>
    </head>
    <body>
        <h1>Lab 10:Calling and Building Web Services Using Java </h1>
        <ul>
            <li> <a href="http://localhost:8080/5630402000-pichaichan-lab10/CallingFeed"><font size='4'>Calling Feed </font><br/></a></li>
            <li> <a href="http://localhost:8080/5630402000-pichaichan-lab10/CallingPTTWS"><font size='4'>Calling PTT Web Service</font><br/></a></li>
            <li> <a href="http://localhost:8080/5630402000-pichaichan-lab10/CallingIKKU"><font size='4'>Calling iKKU</font><br/></a></li>
            <li> <a href="http://localhost:8080/5630402000-pichaichan-lab10/CallingGoogleMap"><font size='4'>Calling Google Map</font><br/></a></li>
            <li> <a href="http://localhost:8080/5630402000-pichaichan-lab10/BuildingJSON"><font size='4'>Building JSON Data</font><br/></a></li>
        </ul>
    </body>
</html>

