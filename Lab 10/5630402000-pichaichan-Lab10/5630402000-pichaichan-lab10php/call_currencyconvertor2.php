<?php

$fromC = filter_input(INPUT_GET, 'fromC');
$toC = filter_input(INPUT_GET, 'toC');
if ($fromC != '' && $toC != '') {
    $wsdl = 'http://webservicex.net/CurrencyConvertor.asmx?WSDL';
    $client = new SoapClient($wsdl);
    $methodName = 'ConversionRate';
    $params = array('FromCurrency' => $_GET["fromC"],
        'ToCurrency' => $_GET["toC"]);

    $soapAction = 'http://www.webserviceX.NET/ConversionRate';

    $objectResult = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));

    $conversionRateResult = $objectResult->ConversionRateResult;

    $response_data = array(
        'fromCurrency' => 'THB',
        'toCurrency' => 'USD',
        'rate' => $conversionRateResult
    );

    echo json_encode($response_data);
} else {
    echo 'please type call_currencyconvertor.php?fromC=THB&toC=USD';
}

