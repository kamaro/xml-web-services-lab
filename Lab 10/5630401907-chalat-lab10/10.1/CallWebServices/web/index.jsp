<%-- 
    Document   : index
    Created on : Nov 12, 2015, 6:01:23 PM
    Author     : CLUSTER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Call web services</title>
    </head>
    <body>
        <ul>
            <h1>Lab 10:Calling and Building Web Services Using Java</h1>
            <li><a href="CallingFeed">Calling Feed</a></li>
            <li><a href="CallingiKKU">Calling iKKU</a></li>
            <li><a href="CallingPTTWS">Calling PTTWS</a></li>
            <li><a href="GoogleMap.html">Calling GoogleMap</a></li>
            <li><a href="BuildingJSON">Building JSON</a></li>
        </ul>
    </body>
</html>
