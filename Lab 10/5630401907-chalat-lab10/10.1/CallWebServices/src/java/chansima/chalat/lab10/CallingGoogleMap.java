package chansima.chalat.lab10;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@WebServlet(name = "CallingGoogleMap", urlPatterns = {"/CallingGoogleMap"})
public class CallingGoogleMap extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws org.json.JSONException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("text/html;charset=utf-8");
        
        try (PrintWriter out = response.getWriter()) {
        String address = request.getParameter("address");
        String add = new String(address.getBytes("UTF-8")); 
        String url = "http://maps.googleapis.com/maps/api/geocode/json?address=\""  + add  + "\"&sensor=false&language=en";
        
        URL mapUrl = new URL(url);
        URLConnection con = mapUrl.openConnection();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(con.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String input = null;
        while ((input = (buffer.readLine())) != null) {
            builder.append(input);
        }//Json
        
        Double lat = null,lng = null ;
        String maps = "http://maps.google.com/?q=" + lat + "," + lng;
       
        JSONObject jsOb = new JSONObject(builder.toString());
        JSONArray result = jsOb.getJSONArray("results");
        for (int i = 0; i < result.length(); i++) {
            JSONObject Ob = result.getJSONObject(i);
            JSONObject geometry = Ob.getJSONObject("geometry");
            lat = geometry.getJSONObject("location").getDouble("lat");
            lng = geometry.getJSONObject("location").getDouble("lng");
            maps = "http://maps.google.com/?q=" + lat + "," + lng;
        }
        //input ได้แต่ภาษาอังกฤา
        
        
            out.println("<meta http-equiv=\"refresh\" content=\"0; URL=" + maps + "\" />");
            out.print(builder.toString());
            //out.print(builder.toString());
    
    }
    }
          

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(CallingGoogleMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(CallingGoogleMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

