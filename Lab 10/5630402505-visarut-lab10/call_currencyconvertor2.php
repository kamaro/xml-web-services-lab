<?php
	header('Content-Type: application/json; charset=utf-8');

	$wsdl = 'http://webservicex.net/CurrencyConvertor.asmx?WSDL';
	$client = new SoapClient($wsdl);
	$methodName = 'ConversionRate';
	$params = array('FromCurrency' =>$_GET['fromC'],
   					 'ToCurrency' => $_GET['toC']);

	$soapAction = 'http://www.webserviceX.NET/ConversionRate';

	$Result = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));
	//Result have these <ConvertionRateResult>double</ConvertionRateResult>
	$xml = $Result->ConversionRateResult;
	//this will get the value in xml tah
	$currency_data = array('fromCurrency' => 'THB',
							'toCurrency' => 'USD',
							'rate' => $xml
							);
	print json_encode($currency_data);
	//what's json_encode do? It's translate string data to JSON form? I guess.
	//something like that

?>