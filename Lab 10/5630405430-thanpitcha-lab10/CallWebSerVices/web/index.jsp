<%-- 
    Document   : index
    Created on : Nov 5, 2014, 3:14:33 PM
    Author     : baslenvm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div><h1>Lab 10:Calling and Building Web Services Using Java</h1></div>
        <ul>
            <li><a href="./CalingFeed">Calling Feed</a></li>
            <li><a href="./CallingPTTWS">CallingPTTWS</a></li>
            <li><a href="./CalingIKKU">CalingIKKU</a></li>
            <li><a href="CallingGoogleMap.jsp">CallingGoogleMap</a></li>
            <li><a href="./BuildingJSON">BuildingJSON</a></li>
        </ul>
    </body>
</html>
