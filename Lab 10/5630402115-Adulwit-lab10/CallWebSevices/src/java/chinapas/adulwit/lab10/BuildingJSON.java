/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chinapas.adulwit.lab10;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Aspire
 */
@WebServlet(name = "BuildingJSON", urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            JSONArray ja = new JSONArray();

            JSONObject loop = new JSONObject();
            JSONObject joAuthor = new JSONObject();
            joAuthor.put("pettition", "พร พิรุณ");
            joAuthor.put("meloday", "เอื้อ สุนทรสนาน");
            loop.put("author", joAuthor);
            loop.put("title", "มาร์ช ม.ข.");
            loop.put("content", "มหาวิทยาลัยขอนแก่นเกริกไกรวิทยา");
            ja.put(loop);
            
            JSONObject loop2 = new JSONObject();
            JSONObject joAuthor2 = new JSONObject();
            joAuthor2.put("pettition", "สุนทราภรณ์");
            joAuthor2.put("meloday", "สุนทราภรณ์");
            loop2.put("author", joAuthor2);
            loop2.put("title", "เสียงสนครวญ");
            loop2.put("content", "เสียงยอดสนอ่อนโอนพลิ้วโยนยอดไหว");
            ja.put(loop2);

            out.println(ja);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(BuildingJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(BuildingJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
