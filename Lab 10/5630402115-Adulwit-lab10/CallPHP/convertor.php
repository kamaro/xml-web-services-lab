<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    </head>
    <body>
        <h1>Simple Convertor Web Application</h1>
        1 <input type="text" id="fromC" value="THB" name="fromC" size="1"/> 
        = <input type="text" id="output" size="4" disabled="Yes"/>
        <input type="text" value="USD" id="toC" size="1"/>
        <input type="submit" value="Convert" id="convert"/>
    </body>
    <script>
        $("#convert").click(function () {
            var formC = "fromC=" + $("#fromC").val();
            var toC = "toC=" + $("#toC").val();
            var formData = formC + "&" + toC;
            $.ajax(
                    {
                        dataType: "json",
                        url: "call_currencyconvertor2.php",
                        type: "GET",
                        data: formData,
                        success: function (data)
                        {
                            $("#output").val(data['rate']);
                            //alert(formData);
                        }
                    });
        });
    </script>
</html>
