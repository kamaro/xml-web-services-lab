/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sanitin.yotravee.lab10;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CallingGoogleMap", urlPatterns = {"/CallingGoogleMap"})
public class CallingGoogleMap extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String q = request.getParameter("q");
        URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address=" + q + "&sensor=false&language=en");
        InputStream in = url.openStream();
        JsonReader rd = Json.createReader(in);
        JsonObject obj = rd.readObject();
        JsonArray jsoj1 = (JsonArray) obj.get("results");
        JsonObject jsoj2 = (JsonObject) jsoj1.get(0);
        JsonObject jsoj3 = (JsonObject) jsoj2.get("geometry");
        JsonObject location = (JsonObject) jsoj3.get("location");
        String maps = "http://maps.google.com/?q=" + location.get("lat") + "," + location.get("lng");
        try (PrintWriter out = response.getWriter()) {
            out.println("Redirecting to " + maps);
            out.println("<meta http-equiv=\"refresh\" content=\"0; URL=" + maps + "\" />");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
