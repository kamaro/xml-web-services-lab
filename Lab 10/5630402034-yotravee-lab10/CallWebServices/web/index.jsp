<%-- 
    Document   : index
    Created on : Nov 12, 2015, 5:13:39 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Call Web Services</title>
    </head>
    <body>
        <h1>Lab 10:Calling and Building Web Services Using Java</h1>
        <ul>
            <li><a href="CallingFeed">Calling Feed</a></li>
            <li><a href="CallingPTTWS">Calling PTT Web Service</a></li>
            <li><a href="CallingIKKU">Calling iKKU</a></li>
            <li><a href="CallingGoogleMap.html">Calling Google Map</a></li>
            <li><a href="CallingJsonData">Calling JSON Data</a></li>
        </ul>
    </body>
</html>
