<?php
header('Content-Type: application/json; charset=utf-8');

$client = new SoapClient('http://webservicex.net/CurrencyConvertor.asmx?WSDL');

$methodName = 'ConversionRate';

$params = array('FromCurrency' => $_GET["fromC"], 'ToCurrency' => $_GET["toC"]);

$soapAction = 'http://www.webserviceX.NET/ConversionRate';

$objectResult = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));

echo json_encode( array(
"FromCurrency" => 'THB',
"ToCurrency" => 'USD',
"rate" => $objectResult->ConversionRateResult));

?>