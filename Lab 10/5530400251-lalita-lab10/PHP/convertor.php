<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Convertor</title>
        <link href="http://www.myquestionth.com/bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://www.myquestionth.com/js/jquery.min.js"></script>
    </head>

    <body>

        <?php
        $s = "AFA or ALL or DZD or ARS or AWG or AUD or BSD or BHD or BDT or BBD or BZD or BMD or BTN or BOB or BWP or BRL or GBP or BND or BIF or XOF or XAF or KHR or CAD or CVE or KYD or CLP or CNY or COP or KMF or CRC or HRK or CUP or CYP or CZK or DKK or DJF or DOP or XCD or EGP or SVC or EEK or ETB or EUR or FKP or GMD or GHC or GIP or XAU or GTQ or GNF or GYD or HTG or HNL or HKD or HUF or ISK or INR or IDR or IQD or ILS or JMD or JPY or JOD or KZT or KES or KRW or KWD or LAK or LVL or LBP or LSL or LRD or LYD or LTL or MOP or MKD or MGF or MWK or MYR or MVR or MTL or MRO or MUR or MXN or MDL or MNT or MAD or MZM or MMK or NAD or NPR or ANG or NZD or NIO or NGN or KPW or NOK or OMR or XPF or PKR or XPD or PAB or PGK or PYG or PEN or PHP or XPT or PLN or QAR or ROL or RUB or WST or STD or SAR or SCR or SLL or XAG or SGD or SKK or SIT or SBD or SOS or ZAR or LKR or SHP or SDD or SRG or SZL or SEK or CHF or SYP or TWD or TZS or THB or TOP or TTD or TND or TRL or USD or AED or UGX or UAH or UYU or VUV or VEB or VND or YER or YUM or ZMK or ZWD or TRY";

        $new = split(" or ", $s);
        ?>

        <div class="text-center" style="width: 800px; margin-left: auto; margin-right: auto;">
            <h1>Simple Convertor Web Application</h1>
            <form class="form-inline">
                <div class="form-group">
                    <label>From: </label>
                    <input type="text" class="form-control" id="from_value" placeholder="type a number" autocomplete="off">
                </div>
                <select id="FromCurrency" class="form-control">
<?php
for ($i = 0; $i < count($new); $i++) {
    echo '<option value="' . $new[$i] . '"' . ($new[$i] == "THB" ? " selected" : "") . '>' . $new[$i] . '</option>';
}
?>
                </select>
                <div class="form-group">
                    <label>To: </label>
                    <input type="text" class="form-control" id="to_value" placeholder="result" autocomplete="off">
                </div>
                <select id="ToCurrency" class="form-control">
<?php
for ($i = 0; $i < count($new); $i++) {
    echo '<option value="' . $new[$i] . '"' . ($new[$i] == "USD" ? " selected" : "") . '>' . $new[$i] . '</option>';
}
?>
                </select>
                <button id="submitBT" type="submit" class="btn btn-default">Convert</button>
            </form>
        </div>

        <script>
            (function (d, w) {
                $(document).on('click', '#submitBT', function (a) {

                    var from1 = $("#FromCurrency").val();
                    var to1 = $("#ToCurrency").val();

                    var from_value = Number($("#from_value").val());
                    var to_value = Number($("#to_value").val());

                    $.ajax({
                        url: "call_currencyconvertor2.php?fromC=THB&toC=USD",
                        type: 'get',
                        dataType: 'json',
                        data: {fromC: from1, toC: to1},
                        success: function (data) {
                            $("#to_value").val(from_value * data.rate);
                            $("#submitBT").removeAttr("disabled");
                            $("#submitBT").text("Convert");
                        },
                        error: function () {
                            $("#submitBT").removeAttr("disabled");
                            $("#submitBT").text("Convert");
                            alert("Error!");
                        },
                        beforeSend: function () {
                            $("#submitBT").attr("disabled", true);
                            $("#submitBT").text("Converting...");
                        }
                    });
                    a.preventDefault();
                    return false;
                });
            })(document, window);
        </script>

    </body>
</html>
