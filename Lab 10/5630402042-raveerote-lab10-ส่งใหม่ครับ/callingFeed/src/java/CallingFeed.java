/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author golfyzzz
 */
@WebServlet(urlPatterns = {"/CalingFeed"})
public class CallingFeed extends HttpServlet {
    String title = "" ,link = "";
    int numTitle = 0;
    int numLink = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<h1>Servlet CallingFeed at /CallWebServices</h1>");
        try {
                String xml= "http://www.dailynews.co.th/rss/rss.xml";
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder parser = factory.newDocumentBuilder();
                Document document = null;
                document = parser.parse(xml);
                
        	Element rootEle = document.getDocumentElement();
                rootEle.getElementsByTagName("link");
    
                followNode(rootEle, out);
                     
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CalingFeed</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<ul>");
            out.println("</ul>");
            out.println("</body>");
            out.println("</html>");
            } 
        catch (Exception ex) {
        	ex.printStackTrace();
    	} 
        
        }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void followNode(org.w3c.dom.Node  rootEle, PrintWriter out) {
        
        if(rootEle.hasChildNodes()){
            String name = rootEle.getNodeName();
            if(name.equals("title")){
                    ++numTitle;
                        if (numTitle != 1)
                        title = rootEle.getFirstChild().getNodeValue();
            }
            else if(name.equals("link")){
                    ++numLink; 
                        if (numLink != 1)
                        link = rootEle.getFirstChild().getNodeValue();
                
            }
             if(!title.equals("") && !link.equals("")){
                out.println("<ul><li><a href = " + link + " target=\"_blank\">" + title + "</a></li></ul>");
                title = "";
                link = "";
                }
                  
            org.w3c.dom.Node firstChild = rootEle.getFirstChild();
            followNode(firstChild, out);
        }
        org.w3c.dom.Node nextNode = rootEle.getNextSibling();
        if(nextNode != null)
            followNode(nextNode, out);
        }
        
}