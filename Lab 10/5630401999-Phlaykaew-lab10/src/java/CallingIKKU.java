/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.*;

public class CallingIKKU extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        URL url = new URL("https://www.kku.ac.th/ikku/api/activities/services/topActivity.php");
        try (PrintWriter out = response.getWriter()) {
            URLConnection connection = url.openConnection();
            BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                    connection.getInputStream()));
            
            StringBuilder sb = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }

            in.close();
            
            String jsonText = sb.toString();
            JSONObject json = new JSONObject(jsonText);
            JSONArray arr = json.getJSONArray("activities");
            
            out.println("<h1>Servlet CallingIKKU at " + request.getContextPath() + "</h1>");
            out.println("<table>");
            for (int count=0; count<arr.length(); count++) {
                out.println("<tr><td>");
                out.println(arr.getJSONObject(count).getString("dateSt"));
                out.println("</td>");
                out.println("<td><a href='");
                out.println(arr.getJSONObject(count).getString("url"));
                out.println("'>");
                out.println(arr.getJSONObject(count).getString("title"));
                out.println("</a></td></tr>");                
            }
            out.println("</table>");
            
           
        } catch (JSONException ex) {
            
        }
    }
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
