<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Simple Convertor Web Application</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>
<body>

<div class="text-center">
<h1>Simple Convertor Web Application</h1>
<form class="convertor">
  <div class="form-group">
    <label> 1 </label>
    <input type="text" class="form-control" id="from" size="1">
    <label> = </label>
    <input type="text" class="form-control" id="output" size="8">
    <input type="text" class="form-control" id="to" size="1">
   <button id="submit" type="submit" class="btn btn-default">Convert</button>
  </div>
</form>
</div>

<script>
(function (doc, win) {
	$(document).on('click', '#submit', function(a) {
		var from = $("#from").val().toUpperCase();
		var to = $("#to").val().toUpperCase();
		var output_rate = Number($("#output").val());
		document.getElementById('from').value = from;
		document.getElementById('to').value = to;
		$.ajax({
			url: "call_currencyconvertor2.php?fromC=THB&toC=USD",
			type: 'get',
			dataType: 'json',
			data: {fromC: from , toC: to},
			success: function(data) {
				$("#output").val(data.rate);
				$("#submit").removeAttr("disabled");
				$("#submit").text("Convert");
			},
			error: function(){
				$("#submit").removeAttr("disabled");
				$("#submit").text("Convert");
				alert(" ------ Error!!! ------\nPlase Enter : \n AFA or ALL or DZD or ARS or AWG or AUD or BSD or BHD or BDT or BBD or BZD or BMD or BTN or BOB or BWP or BRL or GBP or BND or BIF or XOF or XAF or KHR or CAD or CVE or KYD or CLP or CNY or COP or KMF or CRC or HRK or CUP or CYP or CZK or DKK or DJF or DOP or XCD or EGP or SVC or EEK or ETB or EUR or FKP or GMD or GHC or GIP or XAU or GTQ or GNF or GYD or HTG or HNL or HKD or HUF or ISK or INR or IDR or IQD or ILS or JMD or JPY or JOD or KZT or KES or KRW or KWD or LAK or LVL or LBP or LSL or LRD or LYD or LTL or MOP or MKD or MGF or MWK or MYR or MVR or MTL or MRO or MUR or MXN or MDL or MNT or MAD or MZM or MMK or NAD or NPR or ANG or NZD or NIO or NGN or KPW or NOK or OMR or XPF or PKR or XPD or PAB or PGK or PYG or PEN or PHP or XPT or PLN or QAR or ROL or RUB or WST or STD or SAR or SCR or SLL or XAG or SGD or SKK or SIT or SBD or SOS or ZAR or LKR or SHP or SDD or SRG or SZL or SEK or CHF or SYP or TWD or TZS or THB or TOP or TTD or TND or TRL or USD or AED or UGX or UAH or UYU or VUV or VEB or VND or YER or YUM or ZMK or ZWD or TRY");
			}
		});
		a.preventDefault();
		return false;
	});
})
(document, window);
</script>
</body>
</html>