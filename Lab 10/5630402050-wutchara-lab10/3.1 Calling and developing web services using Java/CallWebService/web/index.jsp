<html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Call Web Service</title>
    </head>
    <body>
        <h1>Lab 10:Calling and Building Web Service Using Java</h1>
    	<form action="CallingFeed">
        	<ul><li><a onclick="jsfunction()" href="CallingFeed">Calling Feed</a></li></ul>
        </form>
        <form action="CallingPTTWS">
        	<ul><li><a onclick="jsfunction()" href="CallingPTTWS">Calling PTT Web Service</a></li></ul>
        </form>
        <form action="CallingIKKU">
        	<ul><li><a onclick="jsfunction()" href="CallingIKKU">Caling iKKU</a></li></ul>
        </form>
        <form action="CallingGoogleMap">
        	<ul><li><a onclick="jsfunction()" href="CallingGoogleMap.html">Calling Google Map</a></li></ul>
        </form>
        <form action="BuildingJSON">
        	<ul><li><a onclick="jsfunction()" href="BuildingJSON">Building JSON Data</a></li></ul>
        </form>
    </body>
</html>
