/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rachadach.wutchara.lab10;

import com.google.maps.model.GeocodingResult;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import java.net.URLEncoder;
/*
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapType;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;*/

/**
 *
 * @author HAM
 */
@WebServlet(name = "CallingGoogleMap", urlPatterns = {"/CallingGoogleMap"})
public class CallingGoogleMap extends HttpServlet {//implements MapComponentInitializedListener{

    String title = "", link = "";
    Boolean check = true;
    private final String G_API_KET = "AIzaSyDMXE30yE2haKTs4X1kirxcLB8kqCzT0D4";
    private String jsData = "";
    String locationLat = "", locationLng = "";
    //GoogleMapView mapView;
    //GoogleMap map;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String q = request.getParameter("address").trim();
        //out.println("Address : " + q);
        try {
            //launch("");
            
            /*String uRL = "http://maps.googleapis.com/maps/api/js?address=" + q + "&sensor=false&language=en";
            GeoApiContext context = new GeoApiContext().setApiKey(G_API_KET);
            GeocodingResult[] results = GeocodingApi.geocode(context,
                    "1600 Amphitheatre Parkway Mountain View, CA 94043").await();
            out.println(results[0].formattedAddress);
            GeocodingApiRequest req = GeocodingApi.newRequest(context).address("Sydney");
            out.println(req);

            try {
                req.await();
            // Handle successful request.
            } catch (Exception e) {
            // Handle error
            }*/
            
            //get location of q (input)
            getLocation(q, out);
            //getLocation2(q, out);
            
            //call google map from location input
            String shMapURL = "https://www.google.com/maps/place/" + locationLat + "," + locationLng;
            response.sendRedirect(shMapURL);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
    
    private void getLocation(String q, PrintWriter out) throws MalformedURLException, IOException{
        URL googleData = new URL("http://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(q) + "&sensor=false&language=en");
        URLConnection gc = googleData.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                                    gc.getInputStream(), "UTF8"));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            //out.println(inputLine);
            jsData = jsData + inputLine;
        }
        in.close();
            
        //out.println(jsData);
        JSONObject jsonObj = new JSONObject(jsData);
        //out.println(jsonObj);
        JSONArray jsonArr = new JSONArray();
        jsonArr = jsonObj.getJSONArray("results");
        
        locationLat = jsonArr.getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lat").toString();
        locationLng = jsonArr.getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lng").toString();
        //out.println("Lnt : " + locationLat + "</br>Lng : " + locationLng);
        
    }
    
        private void getLocation2(String q, PrintWriter out) throws MalformedURLException, IOException{
        //out.println("Address : " + q);
        URL googleData = new URL("https://maps.googleapis.com/maps/api/geocode/json?address=" + q + "&key=AIzaSyA9oMuzgolqzZ8MO37SPl0VyC2HBXCFXbI&sensor=false&language=en");
        URLConnection gc = googleData.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                                    gc.getInputStream(), "UTF8"));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            //out.println(inputLine);
            jsData = jsData + inputLine;
        }
        in.close();
            
        //out.println(jsData);
        JSONObject jsonObj = new JSONObject(jsData);
        out.println(jsonObj);
        JSONArray jsonArr = new JSONArray();
        jsonArr = jsonObj.getJSONArray("results");
        
        locationLat = jsonArr.getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lat").toString();
        locationLng = jsonArr.getJSONObject(0).getJSONObject("geometry").getJSONObject("location").get("lng").toString();
        //out.println("Lnt : " + locationLat + "</br>Lng : " + locationLng);
        jsData = "";
    }
        /*
public void start(Stage stage) throws Exception {

    //Create the JavaFX component and set this as a listener so we know when 
    //the map has been initialized, at which point we can then begin manipulating it.
    mapView = new GoogleMapView();
    mapView.addMapInializedListener(this);

    Scene scene = new Scene(mapView);

    stage.setTitle("JavaFX and Google Maps");
    stage.setScene(scene);
    stage.show();
}


@Override
public void mapInitialized() {
    //Set the initial properties of the map.
    MapOptions mapOptions = new MapOptions();

    mapOptions.center(new LatLong(47.6097, -122.3331))
            .mapType(MapType.ROADMAP)
            .overviewMapControl(false)
            .panControl(false)
            .rotateControl(false)
            .scaleControl(false)
            .streetViewControl(false)
            .zoomControl(false)
            .zoom(12);

    map = mapView.createMap(mapOptions);

    //Add a marker to the map
    MarkerOptions markerOptions = new MarkerOptions();

    markerOptions.position( new LatLong(47.6, -122.3) )
                .visible(Boolean.TRUE)
                .title("My Marker");

    Marker marker = new Marker( markerOptions );

    map.addMarker(marker);

}*/

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


}
