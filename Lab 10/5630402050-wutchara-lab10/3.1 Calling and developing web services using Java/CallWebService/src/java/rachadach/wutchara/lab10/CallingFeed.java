package rachadach.wutchara.lab10;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author HAM
 */
@WebServlet(urlPatterns = {"/CallingFeed"})
public class CallingFeed extends HttpServlet {
    String title = "", link = "";
    int state = 0;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<h2>Servlet CallingFeed at /CallWebServices</h2>");
        try  {
                Document doc = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder parser = dbf.newDocumentBuilder();
                //file path is "D:\\XML\\LAB5\\src\\rachadach\\wutchara\\lab5\\dom\\nation.xml"
                doc = parser.parse("http://www.dailynews.co.th/rss/rss.xml");
            // แสดงข้อความ SOAP Response
            displayMessage(doc, out);
        } catch (Exception ex) {
            ex.printStackTrace();
        }  finally {
            out.close();
        }
    }

    private void displayMessage(Document xmlDoc, PrintWriter out) {
        try {
            Element rootNode = xmlDoc.getDocumentElement();
            followNode(rootNode, out);
            //out.println("The root element name is " + rootNode.getNodeName());
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void followNode(org.w3c.dom.Node node, PrintWriter out){
        //writeNode(node, out);
        if(node.hasChildNodes()){
            String name = node.getNodeName();
            //out.println("node " + name + " has " + numChildern + " children");
            if(name.equals("title")){
                //org.w3c.dom.Node firstChild = node.getFirstChild();
               
                    title = node.getFirstChild().getNodeValue();
                
               // out.println(title);
            }
            else if(name.equals("link")){
               
                    link = node.getFirstChild().getNodeValue();
                
                //out.println(link);
            }
            if(!title.equals("") && !link.equals("")){
                if(state == 0){
                    state = 1;
                }else
                    out.println("<ul><ul><li><a href = " + link + " target=\"_blank\">" + title + "</a></li></ul></ul>");
                
                title = "";
                link = "";
            }
            
            org.w3c.dom.Node firstChild = node.getFirstChild();
            followNode(firstChild, out);
        }
        org.w3c.dom.Node nextNode = node.getNextSibling();
        if(nextNode != null)
            followNode(nextNode, out);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
