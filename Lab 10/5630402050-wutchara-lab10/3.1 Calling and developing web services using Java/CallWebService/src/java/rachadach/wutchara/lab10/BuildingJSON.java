/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rachadach.wutchara.lab10;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author HAM
 */
@WebServlet(name = "BuildingJSON", urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
             JSONObject dataSet = new JSONObject();
             dataSet.put("pettition", "พร  พิรุณ");
             dataSet.put("meloday", "เอื้อ  สุนทรสนาน");
             JSONObject authorSet = new JSONObject();
             authorSet.put("author", dataSet);
             //out.println(authorSet.toString());
             JSONObject title = new JSONObject();
             title.put("title", "มาร์ช  ม.ข.");
             
             JSONObject content = new JSONObject();
             content.put("content", "มหาวิทยาลัยขอนแก่น");
        
             JSONObject dataSet2 = new JSONObject();
             dataSet2.put("pettition", "สุนทราภรณ์");
             dataSet2.put("meloday", "สุนทราภรณ์");
             JSONObject authorSet2 = new JSONObject();
             authorSet2.put("author", dataSet);       
             
             JSONObject title2 = new JSONObject();
             title2.put("title", "เสียงสนครวญ");
             JSONObject content2 = new JSONObject();
             content2.put("content", "เสียงยอดสนอ่อนโอนพลิ้วโยนยอดไหว");
             
             JSONObject listOfOb = new JSONObject();
             listOfOb.putAll(authorSet);
             //listOfOb.add(authorSet);
             listOfOb.putAll(title);
             listOfOb.putAll(content);
             JSONObject listOfOb2 = new JSONObject();
             listOfOb2.putAll(authorSet2);
             listOfOb2.putAll(title2);
             listOfOb2.putAll(content2);
             
             JSONArray listOfAuthor = new JSONArray();
             listOfAuthor.add(listOfOb);
             listOfAuthor.add(listOfOb2);
             out.println(listOfAuthor.toJSONString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
