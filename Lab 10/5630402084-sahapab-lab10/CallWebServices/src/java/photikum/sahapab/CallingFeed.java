package photikum.sahapab;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

@WebServlet(urlPatterns = {"/CallingFeed"})

public class CallingFeed extends HttpServlet {

    String newsTitle, newsURL;
    boolean isTitle = true;
    int elementCount = 0; //to check that have both newsTitle and newsLink

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Document doc;
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            doc = parser.parse("http://www.dailynews.co.th/rss/rss.xml");
            displayMessage(doc, out);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
        }
    }

    private void displayMessage(Document xmlDoc, PrintWriter out) {
        try {
            out.println("<h2>Servlet CallingFeed at /CallWebServices</h2>");
            Element rootNode = xmlDoc.getDocumentElement();
            followNode(rootNode, out);
        } catch (Exception ex) {
        }
    }

    public void followNode(org.w3c.dom.Node node, PrintWriter out) {
        if (node.hasChildNodes()) {
            String nodeName = node.getNodeName();
            switch (nodeName) {
                case "title":
                    newsTitle = node.getFirstChild().getNodeValue();
                    elementCount++;
                    break;
                case "link":
                    newsURL = node.getFirstChild().getNodeValue();
                    elementCount++;
                    break;
            }
            if (elementCount == 2) {
                if (isTitle == false) {
                    out.println("<ul><li><a href = " + newsURL + ">" + newsTitle + "</a></li></ul>");
                }
                isTitle = false;
                elementCount = 0;
            }

            org.w3c.dom.Node firstChild = node.getFirstChild();
            followNode(firstChild, out);
        }
        org.w3c.dom.Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode, out);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
