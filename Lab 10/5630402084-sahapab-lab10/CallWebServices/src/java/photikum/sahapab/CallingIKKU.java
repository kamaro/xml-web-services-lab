package photikum.sahapab;

import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.servlet.annotation.WebServlet;
import javax.json.*;

@WebServlet(urlPatterns = {"/CallingIKKU"})
public class CallingIKKU extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String link = "https://www.kku.ac.th/ikku/api/activities/services/topActivity.php";
        URL url = new URL(link);
        InputStream input = url.openStream();
        JsonReader jreader = Json.createReader(input);
        JsonObject jobject = jreader.readObject();
        JsonArray jarray = jobject.getJsonArray("activities");
        
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Calling iKKU</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Calling iKKU Web Service at " + request.getContextPath() + "</h1>");

            jarray.getValuesAs(JsonObject.class).stream().forEach((result) -> {
                String title = result.getString("title","");
                String date = result.getString("dateSt","");
                String urlS = result.getString("url","");
                out.println("<p>" + date + "  " + "<a href=" + urlS + ">" + 
                        title + "</a></p>");
            });
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
