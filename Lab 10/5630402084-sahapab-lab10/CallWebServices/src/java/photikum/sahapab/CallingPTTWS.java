package photikum.sahapab;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.util.logging.*;
import javax.servlet.http.*;
import javax.xml.soap.*;
import org.w3c.dom.*;

/**
 *
 * @author FIFA1412
 */
@WebServlet(urlPatterns = {"/CallingPTTWS"})
public class CallingPTTWS extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SOAPException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Calling PTTWS</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Calling PTT Web Service at /CallWebServices" + "</h1>");
            out.println("</body>");

            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPBody body = soapMessage.getSOAPBody();
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            String prefix = "ns";
            String namespace = "http://www.pttplc.com/ptt_webservice/";
            Name name = soapFactory.createName("CurrentOilPrice", prefix, namespace);
            SOAPBodyElement sBodyElement;
            sBodyElement = body.addBodyElement(name);
            SOAPElement sElement = sBodyElement.addChildElement(soapFactory.createName("Language", prefix, namespace));
            sElement.addTextNode("th");
            MimeHeaders mHeaders = soapMessage.getMimeHeaders();
            mHeaders.addHeader("SOAPAction", namespace + "CurrentOilPrice");
            SOAPConnection soapConnection = SOAPConnectionFactory.newInstance().createConnection();
            String end = "http://www.pttplc.com/webservice/pttinfo.asmx";
            SOAPMessage resp;
            resp = soapConnection.call(soapMessage, end);
            Document document = resp.getSOAPBody().getOwnerDocument();
            NodeList node = document.getElementsByTagName("CurrentOilPriceResult");
            Element element = (Element) node.item(0);
            
            out.println(element.getFirstChild().getTextContent());
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
