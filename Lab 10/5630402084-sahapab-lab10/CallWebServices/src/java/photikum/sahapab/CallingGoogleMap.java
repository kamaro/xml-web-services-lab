package photikum.sahapab;

import java.io.*;
import java.net.*;
import java.util.logging.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.*;

@WebServlet(name = "CallingGoogleMap", urlPatterns = {"/CallingGoogleMap"})
public class CallingGoogleMap extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        String Addr = request.getParameter("add");
        float lat = 0,lng=0;
        String input = null;
        URL link = new URL("http://maps.googleapis.com/maps/api/geocode/json?address=" + Addr + "&sensor=false&language=en");
        URLConnection conn = link.openConnection();
        BufferedReader buf = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        while ((input = (buf.readLine())) != null) {
            sb.append(input);
        }
        String map = "";
        JSONObject jObj = new JSONObject(sb.toString());
        JSONArray result = jObj.getJSONArray("results");
        
        for (int i = 0; i < result.length(); i++) {
            JSONObject Ob = result.getJSONObject(i);
            JSONObject geomatry = Ob.getJSONObject("geometry");
            lat = (float) geomatry.getJSONObject("location").getDouble("lat");
            lng = (float) geomatry.getJSONObject("location").getDouble("lng");
            map = "http://maps.google.com/?q=" + lat + "," + lng;
        }
        
        try (PrintWriter out = response.getWriter()) {
            out.print("<META HTTP-EQUIV='Refresh' CONTENT=1;URL='" + map + "'>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(CallingGoogleMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(CallingGoogleMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
