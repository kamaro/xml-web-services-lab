<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
    </head>
    <body>
        <h1>Servlet CallingFeed at /CallWebServices</h1><br />
        <ul>
            <li><a href="CallingFeed">Calling Feed</a><br/></li>
                <li><a href="CallingPTTWS">Calling PTT Web Service</a><br/></li>
                <li><a href="CalingIKKU">Calling iKKU</a><br/></li>
                <li><a href="CallingGoogleMap.html">Calling Google Map</a><br/></li>
                <li><a href="BuildingJSON">Building JSON Data</a><br/></li>
        </ul>
    </body>
</html>
