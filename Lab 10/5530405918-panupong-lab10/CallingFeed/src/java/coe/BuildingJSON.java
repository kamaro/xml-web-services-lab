/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coe;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Poppy
 */
@WebServlet(name = "BuildingJSON", urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            JSONArray all = new JSONArray();
            JSONArray arr = new JSONArray();
            JSONArray arr2 = new JSONArray();
               String petition[] = {"พร ริรุณ", "สุนทราภรณ์"};
               String meloday[] = {"เอื้อ สุนทรสนาน", "สุนทราภรณ์"};
               String title[] = {"มาร์ช ม.ข.", "เสียงสนครวญ"};
               String content[] = {"มหาวิทยาลัยขอนแก่นเกริกไกรวิทยา", "เสียงยอดสนโอนโอนผิ้วโยนอ่อนไหว"};
               
               
                    JSONObject authorObj = new JSONObject();
                    authorObj.put("petition", petition[0]);
                    authorObj.put("meloday", meloday[0]);
                    JSONObject titleObj = new JSONObject();
                    titleObj.put("title", title[0]);
                    JSONObject contentObj = new JSONObject();
                    contentObj.put("content", content[0]);
                    arr.put(authorObj);
                    arr.put(titleObj);
                    arr.put(contentObj);
                    
                    JSONObject authorObj2 = new JSONObject();
                    authorObj2.put("petition", petition[1]);
                    authorObj2.put("meloday", meloday[1]);
                    JSONObject titleObj2 = new JSONObject();
                    titleObj2.put("title", title[1]);
                    JSONObject contentObj2 = new JSONObject();
                    contentObj2.put("content", content[1]);
                    arr2.put(authorObj2);
                    arr2.put(titleObj2);
                    arr2.put(contentObj2);
               all.put(arr);
               all.put(arr2);
            response.getWriter().write(all.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
