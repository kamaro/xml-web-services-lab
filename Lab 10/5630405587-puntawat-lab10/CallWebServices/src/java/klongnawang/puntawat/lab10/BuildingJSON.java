/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klongnawang.puntawat.lab10;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Puntawat
 */
@WebServlet(name = "BuildingJSON", urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            JSONObject author = new JSONObject();
            JSONObject mainauthor = new JSONObject();
            JSONObject mainauthor2 = new JSONObject();
            JSONObject author2 = new JSONObject();
            author.put("petition", "พร พิรุณ");
            author.put("meloday", "เอื้อ สุนทรสนาน");
            author2.put("petition", "สุนทราภรณ์");
            author2.put("meloday", "สุนทราภรณ์");
            mainauthor.put("author", author);
            mainauthor.put("title", "มาร์ช ม.ข.");
            mainauthor.put("content", "มหาวิทยาลัยขอนแก่น");
            mainauthor2.put("author", author2);
            mainauthor2.put("title", "เสียงสนครวญ");
            mainauthor2.put("content", "เสียงยอดสนอ่อนโอนพลิ้วโยนยอดไหว");
            JSONArray array = new JSONArray();
            array.put(mainauthor);
            array.put(mainauthor2);
            out.print(array);
            
            
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
