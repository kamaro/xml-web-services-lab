<html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calling and Developing Web Services</title>
    </head>
    <body>
        <h2>Lab 10 : Calling and Building Web Services Using Java</h2>
    	<form action="CallingFeed">
        	<ul><li><a onclick="jsfunction()" href="CallingFeed">Calling Feed</a></ul>
        </form>
        <form action="CallingPTTWS">
        	<ul><li><a onclick="jsfunction()" href="CallingPTTWS">Calling PTT Web Service</a></ul>
        </form>
        <form action="CalingIKKU">
        	<ul><li><a onclick="jsfunction()" href="CalingIKKU">Caling iKKU</a></ul>
        </form>
        <form action="CallingGoogleMap">
        	<ul><li><a onclick="jsfunction()" href="CallingGoogleMap">Calling Google Map</a></ul>
        </form>
        <form action="BuildingJSON">
        	<ul><li><a onclick="jsfunction()" href="BuildingJSON">Building JSON Data</a></ul>
        </form>
    </body>
</html>
