/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Bourne
 */
@WebServlet(urlPatterns = {"/CallingFeed"})
public class CallingFeed extends HttpServlet {

    String title = "", link = "";
    Boolean check = true;
    private int state;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            out.println("<h1>Servlet CalingIKKU at " + request.getContextPath() + "</h1>");
            Document document = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = dbf.newDocumentBuilder();
            document = parser.parse("http://www.dailynews.co.th/rss/rss.xml");
            displayMessage(document, out);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    private void displayMessage(Document xmlDoc, PrintWriter out) {
        try {
            Element rootNode = xmlDoc.getDocumentElement();
            followNode(rootNode, out);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void followNode(org.w3c.dom.Node node, PrintWriter out) {
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            if (name.equals("title")) {
                if (check) {
                    title = node.getFirstChild().getNodeValue();
                }
            } else if (name.equals("link")) {
                if (check) {
                    link = node.getFirstChild().getNodeValue();
                }
            }
            if (!title.equals("") && !link.equals("")) {
                if (state == 0) {
                    state = 1;
                } else {
                    out.println("<ul><li><a href = " + link + " target=\"_blank\">"
                            + title + "</a></li></ul></ul>");
                }
                title = "";
                link = "";
            }

            org.w3c.dom.Node firstChild = node.getFirstChild();
            followNode(firstChild, out);
        }
        org.w3c.dom.Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode, out);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
