/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Bua
 */
@WebServlet(urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        
        JSONObject place1 = new JSONObject();
        JSONObject place2 = new JSONObject();
        
        JSONObject auth1 = new JSONObject();
        JSONObject auth2 = new JSONObject();

        auth1.put("petition", "พร พิรุณ");
        auth1.put("meloday", "เอื้อ สุนทรสนาน");
        
        place1.put("author", auth1);
        place1.put("title", "มาร์ช ม.ข.");
        place1.put("content", "มหาวิทยาลัยขอนแก่น");

        auth2.put("petition", "สุนทราภรณ์");
        auth2.put("meloday", "สุนทราภรณ์");
        
        place2.put("author", auth2);
        place2.put("title", "เสียงสนครวญ");
        place2.put("content", "เสียงยอดสนอ่อนโอนพลิ้วโยนยอดไหว");
        
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(place1);
        jsonArray.put(place2);
        try (PrintWriter out = response.getWriter()) {
            out.println(jsonArray);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(BuildingJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(BuildingJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
