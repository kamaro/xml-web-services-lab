/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Bua
 */
@WebServlet(urlPatterns = {"/CallingGoogleMap"})
public class CallingGoogleMap extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        String addr = request.getParameter("address");
        String add = new String(addr.getBytes("UTF-8")); 
        String map = "http://maps.googleapis.com/maps/api/geocode/json?address=\""  
                    + add  + "\"&sensor=false&language=en";
        
        URL url = new URL(map);
        URLConnection connect = url.openConnection();
        BufferedReader buff = new BufferedReader(new InputStreamReader(connect.getInputStream()));
        StringBuilder build = new StringBuilder();
        String input = null;
            while ((input = (buff.readLine())) != null) {
            build.append(input);
            }   
        
        Double latitude = null,longitude = null ;
        String maps = "http://maps.google.com/?q=" + latitude + "," + longitude;
       
        JSONObject jsonObj = new JSONObject(build.toString());
        JSONArray result = jsonObj.getJSONArray("results");
            for (int i = 0; i < result.length(); i++) {
            JSONObject obj = result.getJSONObject(i);
            JSONObject geometry = obj.getJSONObject("geometry");
            latitude = geometry.getJSONObject("location").getDouble("lat");
            longitude = geometry.getJSONObject("location").getDouble("lng");
            maps = "http://maps.google.com/?q=" + latitude + "," + longitude;
            }
        out.println("<meta http-equiv=\"refresh\" content=\"0; URL=" + maps + "\" />");
        out.print(build.toString());
    
    }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(CallingGoogleMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(CallingGoogleMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
