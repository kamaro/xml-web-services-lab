/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;


@WebServlet(urlPatterns = {"/CallingPTTWS"})
public class CallingPTTWS extends HttpServlet {

 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            MessageFactory mFac = MessageFactory.newInstance();
            SOAPMessage message = mFac.createMessage();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory soapFactory = SOAPFactory.newInstance();
            String prefix = "ns";
            String namespace = "http://www.pttplc.com/ptt_webservice/";
            Name opName = soapFactory.createName("CurrentOilPrice",prefix, namespace);
            SOAPBodyElement opElem = body.addBodyElement(opName);
            SOAPElement Language = opElem.addChildElement( soapFactory.createName("Language", prefix, namespace));
            Language.addTextNode("ENG"); 
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", "http://www.pttplc.com/ptt_webservice/CurrentOilPrice");
            SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.pttplc.com/webservice/pttinfo.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
            //displayMessage(resp, out);
            
     String oilprice = resp.getSOAPBody().getElementsByTagName("CurrentOilPriceResult").item(0).getFirstChild().getNodeValue();
            
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CallingPTTWS</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Calling PTT Web Service at " + request.getContextPath() + "</h1>");
            out.print(oilprice);
            out.println("</body>");
            out.println("</html>");
            
        } catch (Exception ex) {
        	ex.printStackTrace();
    	} finally {
        	out.close();
    	}
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
