<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$fromCur = $_GET['fromC'];
$toCur = $_GET['toC'];

$client = new SoapClient(
        'http://www.webservicex.net/CurrencyConvertor.asmx?WSDL', array(
        'SOAPAction' => 'http://www.webserviceX.NET/ConversionRate'
        )
);

$response = $client->ConversionRate(array('FromCurrency' => $fromCur, 'ToCurrency' => $toCur));

echo json_encode(array(
    "FromCurrency" => $fromCur,
    "ToCurrency" => $toCur,
    "rate" => $response->ConversionRateResult
  ));
