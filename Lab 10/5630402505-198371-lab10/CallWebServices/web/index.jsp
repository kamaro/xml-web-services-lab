<%-- 
    Document   : index2
    Created on : Nov 12, 2015, 5:27:37 PM
    Author     : VJunsone
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calling web service</title>
    </head>
    <body>
        <h1>Lab 10 : Calling Web Service</h1>
        <ul>
            <li><a href="CallingFeed">CallingFeed</a></li>
            <li><a href="CallingPTTWS">CallingPTTWS</a></li>
            <li><a href="CallingIKKU">CallingIKKU</a></li>
            <li><a href="CallingGoogleMap.html">CallingGoogleMap</a></li>
            <li><a href="BuildingJSON">BuildingJSON</a></li>
        </ul>
        
    </body>
</html>
