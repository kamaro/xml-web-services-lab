/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author VJunsone
 */
@WebServlet(name = "CallingGoogleMap", urlPatterns = {"/CallingGoogleMap"})
public class CallingGoogleMap extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String Addr = request.getParameter("add");
        URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?address=" + Addr + "&sensor=false&language=en");
        URLConnection con = url.openConnection();
        BufferedReader buf = new BufferedReader(new InputStreamReader(con.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String input = null;
        while ((input = (buf.readLine())) != null) {
            sb.append(input);
        }
        double lat = 0;
        double lng = 0;
        String map = "";
        JSONObject jObj = new JSONObject(sb.toString());
        JSONArray result = jObj.getJSONArray("results");
        for (int i = 0; i < result.length(); i++) {
            JSONObject Ob = result.getJSONObject(i);
            JSONObject geomatry = Ob.getJSONObject("geometry");
            lat = geomatry.getJSONObject("location").getDouble("lat");
            lng = geomatry.getJSONObject("location").getDouble("lng");
            map = "http://maps.google.com/?q=" + lat + "," + lng;
        }
        

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.print("<META HTTP-EQUIV='Refresh' CONTENT=1;URL='" + map + "'>");
            //out.print("<a href = '" + map + "'>"+ " ee" +"</a>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
