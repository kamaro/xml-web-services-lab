<html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Call Web Service</title>
    </head>
    <body>
        <h2>Lab 10:Calling and Building Web Service Using Java</h2>
    	<form action="CallingFeed">
        	<ul><li><a href="CallingFeed">Calling Feed</a></li></ul>
        </form>
        <form action="CallingPTTWS">
        	<ul><li><a href="CallingPTTWS">Calling PTT Web Service</a></li></ul>
        </form>
        <form action="CallingIKKU">
        	<ul><li><a href="CallingIKKU">Calling iKKU</a></li></ul>
        </form>
        <form action="CallingGoogleMap">
        	<ul><li><a href="CallingGoogleMap.html">Calling Google Map</a></li></ul>
        </form>
        <form action="BuildingJSON">
        	<ul><li><a href="BuildingJSON">Building JSON Data</a></li></ul>
        </form>
    </body>
</html>
