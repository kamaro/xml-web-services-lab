package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("    \t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Call Web Service</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <h2>Lab 10:Calling and Building Web Service Using Java</h2>\n");
      out.write("    \t<form action=\"CallingFeed\">\n");
      out.write("        \t<ul><li><a onclick=\"jsfunction()\" href=\"CallingFeed\">Calling Feed</a></li></ul>\n");
      out.write("        </form>\n");
      out.write("        <form action=\"CallingPTTWS\">\n");
      out.write("        \t<ul><li><a onclick=\"jsfunction()\" href=\"CallingPTTWS\">Calling PTT Web Service</a></li></ul>\n");
      out.write("        </form>\n");
      out.write("        <form action=\"CallingIKKU\">\n");
      out.write("        \t<ul><li><a onclick=\"jsfunction()\" href=\"CallingIKKU\">Caling iKKU</a></li></ul>\n");
      out.write("        </form>\n");
      out.write("        <form action=\"CallingGoogleMap\">\n");
      out.write("        \t<ul><li><a onclick=\"jsfunction()\" href=\"CallingGoogleMap.html\">Calling Google Map</a></li></ul>\n");
      out.write("        </form>\n");
      out.write("        <form action=\"BuildingJSON\">\n");
      out.write("        \t<ul><li><a onclick=\"jsfunction()\" href=\"BuildingJSON\">Building JSON Data</a></li></ul>\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
