/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.json.Json;
import javax.json.JsonArray;

/**
 *
 * @author Miki
 */
@WebServlet(name = "BuildingJSON", urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json; charset=UTF-8");

        JsonArray array = Json.createArrayBuilder()
                .add(Json.createObjectBuilder()
                        .add("- author",
                                Json.createObjectBuilder()
                                .add("petition","พร พิรุณ")
                                .add("meloday","อื้อ  สุนทรสนาน")
                        )
                        .add("title", "มาร์ช ม.ข.")
                        .add("content", "มหาวิทยาลัยขอนแก่นเกริกไกรวิทยา"))
                .add(Json.createObjectBuilder()
                        .add("- author",
                                Json.createObjectBuilder()
                                .add("petition","สุนทราภรณ์")
                                .add("meloday","สุนทราภรณ์")
                        )
                        .add("title", "เสียงสนครวญ")
                        .add("content", "เสียงยอดสนอ่อนอนพลิ้วโยนยอดไหว"))
                .build();

        try (PrintWriter out = response.getWriter()) {
            out.println(array);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
