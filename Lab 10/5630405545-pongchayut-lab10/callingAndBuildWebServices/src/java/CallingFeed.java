/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Miki
 */
@WebServlet(urlPatterns = {"/CallingFeed"})
public class CallingFeed extends HttpServlet {
String nameTitle = "";
String nameLink = "";   
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response){
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<h1>Servlet CallingFeed at " + request.getContextPath() + "</h1>");

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();

            Document xmlDoc = parser.parse("http://www.dailynews.co.th/rss/rss.xml");

            Element node = xmlDoc.getDocumentElement();
            followNode(node, out);
        }catch (Exception e) {
             System.err.println("Caught IOException: " + e.getMessage());
         }

    }
    
    private void followNode(Node node, PrintWriter out) {
        //writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            

            if(name.equals("title")){
                nameTitle = node.getFirstChild().getNodeValue();
            
            }
            if(name.equals("link")){
                nameLink = node.getFirstChild().getNodeValue();
                
            }
            if(!nameTitle.equals("") && !nameLink.equals("")){
                out.println("<a href=" + nameLink + ">" + nameTitle +"</a></br>");
                nameTitle = "";
                nameLink = ""; 
            }

            Node firstChild = node.getFirstChild();
            followNode(firstChild, out);
        }

        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode, out);
        }
//To change body of generated methods, choose Tools | Templates.
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    

}
