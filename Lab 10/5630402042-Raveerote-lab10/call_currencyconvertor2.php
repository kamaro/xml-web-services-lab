<?php

    $wsdl = 'http://webservicex.net/CurrencyConvertor.asmx?WSDL';
    $client = new SoapClient($wsdl);
    $methodName = 'ConversionRate';

    $params = array('FromCurrency' => $_GET["fromC"],'ToCurrency' => $_GET["toC"]);
    
    $soapAction = 'http://www.webserviceX.NET/ConversionRate';

    $objectResult = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));

    $rateResult = $objectResult->ConversionRateResult;

    $currency_data = array('fromCurrency' => 'THB','toCurrency' => 'USD','rate' => $rateResult);

    print json_encode($currency_data);




