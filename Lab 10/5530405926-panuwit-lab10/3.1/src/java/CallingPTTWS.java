
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

@WebServlet(urlPatterns = {"/CallingPTTWS"})
public class CallingPTTWS extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SOAPException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CallingPTTWS</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CalingFeed at/CallWebServices</h1>");
            
            MessageFactory mFac = MessageFactory.newInstance();
            
            SOAPMessage message = mFac.createMessage();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory soapFactory = SOAPFactory.newInstance();
           
            String prefix = "p";
            String namespace = "http://www.pttplc.com/ptt_webservice/";
            Name opName = soapFactory.createName("CurrentOilPrice", prefix, namespace);
            
            SOAPBodyElement opElem = body.addBodyElement(opName);
            SOAPElement usernameElem = opElem.addChildElement(soapFactory.createName("Language", prefix, namespace));
           
            usernameElem.addTextNode("th");  
            MimeHeaders header = message.getMimeHeaders();
            header.addHeader("SOAPAction", namespace + "CurrentOilPrice");
           
            SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.pttplc.com/webservice/pttinfo.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);
    
            Document doc = resp.getSOAPBody().getOwnerDocument();
            NodeList root = doc.getElementsByTagName("CurrentOilPriceResult");
            Element element = (Element) root.item(0);
            out.println(element.getFirstChild().getTextContent());
            
            out.println("</body>");
            out.println("</html>");
        }
    }


     
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}