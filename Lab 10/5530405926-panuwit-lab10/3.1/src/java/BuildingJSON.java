/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@WebServlet(urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            JSONArray a = new JSONArray();
            try
            {
                    JSONObject author = new JSONObject();
                    JSONObject title = new JSONObject();
                    JSONObject content = new JSONObject();
                    
                    author.put("petition", "พร ริรุณ");
                    author.put("meloday", "เอื้อ สุนทรสนาน");
                    title.put("title", "มาร์ช ม.ข.");
                    content.put("content", "มหาวิทยาลัยขอนแก่นเกริกไกรวิทยา");
                    a.put(author);
                    a.put(title);
                    a.put(content);          
                    author.put("petition", "สุนทราภรณ์");
                    author.put("meloday", "สุนทราภรณ์");
                    title.put("title","เสียงสนครวญ");
                    content.put("content", "เสียงยอดสนโอนโอนผิ้วโยนอ่อนไหว");
                    a.put(author);
                    a.put(title);
                    a.put(content);

            }
            catch (JSONException jse)
            { 

            }

            response.getWriter().write(a.toString());
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

 
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}