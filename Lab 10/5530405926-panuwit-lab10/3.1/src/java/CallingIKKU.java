import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.json.JSONArray;

@WebServlet(urlPatterns = {"/CallingIKKU"})
public class CallingIKKU extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String string = getContent("http://www.kku.ac.th/ikku/api/activities/services/topActivity.php");
            JSONObject js = new JSONObject(string);
            JSONArray arr = js.getJSONArray("activities");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CalingIKKU</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CalingFeed at/CallWebServices</h1>");

            for (int i = 0; i < arr.length(); i++)
            {
                out.println(arr.getJSONObject(i).getString("dateSt"));
                out.println("<a href=\""+arr.getJSONObject(i).getString("url")+"\">");
                out.println(arr.getJSONObject(i).getString("title"));
                out.println("</a>");
                out.println("<br/>");
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }
    
        private String getContent(String urlString) throws Exception {
            BufferedReader reader = null;
            try {
                URL url = new URL(urlString);
                reader = new BufferedReader(new InputStreamReader(url.openStream()));
                StringBuffer buffer = new StringBuffer();
                int read;
                char[] chars = new char[1024];
                while ((read = reader.read(chars)) != -1)
                    buffer.append(chars, 0, read); 

                return buffer.toString();
            } finally {
                if (reader != null)
                    reader.close();
            }
        }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CallingIKKU.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CallingIKKU.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
