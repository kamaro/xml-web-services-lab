/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet(urlPatterns = {"/CallingGoogleMap"})
public class CallingGoogleMap extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {

        String location = request.getParameter("location");
        try (PrintWriter out = response.getWriter()) {


            String url = "http://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(location) + "&sensor=false&language=en";
            
            
            String str = getContent(url);
            JSONObject j1 = new JSONObject(str);
            JSONArray arr = j1.getJSONArray("results"); 
            JSONObject j2 = arr.getJSONObject(0);
            JSONObject j3 = j2.getJSONObject("geometry");
            JSONObject output = j3.getJSONObject("location");
           

            response.sendRedirect("https://www.google.com/maps/place/" + output.get("lat") + "," + output.get("lng"));

        }
    }
    
    private String getContent(String urlString) throws Exception {
            BufferedReader reader = null;
            try {
                URL url = new URL(urlString);
                reader = new BufferedReader(new InputStreamReader(url.openStream()));
                StringBuffer buffer = new StringBuffer();
                int read;
                char[] chars = new char[1024];
                while ((read = reader.read(chars)) != -1)
                    buffer.append(chars, 0, read); 

                return buffer.toString();
            } finally {
                if (reader != null)
                    reader.close();
            }
        }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CallingGoogleMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CallingGoogleMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
