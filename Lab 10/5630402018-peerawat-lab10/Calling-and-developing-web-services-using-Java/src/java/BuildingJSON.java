/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author YeeMine
 */
@WebServlet(urlPatterns = {"/BuildingJSON"})
public class BuildingJSON extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        JSONArray json = new JSONArray();
        JSONObject ob = new JSONObject();
        JSONObject obIn = new JSONObject();
        JSONObject ob2 = new JSONObject();
        JSONObject ob2In = new JSONObject();
        
        json.put(ob);
        ob.put("autor", obIn);
        obIn.put("petution", "พร พิรุณ");
        obIn.put("meloday", "เอื้อ สุนทรสนาน");
        ob.put("content", "มหาวิทยาลัยขอนแก่นเกริกไกรวิทยา");
        ob.put("title", "มาร์ช ม.ข.");
        
        json.put(ob2);
        ob2.put("autor", ob2In);
        ob2In.put("petution", "สุนทราภรณ์");
        ob2In.put("meloday", "สุนทราภรณ์");
        ob2.put("title", "เสียงสนครวญ");
        ob2.put("content", "เสียงยอดสนอ่อนโอนพลิ้วโยนยอดไหว");
        try (PrintWriter out = response.getWriter()) {
            out.append(json.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
