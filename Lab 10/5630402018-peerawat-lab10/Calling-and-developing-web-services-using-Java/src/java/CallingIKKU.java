/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.*;

/**
 *
 * @author YeeMine
 */
@WebServlet(urlPatterns = {"/CallingIKKU"})
public class CallingIKKU extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        URL url = new URL("https://www.kku.ac.th/ikku/api/activities/services/topActivity.php");
        URLConnection connecDon = url.openConnection();
        HttpURLConnection hYp = (HttpURLConnection) connecDon;
        int responseCode = hYp.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            InputStream stream = hYp.getInputStream();
            //convert	web	content	to	string	
            String result = convertStreamToString(stream);
            JSONObject jsonObject = new JSONObject(result);
            JSONArray activities = jsonObject.getJSONArray("activities");
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet CalingIKKU</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Servlet CalingIKKU at " + request.getContextPath() + "</h1>");
                out.println("<table>");
                for (int i = 0; i < activities.length(); i++) {
                    out.println("<tr>");
                    out.println("<td>");
                    out.println(activities.getJSONObject(i).get("dateSt"));
                    out.println("</td>");
                    out.println("<td>");
                    out.println("<a href='" + activities.getJSONObject(i).get("url") + "'>" + activities.getJSONObject(i).get("title") + "</a>");
                    out.println("</td>");
                    out.println("</tr>");
                }
                out.println("</table>");

                out.println("</body>");
                out.println("</html>");
            }
        }
    }

    public String convertStreamToString(InputStream inputStream) throws IOException {
        if (inputStream != null) {
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,
                        "UTF-8"));
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            } finally {
                inputStream.close();
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}