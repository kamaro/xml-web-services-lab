<html lang="en">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#con").click(function () {
                var fromC = $("#fromC").val();
                var toC = $("#toC").val();
                var formData = {fromC:fromC, toC:toC};
                $.ajax({
                    url : "call_currencyconvertor2.php",
                    type: "GET",
                    dataType: 'json',
                    data: formData,
                     success: function(data, textStatus, jqXHR){
                        $("#result").val(data.rate);
                    },
                    error: function(jqXHR, textStatus, errorThrown){

                    }
                });
            });
        });

    </script>
    <head>
        <meta charset="utf-8">
        <title>Simple Calculator Web Application</title>
    </head>
    <body>
        <h1>Simple Calculator Web Application</h1>
        1
        <input type="text" id="fromC" size="3" maxlength="3">
        =
        <input type="text" id="result" size="8" maxlength="3" disabled="true">
        <input type="text" id="toC" size="3" maxlength="3">
        <input type="submit" class="button" id="con" value="Convertor">
    </body>
</html>