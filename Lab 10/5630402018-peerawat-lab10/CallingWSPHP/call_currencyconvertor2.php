<?php

$fromC = "THB";
$toC = "USD";

$client = new SoapClient(
	'http://www.webservicex.net/CurrencyConvertor.asmx?WSDL', 
	array(
		'SOAPAction' => 'http://www.webserviceX.NET/ConversionRate'
		)
	);
$response = $client->ConversionRate(array('FromCurrency' => $fromC, 'ToCurrency' => $toC));

echo json_encode( array(
"FromCurrency" => $fromC,
"ToCurrency" => $toC,
"rate" => $response->ConversionRateResult
));
?>