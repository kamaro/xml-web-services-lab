<?php

$fromC = filter_input(INPUT_GET, 'fromC');
$toC = filter_input(INPUT_GET, 'toC');
if ($fromC != '' && $toC != '') {

    $wsdl = 'http://webservicex.net/CurrencyConvertor.asmx?WSDL';
    $client = new SoapClient($wsdl);
    $methodName = 'ConversionRate';
    $params = array('FromCurrency' => 'THB', 'ToCurrency' => 'USD');
    $soapAction = 'http://www.webserviceX.NET/ConversionRate';
    $objectResult = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));
    
    $response = $objectResult->ConversionRateResult;

    echo $response;
} else {
    echo '';
}