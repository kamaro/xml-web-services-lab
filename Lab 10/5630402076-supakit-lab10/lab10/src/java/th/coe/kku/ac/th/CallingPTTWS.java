/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.coe.kku.ac.th;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author thinkearth
 */
@WebServlet(urlPatterns = {"/CallingPTTWS2"})
public class CallingPTTWS extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SOAPException, TransformerException, ParserConfigurationException, SAXException {
        response.setContentType("text/html;charset=UTF-8");
        String lang = "en";
        try (PrintWriter out = response.getWriter()) {
        	MessageFactory mFac = MessageFactory.newInstance();
        	SOAPMessage message = mFac.createMessage();
        	SOAPBody body = message.getSOAPBody();
        	SOAPFactory soapFactory = SOAPFactory.newInstance();
                String prefix = "ns";
        	String namespace = "http://www.pttplc.com/ptt_webservice/";
        	Name opName = soapFactory.createName("CurrentOilPrice",prefix, namespace);
        	SOAPBodyElement opElem = body.addBodyElement(opName);
        	SOAPElement ip = opElem.addChildElement(
                soapFactory.createName("Language", prefix, namespace));
                ip.addTextNode(lang);
        	MimeHeaders header = message.getMimeHeaders();
        	header.addHeader("SOAPAction", namespace + "CurrentOilPrice");
        	SOAPConnection soapConn = SOAPConnectionFactory.newInstance().createConnection();
        	String endpoint = "http://www.pttplc.com/webservice/pttinfo.asmx";
        	SOAPMessage resp = soapConn.call(message, endpoint);
                
                Document Doc = toDocument(resp);
                
                Element rootElm = Doc.getDocumentElement();
                NodeList nodetex = rootElm.getElementsByTagName("CurrentOilPriceResult");
                Node  nameNode = nodetex.item(0);
                Element nodeChild = (Element)nameNode;
                String getData = nodeChild.getTextContent();
                
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document xmlDoc = builder.parse( new InputSource( new StringReader(getData)));
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CallingPTTWS2</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CallingPTTWS2 at " + request.getContextPath() + "</h1>");
                NodeList itemsList = xmlDoc.getElementsByTagName("DataAccess");
                int itemsCount = itemsList.getLength();
                for(int i=0; i<=itemsCount; i++){
                    Node itemNode = itemsList.item(i);
                    Element item = (Element) itemNode;
                    String allData = item.getTextContent();
                    out.println(allData);
                    if((i+1)%3==0){
                        out.println("<br/>");
                    }
                }
            out.println("</body>");
            out.println("</html>");          
        }
    }   
    public static Document toDocument(SOAPMessage soapMsg)   
        throws TransformerConfigurationException, TransformerException, SOAPException, IOException {  
        Source src = soapMsg.getSOAPPart().getContent();  
        TransformerFactory tf = TransformerFactory.newInstance();  
        Transformer transformer = tf.newTransformer();  
        DOMResult result = new DOMResult();  
        transformer.transform(src, result);  
        return (Document)result.getNode();  
  }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SOAPException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(CallingPTTWS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}