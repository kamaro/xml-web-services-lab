/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7;

import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author Admin
 */
public class ProfileWriter {

    public static void main(String[] args) throws Exception {
        String fileName = "myProfile.xml";
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(new FileWriter(fileName)));;
        xtw.writeStartDocument("utf-8", "1.0");
        xtw.setPrefix("", "http://www.kku.ac.th/puntawat");
        xtw.writeStartElement("http://www.kku.ac.th/puntawat", "profile");
        xtw.writeNamespace("", "http://www.kku.ac.th/puntawat");
        xtw.writeAttribute("id", "th");
        xtw.writeCharacters("My profile");
        xtw.writeStartElement("http://www.kku.ac.th/puntawat", "name");
        xtw.writeAttribute("id", "563040558-7");
        xtw.writeCharacters("Puntawat Klongnawang");
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
        System.out.println("Finish writing myProfile.xml");
    }
}
