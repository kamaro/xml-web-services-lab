/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author Admin
 */
public class VisitNodes {

    public static void main(String[] args) throws MalformedURLException, IOException, XMLStreamException {

        try {
            File file = new File("src/lab7/nation.xml");
            InputStream in = new FileInputStream(file);
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(in);

            for (int event = parser.next();
                    event != XMLStreamConstants.END_DOCUMENT;
                    event = parser.next()) {
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        System.out.println("Element name = " + parser.getLocalName());
                        if (parser.getAttributeCount() >= 1) {
                            System.out.println("ATTRIBUTE : " + parser.getAttributeName(0) + " = " + parser.getAttributeValue(0));
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:

                        break;

                    case XMLStreamConstants.CHARACTERS:
                        System.out.println("#Text : " + parser.getText());
                        break;

                } // end switch
            } // end while
            parser.close();
        } catch (XMLStreamException ex) {
            System.out.println(ex);
        } catch (IOException ex) {

        }

    }
}
