<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:t="http://www.kku.ac.th" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output method="html" indent="yes"/>
    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="t:catalog">
        <html>
            <head>
                <title>catalog.xsl</title>
            </head>
            <body>
                <h1>My CD Collection</h1>
                <table border="0">
                    <tr bgcolor="#9acd32">
                        <th align="left">Title               
                </th>
                        <th align="left">Artist                    
                </th>
                    </tr>
                     
                    <xsl:for-each select="t:cd">
                       
                        <tr>
                            <td>
                                <xsl:value-of select="t:title"/>
                            </td>
                            <td>
                                <xsl:value-of select="t:artist"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
