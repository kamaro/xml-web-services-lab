<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $doc = new DOMDocument();
        $xsl = new XSLTProcessor();
        $xsl_filename = "catalog.xsl";
        $doc->load($xsl_filename);
        $xsl->importStyleSheet($doc);
        $xml_filename = "catalog.xml";
        $doc->load($xml_filename);
        echo $xsl->transformToXML($doc);
        
        ?>
    </body>
</html>
