<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:kku="http://www.kku.ac.th" 
                version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="kku:catalog">
        <html>
            <head>
                <title>Catalog</title>
            </head>
            <body>
                <h2>My CD Collection</h2>
                <table border="0">
                    <tr bgcolor="#9acd32">
                        <th align="left">Title</th>
                        <th align="left">Artist</th>
                    </tr>
                        <xsl:for-each select="kku:cd">

                        <tr>
                            <td>
                                <xsl:value-of select="kku:title"/>
                            </td>
                            <td>
                                <xsl:value-of select="kku:artist"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
