package staxlab8;

import java.io.FileWriter;
import java.io.IOException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class ProfileWriter {
    public static void main(String[] args) {
        try {
            XMLOutputFactory xmlfactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xmlprofile = xmlfactory.createXMLStreamWriter(new FileWriter(ProfileWriter.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "myProfile.xml"));
            
            xmlprofile.writeStartDocument();
            xmlprofile.writeStartElement("profile");
            xmlprofile.writeStartElement("name");
            xmlprofile.writeCharacters("Pichaichan kanjanasorn");
            xmlprofile.writeEndElement();
            xmlprofile.writeEndElement();
            xmlprofile.writeEndDocument();

            xmlprofile.flush();
            xmlprofile.close();
        }catch (IOException | XMLStreamException ex) {
        }
    }
    
}
