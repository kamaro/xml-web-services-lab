package staxlab8;

import java.io.*;
import javax.xml.stream.*;


public class GetRootElem {
    public static void main(String[] args) {
        try {
            FileInputStream in = new FileInputStream("nation.xml");
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(in);
            parser.next();
            System.out.println("The root element is " + parser.getName());
            parser.close();
                 } catch(FileNotFoundException | XMLStreamException e){
        }
    }
}
