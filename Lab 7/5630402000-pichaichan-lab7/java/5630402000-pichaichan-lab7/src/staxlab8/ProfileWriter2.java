package staxlab8;

import java.io.FileWriter;
import java.io.IOException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class ProfileWriter2 {
    
    private static final String kku = "http://www.kku.ac.th";
    
    public static void main(String[] args) {
        try {
            XMLOutputFactory xmlfactory = XMLOutputFactory.newInstance();
            XMLStreamWriter xmlprofile = xmlfactory.createXMLStreamWriter(new FileWriter(ProfileWriter.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "myProfile2.xml"));
            
            xmlprofile.writeStartDocument();
            xmlprofile.setPrefix("", kku);
            {
                xmlprofile.writeStartElement(kku, "ProfileWriter2");
                xmlprofile.setDefaultNamespace(kku);
                xmlprofile.writeNamespace("K", kku);
                {
                    xmlprofile.writeCharacters("My name is");
                    xmlprofile.writeStartElement("name");
                    xmlprofile.writeAttribute(kku, "id", "563040200-0");
                    {
                        xmlprofile.writeCharacters("Pichaichan kanjanasorn");
                    }
                    xmlprofile.writeEndElement();
                    xmlprofile.writeCharacters("Nickname is Earth");
                    xmlprofile.writeStartElement("age");
                    {
                        xmlprofile.writeCharacters("20");
                    }
                    xmlprofile.writeEndElement();
                    xmlprofile.writeStartElement(kku,"Address");
                    xmlprofile.writeAttribute("Country", "thai");
                    {
                        xmlprofile.writeCharacters("Kalasin:Province");
                    }
                    xmlprofile.writeEndElement();
                    
                }xmlprofile.writeEndElement();
            }xmlprofile.writeEndDocument();

            xmlprofile.flush();
            xmlprofile.close();
        }catch (IOException | XMLStreamException ex) {
        }
    }
}
