package staxlab8;

import java.io.*;
import javax.xml.stream.*;

public class VisitNodes {
     public static void main(String[] args) {
        try {
            FileInputStream in = new FileInputStream("nation.xml");
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(in);
            //start
            for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
                if (event == XMLStreamConstants.START_ELEMENT) {
                        System.out.println("Element name = " + parser.getName());
                        if(parser.getAttributeCount() !=0)
                            for(int i = 0; i < parser.getAttributeCount(); i++){
                                System.out.println("Attribute name: " + parser.getAttributeLocalName(i) + " = " + parser.getAttributeValue(i));
                            }
                }
              //if (event == XMLStreamConstants.END_ELEMENT) {}
                if (event == XMLStreamConstants.CHARACTERS) {
                            System.out.println("#Text = " + parser.getText());
                }
            //    if (event == XMLStreamConstants.CDATA) {
            //                System.out.println("#Text = " + parser.getText());
            //    }
            }
            parser.close();
                 } catch(FileNotFoundException | XMLStreamException e){
        }
    }
}
