package pkg5630405529.prat.lab7.Transforming_an_XML_document_using_a_Java_program;

import java.io.File;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class myTransformer {

    public static void main(String[] args) {
        try {
            TransformerFactory factory =
                    TransformerFactory.newInstance();
            Source xslt =
                    new StreamSource(new File("src/pkg5630405529/prat/lab7/catalog.xsl"));
            Transformer transformer =
                    factory.newTransformer(xslt);
            Source text =
                    new StreamSource(new File("src/pkg5630405529/prat/lab7/catalog.xml"));
            transformer.transform(text,
                    new StreamResult(new File("src/pkg5630405529/prat/lab7/Transforming_an_XML_document_using_a_Java_program/catalog.html")));

        } catch (TransformerException ex) {
            ex.printStackTrace(System.err);
        }

    }
}
