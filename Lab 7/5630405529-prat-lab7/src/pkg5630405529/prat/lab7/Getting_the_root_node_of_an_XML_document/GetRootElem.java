package pkg5630405529.prat.lab7.Getting_the_root_node_of_an_XML_document;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class GetRootElem {

    public static void main(String args[]) throws FileNotFoundException,
            XMLStreamException, FactoryConfigurationError {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream in = new FileInputStream("src/pkg5630405529/prat/lab7/nation.xml");
        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                System.out.println("The root element is " + startElement.getName());
                break;
            }
        }
    }
}