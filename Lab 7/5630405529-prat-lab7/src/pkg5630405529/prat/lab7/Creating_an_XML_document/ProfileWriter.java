package pkg5630405529.prat.lab7.Creating_an_XML_document;

import java.io.FileWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class ProfileWriter {
        private static final String Profile = "http://www.kku.ac.th/Profile";
    public static void main(String args[]) throws Exception {
        String fileName = "src/pkg5630405529/prat/lab7/Creating_an_XML_document/myProfile.xml";
        XMLOutputFactory out = XMLOutputFactory.newInstance();
        XMLStreamWriter write = out.createXMLStreamWriter(new FileWriter(fileName));
        
        write.writeStartDocument();
        write.writeStartElement("Profile");
        write.setDefaultNamespace(Profile);
        write.writeNamespace("n", Profile);
        write.writeCharacters("I'm");
        write.writeStartElement("name");
        write.writeAttribute("id", "563040552-9");
        write.writeCharacters("Prat Chantharamanee");
        write.writeEndElement();
        write.writeEndElement();
        write.flush();
        write.close();
        
        System.out.println("Finish write myProfile.xml");
        
    }
}
