package pkg5630405529.prat.lab7.Traversing_an_XML_document;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
public class VisitNodes {

    public static void main(String args[]) throws FileNotFoundException,
            XMLStreamException, FactoryConfigurationError {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream in = new FileInputStream("src/pkg5630405529/prat/lab7/nation.xml");
        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            if (event.getEventType()
                    == XMLEvent.CHARACTERS) {
                Characters chars = event.asCharacters();
                System.out.println("#text = "
                        + chars.getData());
            }
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                System.out.println("Element name = " + startElement.getName());
                Iterator it = startElement.getAttributes();
                while (it.hasNext()) {
                    Attribute attr = (Attribute) it.next();
                    System.out.println("Attribute: " + attr.getName() + " = "+ attr.getValue());}
            }
        }
    }
}