
<?php
$xml = new DOMDocument;
$xml->load('catalog.xml');
$xsl = new DOMDocument;
$xsl->load('catalog.xsl');
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl);
echo $proc->transformToXML($xml);
?>
