<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:c="http://www.kku.ac.th">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>catalog.xsl</title>
            </head>
            <body>
                <h2>My CD Collection</h2>
                <table border="0">
                    <tr bgcolor = "#9acd32">
                        <th align = "left">Title</th>
                        <th align = "left">Artist</th>
                    </tr>
                    <xsl:for-each select="c:catalog/c:cd">
                        <tr>
                        <td><xsl:value-of select="c:title"/></td>
                        <td><xsl:value-of select="c:artist"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
