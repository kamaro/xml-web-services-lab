/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import javax.xml.stream.*;
import java.io.*;


/**
 *
 * @author user
 */
public class GetRoomElem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            FileInputStream in = new FileInputStream("src/staxlab7/nation.xml");
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(in);
            parser.next();
            System.out.println("The root element is " + parser.getLocalName());
            parser.close();
        } catch (XMLStreamException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println("IOException while parsing ");
        } 
    } 

}
