/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import javax.xml.stream.*;
import java.io.*;

/**
 *
 * @author user
 */
public class VisitNodes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            FileInputStream in = new FileInputStream("src/staxlab7/nation.xml");
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(in);

            while (parser.hasNext()) {
                if (parser.isStartElement()) {
                    System.out.println("Element name = " + parser.getLocalName());
                    if (parser.getAttributeCount() != 0) {
                        for (int i = 0; i != parser.getAttributeCount(); i++) {
                            System.out.println("Attribute: " + parser.getAttributeLocalName(i)
                                    + " = " + parser.getAttributeValue(i));
                        } 
                    } parser.next();
                } else if (parser.isCharacters()) {
                    System.out.println("text# = " + parser.getText());
                    parser.next();
                } else {
                    parser.next();
                }
            }

        } catch (XMLStreamException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println("IOException while parsing ");
        }
    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
