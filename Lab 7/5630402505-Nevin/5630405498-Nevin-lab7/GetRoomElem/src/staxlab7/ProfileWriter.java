/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import javax.xml.stream.*;
import java.io.*;

/**
 *
 * @author user
 */
public class ProfileWriter {
    private static final String NS = "http://www.w3.org/1999/xhtml";

    public static void main(String[] args) {
        try {
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            XMLStreamWriter xtw = xof.createXMLStreamWriter(new FileWriter("src/staxlab7/myProfile.xml"));

            xtw.writeStartDocument();
            xtw.writeStartElement("profile");
            xtw.setDefaultNamespace(NS);
            xtw.writeNamespace("", NS);
            xtw.writeStartElement("name");
            xtw.writeAttribute("id","549-8");
            xtw.writeCharacters("My Name is");
            xtw.writeStartElement("First");
            xtw.writeCharacters("Nevin");
            xtw.writeEndElement();
            xtw.writeStartElement("Last");
            xtw.writeCharacters("Ruttanaboonta");
            xtw.writeEndElement();
            xtw.writeEndElement();
            xtw.writeEndElement();
            xtw.flush();
            xtw.close();
            System.out.println("Done");

        } catch (XMLStreamException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println("IOException while parsing ");
        }

    }
}
