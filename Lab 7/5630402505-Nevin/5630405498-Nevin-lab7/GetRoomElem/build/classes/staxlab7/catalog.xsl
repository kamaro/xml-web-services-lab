<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : October 15, 2015, 2:09 PM
    Author     : user
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:ns ="http://www.kku.ac.th">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Catalog Table</title>
            </head>
            <body>
                
                <h2>My CD Collection</h2>
                
                <table border="1">
                    <tr bgcolor="#9acd3b">
                        <th align="left">Title</th>
                        <th align="left">Artist</th>
                    </tr>
                    
                    <xsl:for-each select="ns:catalog/ns:cd">
                        <xsl:sort select="ns:title"/>
                        <tr>
                            <td>
                                <xsl:value-of select="ns:title"/>
                            </td>
                            <td>
                                <xsl:value-of select="ns:artist"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                    
                </table>
            </body>
        </html>
    </xsl:template>


</xsl:stylesheet>
