<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $xmlDoc = new DOMDocument();
        $xmlDoc->load('catalog.xml');
        
        $xslDoc = new DOMDocument();
        $xslDoc->load('catalog.xsl');
        
        $process = new XSLTProcessor();
        $process->importStylesheet($xslDoc);
        echo $process->transformToXML($xmlDoc);
	
        ?>
    </body>
</html>
