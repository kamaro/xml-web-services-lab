<?php
$xslDoc = new DOMDocument();
$xslDoc->load("catalog.xsl");

$xmlDoc = new DomDocument();
$xmlDoc->load("catalog.xml");

$proc = new XSLTProcessor();
$proc->importStylesheet($xslDoc);
$trans = $proc->transformToDoc($xmlDoc);
$trans->saveHTMLFile("catalog.html");
echo $trans->saveXML();
?>
