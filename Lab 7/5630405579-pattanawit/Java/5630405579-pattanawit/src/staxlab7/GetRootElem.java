package staxlab7;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

/**
 * @author Amilaz
 */
public class GetRootElem {

    public static void main(String[] args) {
        try {
            File xml_file = new File("src/staxlab7/nation.xml");
            InputStream in = new FileInputStream(xml_file);
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(in);
            parser.next();
            String parserString = parser.getLocalName();
            System.out.println("The root element is " + parserString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
