package staxlab7;

import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;

/*
 * @author Amilaz
 */
public class ProfileWriter {

    public static void main(String args[]) throws IOException, XMLStreamException {
        final String nameSpace = "http://gear.kku.ac.th/pattnawit";
        final String XHTML = "http://www.w3.org/1999/xhtml";
        String filename = "src/staxlab7/myProfile.xml";
        XMLOutputFactory outFac = XMLOutputFactory.newInstance();
        XMLStreamWriter xmlWriter = new IndentingXMLStreamWriter(outFac.createXMLStreamWriter(new FileWriter(filename)));
        xmlWriter.writeStartDocument();
        xmlWriter.writeStartElement("Profile");
        xmlWriter.writeNamespace("", nameSpace);
        xmlWriter.setDefaultNamespace(XHTML);
        xmlWriter.writeAttribute("id", "5630405579");
        xmlWriter.writeCharacters("About Me");
        xmlWriter.writeStartElement("name");
        xmlWriter.writeCharacters("Pattanawit Wannoot");
        xmlWriter.writeEndElement();
        xmlWriter.writeEndElement();
        xmlWriter.writeEndDocument();
        xmlWriter.flush();
        xmlWriter.close();
        System.out.println("Finish writing myProfile.xml");

    }
}
