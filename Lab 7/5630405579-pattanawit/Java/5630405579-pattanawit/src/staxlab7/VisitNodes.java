package staxlab7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/*
 * @author Amilaz
 */
public class VisitNodes {

    public static void main(String[] args) throws FileNotFoundException, XMLStreamException {
        File xml_file = new File("src/staxlab7/nation.xml");
        InputStream in = new FileInputStream(xml_file);
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader parser = factory.createXMLEventReader(in);
        while (parser.hasNext()) {
            XMLEvent e = parser.nextEvent();
            if (e.getEventType() == XMLEvent.CHARACTERS) {
                Characters chars = e.asCharacters();
                if(!chars.isWhiteSpace()){
                    System.out.println("#text = " + chars.getData());
                }
            }
            if (e.getEventType() == XMLEvent.START_ELEMENT) {
                StartElement startE = e.asStartElement();
                System.out.println("Element name : " + startE.getName());
                Iterator it = startE.getAttributes();
                while (it.hasNext()) {
                    Attribute attr = (Attribute) it.next();
                    System.out.println("Attribute: " + attr.getName() + " = "
                            + attr.getValue());
                }
            }
        }
    }

}
