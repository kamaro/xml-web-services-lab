<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : 15 October 2015, 21:04
    Author     : Amilaz
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:k="http://www.kku.ac.th">
    <xsl:output method="html"/>
    <xsl:template match="k:catalog">
        <html>
            <body>
                <h2>My CD Collection</h2>
                <table border="0">
                    <tr bgcolor="#9acd32">
                        <th align="left">Title</th>
                        <th align="left">Artist</th>
                    </tr>
                    <xsl:for-each select="k:cd">
                         <tr>     
                                <td>
                                    <xsl:value-of select="k:title"/>
                                </td>
                                <td>
                                    <xsl:value-of select="k:artist"/>
                                </td> 
                        </tr>
                    </xsl:for-each>
                         
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
