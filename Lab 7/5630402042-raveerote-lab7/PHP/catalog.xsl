<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : October 15, 2015, 8:09 PM
    Author     : golfyzzz
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:ss="http://www.kku.ac.th">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>catalog.xml</title>
            </head>
            <body>
                <h2>My CD Collection</h2>
                <table border="0">
                    <tr bgcolor="#9acd32">
                        <th align="left">Title</th>
                        <th align="left">Artist</th>
                    </tr>
                    <xsl:for-each select="ss:catalog/ss:cd">
                            <tr>
                                <td>
                                    <xsl:value-of select="ss:title"/>
                                </td>
                                <td>
                                    <xsl:value-of select="ss:artist"/>
                                </td>
                            </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
