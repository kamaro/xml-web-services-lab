/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import java.io.FileWriter;
import java.io.IOException;
import javax.xml.stream.*;

/**
 *
 * @author golfyzzz
 */
public class ProfileWriter {
    private static final String namespace = "http://www.gear.kku.ac.th";

    public static void main(String[] args) throws XMLStreamException, IOException {
        String fileName = "src/staxlab7/myProfile.xml";
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new FileWriter(fileName));
        xtw.writeStartDocument();
        xtw.writeStartElement("profile");
        xtw.setDefaultNamespace(namespace);
        xtw.writeNamespace("", namespace);
        xtw.writeStartElement("name");
        xtw.writeAttribute("id", "563040204-2");
        xtw.writeCharacters("Raveerote");
        xtw.writeStartElement("lastName");
        xtw.writeCharacters("Hansakwiteekul");
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
        System.out.println("Finish writing myProfile.xml");
    }
}
