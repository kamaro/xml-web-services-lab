/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;



/**
 *
 * @author golfyzzz
 */
public class GetRootElem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, XMLStreamException {
        String xml = "src/staxlab7/nation.xml";
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream input = new FileInputStream(new File(xml));
        XMLStreamReader xmlStreamReader = inputFactory.createXMLStreamReader(input);
        xmlStreamReader.next();
        System.out.println("The root element is " + xmlStreamReader.getLocalName());
     
        }
}
