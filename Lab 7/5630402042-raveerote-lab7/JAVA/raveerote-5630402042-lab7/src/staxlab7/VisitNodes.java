/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import java.io.*;
import javax.xml.stream.*;

/**
 *
 * @author golfyzzz
 */
public class VisitNodes {
     public static void main(String[] args) throws FileNotFoundException, XMLStreamException {
        String xml = "src/staxlab7/nation.xml";
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream input = new FileInputStream(new File(xml));
        XMLStreamReader xmlStreamReader = inputFactory.createXMLStreamReader(input);
        while (xmlStreamReader.hasNext()){
        if (xmlStreamReader.next() == XMLStreamConstants.START_ELEMENT){
            System.out.println("Element name = " + xmlStreamReader.getLocalName());
            if(xmlStreamReader.getAttributeCount() != 0)
                System.out.println("Attribute: " + xmlStreamReader.getAttributeLocalName(0) + " = " + xmlStreamReader.getAttributeValue(0));
        }
        else if(xmlStreamReader.isCharacters())
            System.out.println("#text = " + xmlStreamReader.getText());
        }
    }
}
