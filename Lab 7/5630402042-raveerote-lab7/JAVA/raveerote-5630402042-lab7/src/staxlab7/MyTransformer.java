/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author golfyzzz
 */
public class MyTransformer {
       public static void main(String[] args) throws TransformerException {
        StreamSource xmlS = new StreamSource("src/staxlab7/catalog.xml");
        StreamSource xslS = new StreamSource("src/staxlab7/catalog.xsl");
        StreamResult htmlS = new StreamResult("src/staxlab7/catalog.html");
        
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer tran = tf.newTransformer(xslS);
        tran.transform(xmlS, htmlS);
        
    }
    
}

