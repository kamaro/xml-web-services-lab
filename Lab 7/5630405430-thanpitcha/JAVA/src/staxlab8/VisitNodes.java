/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab8;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author fern
 */
public class VisitNodes {
    public static void main(String[] args) throws XMLStreamException {

        try {
            FileInputStream input = new FileInputStream("src/lab7/nation.xml");
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(input);
           
            for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
               
                    if(event== XMLStreamConstants.START_ELEMENT){
                        System.out.println("Element name = " + parser.getName());
                        if(parser.getAttributeCount() !=0)
                            for(int i = 0; i < parser.getAttributeCount(); i++){
                                System.out.println("Attribute name: " + parser.getAttributeLocalName(i) + " = " + parser.getAttributeValue(i));
                            }}
                        
                    if(event==XMLStreamConstants.END_ELEMENT){}
                        
                    if(event==XMLStreamConstants.CHARACTERS){
                            System.out.println("#Text = " + parser.getText());
                    }
                   if(event== XMLStreamConstants.CDATA){
                            System.out.println("#Text = " + parser.getText());
                   
                } 
            } 
            parser.close();
                 } catch(FileNotFoundException | XMLStreamException e){
        }
}
}
