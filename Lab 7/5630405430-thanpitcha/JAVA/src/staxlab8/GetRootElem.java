/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab8;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author fern
 */
public class GetRootElem {
   public static void main(String[] args) {
            try {
            FileInputStream text = new FileInputStream("src/lab7/nation.xml");
            XMLInputFactory inputfactory = XMLInputFactory.newInstance();
            XMLStreamReader parser = inputfactory.createXMLStreamReader(text);
            
            parser.next();
            System.out.println("The root element is " + parser.getName());
            parser.close();
                 } catch(FileNotFoundException | XMLStreamException e){
        }
   } 
}
