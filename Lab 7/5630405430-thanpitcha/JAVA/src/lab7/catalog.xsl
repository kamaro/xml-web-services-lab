<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : October 22, 2015, 7:12 PM
    Author     : fern
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:catalog = "http://www.kku.ac.th">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="catalog:catalog">
        <html>
            
            <body>
                <h2>My CD Collection</h2>
                <table border="0">
                    <tr bgcolor="#9acd32">
                        <th align="left">Title</th>
                        <th align="left">Artist</th>
                    </tr>
                    <xsl:for-each select="catalog:cd">
                    <tr>
                        <td><xsl:value-of select="catalog:title"/></td>
                        <td><xsl:value-of select="catalog:artist"/></td>
                    </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
