<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$xmlDoc = 'catalog.xml';
$xslDoc = 'catalog.xsl';
$doc = new DOMDocument();
$xsl = new XSLTProcessor();
$doc->load($xslDoc);
$xsl->importStylesheet($doc);
$doc->load($xmlDoc);
echo $xsl->transformToXml($doc);

