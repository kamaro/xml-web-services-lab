<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" 
                xmlns:k="http://www.kku.ac.th" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <body>
                <h2>My CD Collection</h2>
                <table border="0">
                    <tr bgcolor="#9acd32">
                        <th align="left" color="green">Title</th>
                        <th align="left">Artist</th>
                    </tr>
                    <xsl:for-each select="k:catalog/k:cd">
                        <tr>
                            <td>
                                <xsl:value-of select="k:title"/>
                            </td>
                            <td>
                                <xsl:value-of select="k:artist"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
