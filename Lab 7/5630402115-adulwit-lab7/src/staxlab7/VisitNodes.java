/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author Adul7
 */
public class VisitNodes {

    public static void main(String[] args) throws FileNotFoundException, XMLStreamException {

        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream input = new FileInputStream(new File("src/staxlab7/nation.xml"));
        XMLStreamReader xmlStreamReader = inputFactory.createXMLStreamReader(input);

        while (xmlStreamReader.hasNext()) {
            int event = xmlStreamReader.next();
            //System.out.println();
            if (event == XMLStreamConstants.START_ELEMENT) {
                System.out.println("Element name = " + xmlStreamReader.getLocalName());
                //System.out.println(xmlStreamReader.getAttributeCount());
                if (xmlStreamReader.getAttributeCount() > 0) {
                    System.out.println("Attribute: " + xmlStreamReader.getAttributeName(0) + " = " + xmlStreamReader.getAttributeValue(0));
                }
            }

            if (event == XMLStreamConstants.CHARACTERS) {
                if (!xmlStreamReader.getText().trim().equals("")) {
                    System.out.println("#text = " + xmlStreamReader.getText().trim());
                }
            }

        }
    }
}
