/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

public class ProfileWriter {

    public static void main(String[] args) throws FileNotFoundException, XMLStreamException {

        try {
            String source = "src/staxlab7/";
            String fileName = "myProfile.xml";

            XMLOutputFactory xof = XMLOutputFactory.newInstance();

            XMLStreamWriter xtw = xof.createXMLStreamWriter(new FileWriter(source + fileName));

            xtw.writeStartDocument();
            xtw.writeStartElement("profile");
            xtw.writeStartElement("name");
            xtw.writeCharacters("Adulwit Chinapas");
            xtw.writeEndElement();
            xtw.writeEndElement();
            xtw.writeEndDocument();

            xtw.flush();
            xtw.close();
            System.out.println("Finish writing " + fileName);
        } catch (Exception ex) {
            System.err.println("Exception occurred whilerunning writer1");
            ex.printStackTrace();
        }

    }
}
