/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class GetRootElem {

    public static void main(String[] args) throws FileNotFoundException, XMLStreamException {

        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream input = new FileInputStream(new File("src/staxlab7/nation.xml"));
        XMLStreamReader xmlStreamReader = inputFactory.createXMLStreamReader(input);
        int event2 = xmlStreamReader.next();
        if (event2 == XMLStreamConstants.START_ELEMENT) {
            System.out.println("The root element is " + xmlStreamReader.getLocalName());
        }
    }
}
