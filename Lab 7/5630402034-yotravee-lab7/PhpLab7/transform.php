<?php

$xmlDoc = new DOMDocument;
$xmlDoc->load('catalog.xml');

$xslDoc = new DOMDocument;
$xslDoc->load('catalog.xsl');

$prog = new XSLTProcessor;
$prog->importStyleSheet($xslDoc);

echo $prog->transformToXML($xmlDoc);

?>