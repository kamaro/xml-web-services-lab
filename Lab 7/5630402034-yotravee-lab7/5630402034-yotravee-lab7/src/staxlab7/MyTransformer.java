package staxlab7;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class MyTransformer {
    public static void main(String[] args) throws TransformerConfigurationException, TransformerException {
        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();

            Source xmlFile = new StreamSource("src/staxlab7/catalog.xml");
            Source xslFile = new StreamSource("src/staxlab7/catalog.xsl");

            String htmlFileName = "src/staxlab7/catalog.html";

            OutputStream htmlFile = new FileOutputStream(htmlFileName);
            Transformer trasform = tFactory.newTransformer(xslFile);
            trasform.transform(xmlFile, new StreamResult(htmlFile));
        } catch (FileNotFoundException | TransformerFactoryConfigurationError e) {
            System.out.print(e);
        } catch (TransformerConfigurationException e) {
            System.out.print(e);
        } catch (TransformerException e) {
            System.out.print(e);
        }
    }
}
