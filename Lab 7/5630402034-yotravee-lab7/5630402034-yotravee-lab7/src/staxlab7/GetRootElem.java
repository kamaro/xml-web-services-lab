package staxlab7;

import java.io.FileInputStream;
import java.io.IOException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class GetRootElem {

    public static void main(String[] args) throws XMLStreamException {
        try {
            String file = "src/staxlab8/nation.xml";
            FileInputStream input = new FileInputStream(file);
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader r = factory.createXMLStreamReader(input);
            r.next();
            System.out.println("The root element is " + r.getName());
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
