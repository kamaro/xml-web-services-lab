<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:k="http://www.kku.ac.th" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>
    <xsl:template match="/k:catalog">
        <html>
            <body>
                <h2>My CD Collection</h2>
                    <table>
                    <xsl:attribute name="border">0</xsl:attribute>
                    <tr>
                        <xsl:attribute name="bgcolor">#9acd32</xsl:attribute>
                        <th>
                            <xsl:attribute name="align">left</xsl:attribute>
                            Title
                        </th>
                        <th>
                            <xsl:attribute name="align">left</xsl:attribute>
                            Artist
                        </th>  
                    </tr>
                    <xsl:for-each select="k:cd">
                        <tr>
                            <td>
                                <xsl:value-of select="k:title"/>
                            </td>
                            <td>
                                <xsl:value-of select="k:artist"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>