package staxlab7;

import java.io.FileWriter;
import java.io.IOException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class ProfileWriter {

    public static void main(String[] args) throws XMLStreamException {
        String xmlFile = "myProfile.xml";
        try {
            XMLOutputFactory outputF = XMLOutputFactory.newInstance();
            XMLStreamWriter streamF = outputF.createXMLStreamWriter(new FileWriter("src/staxlab7/" + xmlFile));
            streamF.writeStartDocument();
            streamF.writeStartElement("profile");
            streamF.writeStartElement("name");
            streamF.writeCharacters("Yotravee Sanit-in");
            streamF.writeEndElement();
            streamF.writeEndElement();
            streamF.writeEndDocument();
            streamF.flush();
            streamF.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        System.out.println("Finish writing " + xmlFile);
    }
}
