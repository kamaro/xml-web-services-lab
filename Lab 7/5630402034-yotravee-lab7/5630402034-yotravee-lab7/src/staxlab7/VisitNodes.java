package staxlab7;

import java.io.FileInputStream;
import java.io.IOException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class VisitNodes {

    public static void main(String[] args) throws XMLStreamException {
        try {
            String file = "src/staxlab8/nation.xml";
            FileInputStream input = new FileInputStream(file);
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader r = factory.createXMLStreamReader(input);

            while (r.hasNext()) {
                int event = r.next();
                if (event == XMLStreamConstants.START_ELEMENT) {
                    System.out.println("Element name = " + r.getLocalName());
                    if(r.getAttributeCount()!= 0)
                        System.out.println("Attibute: " + r.getAttributeName(0) +" = "+ r.getAttributeValue(0));
                }
                if (event == XMLStreamConstants.CHARACTERS) {
                    if (!r.getText().trim().equals("")) {
                        System.out.println("#text = " + r.getText());
                    }
                }
                
                
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
