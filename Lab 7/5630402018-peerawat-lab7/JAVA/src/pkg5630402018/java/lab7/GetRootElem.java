package pkg5630402018.java.lab7;

import java.io.*;
import java.nio.file.FileSystems;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

public class GetRootElem {

    public static void main(String[] args) throws XMLStreamException, FileNotFoundException {
        FileInputStream file = new FileInputStream("src/pkg5630402018/java/lab7/nation.xml");
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader r = factory.createXMLStreamReader(file);
        r.next();
        System.out.println("The root element is " + r.getName());

    }
}
