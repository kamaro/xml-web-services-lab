package pkg5630402018.java.lab7;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class VisitNodes {

    public static void main(String[] args) throws FileNotFoundException, XMLStreamException {
        FileInputStream file = new FileInputStream("src/pkg5630402018/java/lab7/nation.xml");
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader r = factory.createXMLEventReader(file);
        while (r.hasNext()) {
            XMLEvent e = r.nextEvent();

            if (e.getEventType() == XMLEvent.START_ELEMENT) {
                StartElement startE = e.asStartElement();
                System.out.println("Element name = " + startE.getName());
                Iterator it = startE.getAttributes();
                while (it.hasNext()) {
                    Attribute attr = (Attribute) it.next();
                    System.out.println("Attribute: " + attr.getName() + " = "
                            + attr.getValue());

                }
            }
            if (e.getEventType() == XMLEvent.CHARACTERS) {
                Characters chars = e.asCharacters();
                System.out.println("#text = " + chars.getData());
                
            }
        }
    }
}
