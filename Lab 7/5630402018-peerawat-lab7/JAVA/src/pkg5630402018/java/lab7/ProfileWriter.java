package pkg5630402018.java.lab7;

import java.io.FileWriter;
import java.io.IOException;
import javax.xml.stream.*;

public class ProfileWriter {

    private static final String YEE = "http://www.facebook.com/lookyeeman";
    private static final String XHTML = "http://www.w3.org/1999/xhtml";

    public static void main(String[] args) throws XMLStreamException, IOException {
        String fileName = "src/pkg5630402018/java/lab7/myProfile.xml";
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new FileWriter(fileName));
        xtw.writeStartDocument();
        xtw.setPrefix("h", XHTML);
        xtw.writeStartElement(XHTML, "html");
        xtw.writeNamespace("h", XHTML);
        xtw.writeStartElement("yee");
        xtw.setDefaultNamespace(YEE);
        xtw.writeNamespace("", YEE);
        xtw.writeStartElement("Profile");
        xtw.writeCharacters("Name");
        xtw.writeStartElement("name");
        xtw.writeAttribute("id", "563040201-8");
        xtw.writeCharacters("Peerawat Wongpakdee");
        xtw.writeEndElement();
        xtw.writeCharacters("University");
        xtw.writeStartElement("university");
        xtw.writeCharacters("Khonkaen-university");
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
        System.out.println("Finish writing myProfile.xml");
    }
}
