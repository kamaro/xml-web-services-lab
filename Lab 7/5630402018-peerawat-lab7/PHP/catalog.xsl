<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:y="http://www.kku.ac.th">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>catalog.xml</title>
            </head>
            <body>
                <h1>My CD Collection</h1>
                <table>
                    <tr bgcolor="#9acd32">
                        <th>title</th>
                        <th>Artist</th>
                    </tr>
                    <xsl:for-each select="y:catalog/y:cd">
                            <tr>
                                <td>
                                    <xsl:value-of select="y:title"/>
                                </td>
                                <td>
                                    <xsl:value-of select="y:artist"/>
                                </td>
                            </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
