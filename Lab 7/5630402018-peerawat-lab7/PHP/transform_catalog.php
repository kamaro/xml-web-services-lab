        <?php
        $catalog_xml = new DOMDocument;
        $catalog_xml->load('catalog.xml');
        $catalog_xsl = new DOMDocument;
        $catalog_xsl->load('catalog.xsl');
        $transform = new XSLTProcessor;
        $transform->importStyleSheet($catalog_xsl);
        echo $transform->transformToXML($catalog_xml);
        ?>


