/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab8;

import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author VJunsone
 */
public class MyTransform {
     public static void main(String[] args) throws Exception {
          TransformerFactory tFactory = TransformerFactory.newInstance();

            Source xslDoc = new StreamSource("src\\staxlab8\\catalog.xsl");
            Source xmlDoc = new StreamSource("src\\staxlab8\\catalog.xml");

            String outputFileName = "src\\staxlab8\\catalog.html";

            OutputStream htmlFile = new FileOutputStream(outputFileName);
            Transformer trasform = tFactory.newTransformer(xslDoc);
            trasform.transform(xmlDoc, new StreamResult(htmlFile));
     }
    
}
