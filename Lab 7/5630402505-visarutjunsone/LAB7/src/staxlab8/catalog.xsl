<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : October 15, 2015, 6:23 PM
    Author     : VJunsone
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet 
    xmlns:call="http://www.kku.ac.th" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" indent="yes"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="call:catalog">
        <html>
            <head>
                <title>catalog.xsl</title>
            </head>
            <body>
                <h2>My CD Collection</h2>
                <table border="0">
                    <tr bgcolor="#9acd32">
                        <th>Title</th>
                        <th>Artist</th>      
                    </tr>
                    <xsl:for-each select="call:cd">
                        
                        <tr>
                            <td>
                                <xsl:value-of select="call:title"/>
                            </td>
                            <td>
                                <xsl:value-of select="call:artist"/>
                            </td>       
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
