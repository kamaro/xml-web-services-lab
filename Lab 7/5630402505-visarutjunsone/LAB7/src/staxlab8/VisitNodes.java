/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab8;

import java.io.FileInputStream;
import java.io.InputStream;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author VJunsone
 */
public class VisitNodes {

    public static void main(String[] args) throws Exception {
        InputStream in = new FileInputStream("src/staxlab8/nation.xml");
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader parser = factory.createXMLStreamReader(in);
        for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    if (isHeader(parser.getLocalName())) {
                        System.out.println("Element name = " + parser.getLocalName());
                    }
                    if (parser.getAttributeCount() != 0) {
                        System.out.println("Attribute: "
                                + parser.getAttributeLocalName(0) + " = " + parser.getAttributeValue(0));
                    }

                    break;
                case XMLStreamConstants.END_ELEMENT:
                    break;
                case XMLStreamConstants.CHARACTERS:
                    System.out.println("#text = " + parser.getText());
                    break;
            }
        }
        parser.close();

    }

    private static boolean isHeader(String name) {
        if (name.equals("nation") || name.equals("name")
                || name.equals("location")) {
            return true;
        } else {
            return false;
        }
    }

}
