/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staxlab8;

import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import java.io.FileWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author VJunsone
 */
public class ProfileWriter {

    public static void main(String[] args) throws Exception {
        String file = "myProfile.xml";
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(new FileWriter(file)));
        xtw.writeStartDocument();
        xtw.writeStartElement("ns1", "sample", "http://gear.kku.ac.th/Visarut");
        xtw.writeNamespace("ns1", "http://gear.kku.ac.th/Visarut");
        xtw.writeEmptyElement("http://gear.kku.ac.th/Visarut", "inner1");
        xtw.writeEndElement();
        xtw.writeCharacters("\n");
        xtw.writeStartElement("profile");
        xtw.writeAttribute("id", "563040250-5");
        xtw.writeStartElement("name");
        xtw.writeCharacters("Visarut Junsone");
        xtw.setPrefix("TestPrefix", "http://gear.kku.ac.th/Visarut");
        xtw.writeStartElement("http://gear.kku.ac.th/Visarut", "Test");
        xtw.writeEmptyElement("http://gear.kku.ac.th/Visarut", "Testhowtowritemixedcontentelement");
        xtw.writeEndElement();
        xtw.writeEndElement();// End name Element
        xtw.writeEndElement();//End profile Element
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
    }
}
