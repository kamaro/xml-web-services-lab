        <?php
        // Load the XML source
    $xml = new DOMDocument;
    $xml->load('catalog.xml');
    $xsl = new DOMDocument;
    $xsl->load('catalog.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$proc->transformToURI($xml, 'catalog.html');
echo file_get_contents('catalog.html')
        ?>
