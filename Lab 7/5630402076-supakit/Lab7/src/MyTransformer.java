
import java.io.File;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author thinkearth
 */
public class MyTransformer {

  
    
    public static void main(String[] args) {
         try{
              StreamSource in = new StreamSource("src/catalog.xml");
              StreamSource xsl = new StreamSource("src/catalog.xsl");
              StreamResult out = new StreamResult("src/catalog.html");
              TransformerFactory transformer = TransformerFactory.newInstance();
              Transformer transforms = transformer.newTransformer(xsl);
              transforms.transform(in,out);
              System.out.println("NO ERROR");
              }
        catch (TransformerException e){
            System.out.println("FOUND ERROR");
                      
        }
        
        // TODO code application logic here
    }
    
}