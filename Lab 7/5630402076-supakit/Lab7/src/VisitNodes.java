
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author thinkearth
 */
public class VisitNodes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws XMLStreamException, FileNotFoundException {
            FileInputStream origin = new FileInputStream("src/nation.xml");
            XMLInputFactory fac = XMLInputFactory.newInstance();
            XMLStreamReader Reader = fac.createXMLStreamReader(origin);
            while (Reader.hasNext()) {
                int Event = Reader.next();

                if (Event == XMLStreamConstants.START_ELEMENT) {
                    System.out.println("Element name = " + Reader.getLocalName());
                    if (Reader.getLocalName().equals("nation")) {
                        System.out.println("Attribute : " + Reader.getAttributeName(0) + " = " + Reader.getAttributeValue(0));
                    }
                }

                if (Event == XMLStreamConstants.CHARACTERS) {
                    if (!Reader.getText().trim().equals("")) {
                        System.out.println("#text = " + Reader.getText().trim());
                    }
                }

            }
    }
}


