/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author thinkearth
 */
public class ProfileWriter {
    private static final String nsp = "http://www.opengis.net/kml/2.2";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws XMLStreamException, IOException {
            String fileName = "myProfile.xml";
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            XMLStreamWriter xtw = xof.createXMLStreamWriter(new FileWriter("src/MyProfile.xml"));
            xtw.writeStartDocument();
            xtw.writeStartElement("profile");
            xtw.setDefaultNamespace(nsp);
            xtw.writeNamespace("",nsp);
            xtw.writeEndElement();
            xtw.writeStartElement("name");
            xtw.writeAttribute("sex","Male");
            xtw.writeCharacters("My name is");
            xtw.writeStartElement("FirstName");
            xtw.writeCharacters("Supakit");
            xtw.writeEndElement();
            xtw.writeStartElement("LastName");
            xtw.writeCharacters("Siri-amnat");
            xtw.writeEndElement(); 
            xtw.writeEndElement();
            xtw.writeEndDocument(); 
            xtw.flush();
            xtw.close();
            System.out.println("Created Finish");
        }
    }


