<?php
    // XML format for output
    header("Content-type:text/xml");

    // xml post structure
    $soapUrl = 'http://www.pttplc.com/webservice/pttinfo.asmx?WSDL';

    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/">
        <Language>string</Language>
        </CurrentOilPrice>
    </soap:Body>
    </soap:Envelope>';   // data from the form, e.g. some ID number

    //SOAPAction: your op URL
    $headers = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: http://www.pttplc.com/ptt_webservice/CurrentOilPrice",
        "Content-length: " . strlen($xml_post_string),
    );

    // PHP cURL  for https connection with auth
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLINFO_HEADER_OUT, 0);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_URL, $soapUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request

    // Converting response
    $response = curl_exec($ch);
    curl_close($ch);

    // edit xml with DOM
    $dom = new DomDocument();
    $dom->loadXML($response);
    $node = $dom->documentElement;
    foreach ($node->childNodes as $item) {
        if ($item->nodeName == "soap:Header") {
        $item->parentNode->removeChild($item);
        }
    }
    echo $dom->saveXML();
?>