<?php
	header("Content-type: text/html;charset=utf-8");
	
	$slf = simplexml_load_file('https://www.blognone.com/atom.xml') or die("error");
	
	echo "<h1>Feed items from ".$slf -> channel -> title."</h1>";
	echo "<ul>";
	foreach ($slf -> channel -> item as $item) {
		echo "<li><a href = ".$item -> link.">".$item -> title."</a></li>";
	}
	echo "</ul>";
	
?>