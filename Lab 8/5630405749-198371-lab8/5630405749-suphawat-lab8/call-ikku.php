<?php
	header("Content-type: text/html;charset=utf-8");
	$jsonObj = file_get_contents('https://www.kku.ac.th/ikku/api/activities/services/topActivity.php');
	$final_res = json_decode($jsonObj, true);
	//var_dump($final_res);
	echo "<table>";	
	foreach ($final_res["activities"] as $num => $value) {
		$num++;
		echo "<tr><td>".$num.".</td><td>".$value['dateSt']."</td><td><a href=".$value['image'].">".$value['title']."</a></td><td>".$value['contact']['phone']."</td></tr>";
	}
	echo "</table>";
?>