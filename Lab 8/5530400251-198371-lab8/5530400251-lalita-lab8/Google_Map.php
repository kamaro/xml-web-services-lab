<!DOCTYPE html>
<html>
    <head>
        <script
            src="http://maps.googleapis.com/maps/api/js">
        </script>

        <script>
            var myCenter = new google.maps.LatLng(16.442879, 102.822493);

            function initialize()
            {
                var mapProp = {
                    center: myCenter,
                    zoom: 12,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                };

                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
    </head>

    <body>
        <div id="googleMap" style="width:700px;height:580px;"></div>
    </body>
</html>