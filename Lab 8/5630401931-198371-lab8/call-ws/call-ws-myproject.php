<?php

$rss = simplexml_load_file('http://www.thefreedictionary.com/_/WoD/rss.aspx')  or die("Error: Cannot create object");
echo '<div id="WordOfTheDay">';
echo "<h2>".$rss->channel->title."</h2>";
echo "<ul>";
foreach ($rss->channel as $item) {
    echo "<li><a href=".$item->item->link.">".$item->item->title."</a></li>";
    echo "<p>".$item->item->description."</p>";
}
echo "</ul>";
echo '</div>';


