<!DOCTYPE html>
<html>
	<head>
		<title>Show Location Map</title>
		<script src="http://maps.googleapis.com/maps/api/js"></script>
		<script>
			function initialize() {
				var mapProp = {
				center:new google.maps.LatLng(16.472439, 102.823488),
				zoom:17,
				mapTypeId:google.maps.MapTypeId.HYBRID
				};
				var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
				}
				google.maps.event.addDomListener(window, 'load', initialize);
			</script>
	</head>
	<body>
		<div id="googleMap" style="width:500px;height:400px;"></div>
		<h3>Location : Faculty Of Engineering, Khon Kaen University.</h3>
	</body>
</html>