<?php
	header("Content-type:text/xml");
	$stringHeader = 
	'<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			<soap:Body>
				<CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/">
					<Language>string</Language>
				</CurrentOilPrice>
			</soap:Body>
		</soap:Envelope>';
	$header = array(
		'Content-type:text/xml;charset="utf-8"',
		'Accept: text/xml',
		'SOAPAction: http://www.pttplc.com/ptt_webservice/CurrentOilPrice',
		'Content-length: ' . strlen($stringHeader),);
		
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, "http://www.pttplc.com/webservice/pttinfo.asmx?WSDL");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $stringHeader);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

	$respond = curl_exec($curl);
	$output = str_replace(strstr($respond, "<soap:Header>"), strstr($respond, "<soap:Body>"), $respond);
	curl_close($curl);
	echo $output;
?>
