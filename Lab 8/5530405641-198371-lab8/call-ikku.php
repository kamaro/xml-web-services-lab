<?php
header("content-type:text/html;charset=UTF-8");
$url = "https://www.kku.ac.th/ikku/api/activities/services/topActivity.php";
$json = file_get_contents($url);
$data = json_decode($json, true);
?>

<html>
    <head><title>Calling a JSON Web Service</title>
    </head>
    <body>
        <table border="0">
        
            <?php
            $key = 0;
            foreach ($data["activities"] as $key => $value) {
                $key += 1;
                echo '<tr><td>' . $key . '</td><td>'. $value["dateSt"] . '</td><td>'  . '<a href="' . $value["image"] . '">' . $value["title"] . '</a>' . '</td><td>' . $value["contact"]["phone"] . '</td></tr>';
            }
            ?>
        </table>
    </body>
</html>

