<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>SoundCloud Searchers</title>

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
#main-container {
	width: 820px;
	margin-left: auto;
	margin-right: auto;
}
.stream-item {
	cursor: pointer;
	width: 150px;
	height: 150px;
	display: inline-block;
	margin-right: 10px;
	margin-bottom: 10px;
	background-size: cover;
	position: relative;
}
</style>
</head>
<body>
<div id="doc">
  <div id="main-container" class="text-center">
    <h1>SoundCloud music search</h1>
    <form>
      <input id="sp" type="hidden" value="0">
      <div class="form-group">
        <input type="text" class="form-control input-lg" id="sq" placeholder="Seach music or paste a URL" autocomplete="off">
      </div>
    </form>
    
    <div id="musicselector">
    </div>
    <div style="text-align: right; font-size: 17px;">
    <a id="dock" href="#" style="display: none;">Dock</a>
    </div>
    <div id="musiccontainer" style="margin-bottom: 40px;">
    </div>
  </div>
</div>

<div id="m23" style="position: fixed; right: 15px; bottom: 15px; width: 200px; height: 100px; overflow: hidden; background-color: transparent;">
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> 
<script src="http://www.myquestionth.com/js/jquery-ui-1.10.4.custom.min.js"></script>
<link href="http://www.myquestionth.com/js/jquery-ui.css" rel="stylesheet">

<script src="https://connect.soundcloud.com/sdk/sdk-3.0.0.js"></script> 
<script>
SC.initialize({
  client_id: 'd462632ba310c5177d5e129d8c0479d6'
});
/*
var track_url = 'https://soundcloud.com/motezmusic/motez-own-up-original-mix';
SC.oEmbed(track_url, { auto_play: false }).then(function(oEmbed) {
	//console.log('oEmbed response: ', oEmbed);
	$("#musiccontainer").html(oEmbed.html)
});*/
$(document).on('click','.stream-item',function(e){
	var track_url = $(this).attr("data-u");
	b(track_url);
});
$(document).on('click','#dock',function(e){
	$("#m23").html($("#musiccontainer").html());
	$("#musiccontainer").html("");
	$(this).hide();
});
/*
$(document).on('paste','#sq',function(a){
	return;
	var q = $(this).val();
	if (a.originalEvent && a.originalEvent.clipboardData) {
		var c = a.originalEvent.clipboardData.getData('text/plain');
		$(this).val(c);
		b(c);
		a.preventDefault();
		return;
	};
});*/
$(document).on('keyup','#sq',function(e){
	
	var q = $(this).val();
	if(q.length==0) return;
	if(q.startsWith("https://soundcloud.com")){
		b(q);
		return;
	}
	var sp = Number($('#sp').val());
	$('#sp').val(sp+1);
	sp = sp + 1;
	
	// find all sounds of buskers licensed under 'creative commons share alike'
	SC.get('/tracks', {
	  q: q/*, license: 'cc-by-sa'*/
	}).then(function(tracks) {
		var sp2 = Number($('#sp').val());
		if(sp!=sp2) return;
		a(tracks);
	  	console.log(tracks);
	});	
});
function b(track_url){
	SC.oEmbed(track_url, { auto_play: true }).then(function(oEmbed) {
		//console.log('oEmbed response: ', oEmbed);
		if(oEmbed.html.length==0) return;
		$("#musicselector").html("");
		$("#musiccontainer").html(oEmbed.html);
		$("#dock").show();
	});	
}
function a(tracks){
	
	$("#musicselector").html("");
	if(tracks.length==0){
		$("#musicselector").html('<h4 style="color: #666;">No tracks found!</h4>');
	}
	for(var i = 0; i < tracks.length; i++){
		var str = '<div data-u="'+tracks[i].permalink_url+'" class="stream-item" style="background: url('+tracks[i].artwork_url+');">';
		
        str += '<span style="position: absolute; bottom: 0px; left: 0px; background-color: #000; color: #FFF;">'+tracks[i].title+'</span>';
        str += '</div>';
		$("#musicselector").append(str);
		
	}
}
</script>
</body>
</html>