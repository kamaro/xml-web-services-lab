<?php
	function url_get_contents ($Url) {
    		if (!function_exists('curl_init')){ 
        		die('CURL is not installed!');
    		}
    		$ch = curl_init();
    		curl_setopt($ch, CURLOPT_URL, $Url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		$output = curl_exec($ch);
    		curl_close($ch);
    		return $output;
	}
	header("content-type:text/html;charset=UTF-8");
	$url = "http://www.kku.ac.th/ikku/api/activities/services/topActivity.php";
	// how to call the given API url
	$json = file_get_contents($url);
	// $json = url_get_contents($url);
	$data = json_decode($json, true);
	// print_r($data);
	// display the result as the name of faculties 
	echo "<table>";	
     foreach ($data["activities"] as $key => $value) {
                $key += 1;
                echo '<tr><td>'.$key.'</td><td>'.$value["dateSt"].' '. '<a href="' . $value["image"] . '">' . $value["title"] . '</a>'.'</td><td>'.$value["contact"]["phone"].'</td></tr>';
            }
	echo "</table>";
?>
