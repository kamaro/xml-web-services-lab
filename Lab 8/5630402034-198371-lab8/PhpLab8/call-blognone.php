<?php
    header("content-type:text/html;charset=UTF-8");
    $content = file_get_contents("https://www.blognone.com/node/feed");
    $x = new SimpleXmlElement($content);
    echo "<h2>Feed items from Blognone</h2>";
    echo "<ul>";
     
    foreach($x->channel->item as $entry) {
        echo "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
    }
    echo "</ul>";
?>