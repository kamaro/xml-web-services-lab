<?php
	function url_get_contents ($Url) {
    		if (!function_exists('curl_init')){ 
        		die('CURL is not installed!');
    		}
    		$ch = curl_init();
    		curl_setopt($ch, CURLOPT_URL, $Url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		$output = curl_exec($ch);
    		curl_close($ch);
    		return $output;
	}
	header("content-type:text/html;charset=UTF-8");
	$url = "https://www.kku.ac.th/ikku/api/activities/services/topActivity.php";
	// how to call the given API url
	$json = file_get_contents($url);
	// $json = url_get_contents($url);
	$data = json_decode($json, true);
	// print_r($data);
	// display the result as the name of new-feed
        $num = 1;
	echo "<table>";	
	foreach ($data['activities'] as $items => $item) {
		echo "<tr><td>".$num.'. '."</td>";
                echo "<td>".$item['dateSt']."</td>";
                echo "<td><a href=".$item['image'].'>'.$item['title']."</a></td>";
                echo "<td>".$item['contact']['phone']."</td>";
                echo '</tr>';
                $num++;
	}
	echo "</table>";
	
?>