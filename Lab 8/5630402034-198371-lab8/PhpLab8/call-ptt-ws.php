<?php
header("content-type:text/xml;charset=UTF-8");
$soapUrl = "http://www.pttplc.com/webservice/pttinfo.asmx?op=CurrentOilPrice";

$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/">
      <Language>string</Language>
    </CurrentOilPrice>
  </soap12:Body>
</soap12:Envelope>';

$headers = array(
    "POST /webservice/pttinfo.asmx HTTP/1.2",
    "Host: www.pttplc.com",
    "Content-Type: application/soap+xml; charset=utf-8",
    "Content-Length: " . strlen($xml_post_string)
);

$url = $soapUrl;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$response = curl_exec($ch);
curl_close($ch);

$response1 = str_replace("<soap:Header>", "", $response);
$response2 = str_replace("</soap:Header>", "", $response1);
$count = strpos($response2, "<WSCor");
$count2 = strpos($response2,"<soap:Body>");
$subout = substr($response2, $count,$count2-$count);
$response3 = str_replace($subout, "", $response2);

$parser = simplexml_load_string($response3);
echo $parser->asXML();
?>