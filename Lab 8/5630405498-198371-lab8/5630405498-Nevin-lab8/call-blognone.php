<html>
    <body>
        <h1>Feed items from Blognone</h1>
        <ul>
            <?php
            header("content-type:text/html;charset=utf-8");

            $url = 'https://www.blognone.com/atom.xml';

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            $curl_response = curl_exec($curl);
            curl_close($curl);

            //$parser = simplexml_load_string($curl_response);
            $doc = new DOMDocument();

            $doc->loadXML($curl_response);
            $root = $doc->documentElement;
            $child = $root->childNodes;


            foreach ($child as $item) {
                if ($item->hasChildNodes()) {
                    $item_child = $item->childNodes;

                    foreach ($item_child as $item_b) {
                        if ($item_b->nodeName == 'item') {

                            echo '<li>';
                            foreach ($item_b->childNodes as $sub_item) {
                                if ($sub_item->nodeName == 'title') {
                                    $sib_item = $sub_item->nextSibling;
                                    for ($i = 0; $i <= $sub_item->childNodes->length; $i++) {
                                        if ($sib_item->nodeName == 'link') {
                                            echo '<a href="' . $sib_item->nodeValue . '">';
                                        } else {
                                            $sib_item = $sib_item->nextSibling;
                                        }
                                    }
                                    //$sib_item = $sub_item->nextSibling->nextSibling;
                                    //echo $sib_item->nodeName;
                                    echo $sub_item->nodeValue . '</a>';
                                }
                            } echo '</li>';
                        }
                    }
                }
            }
            ?>
        </ul>
    </body>
</html>