<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h2>Feed items from Blognone</h2>

        <?php
        $contents = file_get_contents("https://www.blognone.com/atom.xml");
        $xml = new SimpleXMLElement($contents);
        echo "<ul>";
        foreach ($xml->channel->item as $item) {
            echo "<li><a href=" . $item->link . ">";
            echo $item->title . "</li>";
        }
        echo "</ul>"
        ?>

    </body>
</html>
