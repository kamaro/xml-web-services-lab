<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h2>The weather in Khon kean</h2>
        <?php
        $city = 'Khonkaen';
        $country = 'th'; //Two digit country code
        $appid = '8e071b555cc7eb7b9839ae81e55d6125';
        $url = 'http://api.openweathermap.org/data/2.5/weather'
                . '?q=' . $city . ',' . $country . '&units=metric&cnt=7&lang=en'
                . '&appid=' . $appid;
        $json = file_get_contents($url);
        $data = json_decode($json, true);
//Get current Temperature in Celsius
        echo 'temperature:' . $data['main']['temp'] . 'c' . "<br>";
//Get weather condition
        echo 'weather condition:' . $data['weather'][0]['main'] . "<br>";
//Get cloud percentage
        echo 'cloud percentage:'.$data['clouds']['all'] . "<br>";
//Get wind speed
        echo 'wind speed:'.$data['wind']['speed'] . "<br>";
        ?>
    </body>
</html>
