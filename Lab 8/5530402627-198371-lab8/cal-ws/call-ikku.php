<?php
	$url = "https://www.kku.ac.th/ikku/api/activities/services/topActivity.php";
	$data = json_decode(file_get_contents($url));
	
	$i = 1;
	
	echo '<table>';
	//echo '<tr style="text-align: left;"><th>No.</th><th>Date</th><th>Title</th><th>Tel.</th></tr>';
	foreach($data->activities as $item) {
		echo '<tr>';
		echo '<td>'.$i++.'.</td>';
		echo '<td>'.$item->dateSt.'</td>';
		echo '<td><a href="'.$item->image.'">'.$item->title.'</a></td>';
		echo '<td>'.$item->contact->phone.'</td>';
		echo '</tr>';
	}
	echo '</table>';
?>