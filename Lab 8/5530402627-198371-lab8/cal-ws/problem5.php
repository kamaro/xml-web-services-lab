<?php
	$url = "https://www.blognone.com/atom.xml";
	$content = file_get_contents($url);
    $x = new SimpleXmlElement($content);
     
	echo "<h1>Feed items from Blognone</h1>";
    echo "<ul>";
     
    foreach($x->channel->item as $entry) {
        echo "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
    }
    echo "</ul>";
?>