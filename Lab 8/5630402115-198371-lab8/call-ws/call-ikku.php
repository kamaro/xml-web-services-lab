<?php

header("content-type:text/html;charset=UTF-8");
$url = "https://www.kku.ac.th/ikku/api/activities/services/topActivity.php";
// how to call the given API url
$json = file_get_contents($url);
// $json = url_get_contents($url);
$data = json_decode($json, true);
// print_r($data);
// display the result as the name of faculties 
echo "<table>";
$i = 1;
foreach ($data['activities'] as $items => $item) {
    echo "<tr>";
    echo "<td>" . $i++ . ".</td>";
    echo "<td>" . $item['dateSt'] . "<td>";
    echo "<td><a href=" . $item['image'] . ">" . $item['title'] . "</a></td>";
    echo "<td>" . $item['contact']['phone'] . "</td>";
    echo "</tr>";
}
echo "</table>";
?>
	
