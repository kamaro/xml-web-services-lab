<?php

$rss = simplexml_load_file('https://www.blognone.com/atom.xml');

echo '<h1>' . "Feed items from Blognone" . '</h1>';

echo '<ul>';
foreach ($rss->channel->item as $item) {
    echo '<li><a href="' . $item->link . '">' . $item->title . "</a></li>";
}
echo '</ul>';
?>
