<?php
header('Content-type: text/xml');
$soap_url = "http://www.pttplc.com/webservice/pttinfo.asmx?WSDL";

$xmlPostString = '<?xml version="1.0" encoding="utf-8"?>
                        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                                         xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
                                         xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                        <soap12:Body>
                        <CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/">
                            <Language>th</Language>
                        </CurrentOilPrice>
                        </soap12:Body>
                        </soap12:Envelope>';

$headers = array(
    "Content-Type: text/xml; charset=utf-8",
    "Content-Length: " . strlen($xmlPostString) ,
    "SOAPAction: http://www.pttplc.com/ptt_webservice/CurrentOilPrice"
);

$url = $soap_url;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlPostString);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$response = curl_exec($ch);
curl_close($ch);

//To delete Header
$domDoc = new DomDocument();
$domDoc->loadXML($response);
$node = $domDoc->documentElement;
foreach ($node->childNodes as $i) {
    if ($i->nodeName == "soap:Header") {
      $i->parentNode->removeChild($i);
    }
}
echo $domDoc->saveXML();
?>

