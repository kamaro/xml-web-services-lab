<?php
	function url_get_contents ($Url) {
    		if (!function_exists('curl_init')){ 
        		die('CURL is not installed!');
    		}
    		$ch = curl_init();
    		curl_setopt($ch, CURLOPT_URL, $Url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		$output = curl_exec($ch);
    		curl_close($ch);
    		return $output;
	}
	header("content-type:text/html;charset=UTF-8");
	$url = "http://www.kku.ac.th/ikku/api/activities/services/topActivity.php";
	$json = file_get_contents($url);
	$data = json_decode($json, true);
	echo "<table>";
	foreach ($data['activities'] as $i => $kku) {
		$i += 1;
		echo '<tr><td>'.$i.'</td>'
		.'<td>'.$kku["dateSt"].'</td>'
		.'<td><a href="'.$kku["image"].'">' . $kku["title"] . '</a></td>'
		.'<td>'.$kku["contact"]["phone"].'</td></tr>';
	}
	echo "</table>";
?>