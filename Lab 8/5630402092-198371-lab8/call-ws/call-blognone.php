<html>
    <body>
        <h1>Feed items from Blognone</h1>
    <?php
       $contents = file_get_contents("https://www.blognone.com/atom.xml");
       $xml = new SimpleXMLElement($contents);
       echo "<ul>";
       foreach ($xml->channel->item as $i) {
           echo "<li><a href=" 
                . $i->link . ">"
                . $i->title . "</li>";
       }
       echo "</ul>"
    ?>
    </body>
</html>

