<html>
    <head>
        <meta charset="utf-8">
        <title>Marker Central</title>
        <style>
            html, body, #map-canvas {
                height: 100%;
                margin: 0px;
                padding: 0px
            }
        </style>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
        <script>

            var central = new google.maps.LatLng(13.9037103, 100.5280379);
            var parliament = new google.maps.LatLng(13.9037255, 100.5259005);
            var marker;
            var map;

            function initialize() {
                var mapOptions = {
                    zoom: 17,
                    center: central
                };

                map = new google.maps.Map(document.getElementById('map-canvas'),
                        mapOptions);

                marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    position: parliament
                });
                google.maps.event.addListener(marker, 'click', toggleBounce);
            }

            function toggleBounce() {

                if (marker.getAnimation() != null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                }
            }

            google.maps.event.addDomListener(window, 'load', initialize);

        </script>
    </head>
    <body>
        <div id="map-canvas"></div>
    </body>
</html>