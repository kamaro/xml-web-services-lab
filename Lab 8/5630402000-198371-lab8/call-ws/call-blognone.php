<?php

$url = "https://www.blognone.com/atom.xml";
$rss = simplexml_load_file($url);

echo "<h1>Feed items from Blognone</h1>";
echo "<ul>";
    foreach ($rss->channel->item as $Datas => $Data) {
        echo '<li>'
        . '<a href="' . $Data->link . '">' . $Data->title . "</a></li>";
    }
echo "</ul>";