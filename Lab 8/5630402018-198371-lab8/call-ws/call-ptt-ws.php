<?php
ini_set('display_errors', 'On');
header("Content-type:text/xml");
$soapUrl = "http://www.pttplc.com/webservice/pttinfo.asmx?WSDL";
$xml_post_string = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/"><Language>string</Language></CurrentOilPrice></soap:Body></soap:Envelope>';
$header = array(
    'Content-type:text/xml;charset="utf-8"',
    'Accept: text/xml',
    'Cache-Control: no-cache',
    'Pragma: no-cache',
    'SOAPAction: http://www.pttplc.com/ptt_webservice/CurrentOilPrice',
    'Content-length: ' . strlen($xml_post_string),);
$url = $soapUrl;
$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_TIMEOUT, 10);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $xml_post_string);
curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

$respond = curl_exec($curl);
curl_close($curl);
$result = str_replace(strstr($respond, "<soap:Header>"), strstr($respond, "<soap:Body>"), $respond);
echo $result;
?>
