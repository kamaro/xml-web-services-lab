<!DOCTYPE html>
<html>
<head>
<script
src="http://maps.googleapis.com/maps/api/js">
</script>

<script>
var start=new google.maps.LatLng(16.4811894,102.8178126,19.75);
var end=new google.maps.LatLng(16.4299744,102.840271,18);

function initialize()
{
var mapProp = {
  center:start,
  zoom:12,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:start,
  });
var marker2=new google.maps.Marker({
  position:end,
  });

marker.setMap(map);
marker2.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>

<body>
<div id="googleMap" style="width:700px;height:600px;"></div>
</body>
</html>
