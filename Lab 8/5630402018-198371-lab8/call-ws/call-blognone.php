<?php
    header("Content-type: text/html; charset=utf-8");
    $url = 'https://www.blognone.com/atom.xml';

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

    $curl_response = curl_exec($curl);
    curl_close($curl);
    $parser = simplexml_load_string($curl_response);

    $doc = new DOMDocument();
    $doc->loadXML($curl_response);

    $root = $doc->documentElement;

    function traveling ($node) {
        foreach ($node->childNodes AS $item) {
            if ($item->nodeName == 'item') {
                echo '<li>';
                echo '<a href="'.$item->firstChild->nextSibling->nextSibling->nextSibling->nodeValue.'">';
                echo $item->firstChild->nextSibling->nodeValue;
                echo '</li>';
            }
            if ($item->hasChildNodes()) {
                traveling ($item);
            }
        }
    }
?>

<html>
    <body>
        <h1>Feed items from Blognone</h1>
        <ul>
            <ll>
            <?php traveling ($root); ?>
            </ll>
        </ul>
    </body>
</html>
 No newline at end of file
