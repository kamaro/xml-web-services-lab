<?php
    function url_get_contents ($Url) {
    	if (!function_exists('curl_init')){ 
      		die('CURL is not installed!');
    	}
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $Url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$output = curl_exec($ch);
    	curl_close($ch);
    	return $output;
    }
    $url = "https://www.blognone.com/atom.xml";
    //$xml = file_get_contents($url);

    $doc = new DomDocument('1.0');
    header("Content-Type: application/xml; charset=utf-8");
    $doc->load($url);

    // get completed xml document
    $xml_string = $doc->saveXML();
    //echo $xml_string;
    header("content-type:text/html;charset=UTF-8");
    $root = $doc->documentElement;
    //echo $root->nodeName;
    echo "<h2>Feed items from Blognone</h2>";
    //echo "<table>";
    foreach ($root->childNodes as $item){
        if($item->hasChildNodes()){
            //echo $item->nodeName."<br/>";
            foreach ($item->childNodes as $node){
            	//echo $node->nodeName;
            	if($node->nodeName == "item"){
	            //echo "</br><ul><li>".$node->nodeValue."</ul></li>";
                    $title = null;
		    $link = null;
		    if($node->hasChildNodes()){
		    //echo $node->nodeName."<br/>";
                        foreach ($node->childNodes as $node2){
                            //echo $node2->nodeName;	            	
                            if($node2->nodeName == "title")
                                $title = $node2->nodeValue;
                            if($node2->nodeName == "link")
                                $link = $node2->nodeValue;
                            if($title != null && $link != null){
                                echo "<ul><li><a href = ".$link." target = &quot_blank&quot >".$title."</a></ul></li>";
                                $title = null;
                                $link = null;
                            }

                        }
                    }
		}
            }
        }
    }
?>
	
