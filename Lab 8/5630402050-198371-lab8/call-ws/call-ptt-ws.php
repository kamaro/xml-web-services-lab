<?php

$url = "http://www.pttplc.com/webservice/pttinfo.asmx?op=CurrentOilPrice/";
// The value for the SOAPAction: header
$action = "http://www.pttplc.com/ptt_webservice/CurrentOilPrice";

// Get the SOAP data into a string, I am using HEREDOC syntax
// but how you do this is irrelevant, the point is just get the
// body of the request into a string
$mySOAP = <<<EOD
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/">
      <Language>string</Language>
    </CurrentOilPrice>
  </soap:Body>
</soap:Envelope>
EOD;

// The HTTP headers for the request (based on image above)
$headers = array(
    'Content-Type: text/xml; charset=utf-8',
    'Content-Length: ' . strlen($mySOAP),
    'SOAPAction: ' . $action
);


// Build the cURL session
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POSTFIELDS, $mySOAP);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);


$result = curl_exec($ch);
$doc = new DomDocument('1.0');
$xml_string = str_replace(strstr($result, "<soap:Header>"), strstr($result, "<soap:Body>"), $result);
//$xml_string = $doc->saveXML();
header("Content-type:text/xml;charset=UTF-8");
echo $xml_string;
?>