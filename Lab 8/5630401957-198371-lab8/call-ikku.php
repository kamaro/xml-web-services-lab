<?php
header("Content-type: text/html;charset=utf-8");
$json = file_get_contents('https://www.kku.ac.th/ikku/api/activities/services/topActivity.php');
$data = json_decode($json, true);
echo "<table>";
foreach ($data["activities"] as $val => $value) {
    $num++;
    echo "<tr><td>" . 
         $num . ".</td><td>" . $value['dateSt'] . "</td><td><a href=" . 
         $value['image'] . ">" . $value['title'] . "</a></td><td>" . $value['contact']['phone'] . 
         "</td></tr>";
}
echo "</table>";
?>

