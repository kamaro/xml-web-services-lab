<html>
	<head>
		<meta http-equiv="Content-Type" content="text/xml; charset=UTF-8"/>		
        <meta charset="UTF-8"/>	
        <style type="text/css"></style>
    </head>
    <body>
		<?php
			$url = "https://www.blognone.com/atom.xml";
			$xml = file_get_contents($url);
			$xml = new SimpleXMLElement($xml);
			echo "<h2>Feeds items from blognone</h2>";
			foreach ($xml->channel->item as $item){
				echo "<ul>";
 				echo "<li><a href =".$item->link.' target="_blank">'.$item->title."</a></li>";
 				echo "</ul>";
			}
		?>
	</body>
</html>
