<?php 
        header("Content-type: text/xml");
		//Data, connection, auth
        //$dataFromTheForm = $_POST['fieldName']; // request data from the form
        $soapUrl = "http://www.pttplc.com/webservice/pttinfo.asmx?op=CurrentOilPrice"; // asmx URL of WSDL

        // xml post structure

        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
							<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
							  <soap:Body>
								<CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/">
								  <Language>th</Language>
								</CurrentOilPrice>
							  </soap:Body>
							</soap:Envelope>';   // data from the form, e.g. some ID number
			$lengthHeader = strlen($xml_post_string);
           $headers = array(
                        "POST /webservice/pttinfo.asmx HTTP/1.1",
                        "Host: www.pttplc.com",
                        "Content-Type: text/xml; charset=utf-8",
                        "Content-Length: $lengthHeader",
                        "SOAPAction: http://www.pttplc.com/ptt_webservice/CurrentOilPrice"
                    ); //SOAPAction: your op URL

            $url = $soapUrl;

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch); 
            //curl_close($ch);
			if(curl_exec($ch) === false) {
				$err = 'Curl error: ' . curl_error($ch);
				curl_close($ch);
				print $err;
			} else {
				curl_close($ch);
			}

            // converting
			$response2 = substr($response,0,strpos($response,"<soap:Header>"));
			$response3 = substr($response,strpos($response,"<soap:Body>"));
            // convertingc to XML
            //$parser = simplexml_load_string($response2);
            // user $parser to get your data out of XML response and to display it.
			echo $response2.$response3;
    ?>