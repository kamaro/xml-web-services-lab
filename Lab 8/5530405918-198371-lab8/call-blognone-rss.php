<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>
    <head>
		<title>
		</title>
    </head>
    <body>
		<?php
			header("Content-type: text/html; charset=utf-8");
			$rss_url = 'https://www.blognone.com/atom.xml';
			$ch = curl_init($rss_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			$contents = curl_exec($ch);
			curl_close($ch);
			 
			  $doc = new SimpleXMLElement($contents, LIBXML_NOCDATA);
			  
			if(isset($doc->channel))
			{
				parseRSS($doc);
			}
			if(isset($doc->entry))
			{
				parseAtom($doc);
			}
			
			function parseRSS($xml)
			{
				echo "<h1>".'Feed items from Blognone'.$xml->channel->title."</h1>";
				$cnt = count($xml->channel->item);
				for($i=0; $i<$cnt; $i++)
				{
				$url 	= $xml->channel->item[$i]->link;
				$title 	= $xml->channel->item[$i]->title;
				$desc = $xml->channel->item[$i]->description;
			 
				echo '<li><a href="'.$url.'">'.$title.'</a></li>';
				}
			}
			
			function parseAtom($xml)
			{
				echo "<strong>".$xml->author->name."</strong>";
				$cnt = count($xml->entry);
				for($i=0; $i<$cnt; $i++)
				{
				$urlAtt = $xml->entry->link[$i]->attributes();
				$url	= $urlAtt['href'];
				$title 	= $xml->entry->title;
				$desc	= strip_tags($xml->entry->content);
			 
				echo '<li><a href="'.$url.'">'.$title.'</a></li>';
				}
			}
		?>
    </body>
</html>