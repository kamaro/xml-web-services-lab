<?php
ini_set('display_errors', 'On');
header("Content-type:text/xml");
$soapUrl = "http://www.pttplc.com/webservice/pttinfo.asmx?WSDL";
$xml_post_string = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/"><Language>string</Language></CurrentOilPrice></soap:Body></soap:Envelope>';
$header = array(
    'Content-type:text/xml;charset="utf-8"',
    'Accept: text/xml',
    'Cache-Control: no-cache',
    'Pragma: no-cache',
    'SOAPAction: http://www.pttplc.com/ptt_webservice/CurrentOilPrice',
    'Content-length: ' . strlen($xml_post_string),);
$url = $soapUrl;
$c = curl_init();

curl_setopt($c, CURLOPT_URL, $url);
curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
curl_setopt($c, CURLOPT_TIMEOUT, 10);
curl_setopt($c, CURLOPT_POST, true);
curl_setopt($c, CURLOPT_POSTFIELDS, $xml_post_string);
curl_setopt($c, CURLOPT_HTTPHEADER, $header);

$respond = curl_exec($c);
curl_close($c);
$result = str_replace(strstr($respond, "<soap:Header>"), strstr($respond, "<soap:Body>"), $respond);
echo $result;
?>