<?php
header("Content-type:text/xml");
$url = "http://www.pttplc.com/webservice/pttinfo.asmx?op=CurrentOilPrice";

$xml_post_string = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/"><Language>string</Language></CurrentOilPrice></soap:Body></soap:Envelope>';

$headers = array(
	'POST /webservice/pttinfo.asmx HTTP/1.1',
	'Host: www.pttplc.com',
	'Content-Type: text/xml; charset=utf-8',
	'Content-Length: ' . strlen($xml_post_string),
	'SOAPAction: "http://www.pttplc.com/ptt_webservice/CurrentOilPrice"'
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$response = curl_exec($ch);
curl_close($ch);

$response1 = substr_replace($response,substr($response,strpos($response,"<soap:Body>")),strpos($response,"<soap:Header>"));
echo $response1;

?>