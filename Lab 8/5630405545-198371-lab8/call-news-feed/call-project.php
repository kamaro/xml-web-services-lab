<html>
    <head>
        <style>
            #map {
                width: 800px;
                height: 500px;
            }
        </style>
        <script src="https://maps.googleapis.com/maps/api/js"></script>
        <script>
            function initialize() {
                var mapCanvas = document.getElementById('map');
                var mapOptions = {
                    center: new google.maps.LatLng(16.4788259, 102.8119871),
                    zoom: 8,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                var map = new google.maps.Map(mapCanvas, mapOptions)
            }
            google.maps.event.addDomListener(window, 'load', initialize);


        </script>
    </head>
    <body>
        <div id="map"></div>

        <?php
        $city = 'Khonkaen';
        $country = 'th'; //Two digit country code
        $appid = '8e071b555cc7eb7b9839ae81e55d6125';
        $url = 'http://api.openweathermap.org/data/2.5/weather'
                . '?q=' . $city . ',' . $country . '&units=metric&cnt=7&lang=en'
                . '&appid=' . $appid;
        $json = file_get_contents($url);
        $data = json_decode($json, true);
        //Get current Temperature in Celsius
        echo 'temperature:' . $data['main']['temp'] . 'c' . "<br>";
        //Get weather condition
        echo 'weather condition:' . $data['weather'][0]['main'] . "<br>";
        //Get cloud percentage
        echo 'cloud percentage:' . $data['clouds']['all'] . "<br>";
        //Get wind speed
        echo 'wind speed:' . $data['wind']['speed'] . "<br>";
        ?>
    </body>
</html>

