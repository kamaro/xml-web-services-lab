<?php

        header("content-type:text/xml");
        $soapUrl = "http://www.pttplc.com/webservice/pttinfo.asmx?op=CurrentOilPrice";
        
        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
        <CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/">
        <Language>string</Language>
        </CurrentOilPrice>
        </soap:Body>
        </soap:Envelope>';
        
        //array http header open url
        $headers = array(
            'Content-type:text/xml;charset="utf-8"',
            'SOAPAction: http://www.pttplc.com/ptt_webservice/CurrentOilPrice',
            'Content-length: ' . strlen($xml_post_string),
            );
        
        //create curl for link
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);    
        curl_setopt($ch, CURLOPT_URL, $soapUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        $resultFinal = str_replace(strstr($result, "<soap:Header>"), strstr($result, "<soap:Body>"), $result);
        echo $resultFinal;
?>


