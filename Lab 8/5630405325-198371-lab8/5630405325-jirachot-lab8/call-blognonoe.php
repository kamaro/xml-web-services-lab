<?php
    $rssfeed = simplexml_load_file('https://www.blognone.com/atom.xml');
    echo "<h2>Feed items from Blognone</h2>";
    
    echo "<ul>";
        foreach ($rssfeed -> channel -> item as $item) {
            echo '<li>' . '<a href="' . $item -> link . '">' . $item -> title . "</a></li>"; 
        }
    echo "</ul>";