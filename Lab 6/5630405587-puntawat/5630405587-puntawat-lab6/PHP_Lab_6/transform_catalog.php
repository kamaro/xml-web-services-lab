<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    
    <body>
        <?php

// XML
$xml_doc = new DOMDocument();
$xml_doc->load("catalog.xml");


// XSL
$xsl_doc = new DOMDocument();
$xsl_doc->load("catalog.xsl");

// Proc
$proc = new XSLTProcessor();
$proc->importStylesheet($xsl_doc);
$newdom = $proc->transformToDoc($xml_doc);

print $newdom->saveXML();

?>
    </body>
</html>
