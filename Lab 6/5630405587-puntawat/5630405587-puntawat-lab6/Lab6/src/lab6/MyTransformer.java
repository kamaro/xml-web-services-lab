/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author Admin
 */
public class MyTransformer {

    /**
     * @param args the command line arguments
     */
   

public static void main(String args[])
{
try {
        TransformerFactory tFactory=TransformerFactory.newInstance();

        Source xslDoc=new StreamSource("src/lab6/" + args[1]);
        Source xmlDoc=new StreamSource("src/lab6/" + args[0]);

        String outputFileName="src/lab6/" + args[2];

        OutputStream htmlFile=new FileOutputStream(outputFileName);
        Transformer trasform=tFactory.newTransformer(xslDoc);
        trasform.transform(xmlDoc, new StreamResult(htmlFile));
    } 
    catch (FileNotFoundException e) 
    {
        e.printStackTrace();
    }
    catch (TransformerConfigurationException e) 
    {
        e.printStackTrace();
    }
    catch (TransformerFactoryConfigurationError e) 
    {
        e.printStackTrace();
    }
    catch (TransformerException e) 
    {
        e.printStackTrace();
    }
}
}
    

