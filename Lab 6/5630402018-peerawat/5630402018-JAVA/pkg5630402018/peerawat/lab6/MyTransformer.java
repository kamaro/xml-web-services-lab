package pkg5630402018.peerawat.lab6;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class MyTransformer {

    public static void main(String[] args) throws TransformerConfigurationException, TransformerException {
        TransformerFactory factory = TransformerFactory.newInstance();
        StreamSource xmlstream = new StreamSource("src/pkg5630402018/peerawat/lab6/catalog.xml");
        StreamSource xslstream = new StreamSource("src/pkg5630402018/peerawat/lab6/catalog.xsl");
        StreamResult htmlstream = new StreamResult("src/pkg5630402018/peerawat/lab6/catalog.html");
        Transformer trans = factory.newTransformer(xslstream);
        trans.transform(xmlstream, htmlstream);

    }

}
