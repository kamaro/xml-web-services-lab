<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>catalog.html</title>
            </head>
            <body>
                <h2>
                    My CD Collection
                </h2>
                <p>
                    <b>
                        <xsl:for-each select="catalog/cd">
                            <xsl:value-of select="title"/>
                            <br></br>
                        </xsl:for-each>
                    </b>
                </p>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
