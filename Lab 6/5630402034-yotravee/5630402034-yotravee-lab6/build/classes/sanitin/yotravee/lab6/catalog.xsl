<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <body>
                <h2>My CD Collection</h2>
                <p>
                    <xsl:for-each select="catalog/cd">
                        <b>
                            <xsl:value-of select="title"/>
                        </b>
                        <br/>
                    </xsl:for-each>
                </p>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
