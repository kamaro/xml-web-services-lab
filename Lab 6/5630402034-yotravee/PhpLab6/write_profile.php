<?php
    $fileName = "myProfile.xml";
    $doc = new DomDocument('1.0');

    $root = $doc->createElement('profile');
    $doc->appendChild($root);

    $name = $doc->createElement('name');
    $nameValue = $doc->createTextNode('Yotravee Sanit-in');
    $name->appendChild($nameValue);
    $root->appendChild($name);

    $doc->save($fileName);
    echo "Finish writing file $fileName";
?>