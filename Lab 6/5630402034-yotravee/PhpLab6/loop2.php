<?php
if (file_exists('nation.xml')) {
    $xmlDoc = new DOMDocument();

    $xmlDoc->load("nation.xml");

    $x = $xmlDoc->documentElement;
    echo $x->nodeName . " has ". $x->childNodes->length . " children " . "<br/>";
    
    foreach ($x->childNodes AS $item) {
        if($item->nodeName != "#text"){
        echo $item->nodeName . " has ". $item->childNodes->length . " children " . "<br/>";
        }
        echo $item->nodeName . " = " . $item->nodeValue ."<br/>";
        if ($item->hasChildNodes()) {
            foreach ($item->childNodes as $node) {
                echo $node->nodeName . " = " . $node->nodeValue . "<br/>";
            }
        }
    }
}
