<?php


$xml_file = new DOMDocument;
$xsl_file = new DOMDocument;

$xml_file->load('catalog.xml');
$xsl_file->load('catalog.xsl');

$doc = new XSLTProcessor;
$doc->importStyleSheet($xsl_file);

$doc->transformToURI($xml_file, 'catalog.html');

echo file_get_contents('catalog.html');
?>

