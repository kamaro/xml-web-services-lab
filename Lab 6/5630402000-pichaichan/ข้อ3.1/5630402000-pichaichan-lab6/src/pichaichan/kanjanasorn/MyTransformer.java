
package pichaichan.kanjanasorn;

import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import java.io.File;


public class MyTransformer {
     public static void main(String[] args) {  
        try{
        String address = "src/pichaichan/kanjanasorn/";
        String xmlfile = address +  args[1];
        String xslSource = address +  args[0];
        String htmloutput = address +  args[2];
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(new File(xslSource));
        Transformer transformer = factory.newTransformer(xslt);
        Source text = new StreamSource(new File(xmlfile));
        transformer.transform(text, new StreamResult(new File(htmloutput)));
    }catch (TransformerException ex){
        ex.printStackTrace(System.err);
    }
    }
}