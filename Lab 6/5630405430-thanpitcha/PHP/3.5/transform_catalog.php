<?php
$xmlDoc = 'catalog.xml';
$xslDoc = 'catalog.xsl';
$doc = new DOMDocument();
$xsl = new XSLTProcessor();
$doc->load($xslDoc);
$xsl->importStylesheet($doc);
$doc->load($xmlDoc);
echo $xsl->transformToXml($doc);

