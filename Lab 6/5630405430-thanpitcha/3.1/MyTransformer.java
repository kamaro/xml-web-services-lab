package lab6;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


public class MyTransformer {
     public static void main(String[] args) {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Source xslt = new StreamSource(new File("src/lab6/catalog.xsl"));
            Transformer transformer = factory.newTransformer(xslt);

            Source text = new StreamSource(new File("src/lab6/catalog.xml"));
            transformer.transform(text, new StreamResult(new File("src/lab6/catalog.html")));

        } catch (TransformerException ex) {
            Logger.getLogger(MyTransformer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
