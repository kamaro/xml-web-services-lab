<?php
$xslDoc = new DOMDocument();
$xslDoc->load("catalog.xsl");

$xmlDoc = new DomDocument();
$xmlDoc->load("catalog.xml");

$proc = new XSLTProcessor();
$proc->importStylesheet($xslDoc);
$newdom = $proc->transformToDoc($xmlDoc);
echo $newdom->saveXML();
?>


