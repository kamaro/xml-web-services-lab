<?php

$xmlDoc = new DOMDocument();
$xmlDoc->load("nation.xml");
$node = $xmlDoc->documentElement;
echo $node->nodeName . " has " . $node->childNodes->length . " children" . "<br/>";
foreach ($node->childNodes as $item) {
    echo $item->nodeName . " = " . $item->nodeValue . "<br/>";
    if ($item->nodeName != "#text") {
        echo $item->nodeName . " has " . $item->childNodes->length . " children" . "<br/>";
    }
    if ($item->hasChildNodes()) {
        foreach ($item->childNodes as $nodeChid) {
            echo $nodeChid->nodeName . " = " . $nodeChid->nodeValue . "<br/>";
        }
    }
}
?>

