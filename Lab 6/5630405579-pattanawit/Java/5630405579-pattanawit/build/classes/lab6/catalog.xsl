<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : 15 October 2015, 19:05
    Author     : Amilaz
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="catalog">
        <html>
            <body>
                <h2>My CD Collection</h2>
                <p>
                    <xsl:for-each select="cd">
                        <b><xsl:value-of select="title"/></b>
                        <br/>
                    </xsl:for-each>
                </p>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
