<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // put your code here
        $xmlDoc = new DOMDocument();
        $xmlDoc->load("nation.xml");
        $rootnode = $xmlDoc->documentElement;
        print $rootnode->nodeName." has ".$rootnode->childNodes->length." children"."<br/>";
        foreach ($rootnode->childNodes as $item) {
            if ($item->nodeName != "#text") {
                print $item->nodeName . " has " . $item->childNodes->length . " children" . "<br/>";
            }
            print $item->nodeName . " = " . $item->nodeValue . "<br/>";
            if ($item->hasChildNodes()) {
                foreach ($item->childNodes as $node) {
                    print $node->nodeName . " = " . $node->nodeValue . "<br/>";
                }
            }
        }
        ?>
    </body>
</html>