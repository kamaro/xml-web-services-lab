<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : October 14, 2015, 3:02 PM
    Author     : VJunsone
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>catalog.xsl</title>
            </head>
            <body>
                <h2>My CD Collection</h2>
                <p>
                <xsl:for-each select="catalog/cd">
                    <b>
                        <xsl:value-of select="title"/><br/>
                    </b>
                </xsl:for-each>
                </p>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
