/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6new;


import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
/**
 *
 * @author VJunsone
 */
public class MyTranformer {

    /**
     * @param args the command line arguments
     * @throws jdk.internal.org.xml.sax.SAXException
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here


            TransformerFactory tFactory = TransformerFactory.newInstance();

            Source xslDoc = new StreamSource("src\\lab6new\\" + args[0]);
            Source xmlDoc = new StreamSource("src\\lab6new\\" + args[1]);

            String outputFileName = "src\\lab6new\\" + args[2];

            OutputStream htmlFile = new FileOutputStream(outputFileName);
            Transformer trasform = tFactory.newTransformer(xslDoc);
            trasform.transform(xmlDoc, new StreamResult(htmlFile));

    }
}
