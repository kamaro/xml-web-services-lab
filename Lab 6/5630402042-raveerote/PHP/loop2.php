<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // put your code here
        $nation_xml = new DOMDocument;
        $nation_xml ->load('nation.xml');
        $rootNode = $nation_xml->documentElement;
        echo $rootNode->nodeName . " has " . $rootNode->childNodes->length . " children" ."<br>";
        foreach($rootNode->childNodes as $item){
            echo $item->nodeName . " = " . $item->nodeValue . "<br>";
            if($item->nodeName != "#text"){
                echo $item->nodeName . " has " . $item->childNodes->length . " children" . "<br>"; 
            }
            if($item->hasChildNodes()){
                foreach($item->childNodes as $child){
                echo $child->nodeName . " = " .$child->nodeValue . "<br>";
                }
            }
        }
        ?>
    </body>
</html>
