<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : October 15, 2015, 8:09 PM
    Author     : golfyzzz
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>catalog.xml</title>
            </head>
            <body>
                <h2>My CD Collection</h2>
                <p>
                    <xsl:for-each select="catalog/cd">
                                <b>
                                    <xsl:value-of select="title"/>
                                </b>
                                <br/>                            
                    </xsl:for-each>
                </p>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
