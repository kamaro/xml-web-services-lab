/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg5630402042.raveerote.lab6;

import javax.xml.transform.*;
import javax.xml.transform.stream.*;

/**
 *
 * @author golfyzzz
 */
public class MyTransformer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws TransformerException {
        StreamSource xmlS = new StreamSource("src/pkg5630402042/raveerote/lab6/" + args[0]);
        StreamSource xslS = new StreamSource("src/pkg5630402042/raveerote/lab6/" + args[1]);
        StreamResult htmlS = new StreamResult("src/pkg5630402042/raveerote/lab6/" + args[2]);
        
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer tran = tf.newTransformer(xslS);
        tran.transform(xmlS, htmlS);
        
    }
    
}
