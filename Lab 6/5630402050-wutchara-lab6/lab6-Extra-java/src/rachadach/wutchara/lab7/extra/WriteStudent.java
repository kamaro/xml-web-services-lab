package rachadach.wutchara.lab7.extra;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author HAM
 * add my profile element to XMLfile
 */
public class WriteStudent {
    private static Object nodelist;

    public static void main(String[] args) throws TransformerException {
        Document doc = null;
        
	  try {
              String inFile = "student.xml";
              String outFile = "student2.xml";
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder parser = dbf.newDocumentBuilder();
                doc = parser.parse("src\\rachadach\\wutchara\\lab7\\extra\\" + inFile);
                
                //add node
                Element rootNode = doc.getDocumentElement();
                NodeList nodelist = doc.getElementsByTagName("student");
                Element nRootNode = doc.createElement("student");
                    nRootNode.setAttribute("id", "563040205-0");
                    Element nameNode = doc.createElement("name");
                        Text nameValue = doc.createTextNode("Wutchara    Rachadach");
                    nRootNode.appendChild(nameValue);
                    Element csNode = doc.createElement("courses");
                        Element cNode = doc.createElement("cours");
                        cNode.setAttribute("id", "198371");
                        Text cValue = doc.createTextNode("XML and Web Service");
                        cNode.appendChild(cValue);
                    csNode.appendChild(cNode);
                nRootNode.appendChild(nameNode);
                nRootNode.appendChild(csNode);
                rootNode.insertBefore(nRootNode, nodelist.item(2));
                
                //write file output
                Source source = new DOMSource(doc);
                File file = new File("src\\rachadach\\wutchara\\lab7\\extra\\" + outFile);
                
                Result result = new StreamResult(file);
                
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer xformer = transformerFactory.newTransformer();

                xformer.setOutputProperty(OutputKeys.INDENT, "yes");
                xformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                
		xformer.transform(source, result);
                
		System.out.println("Succss read file " + inFile + " and write file " + outFile);

	  } catch (ParserConfigurationException | SAXException | IOException pce) {
		System.err.println("---- ERROR ----");
	  }
	}
}
