package rachadach.wutchara.lab7.extra;


import java.io.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

public class Student {
    /**
     * @param args the command line arguments
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     * @throws javax.xml.transform.TransformerConfigurationException
     */
    public static void main(String[] args) throws SAXException, IOException, TransformerConfigurationException, TransformerException {
        if(args.length != 3){
            System.err.println("---------------------------- ERROR ----------------------------\n"
                    + "Please enter <XML file name> "
                    + " <XSL file name> <output file name>");
            System.exit(1);
        }
        try{
            String pac = "src\\\\rachadach\\\\wutchara\\\\lab7\\\\extra\\\\";
            String xmlFile = pac.concat(args[0]);
            String xslFile = pac.concat(args[1]);
            String outFile = pac.concat(args[2]);
            //"src\\\\rachadach\\\\wutchara\\\\lab7\\\\" is package
            //xmlFile = "src\\rachadach\\wutchara\\lab7\\catalog.xml";
            //xslFile = "src\\rachadach\\wutchara\\lab7\\catalog.xsl";
            //outFile = "src\\rachadach\\wutchara\\lab7\\catalog.html";
            
            //xsl file
            String xsl = xslFile;
            Source stylesheetSource = new StreamSource(new File(xsl).getAbsoluteFile());
            
            //input file(XML file)
            String input = xmlFile;
            Source inputSource = new StreamSource(new File(input).getAbsoluteFile());
            
            //output file
            String output = outFile;
            Result outputResult = new StreamResult(new File(output).getAbsoluteFile());
            
            //tranform xml file form xslt
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer xtransformer = factory.newTransformer(stylesheetSource);
            
            xtransformer.setOutputProperty(OutputKeys.INDENT, "yes");
            xtransformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            
            xtransformer.transform(inputSource, outputResult);
            System.out.println("Successfull write " + args[0] + " transform form " + args[1] + " to " + args[2]);
        
        }catch (DOMException pce) {
		System.err.println("---- ERROR ----");
        }
    }
    
}
