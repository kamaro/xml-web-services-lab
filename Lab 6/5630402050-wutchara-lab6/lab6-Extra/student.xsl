<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : student.xsl
    Created on : October 9, 2015, 11:11 AM
    Author     : HAM
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Courses Registration</title>
            </head>
            <body>
                <table>
                    <xsl:attribute name="border">
                            <xsl:text>1</xsl:text>
                    </xsl:attribute>
                    <th>
                        <xsl:attribute name="align">
                            <xsl:text>left</xsl:text>
                        </xsl:attribute>
                        <xsl:text>Name</xsl:text>
                    </th>
                    <th>
                        <xsl:attribute name="align">
                            <xsl:text>left</xsl:text>
                        </xsl:attribute>
                        <xsl:text>Course</xsl:text>
                    </th>
                    
                    <xsl:for-each select="//*[contains(text(),'Programming')]">
                        <tr>
                            <td><xsl:value-of select="/../../name"/></td>
                            <td><xsl:value-of select="."/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
