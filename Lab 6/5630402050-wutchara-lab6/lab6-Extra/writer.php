<?php
    $inFile = "student.xml";
    $outFile = "student2.xml";
    $doc = new DOMDocument();
    //load file
    $doc->load($inFile);
    
    //create xml file
    $root = $doc->createElement('student');
        $doc->appendChild($root);
        $root->setAttribute("id", "563040205-0");
    $name = $doc->createElement('name');
    $nameValue = $doc->createTextNode('Wutchara  Rachadach');
        $name->appendChild($nameValue);
    $root->appendChild($name);
    $courses = $doc->createElement('courses');
        $doc->appendChild($courses);
    $cours = $doc->createElement('cours');
        $courses->appendChild($cours);
    $coursValue = $doc->createTextNode('XML and Web Service');
        $cours->appendChild($coursValue);
        $cours->setAttribute("id", "198371");
    $root->appendChild($courses);
    
    //save file
    $str = $doc->saveXML();
    $doc->save($outFile);
    echo "Finish edit file ".$inFile." and saved to file ".$outFile."</br>"; 
?>