<?php

//load XML file
$xmlDoc = new DOMDocument;
$xmlDoc->load('student.xml');
//load xsl file
$xslDoc = new DOMDocument;
$xslDoc->load('student.xsl');

//transform XML file with xsl file
$tran = new XSLTProcessor;
$tran->importStyleSheet($xslDoc);

echo $tran->transformToXML($xmlDoc);
print 'Tranform Success';
?>