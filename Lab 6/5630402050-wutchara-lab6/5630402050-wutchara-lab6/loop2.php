<?php
    $xmlDoc = new DOMDocument();
    //read file nation.xml
    $xmlDoc->load("nation.xml");
    //drecare root node
    $root = $xmlDoc->documentElement;
    $numChild = $root->childNodes->length;
    print $root->nodeName.' has '.$numChild." children<br/>";
    foreach ($root->childNodes as $item){
        print $item->nodeName."=".$item->nodeValue."<br/>";
        if($item->hasChildNodes()){
            $numChild = $root->getElementsByTagName($item->nodeName)->length;
            print $item->nodeName.' has '.$numChild." children<br/>";
            foreach ($item->childNodes as $node){
                print $node->nodeName."=".$node->nodeValue."<br/>";
            }
        }
    }
?>