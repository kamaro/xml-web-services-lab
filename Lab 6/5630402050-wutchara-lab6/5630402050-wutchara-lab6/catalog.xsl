<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : catalog.xsl
    Created on : October 14, 2015, 8:28 PM
    Author     : HAM
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title></title>
            </head>
            <body>
                <h2>My CD Collection</h2>
                
                <!--create variable to save color code -->
                <xsl:variable name="color">
                    <xsl:text>#B8E65C</xsl:text>
                </xsl:variable>
                    
                <!--create table-->
                <table>                
                    <xsl:attribute name="border">
                            <xsl:text>0</xsl:text>
                    </xsl:attribute>
                    
                    <th>
                        <xsl:attribute name="align">
                            <xsl:text>left</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="bgcolor">
                            <xsl:copy-of select="$color"/>
                        </xsl:attribute>
                        <xsl:text>Title</xsl:text>
                    </th>
                    <th>
                        <xsl:attribute name="align">
                            <xsl:text>left</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="bgcolor">
                            <xsl:copy-of select="$color"/>
                        </xsl:attribute>
                        <xsl:text>Artist</xsl:text>
                    </th>
                    <!--select value of title and artist for create table-->
                    <xsl:for-each select="//catalog/cd">
                        <xsl:sort select="title" data-type="text"/>
                        <tr>
                            <td><xsl:value-of select="title"/></td>
                            <td><xsl:value-of select="artist"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>