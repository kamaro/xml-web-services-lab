<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/catalog">
        <html>
            <body>
                <h2>My CD Collections </h2>
                <p>
                    <xsl:for-each select="cd">	
                        <b>                            
                            <xsl:value-of select ="title"/>                          
                        </b> 
                        <br></br>
                    </xsl:for-each>	
                </p>                      
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

