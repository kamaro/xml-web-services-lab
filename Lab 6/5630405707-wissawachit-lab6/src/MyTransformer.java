
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class MyTransformer {

    public static void main(String[] args) {
        try {
            StreamSource xmlStream = new StreamSource("src/" + args[0]);
            StreamSource xslStream = new StreamSource("src/" + args[1]);
            StreamResult htmlStream = new StreamResult("src/" + args[2]);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer(xslStream);
            trans.transform(xmlStream, htmlStream);
            System.out.println("SUCCESSFUL");
        }   catch (TransformerException e) {
            System.err.println("ERROR");
        }
    }
}
