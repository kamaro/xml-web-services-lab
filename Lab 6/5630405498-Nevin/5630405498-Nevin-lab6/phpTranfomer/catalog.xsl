<?xml version="1.0" encoding="UTF-8"?>
<!--
    Document   : catalog.xsl
    Created on : October 15, 2015, 9:26 PM
    Author     : user
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    
    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Catalog Table - Lab6 ex5.5</title>
            </head>
            <body>
                
                <h2>My CD Collection</h2>
                
                <table border="1">
                    <tr bgcolor="#9acd3b">
                        <th align="left">Title</th>
                        <th align="left">Artist</th>
                    </tr>
                    
                    <xsl:for-each select="catalog/cd">
                        <xsl:sort select="title"/>
                        <tr>
                            <td>
                                <xsl:value-of select="title"/>
                            </td>
                            <td>
                                <xsl:value-of select="artist"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                    
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
