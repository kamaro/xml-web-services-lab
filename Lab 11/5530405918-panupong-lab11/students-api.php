<?php

function array_to_xml( $data, &$xml , $dept) {
		$dept++;
		foreach( $data as $key => $value ) {
			if( is_array($value) ) {
				if( is_numeric($key) ){
					if($dept == 2)
						$key = 'student';
				}

				if($dept > 1 && $dept < 4) {
					$subnode = $xml->addChild($key);
				}else if($dept == 4){
					$subnode = $xml;
				}else {
					$subnode = $xml;
				}

				if($dept < 5)
				array_to_xml($value, $subnode, $dept);
			} else {
				if( is_numeric($key) ){
					if($dept == 4) $key = 'book';
				}
				
				if($dept > 1){
					$xml->addChild($key, $value);
				}
			}
		 }
	}

$information = array(
					"students" => 
					array(
						array(
							"name" => "Panupong",
							"books" =>
								array(
								"The World is Flat - Thomas L. Friedman", 
								"Revolutionary Wealth - Alvin Toffler", 
								"แมงเม่าคลับ แบ่งปันความรู้ในการเล่นหุ้น \"อย่างเป็นระบบ\" - มนสิช จันทนปุ่ม"),
							"education" => array(
								array("high_school" => "Sakolraj Wittyanukul"),
								array("undergrade_school" => "Khon Kaen University")
							)
						)
					)
				);

//if recieved HTTP GET parameter xml alt
if(isset($_GET['alt'])) {
	
	$xml = new SimpleXMLElement('<?xml version="1.0"?><students></students>');
	
	array_to_xml($information,$xml, 0);
	
	header("Content-type: text/xml");
	echo $xml->asXML();
	
} else {
	header('Content-Type: application/json');
	echo json_encode($information);	
}
?>