<?php

if (isset($_GET['alt']) && ($_GET['alt'] == "xml")) {

    $xml = new SimpleXMLElement("<students></students>");

    $student = $xml->addChild('student');
    $student->addChild('name', 'Pattanawit');
    $books = $student->addChild('books');
    $books->addChild('book', 'Steve Jobs');
    $books->addChild('book', 'Albert Einstein');
    $books->addChild('book', 'Introduction to Languages and the Theoty of Computation');
    $education = $student->addChild('education');
    $education->addChild('highSchool', 'Suratthani school');
    $education->addChild('undergradSchool', 'Khon Kaen University');

    $student = $xml->addChild('student');
    $student->addChild('name', 'Pongsakorn');
    $books = $student->addChild('books');
    $books->addChild('book', 'Soccer');
    $books->addChild('book', 'LIVERPOOL FC');
    $books->addChild('book', 'Reborn');
    $education = $student->addChild('education');
    $education->addChild('highSchool', 'The fifth municipal school');
    $education->addChild('undergradSchool', 'Prince of Songkla University');

    Header('Content-type: text/xml');
    echo $xml->asXML();
} else {

    $data = array('student' =>
        array(
            array('name' => 'Pattanawit',
                'books' => array('Steve Jobs', 'Albert Einstein', 'Introduction to Languages and the Theoty of Computation'),
                'education' => array(
                    array('highschool' => 'Suratthani school')
                    ,
                    array('undergradschool' => 'Khon Kaen University')
                )
            ),
            array('name' => 'Pongsakorn',
                'books' => array('Soccer', 'LIVERPOOL FC', 'Reborn'),
                'education' => array(
                    array('highschool' => 'The fifth municipal school')
                    ,
                    array('undergradschool' => 'Prince of Songkla University')
                )
            )
        )
    );

    $json = json_encode($data);
    echo $json;
}

?>