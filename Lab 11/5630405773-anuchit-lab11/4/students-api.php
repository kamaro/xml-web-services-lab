﻿<?php
$obj = new stdClass();
$student[0] = array(
		'name' => "Anuchit",
		'books' => array(
			"น่าจะรู้อย่างนี้ตั้งแต่อายุ 20 ",
			"ไม่ได้ท้อ แค่รอวันลุก ",
			"ก็ รักกันได้นนะ "
		),
		'education' => array(
			array ('high school' =>'Sisaket Wittayalai School'),
			array ('undergrad school' => 'Khon Kaen University')
		)
	);
$student[1] = array(
		'name' => "ชนพัฒน์",
		'books' => array(
			"ภาพปริศนา",
			"พระพุทธเจ้า",
			"หมากกระดาน"
		),
		'education' => array(
			array('high school' => 'Satit KKU'),
			array('undergrad school' => 'Khon Kaen University')
		)
	);
$array = $student;

$obj->students = $array;

if (isset($_GET['alt']) && !empty($_GET['alt'])) {
	if ($_GET['alt'] == 'xml') {
		header('Content-type: application/xml; charset=utf-8');
		$doc = new DomDocument();
		$doc->formatOutput = true;
		$root = $doc->createElement('students');
		$doc->appendChild($root);
		
		foreach ($student as $student) {
			$studentElement = $doc->createElement('student');
			$root->appendChild($studentElement);
		
			$name = $doc->createElement('name');
			$studentElement->appendChild($name);
			$name->appendChild($doc->createTextNode($student['name']));
			
			$books = $doc->createElement('books');
			$studentElement->appendChild($books);
			
			foreach ($student['books'] as $bookName) {
				$book = $doc->createElement('book');
				$books->appendChild($book);
				$book->appendChild($doc->createTextNode($bookName));
			}
			
			$education = $doc->createElement('education');
			$studentElement->appendChild($education);
			
			$highSchoolName = $doc->createElement('highSchool');
			$universityName = $doc->createElement('undergradSchool');
			$education->appendChild($highSchoolName);
			$education->appendChild($universityName);
			$highSchoolName->appendChild($doc->createTextNode($student['education'][0]['high school']));
			$universityName->appendChild($doc->createTextNode($student['education'][1]['undergrad school']));
			
		}
		header("Access-Control-Allow-Origin: http://localhost:1111/anuchit/lab11/students-api.php");
		echo $doc->saveXML();
		
	}
} else {
	header('Content-type: application/json; charset=utf-8');
	$jsonValue = json_encode($obj, JSON_PRETTY_PRINT);
	header("Access-Control-Allow-Origin: http://localhost:1111/anuchit/lab11/students-api.php");
	echo $jsonValue;
}
?>