<?php

$data = array(
    "students" => array(
        array(
            "name" => "Jirachot",
            "books" => array(
                "BIG THINK",
                "Rich Dad Poor Dad",
                "The Energy bus"
            ),
            "education" => array(
                array(
                    "high school" => "Princess Chulabhon College Mukdahan"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )),
        array(
            "name" => "Kamolpun",
            "books" => array(
                "New York",
                "The Carrot Seed",
                "Marketing Everything"
            ),
            "education" => array(
                array(
                    "high school" => "Nareerat School"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )
        )
    )
);

if ($_GET["alt"] == "xml") {
    header('Content-Type: application/xml');
    $xml = '<?xml version="1.0" encoding="UTF-8" ?>'
            . '<students>'
                . '<student>'
                    .'<name>Jirachot</name>'
                        .'<books>'
                            .'<book>BIG THINK</book>'
                            .'<book>Rich Dad Poor Dad</book>'
                            .'<book>The Energy bus</book>'
                        . '</books>'
                        . '<education>'
                            .'<highSchool>Princess Chulabhon College Mukdahan</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        . '</education>'
                . '</student>'
            . '<student>'
                    .'<name>Kamolpun</name>'
                        .'<books>'
                            .'<book>New York</book>'
                            .'<book>The Carrot Seed</book>'
                            .'<book>Marketing Everything</book>'
                        . '</books>'
                        . '<education>'
                            .'<highSchool>Nareerat School</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        . '</education>'
                . '</student>'
            . '</students>';
    echo $xml;
} else {
    header('Content-Type: application/json');
    echo json_encode($data);
}