<?php

$data = array(
    "students" => array(
        array(
            "name" => "Nicha",
            "books" => array(
                "Harry potter",
                "Vampire Twilight",
                "Persy Jackson"
            ),
            "education" => array(
                array(
                    "high school" => "Marie Vitthaya"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )),
        array(
            "name" => "Phornnicha",
            "books" => array(
                "ตายแล้วไปไหน",
                "โลกและความตาย",
                "วิธีการรักษาศพ"
            ),
            "education" => array(
                array(
                    "high school" => "BK schools"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )
        )
    )
    );

if ($_GET["alt"] == "xml") {
    header('Content-Type: application/xml');
    $xml = '<?xml version="1.0" encoding="UTF-8" ?>'
            . '<students>'
                . '<student>'
                    .'<name>Nicha</name>'
                        .'<books>'
                            .'<book>Harry potter</book>'
                            .'<book>Vampire Twilight</book>'
                            .'<book>Persy Jackson</book>'
                        . '</books>'
                        . '<education>'
                            .'<highSchool>Marie Vitthaya</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        . '</education>'
                . '</student>'
                . '<student>'
                    .'<name>Phornnicha</name>'
                        .'<books>'
                            .'<book>ตายแล้วไปไหน</book>'
                            .'<book>โลกและความตาย</book>'
                            .'<book>วิธีการรักษาศพ</book>'
                        . '</books>'
                        . '<education>'
                            .'<highSchool>BK schools</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        . '</education>'
            . '</student>'
            . '</students>';
    echo $xml;
} else {
    header('Content-Type: application/json');
    echo json_encode($data);
}
