<?php
	$test_array = array (
	  'students' => array (
		array (
			'name' => 'Mine',
			'books' => array (
				"Death Notes",
				"Bakuman",
				"Sword Art Online"
			),
			'education' => array (
				array (
					'high_school' => 'Kaennakhon Wittayalai'
				),
				array (
					'undergrade_school' => 'Khon Kaen University'
				)
			)
		),
		array (
			'name' => 'Nook',
			'books' => array (
				"One Piece",
				"ร้อยปักถักร้อย",
				"ดอกไม้ประดิษฐ์"
			),
			'education' => array (
				array (
					'high_school' => 'Satrichaiyaphum'
				),
				array (
					'undergrade_school' => 'Khon Kaen University'
				)
			)
		)
	  )
	);
	
	if (isset($_GET['alt'])) {
		header('Content-type: application/xml; charset=utf-8');
		$xml = new SimpleXMLElement('<students></students>');
		
		for($i = 0; $i < count($test_array['students']); $i++) {
			$students = $test_array['students'][$i];
			
			$student = $xml -> addChild("student");
			$student -> addChild("name", $students['name']);
			
			$books = $student -> addChild("books");
			
			for($j = 0; $j < count($students['books']); $j++) {
				$books -> addChild("book", $students['books'][$j]);
			}
			
			$books = $student -> addChild("education");
			
			for($j = 0; $j < count($students['education']); $j++) {
				foreach($students['education'][$j] as $k => $v) {
					$books -> addChild($k, $v);
				}
			}
		}
				print $xml -> asXML();
			
	} else {
		header('Content-type: application/json; charset=utf-8');
		echo json_encode($test_array, JSON_PRETTY_PRINT);
	}
	
?>