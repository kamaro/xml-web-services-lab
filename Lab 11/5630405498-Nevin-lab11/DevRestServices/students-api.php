<?php

$data = array(
    "students" => array(
        array(
            "name" => "Nevin",
            "books" => array(
                "The Lord of the Rings - The Return of the King",
                "Harry Potter - the Order of Phoenix",
                "Sherlock Holmes"
            ),
            "education" => array(
                array(
                    "high school" => "Khonkaen Wittayayon School"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )),
        array(
            "name" => "Neymar",
            "books" => array(
                "ฉันเป็นใคร",
                "สุดยอดเคล็ดลับขายตรง",
                "โคตรโหด กระโดดเรียน เซียนไพ่"
            ),
            "education" => array(
                array(
                    "high school" => "Kaennakhon Wittayalai School"
                ),
                array(
                    "undergrad school" => "Stanford University"
                )
            )
        )
    )
);

if ($_GET["alt"] == 'xml') {
    header('Content-Type: application/xml; charset=utf-8');
    echo '<?xml version="1.0" encoding="UTF-8" ?> '
            . '<students>'
                . '<student>'
                    . '<name>Nevin</name>'
                    . '<books>'
                        . '<book>The Lord of the Rings - The Return of the King</book>'
                        . '<book>Harry Potter - the Order of Phoenix</book>'
                        . '<book>Sherlock Holmes</book>'
                    . '\</books>'
                    . '<education>'
                        . '<highschool>Khonkaen Wittayayon School</highschool>'
                        . '<undergradschool>Khon Kaen University</undergradschool>'
                    . '</education>'
                . '</student>'
                . '<student>'
                    . '<name>Neymar</name>'
                    . '<books>'
                        . '<book>ฉันเป็นใคร</book>'
                        . '<book>สุดยอดเคล็ดลับขายตรง</book>'
                        . '<book>โคตรโหด กระโดดเรียน เซียนไพ่</book>'
                    . '</books>'
                    . '<education>'
                        . '<highschool>Kaennakhon Wittayalai School</highschool>'
                        . '<undergradschool>Stanford University"</undergradschool>'
                    . '</education>'
                . '</student>'
            . '</students>';
    
} else {
    header('Content-Type: application/json');
    echo json_encode($data);
}
?>