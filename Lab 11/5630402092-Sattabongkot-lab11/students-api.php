<?php

if (isset($_GET['alt']) && ($_GET['alt'] == "xml")) {

    $xml = new SimpleXMLElement("<students></students>");

    $student = $xml->addChild('student');
    $student->addChild('name', 'Sattabongkot');
    $books = $student->addChild('books');
    $books->addChild('book', 'Harry Potter');
    $books->addChild('book', 'The Maze Runner');
    $books->addChild('book', 'Wonder by R.J. Palacio');
    $education = $student->addChild('education');
    $education->addChild('highSchool', 'Udonpittayanukoon School');
    $education->addChild('undergradSchool', 'Khon Kaen University');

    $student = $xml->addChild('student');
    $student->addChild('name', 'Meridith');
    $books = $student->addChild('books');
    $books->addChild('book', 'How To Be Single');
    $books->addChild('book', 'The Bad Beginning');
    $books->addChild('book', 'The Giver');
    $education = $student->addChild('education');
    $education->addChild('highSchool', 'Donbosco Wittaya School');
    $education->addChild('undergradSchool', 'Chiang Mai University');

    Header('Content-type: text/xml');
    echo $xml->asXML();
} else {

    $data = array('student' =>
        array(
            array('name' => 'Sattabongkot',
                'books' => array('Harry Potter', 'The Maze Runner', 'Wonder by R.J. Palacio'),
                'education' => array(
                    array('highschool' => 'Udonpittayanukoon School')
                    ,
                    array('undergradschool' => 'Khon Kaen University')
                )
            ),
            array('name' => 'Meridith',
                'books' => array('How To Be Single', 'The Bad Beginning', 'The Giver'),
                'education' => array(
                    array('highschool' => 'Donbosco Wittaya School')
                    ,
                    array('undergradschool' => 'Chiang Mai University')
                )
            )
        )
    );

    $json = json_encode($data);
    echo $json;
}

?>