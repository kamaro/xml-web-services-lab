<?php
	$data = array(
		"students" => 
		array(
			array(
				"name" => "วัชร",
				"books" =>
					array(
					"The Pragmatic Programmer", 
					"Land the Tech Job You Love", 
					"The Clean Coder"),
				"education" => array(
					array("highSchool" => "Thawangphapittayakhom School"),
					array("undergradeSchool" => "Khon Kaen University")
				)
			),
			array(
				"name" => "ชนพัฒน์",
				"books" =>
					array(
					"ภาพปริศนา", 
					"พระพุทธเจ้า", 
					"หมากกระดาน"),
				"education" => array(
					array("highSchool" => "Satit KKU"),
					array("undergradeSchool" => "Khon Kaen University")
				)
			)
		)
	);
	//echo json_encode($data);
//================================================================
	//echo "input = &quot".$input."&quot<br/>".$lowInput;
	if ( isset($_GET['alt'])) {
		$input = htmlspecialchars($_GET["alt"]);
		$lowInput = strtolower($input);
		if($lowInput == "xml"){
			$writer = new XMLWriter();
			$writer->openMemory();
			$writer->setIndent(true);
			$writer->setIndentString(" ");
				$writer->startDocument("1.0", "UTF-8");
				$writer->startElement('students');
					$writer->startElement("student");
						$writer->writeElement("name", "วัชร");
						$writer->startElement("books");
							$writer->writeElement("book", "The Pragmatic Programmer");
							$writer->writeElement("book", "Land the Tech Job You Love");
							$writer->writeElement("book", "The Clean Coder");
						$writer->endElement();
						$writer->startElement("education");
							$writer->writeElement("hightSchool", "Thawangphapittakhom School");
							$writer->writeElement("undergradSchool", "Khon Khen University");
						$writer->endElement();
					$writer->endElement();

					$writer->startElement("student");
						$writer->writeElement("name", "ชนพัฒน์");
						$writer->startElement("books");
							$writer->writeElement("book", "ภาพปริศนา");
							$writer->writeElement("book", "พระพุทธเจ้า");
							$writer->writeElement("book", "หมากกระดาน");
						$writer->endElement();
						$writer->startElement("education");
							$writer->writeElement("hightSchool", "Satit KKU");
							$writer->writeElement("undergradSchool", "Khon Khen University");
						$writer->endElement();
					$writer->endElement();
				$writer->endDocument();
			header('Content-type: text/xml');
			echo $writer->outputMemory();
		}
		elseif($lowInput == "json"){
			echo json_encode($data);
		}
		else
			echo "==================== Input ERROR ====================";
	}else{
		//header('Content-Type: application/json');
		echo json_encode($data);
	}
?>