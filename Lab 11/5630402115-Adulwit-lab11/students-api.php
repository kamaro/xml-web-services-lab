<?php

$name = "Adulwit";
$books = array("C++", "PHP", "JAVA");
$school = array("high school" => "Assumption College Ubonratchathni");
$university = array("undergrade school" => "Khon Kaen University");
$education = array($school, $university);
$student1 = array("name" => $name, "books" => $books, "education" => $education);

$name2 = "Chinapas";
$books2 = array("Ant", "Bird", "Cat");
$school2 = array("high school" => "Detudom school");
$university2 = array("undergrade school" => "Ubonratchathani University");
$education2 = array($school2, $university2);
$student2 = array("name" => $name2, "books" => $books2, "education" => $education2);

$arr = array("students" => array($student1, $student2));

$xml = new SimpleXMLElement('<?xml version="1.0"?><students></students>');
for ($i = 0; $i < count($arr['students']); $i++) {
    $student_xml = $xml->addChild("student");
    $student = $arr['students'][$i];

    $student_xml->addChild("name", $student['name']);

    $book = $student_xml->addChild("books");
    for ($j = 0; $j < count($student['books']); $j++) {
        $book->addChild("book", $student['books'][$j]);
    }

    $education = $student_xml->addChild("education");
    $education->addChild("highschool", $student['education'][0]['high school']);
    $education->addChild("undergradeschool", $student['education'][1]['undergrade school']);
}

if (isset($_GET['alt'])) {
    if ($_GET['alt'] == "xml") {
        header("Content-type: text/xml");
        echo $xml->asXML();
    }else
        printjson($arr);
} else {
    printjson($arr);
}
function printjson($arr){
    header('Content-Type: application/json');
    echo(json_encode($arr));
}
?>