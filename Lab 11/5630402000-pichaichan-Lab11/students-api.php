<?php

$datame = array(
	"students" => 
	array(
		array(
			"name" => "พิชัยชาญ",
			"books" =>
				array(
				"GantZ", 
				"Dead Tube", 
				"No game No life"),
			"education" => array(
				array("high_school" => "Anukoolnaree School"),
				array("undergrade_school" => "Khon Kaen University")
			)
		),
		array(
			"name" => "วิมล",
			"books" =>
				array(
				"Wu Dong Qian Kun", 
				"Tales of Demons and Gods", 
				"Douluo Dalu"),
			"education" => array(
				array("high_school" => "Kalasinpittayasai School"),
				array("undergrade_school" => "Rajabhat Mahasarakham University")
			)
		)
	)
);

 	if(isset($_GET['alt'])) {
    	$writer = new XMLWriter();

		$writer->openMemory();
		$writer->setIndent(true);
		$writer->setIndentString("");
		$writer->startDocument("1.0", "UTF-8");

		$writer->startElement('students');

		$writer->startElement("student");

		$writer->writeElement("name", "พิชัยชาญ");

		$writer->startElement("books");
		$writer->writeElement("book", "GantZ");
		$writer->writeElement("book", "Dead Tube");
		$writer->writeElement("book", "No game No life");
		$writer->endElement();

		$writer->startElement("education");
		$writer->writeElement("highschool", "Anukoolnaree School");
		$writer->writeElement("undergrad", "Khon Kaen University");
		$writer->endElement();

		$writer->endElement();

		$writer->startElement("student");

		$writer->writeElement("name", "วิมล");

		$writer->startElement("books");
		$writer->writeElement("book", "Wu Dong Qian Kun");
		$writer->writeElement("book", "Tales of Demons and Gods");
		$writer->writeElement("book", "Douluo Dalu");
		$writer->endElement();

		$writer->startElement("education");
		$writer->writeElement("highschool", "Kalasinpittayasai School");
		$writer->writeElement("undergrad", "Rajabhat Mahasarakham University");
		$writer->endElement();

		$writer->endElement();

		$writer->endDocument();
		header('Content-type: text/xml');

		echo $writer->outputMemory();
	}
	else{
		$dErr = json_encode($datame);
 		echo  json_encode($datame);
 	}
?>

