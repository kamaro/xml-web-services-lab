<?php

$arr = array('student' => array(
                                array(  'name' => 'พงศ์ชยุตม์',
                                        'books' => array('Art of Business', 'Batman: Year One, Part 1 (1987)', 'พุทธวจน'),
                                        'education' => array(
                                            array('highschool' => 'Rajsima Wittayalai School'),
                                            array('undergrad' => 'Khon Kaen University'))),
                                array(  'name' => 'พลังใจ',
                                        'books' => array('The Origin of the Galaxy and Local Group', 'Quantum Enigma', 'Tom Clancy : Rainbow Six'),
                                        'education' => array(
                                            array('highschool' => 'Khon Kaen Wittayalai School'),
                                            array('undergrad' => 'Massachusetts Institute of Technology'))))
);
//echo json_encode($arr);

if ($_GET["alt"] == "xml") {
    header('Content-Type: application/xml');
    $xml = '<?xml version="1.0" encoding="UTF-8" ?>'
            . '<students>'
                . '<student>'
                    .'<name>พงศ์ชยุตม์</name>'
                        .'<books>'
                            .'<book>Art of Business</book>'
                            .'<book>Batman: Year One, Part 1 (1987)</book>'
                            .'<book>พุทธวจน</book>'
                        . '</books>'
                        . '<education>'
                            .'<highschool>Rajsima Wittayalai School</highschool>'
                            .'<undergrad>Khon Kaen University</undergrad>'
                        . '</education>'
                . '</student>'
                . '<student>'
                    .'<name>พลังใจ</name>'
                        .'<books>'
                            .'<book>The Origin of the Galaxy and Local Group</book>'
                            .'<book>Quantum Enigma</book>'
                            .'<book>Tom Clancy : Rainbow Six</book>'
                        . '</books>'
                        . '<education>'
                            .'<highschool>Khon Kaen Wittayalai School</highschool>'
                            .'<undergrad>Massachusetts Institute of Technology</undergrad>'
                        . '</education>'
            . '</student>'
            . '</students>';
    echo $xml;
} elseif ($_GET["alt"] == "json") {
    header('Content-Type: application/json');
    echo json_encode($arr);
    }
?>
