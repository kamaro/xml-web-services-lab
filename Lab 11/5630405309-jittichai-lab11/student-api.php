<?php

    $books = array('NARUTO', 'SLAMDUNK', 'อิคิงามิ สาส์นสั่งตาย');
    $education = array
        (array('high school' => 'Assumption College Nakhonratchasima'),array('undergrad school' => 'Khon Kaen University'));
    $name = array('name' => 'Jittichai','books' => $books, 'education' => $education); // Create Root element Name that has child as book and education
   
    $books2 = array('GAMEMAG', 'STARWAR', 'MARVEL');
    $education2 =  array
        (array('high school' => 'Assumption College Nakhonratchasima'),array('undergrad school' => 'Khon Kaen University'));
    $name2 = array('name' => 'Tanapon','books' => $books2, 'education' =>$education2);
    
    $students = array('student' => array($name,$name2)); // Create element students that contains $name1 and $name2
    
if (isset($_GET['alt'])) {
    if ($_GET['alt'] == "xml" ) {
    header('Content-Type: application/xml');
    $xml = '<?xml version="1.0" encoding="UTF-8" ?>'
            .'<students>'
                .'<student>'
                    .'<name>Jittichai</name>'
                        .'<books>'
                            .'<book>NARUTO</book>'
                            .'<book>SLAMDUNK</book>'
                            .'<book>อิคิงามิ สาส์นสั่งตาย</book>'
                        .'</books>'
                        .'<education>'
                            .'<highSchool>Assumption College Nakhonratchasima</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        .'</education>'
                .'</student>'
                .'<student>'
                    .'<name>Tanapon</name>'
                        .'<books>'
                            .'<book>GAMEMAG</book>'
                            .'<book>STARWAR</book>'
                            .'<book>MARVEL</book>'
                        .'</books>'
                        .'<education>'
                            .'<highSchool>Assumption College Nakhonratchasima</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        .'</education>'
                .'</student>'
            .'</students>';
    print $xml;
}} 
else {// no data passed by get
    header('Content-type: application/json; charset=UTF-8');
    print json_encode($students);
}
?>
