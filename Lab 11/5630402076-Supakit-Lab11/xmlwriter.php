<?php
// create an object of XMLWriter
$writer = new XMLWriter();
//lets store our XML into the memory so we can output it later
$writer->openMemory();
//lets also set the indent so its a very clean and formatted XML
$writer->setIndent(true);
//now we need to define our Indent string,which is basically how many blank spaces we want to have for the indent
$writer->setIndentString(" 	");
//Lets start our document,setting the version of the XML and also the encoding we gonna use
//XMLWriter->startDocument($string version, $string encoding);
$writer->startDocument("1.0", "UTF-8");
//lets start our main element,lets call it “BookList” by using function startElement
// for the element that has sub-elements 
$writer->startElement('BookList');
// Create element “Book” nested in element “BookList”
$writer->startElement("Book");
 // For an element with only content, we can use function writeElement
$writer->writeElement("Title", "jQuery: Novice to Ninj");
$writer->writeElement("Publisher", "Site point");
//Now lets close the first book element
$writer->endElement();
$writer->startElement("Book");
 // For an element with only content, we can use function writeElement
$writer->writeElement("Title", "Learning jQuery");
$writer->writeElement("Publisher", "PACKT");
//Now lets close the first book element
$writer->endElement();
$writer->startElement("Book");
 // For an element with only content, we can use function writeElement
$writer->writeElement("Title", "Head First jQuery");
$writer->writeElement("Publisher", "O'Reilly");
//Now lets close the first book element
$writer->endElement();
$writer->startElement("Book");
 // For an element with only content, we can use function writeElement
$writer->writeElement("Title", "jQuery UT 1.8");
$writer->writeElement("Publisher", "PACKT");
//Now lets close the first book element
$writer->endElement();
//close our document
$writer->endDocument();
/*Lets output what we have so far,first we need to set a header so we can display the XML in the
browser,otherwise you will need to look at the source output. */
header('Content-type: text/xml');
//lets then echo our XML;
echo $writer->outputMemory();
?>