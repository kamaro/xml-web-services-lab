<?php

$data = array(
    "students" => array(
        array(
            "name" => "Supakit",
            "books" => array(
                'Percy Jackson', 'Gone girl', 'Maze runner'
            ),
            "education" => array(
                array(
                    "high school" => "Amnatcharoen School"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )),
        array(
            "name" => "Oliver",
            "books" => array(
                'Vampire diary', 'Vampire twilight', 'Vampire academy'
            ),
            "education" => array(
                array(
                    "high school" => "Saint Luis highschool"
                ),
                array(
                    "undergrad school" => "Oxford University"
                )
            )
        )
    )
);

if ($_GET["alt"] == 'xml'){
header('Content-Type: application/xml; charset=utf-8');
echo '<?xml version="1.0" encoding="UTF-8" ?> <students> <student> <name>Supakit</name> <books> <book>Percy Jackson</book> <book>Gone girl</book> <book>Maze runner</book> </books> <education> <highschool>Amnatcharoen School</highschool> <undergradschool>Khon Kaen University</undergradschool> </education> </student> <student> <name>Oliver</name> <books> <book>Vampire diary</book> <book>Vampire twilight</book> <book>Vampire academy</book> </books> <education> <highschool>Saint Luis highschool</highschool> <undergradschool>Oxford University</undergradschool> </education> </student> </students>';

} else {
    header('Content-Type: application/json');
    echo json_encode($data);
}
