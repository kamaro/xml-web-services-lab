<?php
    $book0 = array('Marvel', 'Rich Dad Poor Dad', 'การ์ตูนความรู้วิทยาศาสตร์');
    $education0 = array(array('highschool' => 'Benchama Maharat School'),array('undergradschool' => 'Khon Kaen University'));
    $student0 = array('name' => 'ยศรวี','books' => $book0, 'education' => $education0);
   
    $book1 = array('การ์ตูนความรู้วิทยาศาสตร์', 'Conan', 'AROWANA');
    $education1 =  array(array('highschool' => 'Benchama Maharat School'),array('undergradschool' => 'Khon Kaen University'));
    $student1 = array('name' => 'วิทวัส','books' => $book1, 'education' => $education1);
    
    $students = array('students' => array($student0,$student1));
    
    if ($_GET["alt"] == "xml") {
    header('Content-Type: application/xml');
    $xml = '<?xml version="1.0" encoding="UTF-8" ?>'
            .'<students>'
                .'<student>'
                    .'<name>ยศรวี</name>'
                        .'<books>'
                            .'<book>Marvel</book>'
                            .'<book>Rich Dad Poor Dad</book>'
                            .'<book>การ์ตูนความรู้วิทยาศาสตร์</book>'
                        .'</books>'
                        .'<education>'
                            .'<highSchool>Benchama Maharat School</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        .'</education>'
                .'</student>'
                .'<student>'
                    .'<name>วิทวัส</name>'
                        .'<books>'
                            .'<book>การ์ตูนความรู้วิทยาศาสตร์</book>'
                            .'<book>Conan</book>'
                            .'<book>AROWANA</book>'
                        .'</books>'
                        .'<education>'
                            .'<highSchool>Benchama Maharat School</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        .'</education>'
                .'</student>'
            .'</students>';
    echo $xml;
} else {
    header('Content-type: application/json; charset=UTF-8');
    echo json_encode($students);
}
?>