<?php

$data = array(
    "students" => array(
        array(
            "name" => "ณัฐพล",
            "books" => array(
                "Don't make me think",
                "On writing well",
                "The craft of research"
            ),
            "education" => array(
                array(
                    "high school" => "Satit MSU"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )),
        array(
            "name" => "ชนพัฒน์",
            "books" => array(
                "ภาพปริศนา",
                "พระพุทธเจ้า",
                "หมากกระดาน"
            ),
            "education" => array(
                array(
                    "high school" => "Satit KKU"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )
        )
    )
);

if ($_GET["alt"] == "xml") {
    header('Content-Type: application/xml');
    $xml = '<?xml version="1.0" encoding="UTF-8" ?>'
            . '<students>'
                . '<student>'
                    .'<name>ณัฐพล</name>'
                        .'<books>'
                            .'<book>Don\'t make me think</book>'
                            .'<book>On writing well</book>'
                            .'<book>The craft of research</book>'
                        . '</books>'
                        . '<education>'
                            .'<highSchool>Satit MSU</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        . '</education>'
                . '</student>'
            . '<student>'
                    .'<name>ชนพัฒน์</name>'
                        .'<books>'
                            .'<book>ภาพปริศนา</book>'
                            .'<book>พระพุทธเจ้า</book>'
                            .'<book>หมากกระดาน</book>'
                        . '</books>'
                        . '<education>'
                            .'<highSchool>Satit KKU</highSchool>'
                            .'<undergradSchool>Khon Kaen University</undergradSchool>'
                        . '</education>'
                . '</student>'
            . '</students>';
    echo $xml;
} else {
    header('Content-Type: application/json');
    echo json_encode($data);
}

