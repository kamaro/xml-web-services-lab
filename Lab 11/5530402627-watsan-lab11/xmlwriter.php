<?php
$writer = new XMLWriter();

$writer->openMemory();

$writer->setIndent(true);

$writer->setIndentString(" 	");

$writer->startDocument("1.0", "UTF-8");

$writer->startElement("BookList");

$books = [
["jQuery: Novice to Ninja", "Site point"],
["Learning jQuery", "PACKT"],
["Head First jQuery", "O\'Reilly"],
["jQuery UI 1.8", "PACKT"],
];

for($i = 0; $i < count($books); $i++){
	$writer->startElement("Book");
	$writer->writeElement("Title", $books[$i][0]);
	$writer->writeElement("Publisher", $books[$i][1]);
	$writer->endElement();
}

$writer->endElement();

$writer->endDocument();

header("Content-type: text/xml");

echo $writer->outputMemory();
?>