<?php
$mydata = array(
	"students" => 
	array(
		array(
			"name" => "Watsan",
			"books" =>
				array(
				"The Pacific", 
				"Band of Brothers", 
				"Medal of Honor Airborne"),
			"education" => array(
				array("high_school" => "Phibun Mangsahan"),
				array("undergrade_school" => "Khon Kaen University")
			)
		),
		array(
			"name" => "Oat",
			"books" =>
				array(
				"Thomas Datt", 
				"One price", 
				"One punch man"),
			"education" => array(
				array("high_school" => "Sakolraj Wittyanukul"),
				array("undergrade_school" => "Khon Kaen University")
			)
		)
	)
);

if(isset($_GET['alt'])) {
	
	if(strtolower($_GET['alt'])!="xml") {
		echo json_encode(array( "status" => 4, "message" => "Invalid request.") );
		exit;
	}
	
	function array_to_xml( $data, &$xml_data , $dept, $debug) {
		$dept++;
		foreach( $data as $key => $value ) {
			if( is_array($value) ) {
				if( is_numeric($key) ){
					if($dept == 2)
						$key = 'student';
				}
				if($dept > 1) {
					if($debug)
						echo $key." -> ".$value."<br>";
					if($dept == 4)
						$subnode = $xml_data;
					else
						$subnode = $xml_data->addChild($key);
				}
				else {
					$subnode = $xml_data;
				}
				if($dept < 5)
				array_to_xml($value, $subnode, $dept, $debug);
			} else {
				if( is_numeric($key) ){
					if($dept == 4)
					$key = 'book';
				}
				
				if($dept > 1){
					if($debug)
					echo $key." -> ".$value."<br>";
					$xml_data->addChild($key, $value);
				}
			}
		 }
	}

	$xml_data = new SimpleXMLElement('<?xml version="1.0"?><students></students>');
	
	array_to_xml($mydata,$xml_data, 0, !true);
	
	header("Content-type: text/xml");
	echo $xml_data->asXML();
	
} else {
	header('Content-Type: application/json');
	echo json_encode($mydata);	
}
?>