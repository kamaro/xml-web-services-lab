<?php
// create an object of XMLWriter
$writer = new XMLWriter();
//lets store our XML into the memory so we can output it later
$writer->openMemory();
//lets also set the indent so its a very clean and formatted XML
$writer->setIndent(true);
//now we need to define our Indent string,which is basically how many blank spaces we want to have for the indent
$writer->setIndentString("	");
//Lets start our document,setting the version of the XML and also the encoding we gonna use
//XMLWriter->startDocument($string version, $string encoding);
$writer->startDocument("1.0", "UTF-8");
//lets start our main element,lets call it "BookList" by using function startElement
// for the element that has sub-elements 
$writer->startElement('BookList');
// Create element "Book" nested in element "BookList"

$title = array("jQuery: Novice to Ninja","Learning jQuery","Head First jQuery","jQuery UI 1.8");
$pusblish = array("Site point","PACKT","O'Reilly","PACKT");

 // For an element with only content, we can use function writeElement
for ($i=0; $i < count($title); $i++) { 
$writer->startElement("Book");
$writer->writeElement("Title", $title[$i]);
$writer->writeElement("Publisher", $pusblish[$i]);
$writer->endElement();
}
//Now lets close the first book element

//close our document
$writer->endDocument();
/*Lets output what we have so far,first we need to set a header so we can display the XML in the
browser,otherwise you will need to look at the source output. */
header('Content-type: text/xml');
//lets then echo our XML;
echo $writer->outputMemory();
?>
