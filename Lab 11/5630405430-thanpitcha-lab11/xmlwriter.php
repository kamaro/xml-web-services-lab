<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// create an object of XMLWriter
header("Content-type: text/xml");
$writer = new XMLWriter();
$writer->openMemory();
$writer->setIndent(true);
$writer->setIndentString("");
$writer->startDocument("1.0", "UTF-8");

$writer->startElement("BookList");

$writer->startElement("Book");
 $writer->writeElement("Title", "jQuery: Novice to Ninja");
$writer->writeElement("Publisher", "Site point");   
$writer->endElement();

$writer->startElement("Book");
 $writer->writeElement("Title", "Learning jQuery");
$writer->writeElement("Publisher", "PACKT");   
$writer->endElement();

$writer->startElement("Book");
 $writer->writeElement("Title", "Head First jQuery");
$writer->writeElement("Publisher", "O'Reilly");   
$writer->endElement();

$writer->startElement("Book");
 $writer->writeElement("Title", "jQuery UI 1.8");
$writer->writeElement("Publisher", "PACKT");   
$writer->endElement();

//close our document
$writer->endDocument();
/*Lets output what we have so far,first we need to set a header so we can display the XML in the
browser,otherwise you will need to look at the source output. */
//lets then echo our XML;
echo $writer->outputMemory();
?>



