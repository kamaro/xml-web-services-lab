<?php

if (isset($_GET['alt']) && ($_GET['alt'] == "xml")) {

    $xml = new SimpleXMLElement("<students></students>");

    $student = $xml->addChild('student');
    $student->addChild('name', 'Chalat');
    $books = $student->addChild('books');
    $books->addChild('book', 'MockingJay');
    $books->addChild('book', 'Mind map English');
    $books->addChild('book', 'think like ZUCK');
    $education = $student->addChild('education');
    $education->addChild('highSchool', 'MuangPhon Pittayakom school');
    $education->addChild('undergradSchool', 'Khon Kaen University');

    $student = $xml->addChild('student');
    $student->addChild('name', 'Tossapon');
    $books = $student->addChild('books');
    $books->addChild('book', 'Seven sins');
    $books->addChild('book', 'One punch man');
    $books->addChild('book', 'One piece');
    $education = $student->addChild('education');
    $education->addChild('highSchool', 'Loei Pittayakom School');
    $education->addChild('undergradSchool', 'Khon kean University');

    Header('Content-type: text/xml');
    echo $xml->asXML();
} else {

    $data = array('student' =>
        array(
            array('name' => 'Chalat',
                'books' => array('MockingJay', 'Mind map English', 'think like ZUCK'),
                'education' => array(
                    array('highschool' => 'MuangPhon Pittayakom school')
                    ,
                    array('undergradschool' => 'Khon Kaen University')
                )
            ),
            array('name' => 'Tossapon',
                'books' => array('Seven sins', 'One punch man', 'One piece'),
                'education' => array(
                    array('highschool' => 'Loei Pittayakom school')
                    ,
                    array('undergradschool' => 'Khon kean University')
                )
            )
        )
    );

    $json = json_encode($data);
    echo $json;
}
?>
