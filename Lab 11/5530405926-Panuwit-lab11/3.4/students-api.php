<?php
$profile = array(
	"students" => 
	array(
		array(
			"name" => "Panuwit",
			"books" =>
				array(
				"One Punch Man", 
				"One piece", 
				"Sherlock Holmes"),
			"education" => array(
				array("high_school" => "Sakolraj Wittyanukul"),
				array("undergrade_school" => "Khon Kaen University")
			)
		),
		array(
			"name" => "Watsan",
			"books" =>
				array(
				"The Pacific", 
				"Band of Brothers", 
				"Medal of Honor Airborne"),
			"education" => array(
				array("high_school" => "Phibun Mangsahan"),
				array("undergrade_school" => "Khon Kaen University")
			)
		)
	)
);

if(!isset($_GET['alt'])){
	header('Content-Type: application/json');
	echo json_encode($profile);
}else{
	
	$xml_data = new SimpleXMLElement('<?xml version="1.0"?><students></students>');
	
	for($i=0; $i < count($profile['students']); $i++) {
//		print_r($profile);exit;
		$student = $profile['students'][$i];
		
		$xml_student = $xml_data->addChild("student");
		$xml_student->addChild("name", $student['name']);
		
		$xml_books = $xml_student->addChild("books");
		
		for($j = 0; $j < count($student['books']); $j++) {
			$xml_books->addChild("book", $student['books'][$j]);
		}
		
		$xml_books = $xml_student->addChild("education");
		
		for($j = 0; $j < count($student['education']); $j++) {
			foreach($student['education'][$j] as $k => $v) {
				$xml_books->addChild($k, $v);
			}
		}
	}
	
	header("Content-type: text/xml");
	echo $xml_data->asXML();
}
?>