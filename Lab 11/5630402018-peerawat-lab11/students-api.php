<?php
	sleep(1);
	$alt = filter_input(INPUT_GET, 'alt');

	$name = array('Peerawat','Lookyee');
	$book = array(
		array("Cal1",
			"Cal2",
			"Cal3",
			),
		array("XML",
			"JAVA",
			"JSON"
			)
	);
	$high = array(
		array("high_school"=>"Ratchasima Wittayalai"),
		array("high_school"=>"Ratchasima Wittayalai")
		);
	$undergrad = array(
		array("undergrad_school"=>"Khon Kaen University"),
		array("undergrad_school"=>"Khon Kaen University")
		);

	function json() {
		global $name;
		global $book;
		global $high;
		global $undergrad;

		$education = array(
			array($high[0],$undergrad[0]),
			array($high[1],$undergrad[1])
			);
		
		$student = array(
			array("name"=>$name[0],"books"=>$book[0],"education"=>$education[0]),
			array("name"=>$name[1],"books"=>$book[1],"education"=>$education[1])
			);

		$data = array("students"=>$student);

		return json_encode($data);
	}
	
	function xml() {
		global $name;
		global $book;
		global $high;
		global $undergrad;

		$XML = new SimpleXMLElement("<students></students>");

		for ($i=0; $i < 2 ; $i++) { 
			$stu1 = $XML->addChild('student');
			$stu1->addChild('name',$name[$i]);
			$books = $stu1->addChild('books');
			for ($j=0; $j < count($book[$i]) ; $j++) { 
				$books->addChild('book',$book[$i][$j]);
			}
			$edu = $stu1->addChild('education');
			$edu->addChild('highschool',$high[$i]['high_school']);
			$edu->addChild('undergradschool',$undergrad[$i]['undergrad_school']);
		}

		return $XML->asXML();
	}

	

	if ($alt != '' && $alt == 'xml') {
		Header('Content-type: text/xml');
		echo XML();
	} else {
		Header('Content-type: application/json');
		echo json();
	}

?>