<?php
header("Content-type:text\xml; charset=UTF-8");

    $book = array('Hunger game', 'Tron', 'Captain Subaza');
    $edu = array(array('highschool' => 'Demonstration school of Khon Kaen university'),array('undergradschool' => 'Khon Kaen University'));
    $student = array('name' => 'รวีโรจน์','book' => $book, 'education' => $edu);
   
    $book2 = array('Sport', 'White Rose', 'Time');
    $edu2 =  array(array('highschool' => 'Satit KKU'),array('undergradschool' => 'Khon Kaen University'));
    $student2 = array('name' => 'พิธิตา','book' => $book2, 'education' =>$edu2);
    $arr = array('student' => array($student,$student2));
    
      
if ($_GET["alt"] == 'xml') {
    header('Content-Type: application/xml; charset=utf-8');
    echo '<?xml version="1.0" encoding="UTF-8" ?> <students><student><name>รวีโรจน์</name><books><book>Hunger game</book> <book>tron</book> <book>Captain Subaza</book> </books> <education> <highschool>Demonstration school of Khon Kaen university</highschool> <undergradschool>Khon Kaen University</undergradschool> </education> </student> <student> <name>พิธิตา</name> <books> <book>sport</book> <book>White Rose</book> <book>Time</book> </books> <education> <highschool>Satit KKU</highschool> <undergradschool>Khon Kaen University</undergradschool> </education> </student> </students>';
} else {
    header('Content-Type: application/json');
    echo json_encode($arr);
}


?>
