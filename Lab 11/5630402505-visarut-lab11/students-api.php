<?php
	function array_to_xml(array $arr, SimpleXMLElement $xml)
		{
    		foreach ($arr as $k => $v) {
      		  is_array($v) ? array_to_xml($v, $xml->addChild($k)) : $xml->addChild($k, $v);
    	}
    	return $xml;
	}
	$Param = $_GET["alt"];
	$arr = array('student' => array(
	 							array('name' => 'วิศรุต',
	 									'books' => array('คู่มือ Start windows 8 quick guide','คู่มือการเขียนโปรแกรมภาษา JAVA','คู่มือเรียน เขียนโปรแกรม Python'),
	 									'education' => array(
	 													array('highschool' =>'Rajavinit bangkhen'), 
	 													array('undergrad' => 'Khon Kaen University'))),
	 							array('name' => 'มณฑล',
	 									'books' => array('เศรษฐศาสตร์จุลภาค','เศรษฐศาสตร์วิศวกรรม','เศรษฐศาสตร์การจัดการ'),
	 									'education' => array(
	 													array('highschool' => 'Rajavinit bangkhen'), 
	 													array('undergrad' => 'Kasetsart University'))))
	 							);
 	if ($Param == 'xml'){
    	$writer = new XMLWriter();

		$writer->openMemory();
		$writer->setIndent(true);
		$writer->setIndentString("");
		$writer->startDocument("1.0", "UTF-8");

		$writer->startElement('students');

		$writer->startElement("student");

		$writer->writeElement("name", "วิศรุต");

		$writer->startElement("books");
		$writer->writeElement("book", "คู่มือ Start windows 8 quick guide");
		$writer->writeElement("book", "คู่มือการเขียนโปรแกรมภาษา JAVA");
		$writer->writeElement("book", "คู่มือเรียน เขียนโปรแกรม Python");
		$writer->endElement();

		$writer->startElement("education");
		$writer->writeElement("highschool", "Rajavinit bangkhen");
		$writer->writeElement("undergrad", "Khon Kaen University");
		$writer->endElement();

		$writer->endElement();

		$writer->startElement("student");

		$writer->writeElement("name", "มณฑล");

		$writer->startElement("books");
		$writer->writeElement("book", "เศรษฐศาสตร์จุลภาค");
		$writer->writeElement("book", "เศรษฐศาสตร์วิศวกรรม");
		$writer->writeElement("book", "เศรษฐศาสตร์การจัดการ");
		$writer->endElement();

		$writer->startElement("education");
		$writer->writeElement("highschool", "Rajavinit bangkhen");
		$writer->writeElement("undergrad", "Kasetsart University");
		$writer->endElement();

		$writer->endElement();

		$writer->endDocument();
		header('Content-type: text/xml');

		echo $writer->outputMemory();
	}
	elseif ($Param =='json'){
		$x = json_encode($arr);
 		echo  json_encode($arr);
 	}
?>