<?php

// XML
if (isset($_GET['alt'])) {

    // request parameter must be xml
    if ($_GET['alt'] != "xml") {
        echo "Invalid request.";
        exit;
    }

    $xml_data = new SimpleXMLElement('<?xml version="1.0"?><students></students>');

    $xml_student = $xml_data->addChild("student");

    $xml_student->addChild("name", "Chanagarn");

    $xml_books = $xml_student->addChild("books");
    $xml_books->addChild("book", "Pretty Little Liar");
    $xml_books->addChild("book", "Dairy of a wimpy kids");
    $xml_books->addChild("book", "The walking dead");

    $xml_education = $xml_student->addChild("education");
    $xml_education->addChild("high_school", "Triamudom Suksa School");
    $xml_education->addChild("undergrade_school", "Khon Kaen University");

    $xml_student = $xml_data->addChild("student");

    $xml_student->addChild("name", "Panupong");

    $xml_books = $xml_student->addChild("books");
    $xml_books->addChild("book", "Tintin: Herge's Masterpiece");
    $xml_books->addChild("book", "The DC Comics Encyclopedia");
    $xml_books->addChild("book", "The Nine Old Men");

    $xml_education = $xml_student->addChild("education");
    $xml_education->addChild("high_school", "Panupong school's name");
    $xml_education->addChild("undergrade_school", "Khon Kaen University");

    header("Content-type: text/xml");
    echo $xml_data->asXML();
}

// JSON
else {

    $mydata = array(
        "students" =>
        array(
            array(
                "name" => "Chanagarn",
                "books" =>
                array(
                    "Pretty Little Liar",
                    "Dairy of a wimpy kids",
                    "The walking dead"),
                "education" => array(
                    array("high_school" => "Triamudom Suksa"),
                    array("undergrade_school" => "Khon Kaen University")
                )
            ),
            array(
                "name" => "Panupong",
                "books" =>
                array(
                    "Tintin: Herge's Masterpiece",
                    "The DC Comics Encyclopedia",
                    "The Nine Old Men"),
                "education" => array(
                    array("high_school" => "Panupong school's name"),
                    array("undergrade_school" => "Khon Kaen University")
                )
            )
        )
    );

    header('Content-Type: application/json');
    echo json_encode($mydata);
}
?>